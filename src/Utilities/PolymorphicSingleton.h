#ifndef Utilities_PolymorphicSingleton_h
#define Utilities_PolymorphicSingleton_h

template <class T>
class PolymorphicSingleton {

 public:

  // get the object instance
  static T* instance( T* t = nullptr );
  static bool verbose;

 protected:

  // the object can be created through a derived object
  // created in its turn by the "instance( ... )" function
  PolymorphicSingleton();
  virtual ~PolymorphicSingleton();

};

#include "PolymorphicSingleton.hpp"

#endif // Utilities_PolymorphicSingleton_h

