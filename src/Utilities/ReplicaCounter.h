#ifndef Utilities_ReplicaCounter_h_h
#define Utilities_ReplicaCounter_h_h

#include "Utilities/IdentifiableObject.h"

#include <string>
#include <cstdlib>
#include <iostream>
template <class T>
class ReplicaCounter: public virtual IdentifiableObject {
 public:
  typedef ReplicaCounter basic_type;
  ReplicaCounter() { label = std::to_string( id = ++counter() ); }
  ~ReplicaCounter() override {}
/*
  template <class O, class M>
  static O* nextInstance( const std::string& k, M* m         ) {
    int n = nReplica( m, k );
    int i;
    while ( ( i = ++counter() ) <= n ) {
      if ( isActive( m, k, i ) ) return new O( m );
    }
    return nullptr;
//    int i = ++counter();
//    if ( isActive( m, k, i ) ) return new O( m );
//    else                 return nullptr;
  }
*/
  template <class O, class M, class ...P>
  static O* nextInstance( const std::string& k, M* m, P... p ) {
    int n = nReplica( m, k );
    std::cout << "nextInstance " << n << std::endl;
    int i;
    while ( ( i = counter() ) < n ) {
      std::cout << "nextInstance " << i << '/' << n << ' ' << isActive( m, k, i ) << std::endl;
      if ( isActive( m, k, i + 1 ) ) {
        std::cout << "nextInstance " << i << '/' << n << " create" << std::endl;
        O* ptr = new O( m, p... );
        m->getUserParameter( k + "_label_" + ptr->label, ptr->label );
        return ptr;
      }
      else {
        ++counter();
      }
    }
    return nullptr;
//    int i = ++counter();
//    if ( isActive( m, k, i ) ) return new O( m, p... );
//    else                       return nullptr;
  }
  template <class O, class M, class ...P>
  static O* create( const std::string& k, M* m, P... p ) {
    return nextInstance<O>( k, m, p... );
  }
  bool isReplicable() override { return true; }
  int replicaId() override { return id; }
//  const std::string& replicaLabel() { return label; }
  int replicaCount() override { return counter(); }
 private:
  int id;
  static int& counter() {
    static int i = 0;
    return i;
  }
  template <class M>
  static
  bool isActive( M* m, const std::string& k, int i ) {
    bool v = true;
    std::string label = std::to_string( i );
    m->getUserParameter( k + "_enable_" + label, v );
    m->getUserParameter( k + "_label_"  + label, label );
    m->getUserParameter( k + "_enable_" + label, v );
    return v;
  }
  template <class M>
  static
  int nReplica( M* m, const std::string& k ) {
    int n = 1;
    m->getUserParameter( k + "_replica", n );
    return n;
  }
};

#endif // Utilities_ReplicaCounter_h

