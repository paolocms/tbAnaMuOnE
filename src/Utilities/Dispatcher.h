#ifndef Utilities_Dispatcher_h
#define Utilities_Dispatcher_h

#include <set>

template <class T> class ActiveObserver;
template <class T> class   LazyObserver;

template <class T>
class Dispatcher {

  friend class ActiveObserver<T>;
  friend class   LazyObserver<T>;

 public:

  Dispatcher()  = delete;
  ~Dispatcher() = delete;

  static void notify( const T& x );

 private:

  static void activeNotify( const T& x );
  static void   lazyNotify( const T& x );

  static void   subscribe( ActiveObserver<T>* obs );
  static void unsubscribe( ActiveObserver<T>* obs );
  static void   subscribe(   LazyObserver<T>* obs );
  static void unsubscribe(   LazyObserver<T>* obs );

  static const T* last;
  static std::set<ActiveObserver<T>*>* activeObserverList();
  static std::set<  LazyObserver<T>*>*   lazyObserverList();

};

#include "Dispatcher.hpp"

#endif // Utilities_Dispatcher_h

