//#include "ActiveObserver.h"
#include "Utilities/Dispatcher.h"
#include <iostream>

template <class T>
ActiveObserver<T>::ActiveObserver() {
  Dispatcher<T>::subscribe( this );
}

template <class T>
ActiveObserver<T>::~ActiveObserver() {
  Dispatcher<T>::unsubscribe( this );
}

