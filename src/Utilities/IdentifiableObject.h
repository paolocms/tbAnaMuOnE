#ifndef Utilities_IdentifiableObject_h
#define Utilities_IdentifiableObject_h

#include <string>
//#include <cstdlib>

class IdentifiableObject {
  template <class T> friend class ReplicaCounter;
 public:
  typedef IdentifiableObject basic_type;
  enum ParameterReplicaGranularity { common, byReplica };
//  template <class O, class M>
//  static O* nextInstance( const std::string& k, M* m         ) {
//    return new O( m );
//  }
  template <class O, class M, class ...P>
  static O* nextInstance( const std::string& k, M* m, P... p ) {
    static bool firstInstance = true;
    if ( firstInstance ) {
      firstInstance = false;
      O* ptr = new O( m, p... );
      m->getUserParameter( k + "_label_" + ptr->label, ptr->label );
      return ptr;
//      return new O( m, p... );
    }
    return nullptr;
  }
  template <class O, class M, class ...P>
  static O* create( const std::string& k, M* m, P... p ) {
    return nextInstance<O>( k, m, p... );
  }
  IdentifiableObject()          = default;
  virtual ~IdentifiableObject() = default;
  virtual bool isReplicable() { return false; }
  virtual int replicaId() { return 1; }
  const std::string& replicaLabel() { return label; }
  virtual int replicaCount() { return 1; }
  template <class M, class V>
  void getUserParameter( const M* m, const std::string& k, V& v ) {
    m->getUserParameter( k, v );
    m->getUserParameter( k + "_" + std::to_string( replicaId() ), v );
    m->getUserParameter( k + "_" + label                        , v );
  }
 private:
  std::string label = "1";
};

#endif // Utilities_IdentifiableObject_h

