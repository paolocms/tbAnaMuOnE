#ifndef Utilities_TypeManipulator_h
#define Utilities_TypeManipulator_h

#include <type_traits>

// class to get types from other related types.
//
/////////////////////////////////
//
// get the pointed type from a pointer-like type, e.g.:
//
// TypeManipulator::from<std::vector<float>::iterator>::pointed_type
//
// corresponds to "float"
//
/////////////////////////////////
//
// get a mutable copy from a const object, e.g.:
//
// TypeManipulator<int>::get_mutable<const int>( 0 )
//
// returns a (non-const) int initialized at 0
//
/////////////////////////////////
//
// get the product type from two types
//
// TypeManipulator<int,float>::prod::type
//
// corresponds to "float"
//

class TypeManipulator {

 public:

  // deleted constructor and assignment, only static functions
  TypeManipulator()                                      = delete;
  TypeManipulator           ( const TypeManipulator& x ) = delete;
  TypeManipulator& operator=( const TypeManipulator& x ) = delete;

  // destructor
  ~TypeManipulator() = default;

  template <class T>
  class from {
   private:
    T t;
   public:
    typedef typename std::remove_reference<decltype(*t)>::type pointed_type;
  };

  template <class T>
  static
  T get_mutable( const T t ) { return t; }

  template <class L, class R>
  class prod {
   private:
    L l;
    R r;
   public:
    typedef decltype( l*r ) type;
  };

};

#endif // Utilities_TypeManipulator_h

