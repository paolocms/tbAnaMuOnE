#ifndef Utilities_ActiveObserver_h
#define Utilities_ActiveObserver_h

template <class T>
class ActiveObserver {

 public:

  // constructor
  ActiveObserver();
  // deleted copy constructor and assignment to prevent unadvertent copy
  ActiveObserver           ( const ActiveObserver& x ) = delete;
  ActiveObserver& operator=( const ActiveObserver& x ) = delete;

  // destructor
  virtual ~ActiveObserver();

  virtual void update( const T& x ) = 0;

};

#include "ActiveObserver.hpp"

#endif // Utilities_ActiveObserver_h

