#ifndef Utilities_Singleton_h
#define Utilities_Singleton_h

template <class T>
class Singleton {

 public:

  // get the object instance
  static T* instance();
  static bool verbose;

 protected:

  // the object can be created only through a derived object
  // created in its turn by the "instance()" function
  Singleton();
  virtual ~Singleton();

};

#include "Singleton.hpp"

#endif // Utilities_Singleton_h

