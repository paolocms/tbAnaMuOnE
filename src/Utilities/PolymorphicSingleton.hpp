// included by "PolymorphicSingleton.h"
#include <iostream>

template <class T>
bool PolymorphicSingleton<T>::verbose = false;

template <class T>
PolymorphicSingleton<T>::PolymorphicSingleton() {
}

template <class T>
PolymorphicSingleton<T>::~PolymorphicSingleton() {
  if ( verbose ) std::cout << "delete PolymorphicSingleton "
                           << this << std::endl; 
}

template <class T>
T* PolymorphicSingleton<T>::instance( T* t ) {
  if ( verbose ) std::cout << "PolymorphicSingleton::instance " << std::endl; 
  // the object is created only once, the first time "instance()" is called:
  // if a pointer is given, then the address of that object is taken,
  // otherwise an object of type T is created automatically
  static T* ptr = nullptr;
  if ( ptr == nullptr ) ptr = ( t == nullptr ? new T : t );
  return ptr;
}

