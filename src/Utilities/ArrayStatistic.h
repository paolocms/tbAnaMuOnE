#ifndef Utilities_ArrayStatistic_h
#define Utilities_ArrayStatistic_h

#include "Utilities/TypeManipulator.h"

#include <utility>
#include <tuple>
#include <functional>
#include <vector>
#include <set>
#include <map>
#include <iostream>

class ArrayStatistic {

 public:

  // deleted constructor and assignment, only static functions
  ArrayStatistic()                                     = delete;
  ArrayStatistic           ( const ArrayStatistic& x ) = delete;
  ArrayStatistic& operator=( const ArrayStatistic& x ) = delete;

  // destructor
  ~ArrayStatistic() = default;

  // shorthand for TypeManipulator::from<T>::pointed_type
  template <class T>
  class from {
   public:
    typedef typename TypeManipulator::from<T>::pointed_type pointed_type;
  };

  // function to control pedestal calculation, returning
  // - peak position
  // - peak width
  // - side to use: -1=left , 0=both , +1=right
  typedef std::function<std::tuple<int,int,int>( int )> funcSide;

  // get the min in a sequence
  //
  // arguments:
  // - T = pointer or iterator : begin, end
  // - S = pointed type        : min. starting value
  // result:
  // - int : min. position
  // - S   : min. value
  template <class T, class S = typename from<T>::pointed_type>
  static
  std::pair<int,S> getMin( T f, T l, S m ) {
    auto t = m;
    int i = -1;
    int j = 0;
    while ( f < l ) {
      if ( *f < t ) {
        t = *f;
        i = j;
      }
      ++j;
      ++f;
    }
    return std::make_pair( i, t );
  }

  // get the max in a sequence
  //
  // arguments:
  // - T = pointer or iterator : begin, end
  // - S = pointed type        : max. starting value
  // result:
  // - int : max. position
  // - S   : max. value
  template <class T, class S = typename from<T>::pointed_type>
  static
  std::pair<int,S> getMax( T f, T l, S m = 0 ) {
    auto t = m;
    int i = -1;
    int j = 0;
    while ( f < l ) {
      if ( *f > t ) {
        t = *f;
        i = j;
      }
      ++j;
      ++f;
    }
    return std::make_pair( i, t );
  }

  // get the max in a sequences of sequences
  //
  // arguments:
  // - vector<int>              : channel indices
  // - set   <int>              : channels to skip
  // - function<pair<T,T>(int)> : function returning
  //                              begin, end pointers or iterators for a channel
  // - S = pointed type         : max. starting value
  // result:
  // - channel index
  // - max. found
  //   - max. position
  //   - max. value
  template <class T, class S = typename from<T>::pointed_type>
  static
  std::map<int,std::pair<int,S>> getMax( const std::vector<int>& channels,
                                         const std::set   <int>& chanSkip,
                                 const std::function<std::pair<T,T>( int )>& fSign,
                                 S m = 0 ) {
    std::map<int,std::pair<int,S>> v;
    auto endSkip = chanSkip.end();
    for ( int i: channels ) {
      if ( chanSkip.find( i ) != endSkip ) continue;
      auto sr = fSign( i );
      v[i] = getMax( sr.first, sr.second, m );
//      v[i] = ArrayStatistic::getMax( sr.first, sr.second, m );
    }
    return v;
  }

  // get the max in a sequence of sequences and fill corresponding vectors
  // or arrays
  //
  // arguments:
  // - vector<int>              : channel indices
  // - set   <int>              : channels to skip
  // - function<pair<T,T>(int)> : function returning
  //                              begin, end pointers or iterators for a channel
  // - RP                       : begin pointer or iterator for max. positions
  // - RV                       : begin pointer or iterator for max. values
  // - S = pointed type         : max. starting value
  template <class T, class RP, class RV,
                     class S = typename from<T>::pointed_type>
  static
  void                           getMax( const std::vector<int>& channels,
                                         const std::set   <int>& chanSkip,
                                 const std::function<std::pair<T,T>( int )>& fSign,
                                 RP pos, RV val, S m = 0 ) {
    auto l = getMax( channels, chanSkip, fSign, m );
    auto endSkip = chanSkip.end();
    for ( auto& e: l ) {
      int  i = e.first;
      if ( chanSkip.find( i ) != endSkip ) continue;
      auto s = e.second;
      *( pos + i ) = s.first;
      *( val + i ) = s.second;
    }
    return;
  }

  // get the sum for a sequence
  //
  // arguments:
  // - T = pointer or iterator : begin, end
  // - S = pointed type        : offset to subtract from each value
  // result:
  // - S   : sum
  template <class T, class S = typename from<T>::pointed_type>
  static
  S getSum( T f, T l, S off = 0 ) {
    auto s = TypeManipulator::get_mutable<S>( 0 );
    while ( f < l ) {
      S x = *f - off;
      s += x;
      ++f;
    }
    return s;
  }

  // get the sum of squares for a sequence
  //
  // arguments:
  // - T = pointer or iterator : begin, end
  // - S = pointed type        : offset to subtract from each value
  // result:
  // - S   : sum
  template <class T, class S = typename from<T>::pointed_type>
  static
  S getSumSqr( T f, T l, S off = 0 ) {
    auto s = TypeManipulator::get_mutable<S>( 0 );
    while ( f < l ) {
      S x = *f - off;
      s += x * x;
      ++f;
    }
    return s;
  }

  // get the sum of products for two sequences:
  //
  // arguments:
  // - TL = pointer or iterator : begin, end of first sequence
  // - TR = pointer or iterator : begin of second sequence
  // - SL = pointed type        : offset to subtract from each value
  //                              in first sequence
  // - SL = pointed type        : offset to subtract from each value
  //                              in second sequence
  // result:
  // - SP   : sum
  template <class TL, class TR,
            class SR = typename from<TR>::pointed_type,
            class SL = typename from<TL>::pointed_type,
            class SP = typename TypeManipulator::prod<SR,SL>::type>
  static
  SP getProd( TL f, TL l, TR g, SL off1 = 0, SR off2 = 0 ) {
    auto s = TypeManipulator::get_mutable<SP>( 0 );
    while ( f < l ) {
      SL x = *f - off1;
      SR y = *g - off2;
      s += x * y;
      ++f;
      ++g;
    }
    return s;
  }
/*
  template <class T, class S = typename from<T>::pointed_type>
  static
  S getProd( T f, T l, T g, S off1 = 0, S off2 = 0 ) {
    auto s = TypeManipulator::get_mutable<S>( 0 );
    while ( f < l ) {
      S x = *f - off1;
      S y = *g - off2;
      s += x * y;
      ++f;
      ++g;
    }
    return s;
  }
*/

  // add to the values in a first sequence the values in a second one
  // arguments:
  // - TD = pointer or iterator : begin, end of first (destination) sequence
  // - TS = pointer or iterator : begin of second (source) sequence
  // - S  = pointed type        : offset to subtract from each value in
  //                              second (source) sequence
  template <class TD, class TS, class S = typename from<TS>::pointed_type>
  static
  void accumulate( TD f, TD l, TS g, S off = 0 ) {
    while ( f < l ) {
      *f += ( *g - off );
      ++f;
      ++g;
    }
    return;
  }

  // get the number of entries, sum and sum of squares for a sequence
  //
  // arguments:
  // - T = pointer or iterator : begin, end
  // - S = pointed type        : offset to subtract from each value
  // result:
  // - number of entries with same type as elements
  // - sum
  // - sum of squares
  template <class T, class S = typename from<T>::pointed_type>
  static
  std::tuple<S,S,S> getSums( T f, T l, S off = 0 ) {
    auto s0 = TypeManipulator::get_mutable<S>( 0 );
    auto s1 = TypeManipulator::get_mutable<S>( 0 );
    auto s2 = TypeManipulator::get_mutable<S>( 0 );
    while ( f < l ) {
      S x = *f - off;
      s0 += 1;
      s1 += x;
      s2 += x * x;
      ++f;
    }
    return std::make_tuple( s0, s1, s2 );
  }

  // get the number of entries, sum, sum of squares and products
  // for two sequences (X,Y)
  //
  // arguments:
  // - TX = pointer or iterator : begin, end of first (X) sequence
  // - TY = pointer or iterator : begin of second X) sequence
  // - SX = pointed type        : offset to subtract from each value in
  //                              first (X) sequence
  // - SY = pointed type        : offset to subtract from each value in
  //                              second (Y) sequence
  // result:
  // - SX: number of entries with same type as elements
  // - SX: sum of X
  // - SX: sum of X squares
  // - SY: sum of Y
  // - SY: sum of Y squares
  // - SP: sum of products
  template <class TX, class TY,
            class SX = typename from<TX>::pointed_type,
            class SY = typename from<TY>::pointed_type,
            class SP = typename TypeManipulator::prod<SX,SY>::type>
  static
  std::tuple<SX,SX,SX,SY,SY,SP> getSumLin( TX f, TX l, TY g,
                                           SX off1 = 0, SY off2 = 0 ) {
    auto s0 = TypeManipulator::get_mutable<SX>( 0 );
    auto s1 = TypeManipulator::get_mutable<SX>( 0 );
    auto s2 = TypeManipulator::get_mutable<SX>( 0 );
    auto s3 = TypeManipulator::get_mutable<SY>( 0 );
    auto s4 = TypeManipulator::get_mutable<SY>( 0 );
    SP s5 = 0;
//    auto s5 = TypeManipulator::get_mutable<SP>( 0 );
    while ( f < l ) {
      SX x = *f - off1;
      SY y = *g - off2;
      s0 += 1;
      s1 += x;
      s2 += x * x;
      s3 += y;
      s4 += y * y;
      s5 += x * y;
      ++f;
      ++g;
    }
    return std::make_tuple( s0, s1, s2, s3, s4, s5 );
  }
/*
  template <class T, class S = typename from<T>::pointed_type>
  static
  std::tuple<S,S,S,S,S,S> getSums( T f, T l, T g, S off1 = 0, S off2 = 0 ) {
    auto s0 = TypeManipulator::get_mutable<S>( 0 );
    auto s1 = TypeManipulator::get_mutable<S>( 0 );
    auto s2 = TypeManipulator::get_mutable<S>( 0 );
    auto s3 = TypeManipulator::get_mutable<S>( 0 );
    auto s4 = TypeManipulator::get_mutable<S>( 0 );
    auto s5 = TypeManipulator::get_mutable<S>( 0 );
    while ( f < l ) {
      S x = *f - off1;
      S y = *g - off2;
      s0 += 1;
      s1 += x;
      s2 += x * x;
      s3 += y;
      s4 += y * y;
      s5 += x * y;
      ++f;
      ++g;
    }
    return std::make_tuple( s0, s1, s2, s3, s4, s5 );
  }
*/

  // get statistics in a sequence of sequences and fill corresponding vectors
  // or arrays
  //
  // arguments:
  // - vector<int>                       : channel indices
  // - set   <int>                       : channels to skip
  // - function<pair<T,T>(int)>          : function returning
  //                                       begin, end pointers or iterators
  //                                       for a channel
  // - funcSide =
  //   function<tuple<int,int,int>(int)> : function returning the useful range
  //                                       for statistic:
  //                                       - center of region to ignore
  //                                       - width  of region to ignore
  //                                       - side to use:
  //                                         -1=left , 0=both , +1=right
  // - RE : begin pointer or iterator for numbers of entries
  // - RS : begin pointer or iterator for sums
  // - RQ : begin pointer or iterator for sums of squares
  // - RS : begin pointer or iterator for mean values
  // - RQ : begin pointer or iterator for mean quadratic deviations
  template <class T, class RE, class RS, class RQ, class RM, class RV>
  static
  void getStat( const std::vector<int>& channels,
                const std::set   <int>& chanSkip,
                const std::function<std::pair<T,T>( int )>& fSign,
                const funcSide& fPeak,
                RE eSum, RS sSum, RQ qSum, RM sMea, RV sRMS ) {
    auto endSkip = chanSkip.end();
    for ( int i: channels ) {
      if ( chanSkip.find( i ) != endSkip ) continue;
      auto sr = fSign( i );
      auto sb = TypeManipulator::get_mutable<T>( sr.first  );
      auto se = TypeManipulator::get_mutable<T>( sr.second );
      auto pk = fPeak( i );
      int cpos  = std::get<0>( pk );
      int width = std::get<1>( pk );
      int side  = std::get<2>( pk );
      typename from<RS>::pointed_type offset;
      RE esi = eSum + i;
      RS ssi = sSum + i;
      RQ qsi = qSum + i;
      if ( side <= 0 ) {
        offset = *sb;
        T sm = sb + 1 + cpos - width;
        auto st = getSums( sb, sm, offset );
        *esi += std::get<0>( st );
        *ssi += std::get<1>( st );
        *qsi += std::get<2>( st );
      }
      if ( side >= 0 ) {
        sb += cpos + width;
        if ( side )
        offset = *sb;
        auto st = getSums( sb, se, offset );
        *esi += std::get<0>( st );
        *ssi += std::get<1>( st );
        *qsi += std::get<2>( st );
      }
      typename from<RM>::pointed_type mea =   *ssi / *esi;
      typename from<RV>::pointed_type var = ( *qsi / *esi ) - ( mea * mea );
      *( sMea + i ) = mea + offset;
      *( sRMS + i ) = ( var > 0 ? sqrt( var ) : 0 );
    }
    return;
  }

  // get statistics in a sequence of sequences and fill corresponding vectors
  // or arrays
  //
  // arguments:
  // - vector<int>                       : channel indices
  // - set   <int>                       : channels to skip
  // - function<pair<T,T>(int)>          : function returning
  //                                       begin, end pointers or iterators
  //                                       for a channel
  // - int : index of first channel
  // - int : index of  last channel
  // - RE  : begin pointer or iterator for numbers of entries
  // - RS  : begin pointer or iterator for sums
  // - RQ  : begin pointer or iterator for sums of squares
  // - RS  : begin pointer or iterator for mean values
  // - RQ  : begin pointer or iterator for mean quadratic deviations
  template <class T, class RE, class RS, class RQ, class RM, class RV>
  static
  void getStat( const std::vector<int>& channels,
                const std::set   <int>& chanSkip,
                const std::function<std::pair<T,T>( int )>& fSign,
                unsigned int fPos, unsigned int lPos,
                RE eSum, RS sSum, RQ qSum, RM sMea, RV sRMS ) {
    auto endSkip = chanSkip.end();
    for ( int i: channels ) {
      if ( chanSkip.find( i ) != endSkip ) continue;
      auto sr = fSign( i );
      auto sb = TypeManipulator::get_mutable<T>( sr.first );
      auto se = sb + lPos;
      sb += fPos;
      typename from<RS>::pointed_type offset;
      RE esi = eSum + i;
      RS ssi = sSum + i;
      RQ qsi = qSum + i;
      offset = *sb;
      auto st = getSums( sb, se, offset );
//      auto st = ArrayStatistic::getSums( sb, se, offset );
      *esi += std::get<0>( st );
      *ssi += std::get<1>( st );
      *qsi += std::get<2>( st );
      typename from<RM>::pointed_type mea =   *ssi / *esi;
      typename from<RV>::pointed_type var = ( *qsi / *esi ) - ( mea * mea );
      *( sMea + i ) = mea + offset;
      *( sRMS + i ) = ( var > 0 ? sqrt( var ) : 0 );
    }
    return;
  }

  template <class T, class RP, class RV, class RS = RV>
  static
  std::tuple<int,int,RV,RS> getChannelMax( const std::vector<int>& channels,
                                           const std::set   <int>& chanSkip,
                            const std::function<std::pair<int,T>( int )>& fMax,
                            const std::function<std::pair<RP,RV>( int )>& fPed ) {
    int cha = -1;
    int pos = -1;
    RV  val =  0;
    RS  sum =  0;
    auto endSkip = chanSkip.end();
    for ( int i: channels ) {
      if ( chanSkip.find( i ) != endSkip ) continue;
      auto mv = fMax( i );
      RV cv = mv.second - fPed( i ).first;
      if ( cv < 0 ) continue;
      sum += cv;
      if ( cv > val ) {
        cha = i;
        pos = mv.first;
        val = cv;
      }
    }
    return std::make_tuple( cha, pos, val, sum );
  }

};

#endif // Utilities_ArrayStatistic_h

