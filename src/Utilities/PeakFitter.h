#ifndef Utilities_PeakFitter_h
#define Utilities_PeakFitter_h

#include "Utilities/TypeManipulator.h"
#include "Utilities/QuadraticFitter.h"

#include <tuple>
#include <iterator>
#include <iostream>

class PeakFitter {

  // class to get a mean,rms equivalent from a parabolic fit of a
  // list of bin centers and contents

 public:

  // deleted constructor and assignment, only static functions
  PeakFitter()                                 = delete;
  PeakFitter           ( const PeakFitter& x ) = delete;
  PeakFitter& operator=( const PeakFitter& x ) = delete;

  // destructor
  ~PeakFitter() = default;

  // add a point
  template <class TX, class TY,
            class SX = typename TypeManipulator::from<TX>::pointed_type,
            class SY = typename TypeManipulator::from<TY>::pointed_type>
  static std::tuple<SY,SX,SX> fit( TX fBinCenter, TX lBinCenter,
                                   TY fBinContent,
                                   double minFraction = 0.5 ) {
    QuadraticFitter q;
    TX ix = fBinCenter;
    TY iy = fBinContent;
    TX ip;
    SY cp = 0;
    while ( ix < lBinCenter ) {
      SY cc = *iy;
//      std::cout << "fit(v) " << std::distance( fBinCenter, ix ) << ' ' << *ix << ' ' << cc << ' ' << cp << ' ' << *iy << std::endl;
      if ( cc > cp ) {
        ip = ix;
        cp = cc;
//        std::cout << "fit(v) ... " << cp << std::endl;
      }
      ++ix;
      ++iy;
    }
    int jp = std::distance( fBinCenter, ip );
    int np = std::distance( fBinCenter, lBinCenter );
    SX xc = *ip;
    SY ym = cp * minFraction;
    SY yc = ( cp + ym ) / 2.0;
//    std::cout << "fit(v) -> " << jp << ' ' << xc << ' ' << cp << " === " << ym << ' ' << yc << std::endl;
    q.add( 0, cp - yc );
//    std::cout << "add: " << *( ip + 1 ) << ' ' << *( fBinContent + jp + 1 ) << std::endl;
    q.add( *( ip + 1 ) - xc, *( fBinContent + jp + 1 ) - yc );
//    std::cout << "add: " << *( ip - 1 ) << ' ' << *( fBinContent + jp - 1 ) << std::endl;
    q.add( *( ip - 1 ) - xc, *( fBinContent + jp - 1 ) - yc );
    int id;
    for ( id = jp - 2; ( ( cp = *( fBinContent + id ) ) > ym ) &&
                         ( id >= 0 ); id-- )
//      {std::cout << "add: " << *( fBinCenter + id ) << ' ' << cp << std::endl;
          q.add( *( fBinCenter + id ) - xc, cp - yc );
//      }
    for ( id = jp + 2; ( cp = *( fBinContent + id ) ) > ym  &&
                         ( id < np ); id++ )
//      {std::cout << "add: " << *( fBinCenter + id ) << ' ' << cp << std::endl;
          q.add( *( fBinCenter + id ) - xc, cp - yc );
//      }
    double qa = q.a() + yc;
    double qb = q.b();
    double qc = q.c();
    SX xv = ( qb / ( 2 * qc ) );
    xc -= xv;
    SY yw = qa - ( qb * xv / 2 );
//    double cx = xc * qc;
//    qb -= ( 2 * cx );
//    qa += ( qc * xc * cx );
//    xc -= ( qb / ( 2 * qc ) );
    SX xr = sqrt( -yw / qc ) * 0.62727;
//    SX xr = sqrt( ( pow( qb / qc, 2 ) / 4 ) - ( qa / qc ) ) * 0.62727;
//    std::cout << "fit(v) ===> " << q.n() << " === " << q.a() << ' ' << qa << ' ' << qb << ' ' << qc << " === " << xc << ' ' << xr << std::endl;
    
    return std::make_tuple( yw, xc, xr );
  }

  template <class TH1>
  static std::tuple<double,double,double> fit( const TH1& h,
                                               double minFraction = 0.5 ) {
//    std::cout << "fit(h) " << h.GetName() << std::endl;
    int i;
    int n = h.GetNbinsX();
    double* x = new double[n];
    double* y = new double[n];
    for ( i = 0; i < n; ++i ) {
      x[i] = h.GetBinCenter ( i + 1 );
      y[i] = h.GetBinContent( i + 1 );
    }
    std::tuple<double,double,double> p = fit( x, x + n, y, minFraction );
    delete[] x;
    delete[] y;
    return p;
  }

};

#endif // Utilities_PeakFitter_h

