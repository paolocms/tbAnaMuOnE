#ifndef Utilities_TimeStamp_h
#define Utilities_TimeStamp_h

#include <chrono>
#include <thread>
#include <string>

class TimeStamp {

  // class to get a time stamp

 public:

  enum Unit { seconds, milliseconds, microseconds };

  static constexpr long long  secFactor = 1000000;
  static constexpr long long  minFactor =  secFactor * 100;
  static constexpr long long hourFactor =  minFactor * 100;
  static constexpr long long  dayFactor = hourFactor * 100;
  static constexpr long long  monFactor =  dayFactor * 100;
  static constexpr long long yearFactor =  monFactor * 100;

  static long long sinceEpoch( Unit u = microseconds ) {
//    time_type t = std::chrono::duration_cast<time_unit>
////                ( std::chrono::high_resolution_clock::now()
//                ( clock::now()
//                 .time_since_epoch() ).count();
//    switch ( u ) {
//    default:
//    case microseconds: return static_cast<long long>( t );
//    case milliseconds: return static_cast<long long>( t ) / 1000;
//    case      seconds: return static_cast<long long>( t ) / 1000000;
//    }
    time_type t = time_cast( clock::now().time_since_epoch() ).count();
    return convert( t, microseconds, u );
  }

  static long long packedDateHMS() {
//    auto n = std::chrono::high_resolution_clock::now();
//    auto t = std::chrono::high_resolution_clock::to_time_t( n );
    auto n = clock::now();
    auto t = clock::to_time_t( n );
    auto p = std::localtime( &t );
//    return (   std::chrono::duration_cast<time_unit>( n.time_since_epoch() )
    return (   time_cast( n.time_since_epoch() )
               .count()           %  secFactor )
         + (   p->tm_sec          *  secFactor )
         + (   p->tm_min          *  minFactor )
         + (   p->tm_hour         * hourFactor )
         + (   p->tm_mday         *  dayFactor )
         + ( ( p->tm_mon  + 1   ) *  monFactor )
         + ( ( p->tm_year % 100 ) * yearFactor );
  }

  static void sleepFor( long long t, Unit u = microseconds ) {
//    const std::chrono::duration<long long,std::micro>
//          e( convert( t, u,  microseconds ) );
    const time_unit e( convert( t, u,  microseconds ) );
    std::this_thread::sleep_for( e );
  }

  static void sleepUntil( long long t, Unit u = microseconds ) {
//    const std::chrono::duration<long long,std::micro>
//          e( convert( t, u,  microseconds ) );
//    auto u = std::chrono::high_resolution_clock
    time_unit v( convert( t, u ) );
    std::chrono::time_point<clock,time_unit> e( v );
//    time_unit v;
//    std::chrono::time_point<clock,time_unit> e( time_unit( t ) );
    std::this_thread::sleep_until( e );
  }

  static std::string dateStamp( long long time = 0 ) {
    if ( !time ) time = packedDateHMS();
    return std::to_string( time / TimeStamp::dayFactor );
  }

  static std::string timeStamp( long long time = 0 ) {
    if ( !time ) time = packedDateHMS();
    std::string hour = std::to_string( ( time % TimeStamp::dayFactor ) /
                                                TimeStamp::secFactor );
    while ( hour.length() < 6 ) hour = "0" + hour;
    return hour;
  }

  static std::string fullStamp( long long time = 0 ) {
    if ( !time ) time = packedDateHMS();
    return dateStamp( time ) + "_" + timeStamp( time );
  }

 private:

  typedef std::chrono::high_resolution_clock    clock;
  typedef std::chrono::microseconds             time_unit;
  typedef time_unit::rep                        time_type;
  template<class T> static time_unit time_cast( const T& t ) {
    return std::chrono::duration_cast<time_unit>( t );
  }

  static long long convert( time_type t,
                            Unit from = microseconds,
                            Unit to   = microseconds ) {
    switch ( from ) {
    default:
    case microseconds:               break;
    case milliseconds: t *= 1000   ; break;
    case      seconds: t *= 1000000; break;
    }
    switch ( to ) {
    default:
    case microseconds: return static_cast<long long>( t );
    case milliseconds: return static_cast<long long>( t ) / 1000;
    case      seconds: return static_cast<long long>( t ) / 1000000;
    }
  }

};

#endif // Utilities_TimeStamp_h

