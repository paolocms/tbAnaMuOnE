#ifndef Utilities_GetProducts_h
#define Utilities_GetProducts_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <cmath>
#include <utility>
#include <functional>

class Event;

class GetProducts: public LazyObserver<Event> {

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetProducts           ( const GetProducts& x ) = delete;
  GetProducts& operator=( const GetProducts& x ) = delete;

  virtual ~GetProducts() override = default;

  double getProducts( int chanL, int chanR );

  typedef std::function<double( int )> function;
  function& getFunction() {
    static
      function f = [this]( int i, int j ) {
      return getProducts( i, j );
    };
    return f;
  };

 protected:

  // private constructor being a singleton
  GetProducts() = default;

};

#endif // Utilities_GetProducts_h

