#ifndef Utilities_ObjectFactory_h
#define Utilities_ObjectFactory_h

#include <vector>
#include <string>
#include <map>
#include <iostream>
//class UserParametersManager;

template <class T>
class ObjectFactory {

 public:

  ObjectFactory() {}
  // deleted copy constructor and assignment to prevent unadvertent copy
  ObjectFactory           ( const ObjectFactory& x ) = delete;
  ObjectFactory& operator=( const ObjectFactory& x ) = delete;

  virtual ~ObjectFactory() {}

  // analysis object abstract factory
  class AbsFactory {
   public:
    // Analyzers are registered with a name so that they are actually 
    // created only if, at runtime, their name is listed in the command line
    AbsFactory( const std::string& name ): kWord( name ) {
      registerFactory( name, this );
    }
    virtual ~AbsFactory() {}
    virtual T* create( const UserParametersManager* userPar ) = 0;
    T* bindToThisFactory( T* t ) {
      ObjectFactory<T>::productMap()[t] = this;
      return t;
    }
    const std::string& keyword() const { return kWord; }
   private:
    std::string kWord;
  };

  friend class ObjectFactory<T>::AbsFactory;

  template<class UPM>
  static std::vector<T*> create( const UPM* userPar ) {
    std::vector<T*> aList;
    // loop over analysis object factories
    static std::map<std::string,AbsFactory*>* fm = factoryMap();
    for ( const auto& element: *fm ) {
      // create analysis object if its name is listed in the command line
      if ( ( userPar->   getUserParameter( element.first ) != "" ) ||
           ( userPar->hasNoValueParameter( element.first )       ) ) {
/*
        int n = 1;
        userPar->getUserParameter( element.second->keyword() + "_replica", n );
        if ( n <= 0 ) continue;
        auto a = element.second->create( userPar );
        aList.push_back( a );
        if ( !a->isReplicable() ) continue;
        while ( --n ) aList.push_back( element.second->create( userPar ) );
*/
        T* ptr;
        while ( ( ptr = element.second->create( userPar ) ) != nullptr )
                aList.push_back( ptr );
      }
    }
    return aList;
  }

  template<class UPM>
  static T* createSingle( const UPM* userPar,
                          const std::string name = "keyword" ) {
    static std::vector<T*> v = create( userPar );
    switch ( v.size() ) {
    case 1:
      return v.front();
    case 0:
      std::cout << "Invalid configuration, no " << name << " given"
                << std::endl;
      break;
    default:
      std::cout << "Invalid configuration, multiple " << name << "s given:"
                << std::endl;
      for ( T* o: v ) std::cout << getFactory( o )->keyword()
                                << std::endl;
      break;
    }
    return nullptr;
  }

  static const AbsFactory* getFactory( const T* t ) {
    auto& pMap = productMap();
    auto iter = pMap.find( t );
    return ( iter == pMap.end() ? nullptr : iter->second );
  }

 protected:

  // function to add analyzer concrete factories
  static void registerFactory( const std::string& name, AbsFactory* f ) {
    static std::map<std::string,AbsFactory*>& fm = *factoryMap();
    fm[name] = f;
    return;
  }
  // map to associate analyzer names with corresponding factories
  static std::map<std::string,AbsFactory*>* factoryMap() {
    static std::map<std::string,AbsFactory*>* fm =
       new std::map<std::string,AbsFactory*>;
    return fm;
  }

 private:

  // map to associate products with corresponding factories
  static std::map<const T*,const AbsFactory*>& productMap() {
    static std::map<const T*,const AbsFactory*>* fm =
       new std::map<const T*,const AbsFactory*>;
    return *fm;
  }

};

#endif // Utilities_ObjectFactory_h

