#include <iostream>
#include <fstream>
#include <string>
#include <set>

#include "NtuTool/Common/interface/UserParametersManager.h"

using namespace std;

UserParametersManager::UserParametersManager() {
}


UserParametersManager::~UserParametersManager() {
//  std::cout << "delete UserParametersManager " << this << std::endl;

//  nvpMap()[this].clear();

//  std::cout << "UserParametersManager deleted " << this << std::endl;
}


void UserParametersManager::setUserParameter( const string& key,
                                              const string& val ) {
  map<string,string>::iterator iter = userParameters.find( key );
  map<string,string>::iterator iend = userParameters.end();
  if ( iter != iend ) iter->second = val;
  else                userParameters.insert( make_pair( key, val ) );
  return;
}


void UserParametersManager::setUserParameter( const string& key,
                                              const   bool& val ) {
  setUserParameter( key, val ? "t" : "f" );
  return;
}


void UserParametersManager::addNoValueParameter( const string& key ) {
  noValueParameters.push_back( key );
  nvps.insert( key );
//  nvpMap()[this].insert( key );
  return;
}


void UserParametersManager::include( const UserParametersManager& userPar ) {
  userParameters   .insert( userPar.userParameters   .begin(), userPar.userParameters   .end() );
  noValueParameters.insert( noValueParameters.end(),
                            userPar.noValueParameters.begin(), userPar.noValueParameters.end() );
  nvps             .insert( userPar.nvps             .begin(), userPar.nvps             .end() );
  return;
}


void UserParametersManager::setConfiguration( const string& file ) {
  ifstream cfg( file.c_str() );
  int lenMax = 1000;
  char* line = new char[lenMax];
  char* lptr;
  string::size_type length;
  while ( cfg.getline( line, lenMax ) ) {
    truncateLineString( line, cfg.gcount() );
    lptr = line;
    while ( *lptr == ' ' ) ++lptr;
    if    ( *lptr == '#' ) continue;
    string key( lptr );
    length = key.find( " " );
    if ( length == std::string::npos ) {
      if ( key.empty() ) continue;
      addNoValueParameter( key );
//      cout << "invalid configuration input: " << line << endl;
      continue;
    }
    key = key.substr( 0, length );
    lptr += length;
    while ( *lptr == ' ' ) ++lptr;
    string val( lptr );
    if ( val.empty() ) {
      addNoValueParameter( key );
      continue;
    }
    length = val.find( " " );
    if ( length != std::string::npos )
    val = val.substr( 0, length );
    if ( key == "-c" ) setConfiguration( val );
    else               setUserParameter( key, val );
  }
  delete[] line;
  return;
}


const string& UserParametersManager::getUserParameter( const string& key )
                                                       const {
  static string dum( "" );
  map<string,string>::const_iterator iter = userParameters.find( key );
  map<string,string>::const_iterator iend = userParameters.end();
  if ( iter != iend ) return iter->second;
  return dum;
}


template <>
void UserParametersManager::getUserParameter( const string& key,
                                                      bool& val ) const {
  const std::string& s = getUserParameter( key );
  if ( s.empty() ) return;
  const char* flag = s.c_str();
  if (   ( *flag == 't' ) || ( *flag == 'T' ) ||
       ( ( *flag >= '1' ) && ( *flag <= '9' ) ) ) {
    val = true;
    return;
  }
  if (   ( *flag == 'f' ) || ( *flag == 'F' ) ||
         ( *flag == '0' )                       ) {
    val = false;
    return;
  }
  cerr << "*** INVALID VALUE FOR BOOLEAN : " << key << " = " << s
       << " ***" << endl;
  return;
}


template <>
void UserParametersManager::getUserParameter( const string& key,
                                                    string& val ) const {
  const std::string& s = getUserParameter( key );
  if ( s.empty() ) {
    if ( hasNoValueParameter( key ) ) val = "";
  }
  else {
    val = s;
  }
  return;
}


const std::map<std::string,std::string>& UserParametersManager::getParametersMap() const {
  return userParameters;
}


const vector<string>& UserParametersManager::getNoValueParameters() const {
  return noValueParameters;
}


bool                  UserParametersManager::hasNoValueParameter(
                                                const string& key ) const {
//  static map<const UserParametersManager*,set<string>>& m = nvpMap();
//  const set<string>& s = m[this];
//  return ( s.find( key ) != s.end() );
  return ( nvps.find( key ) != nvps.end() );
}


void UserParametersManager::dumpAll( ostream& os ) const {
  vector<string>::const_iterator v_iter = noValueParameters.begin();
  vector<string>::const_iterator v_iend = noValueParameters.end();
  while ( v_iter != v_iend ) {
    const string& entry = *v_iter++;
    os << entry << endl;
  }
  map<string,string>::const_iterator m_iter = userParameters.begin();
  map<string,string>::const_iterator m_iend = userParameters.end();
  while ( m_iter != m_iend ) {
    const map<string,string>::value_type& entry = *m_iter++;
    os << entry.first << " " << entry.second << endl;
  }
  return;
}

/*
map<const UserParametersManager*,set<string>>& UserParametersManager::nvpMap() {
//  static map<const UserParametersManager*,set<string>> m;
//  return m;
  static auto m = new map<const UserParametersManager*,set<string>>;
  return *m;
}
*/

