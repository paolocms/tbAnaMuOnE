#include <iostream>
#include <iomanip>
#include <sstream>

template <class T>
void UserParametersManager::setUserParameter( const std::string& key,
                                              const           T& val ) {
  std::stringstream sstr;
  sstr.str( "" );
  sstr << val;
  setUserParameter( key, sstr.str() );
  return;
}


template <class T>
void UserParametersManager::getUserParameter( const std::string& key,
                                                              T& val ) const {
  const std::string& s = getUserParameter( key );
  if ( s.empty() ) return;
  bool h = false;
  if ( s.length() > 2 ) {
    const char* p = s.c_str();
    if ( ( *p++ == '0' ) && ( ( *p == 'x' ) || ( *p == 'X' ) ) ) h = true;
  }
  std::stringstream sstr;
  sstr.str( s );
  ( h ? sstr >> std::hex : sstr ) >> val;
  return;
}


template <class T>
void UserParametersManager::getUserParameter( const std::string& key,
                                                              T& val,
                                                        const T& def ) const {
  val = def;
  const std::string& s = getUserParameter( key );
  if ( s.empty() ) return;
  bool h = false;
  if ( s.length() > 2 ) {
    const char* p = s.c_str();
    if ( ( *p++ == '0' ) && ( ( *p == 'x' ) || ( *p == 'X' ) ) ) h = true;
  }
  std::stringstream sstr;
  sstr.str( s );
  ( h ? sstr >> std::hex : sstr ) >> val;
  return;
}


template <class T>
T    UserParametersManager::getUserParameter( const std::string& key ) const {
  T val;
  getUserParameter( key, val );
  return val;
}

