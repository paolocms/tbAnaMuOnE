#ifndef NtuTool_Common_CommandLineParser_h
#define NtuTool_Common_CommandLineParser_h
/** \class CommandLineParser
 *
 *  Description: 
 *    Class to parse the command line and:
 *    - set the input/output files,
 *    - set the number of events, if any,
 *    - set the user parameters, if any.
 *
 *  \author Paolo Ronchese INFN Padova
 *
 */

//----------------------
// Base Class Headers --
//----------------------
#include "NtuTool/Common/interface/UserParametersManager.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------


//---------------
// C++ Headers --
//---------------
#include <iostream>
#include <sstream>
#include <string>

//              ---------------------
//              -- Class Interface --
//              ---------------------

class CommandLineParser {

 public:

  /** Destructor
   */
  virtual ~CommandLineParser() {}

  static CommandLineParser* getInstance() {
    CommandLineParser*& parserInstance = instance();
    if ( parserInstance == nullptr ) new CommandLineParser;
    return parserInstance;
  }

  /** Operations
   */
  /// run the application
  virtual void parse( UserParametersManager* uPar, int argc, char* argv[] ) {
    std::string key = "";
    std::string val = "";
    char** argp = argv;
    char** argl = argp++ + argc;
    while ( argp < argl ) {
      std::string args( *argp++ );
      if ( ( args == "-n" ) ||
           ( args == "-s" ) ||
           ( args == "-a" ) ) {
        uPar->setUserParameter( args, *argp++ );
        continue;
      }
      if ( args == "-c" ) {
        uPar->setConfiguration( *argp++ );
        continue;
      }
      if ( args == "-v" ) {
        key = *argp++;
        val = *argp++;
        uPar->setUserParameter( key, val );
        continue;
      }
      uPar->addNoValueParameter( args );
    }
    return;
  }

 protected:

  /** Constructor
   */
  CommandLineParser() {
    CommandLineParser*& parserInstance = instance();
    if ( parserInstance == nullptr ) parserInstance = this;
  }

 private:

  static CommandLineParser*& instance() {
    static CommandLineParser* parserInstance = nullptr;
    return parserInstance;
  }

  CommandLineParser           ( const CommandLineParser& t ) = delete;
  CommandLineParser& operator=( const CommandLineParser& t ) = delete;

};


#endif // NtuTool_Common_CommandLineParser_h


