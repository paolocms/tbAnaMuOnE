#ifndef NtuTool_Common_UserParametersManager_h
#define NtuTool_Common_UserParametersManager_h

#include <vector>
#include <map>
#include <set>
#include <string>

// class to handle runtime parameters, typically given in the command line
// or specified in a configuration file in its turn specified in the
// command line:
// - the parsing from the command line is done from the "CommandLineParser"
//   class, in NtuTool/Common;
// - the values are specified including "-v key value" in the command line,
//   different behaviour can be implemented in classes derived from
//   "CommandLineParser";
// - the parsing of configuration files is done in this class by the
//   function "setConfiguration";
// - lines beginning with "#" are ignored, different behaviour can be
//   implemented in classes derived from this one;
// - several configuration files can be given repeating the "-c" option;
// - if a parameter is specified more than once in the command line,
//   directly or through a configuration file, the last one is taken.
// all parameters are stored as string:
// - conversions from/to numerical values are done internally;
// - for parameters not given in the command string, directly or through
//   a configuration file, an empty string is given;
// - the getParameter( key, variable ) assigns to the variable, having any
//   type, the value given with the string key;
// - if the key is not present in the command line, the variable is left
//   unchanged.


// useful macros, to be used with care due to the common pitfalls
// arising with preprocessor macros
#define SET_USER_PARAMETER(NAME) setUserParameter(#NAME,NAME);
#define GET_USER_PARAMETER(NAME) getUserParameter(#NAME,NAME);
#define ASS_USER_PARAMETER(NAME,VALUE) setUserParameter(#NAME,NAME=VALUE);

class UserParametersManager {

 public:

  UserParametersManager();
  virtual ~UserParametersManager();

  void setUserParameter( const std::string& key,
                         const std::string& val );
  template <class T>
  void setUserParameter( const std::string& key,
                         const           T& val );
  void setUserParameter( const std::string& key,
                         const        bool& val );
  void addNoValueParameter( const std::string& key );
  void include( const UserParametersManager& userPar );

  virtual
  void setConfiguration( const std::string& file );

  const std::string& getUserParameter( const std::string& key ) const;
  template <class T>
  void               getUserParameter( const std::string& key,
                                                       T& val ) const;
  template <class T>
  void               getUserParameter( const std::string& key,
                                                       T& val,
                                                 const T& def ) const;
  template <class T>
  T                  getUserParameter( const std::string& key ) const;

  const std::map<std::string,std::string>& getParametersMap() const;
  const std::vector<std::string>& getNoValueParameters() const;
  bool                            hasNoValueParameter(
                                const std::string& key ) const;

  virtual void dumpAll( std::ostream& os ) const;

  static void truncateLineString( char* str, int len ) {
    char* ptr = str + len;
    while ( ptr > str ) {
      char& c = *--ptr;
      if ( c == 13 ) {
        c = '\0';
        break;
      }
    }
    return;
  }

 private:

  UserParametersManager           ( const UserParametersManager& t ) = delete;
  UserParametersManager& operator=( const UserParametersManager& t ) = delete;

  std::map<std::string,std::string> userParameters;
  std::vector<std::string>   noValueParameters;
  std::set<std::string> nvps;
//  static std::map<const UserParametersManager*,std::set<std::string>>& nvpMap();

};

template <>
void UserParametersManager::getUserParameter( const std::string& key,
                                                           bool& val ) const;

template <>
void UserParametersManager::getUserParameter( const std::string& key,
                                                    std::string& val ) const;

#include "NtuTool/Common/interface/UserParametersManager.hpp"

#endif // NtuTool_Common_UserParametersManager_h
