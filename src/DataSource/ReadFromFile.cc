#include "DataSource/ReadFromFile.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>

using namespace std;

// read data from file "name"
ReadFromFile::ReadFromFile( const UserParametersManager* userPar,
                            const string& name ):
 channel(  0 ),
 evtRead( -1 ),
 evtSkip(  0 ),
 evnUpdate( false ),
 iVerb    ( false ) {
  userPar->getUserParameter( "nread"            , evtRead );
  userPar->getUserParameter( "nskip"            , evtSkip );
  userPar->getUserParameter( "chaId"            , channel );
  userPar->getUserParameter( "eventNumberUpdate", evnUpdate );
  userPar->getUserParameter( "inputVerbose"     , iVerb );
  mode = 0;
  userPar->getUserParameter( "inputMode"     , mode );
  cout << "open file " << name << ' ' << channel << ' ' << evtSkip << ' ' << evtRead << endl;
  file = new ifstream( name.c_str(), ios::binary );
}


ReadFromFile::~ReadFromFile() {
  delete file;
}


// get an event
const Event* ReadFromFile::get() {
  static int nread = evtRead;
  while ( evtSkip && evtSkip-- ) readFile();
  if ( ( evtRead >= 0 ) && ( evtRead-- <= 0 ) ) {
    if ( !evnUpdate ) return nullptr;
    int nnread;
    cout << "Enter new event number > " << flush;
    cin >> nnread;
    cin.clear();
    cin.ignore();
    evtRead = nnread - nread;
    if ( !evtRead-- ) return nullptr;
    nread = nnread;
  }
  return readFile();
}

