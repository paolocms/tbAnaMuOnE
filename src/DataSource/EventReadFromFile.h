#ifndef DataSource_EventReadFromFile_h
#define DataSource_EventReadFromFile_h

#include "DataSource/ReadFromFile.h"
#include "Framework/EventSource.h"
#include "Framework/Event.h"

#include <iostream>
#include <fstream>
#include <string>

class EventReadFromFile: public ReadFromFile {

 public:

//  static EventReadFromFile* create( const std::string& key,
//                            const UserParametersManager* userPar ) {
//    return nextInstance<EventReadFromFile>( key, userPar,
//                                            userPar->getUserParameter( key ) );
//  }

  // read data from file "name"
  EventReadFromFile( const UserParametersManager* userPar,
                     const std::string& name );
//  EventReadFromFile( const UserParametersManager* userPar,
//                     const std::string& name,
//                     int chann = 4,
//                     int nread = -1, int nskip = 0, bool eventNumUpdate = false, bool verbose = false );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventReadFromFile           ( const EventReadFromFile& x ) = delete;
  EventReadFromFile& operator=( const EventReadFromFile& x ) = delete;

  ~EventReadFromFile() override;

 private:

/*
  Event::acqHeader aHead;

  // get an event
//  int counter;
  const Event* get() override;

  // input file
  std::ifstream* file;
  int channel;
  int evtRead;
  int evtSkip;
  bool evnUpdate;
  bool iVerb;
  int mode;
*/

  // read and event
  const Event* readFile() override;

/*
  const Event* readStream();

//  static const Event* createEvent(
//                      const Event::acqHeader* ah, const Event::evtHeader& eh,
//                      const std::map<int, std::vector<Event::data_type>>& dMap );
  static void fillEvent( Event* ev,
                         const std::map<int,
                                        std::vector<Event::data_type>>& dMap );
  static bool contiguousClocks( unsigned int lo, unsigned int lb,
                                unsigned int ro, unsigned int rb );
*/

};

#endif // DataSource_EventReadFromFile_h

