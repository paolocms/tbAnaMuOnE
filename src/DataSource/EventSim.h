#ifndef DataSource_EventSim_h
#define DataSource_EventSim_h

#include "Framework/EventSource.h"
#include "Framework/Event.h"
#include "Utilities/TimeStamp.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
class Event;

class EventSim: public EventSource {

 public:

  enum Mode { noise, laser, beam };
  struct ConfigData {
    unsigned int nChannel;
    unsigned int chanMask;
    unsigned int nSamples;
    float* pedestal;
    float* pedNoise;
    float* noiseRMS;
    float* laserPos;
    float* laserSig;
    float* laserWid;
    float* laserMax;
    float* laserRMS;
  };

//  static EventSim* create( const std::string& key,
//                           const UserParametersManager* userPar ) {
//    return nextInstance<EventSim>( key, userPar, key );
//  }

  // simulate n events with random seed s and period p
  EventSim( const UserParametersManager* userPar,
            const std::string& name );
//  EventSim( const UserParametersManager* userPar,
//            unsigned int n, unsigned int s,
//            Mode m = laser, ConfigData* c = nullptr,
//            unsigned int p = 0, TimeStamp::Unit u = TimeStamp::milliseconds );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventSim           ( const EventSim& x ) = delete;
  EventSim& operator=( const EventSim& x ) = delete;

  ~EventSim() override;

  // create a configuration data structure with default values
  static ConfigData* createConfigData();

 private:

  ConfigData* confData;
  Event::acqHeader aHead;

  // number of events to simulate
  int nMax;

  // last event identifier
  int evId;

  // last event timestamp
  long long evTS;

  // event period
  unsigned int evtP;

  // time unit
  TimeStamp::Unit unit;

  // get an event
  const Event* get() override;

  // generate and event
  const Event* generate();

  // random number generation

  // uniform 
  static double randF() { return         random() *   1.0         /
                                                      RAND_MAX     ; }
  static double randF( float max ) {
                          return         random() *   max         /
                                                      RAND_MAX     ; }
  static double randF( float min, float max ) {
                          return min + ( random() * ( max - min ) /
                                                      RAND_MAX    ); }

  // phi angle
  static double randPhi() { return randF( 2.0 * M_PI ); }

  // exponential
  static double randE()  { return     -log( randF() ); }
  static double randE( float c )  {
                           return -c * log( randF() ); }

  // gaussian
  static double randG() { return                  sqrt( -log( randF() ) ) *
                                                         sin( randPhi() )    ; }
  static double randG( float sigma ) {
                          return          sigma * sqrt( -log( randF() ) ) *
                                                         sin( randPhi() )    ; }
  static double randG( float mean, float sigma ) {
                          return mean + ( sigma * sqrt( -log( randF() ) ) *
                                                         sin( randPhi() ) )  ; }

};

#endif // DataSource_EventSim_h

