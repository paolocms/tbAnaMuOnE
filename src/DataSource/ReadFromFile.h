#ifndef DataSource_ReadFromFile_h
#define DataSource_ReadFromFile_h

#include "Framework/EventSource.h"
#include "Framework/Event.h"

#include <iostream>
#include <fstream>
#include <string>

class ReadFromFile: public EventSource {

 public:

  // read data from file "name"
  ReadFromFile( const UserParametersManager* userPar,
                const std::string& name );
  // deleted copy constructor and assignment to prevent unadvertent copy
  ReadFromFile           ( const ReadFromFile& x ) = delete;
  ReadFromFile& operator=( const ReadFromFile& x ) = delete;

  ~ReadFromFile() override;

 protected:

  Event::acqHeader aHead;

  // input file
  std::ifstream* file;
  int channel;
  int evtRead;
  int evtSkip;
  bool evnUpdate;
  bool iVerb;
  int mode;

 private:

  // read and event
  virtual const Event* readFile() = 0;
  // get an event
  const Event* get() override;

};

#endif // DataSource_ReadFromFile_h

