#include "DataSource/StreamReadFromFile.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>

using namespace std;

// concrete factory to create an EventDump analyzer
class StreamReadFromFileFactory: public SourceFactory::AbsFactory {
 public:
  // assign "input" as name for this analyzer and factory
  StreamReadFromFileFactory(): SourceFactory::AbsFactory( "instr" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( StreamReadFromFile::basic_type::
                                                  create<StreamReadFromFile>(
                              keyword(), userPar,
                              userPar->getUserParameter( keyword() ) ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an StreamReadFromFileFactory will be available with name "input".
static StreamReadFromFileFactory erf;

// read data from file "name"
StreamReadFromFile::StreamReadFromFile( const UserParametersManager* userPar,
                                        const string& name ):
 ReadFromFile( userPar, name ) {
//  userPar->getUserParameter( "nread"            , evtRead );
////  cout << "StreamReadFromFile  " << evtRead << endl;
//  userPar->getUserParameter( "nskip"            , evtSkip );
////  cout << "StreamReadFromFile  " << evtSkip << endl;
//  userPar->getUserParameter( "chaId"            , channel );
////  cout << "StreamReadFromFile  " << channel << endl;
//  userPar->getUserParameter( "eventNumberUpdate", evnUpdate );
////  cout << "StreamReadFromFile  " << evnUpdate << endl;
//  userPar->getUserParameter( "inputVerbose"     , iVerb );
//  mode = 0;
//  userPar->getUserParameter( "inputMode"     , mode );
//  cout << "open file " << name << ' ' << channel << ' ' << evtSkip << ' ' << evtRead << endl;
//  file = new ifstream( name.c_str(), ios::binary );
//  if ( !mode )
//  file->read( reinterpret_cast<char*>( &aHead ) , sizeof( Event::acqHeader ) );
}


StreamReadFromFile::~StreamReadFromFile() {
}


// read an event
const Event* StreamReadFromFile::readFile() {
//  cout << "StreamReadFromFile::readFile" << endl;
//  static GetRawEventId* gi = GetRawEventId::instance();
//  static GetSignal    * gs = GetSignal    ::instance();
  static Event::evtHeader eHead = []() {
    static Event::evtHeader eh;
    eh.trigger_number = 0;
    return eh;
  }();
  static Event::acqHeader* aHead = GetSignal::getAcqHeader();
  static list<char*> sampleList;
  static map<int, vector<Event::data_type>> dMap;
  if ( sampleList.empty() ) {
    char* c;
//    int n = 2;
//    while ( n-- ) {
      c = new char[GetSignal::dataSize];
      sampleList.push_back( c );
//      cout << "TRY FIRST READ" << endl;
      if ( !file->read( c, GetSignal::dataSize ) ) return nullptr;
//      cout << "START: "
//           << GetSignal::bBytePos << ' '
//           << *reinterpret_cast<Event::data_type*>( c + GetSignal::bBytePos ) << ' '
//           << ( *reinterpret_cast<Event::data_type*>( c + 54 ) & 0xff ) << ' '
//           << ( *reinterpret_cast<Event::data_type*>( c + 55 ) & 0xff ) << ' '
//           << GetRawEventId::orbitCount( c ) << '/'
//           << GetRawEventId::bunchCount( c ) << endl;
//    }
  }
  static auto saveStart =       sampleList.begin();
  static auto nextStart =       sampleList.begin();
//  cout << "START DIST: " << distance( saveStart, nextStart ) << endl;
//  static auto nextEnd   = prev( sampleList.end() );
//  char* pp = sampleList.front();
//  unsigned int prevOrbit = GetRawEventId::orbitCount( pp );
//  unsigned int prevBunch = GetRawEventId::bunchCount( pp );
  char* cp = sampleList.back();
//  cout << "CURR SAMPLE: " << (void*)*saveStart << ' ' << (void*)cp << endl;
  unsigned int currOrbit;// = GetRawEventId::orbitCount( cp );
  unsigned int currBunch;// = GetRawEventId::bunchCount( cp );
  char* np;
  unsigned int nextOrbit = GetRawEventId::orbitCount( cp );//currOrbit;
  unsigned int nextBunch = GetRawEventId::bunchCount( cp );//currBunch;
  do {
    if ( nextStart == sampleList.begin() ) {
//      cout << "create new buffer" << endl;
      sampleList.push_back( np = new char[GetSignal::dataSize] );
    }
    else {
//      cout << "recycle oldest buffer " << distance( sampleList.begin(), nextStart ) << endl;
      sampleList.push_back( np = sampleList.front() );
      sampleList.pop_front();
    }
//    cout << "read next..." << endl;
    if ( !file->read( np, GetSignal::dataSize ) ) {
//      cout << "END OF FILE " << distance( sampleList.begin(), nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
//      cout << "END OF FILE " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
      int ns = GetSignal::fillDataMap( sampleList.begin(),
//                                       sampleList.end(), dMap );
                                 prev( sampleList.end() ), dMap );
      if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
      Event* ev = new Event( aHead, eHead, this );
      fillEvent( ev, dMap );
      sampleList.clear();
      nextStart = sampleList.end();
      return ev;
    }
    currOrbit = nextOrbit;
    currBunch = nextBunch;
    nextOrbit = GetRawEventId::orbitCount( np );
    nextBunch = GetRawEventId::bunchCount( np );
//    cout << "CHECK FOR "
//         << currOrbit << '/' << currBunch << ' '
//         << nextOrbit << '/' << nextBunch << ' ' << sampleList.size() << endl;
  } while ( contiguousClocks( currOrbit, currBunch, nextOrbit, nextBunch ) );
//  cout << "CYCLE END " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << endl;
//  cout << "CYCLE END " << endl;
  while ( nextStart != sampleList.begin() ) {
//    cout << "delete oldest buffer" << endl;
    delete[] sampleList.front();
    sampleList.pop_front();
  }
  saveStart = nextStart;
  nextStart = prev( sampleList.end() );
//  cout << "fillDataMap " << distance( sampleList.begin(), nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
//  cout << "fillDataMap " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
  int ns = GetSignal::fillDataMap( saveStart, nextStart, dMap );
  if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
  Event* ev = new Event( aHead, eHead, this );
//  cout << "fill event" << endl;
  fillEvent( ev, dMap );
  ++eHead.trigger_number;
  return ev;
}


void StreamReadFromFile::fillEvent( Event* ev,
                                    const std::map<int,
                                    std::vector<Event::data_type>>& dMap ) {
  for ( auto& e: dMap ) {
    int chan = e.first;
    vector<Event::data_type> data = e.second;
    if ( !data.empty() ) {
      unsigned int nd = data.size();
//      cout << "fillEvent " << chan << ' ' << nd << endl;
      if ( nd & 1 ) {
        cout << "add zero " << nd << endl;
        data.push_back( 0 );
	++nd;
      }
      nd >>= 1; // shift 1 bit to divide by 2
      Event::evtData eData;
      eData.chID_readPointer = 0;
      eData.numOfValidData = nd;
      memcpy( eData.data, data.data(), nd * Event::ioSize );
      ev->copy( chan, eData );
    }
//    else {
//      cout << "empty " << chan << endl;
//    }
  }
  return;
}


bool StreamReadFromFile::contiguousClocks( unsigned int lo, unsigned int lb,
                                          unsigned int ro, unsigned int rb ) {
  GetSignal::bunchAdvance( lo, lb );
  return ( ( lo == ro ) && ( lb == rb ) );
}

