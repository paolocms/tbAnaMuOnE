#ifndef DataSource_TrigLessSim_h
#define DataSource_TrigLessSim_h

#include "Framework/EventSource.h"
#include "Framework/Event.h"
#include "DataOperation/GetSignal.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

class TrigLessSim: public EventSource {

 public:

  // merge data from signal file and noise file
  TrigLessSim( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  TrigLessSim           ( const TrigLessSim& x ) = delete;
  TrigLessSim& operator=( const TrigLessSim& x ) = delete;

  ~TrigLessSim() override;

  class Filter {
   public:
    Filter() = default;
    virtual ~Filter() = default;
    virtual bool accept( const char* dataSample ) = 0;
  };

 private:

//  static unsigned int dataNBit;
//  static unsigned int dataSize;
//  static unsigned int orbitPos;
//  static unsigned int bunchPos;
//  static unsigned int orbitLen;
//  static unsigned int bunchLen;
//  static unsigned int oBitMask;
//  static unsigned int bBitMask;
//  static unsigned int bunchMax;

  Event::acqHeader sHead;
  Event::acqHeader nHead;

  // get an event
  const Event* get() override;

  // input file
  std::ifstream* sFile;
  std::ifstream* nFile;
  std::ifstream* cFile;
  int maxSelectedEvents;
  int maxSignalEvents;

  char* sSample = nullptr;
  char* nSample = nullptr;
  bool sFailed;
  bool nFailed;
  bool fillStatus;
  bool killSignal;
  unsigned int randomClockCount;
  unsigned int orbitMDiff;
  unsigned int orbitCount;
  unsigned int bunchCount;
  unsigned int dataCenter;
  unsigned int dataLCount;
  unsigned int dataRCount;
  unsigned int dataOldest;
  unsigned int blindCount;
  std::vector<char*> dataBuffer;
  std::string streamDumpName;

  Filter* selector;
  bool nextDataSample( char* dataSample );
  void fillDataBuffer();
  const Event* eventBuild();

  // read an event
  static const Event* readFile( std::ifstream* file = nullptr,
                                Event::acqHeader* aHead = nullptr,
                                bool restart = false );
  static bool fillDataSample( std::ifstream* file, Event::acqHeader* aHead,
                              char* dataSample, unsigned int randomClock,
                              bool restart, int& nmax );
  static Event* eventBuild( //Event::acqHeader  aHead,
                            const Event::evtHeader& eHead,
                            const std::vector<char*>& dataBuffer,
                            unsigned int start,
                            const EventSource* src );
//  static Event::data_type channelContent( char* dataSample, int pos );
  static unsigned int bitMask( unsigned int n ) {
    static unsigned int l = sizeof( unsigned int ) << 3;
    static unsigned int* m = []( unsigned int l ) {
      static unsigned int* a = new unsigned int[l];
      static unsigned int* b = a;
      static unsigned int* c = b++;
      *a = 0;
      unsigned int k = l;
      while ( k-- ) *b++ = ( ( *c++ << 1 ) | 1 );
      return a;
    }( l );
    return ( n < l ? m[n] : 0xffffffff );
  }
  static unsigned int bitMask( unsigned int f, unsigned int n ) {
    unsigned int m = 0;
    f = ( sizeof( m ) << 3 ) - ( n + f ); // shift 3 bit to multiply by 8
    while ( n-- ) ( m <<= 1 ) |= 1;
    while ( f-- ) ( m <<= 1 ) &= 0xfffffffe;
    return m;
  }
//  static void putNumber( unsigned int x,
//                         char* ptr, int f, int n, int mask );
//  static void putOrbit( unsigned int x, char* dataSample ) {
//    putNumber( x, dataSample + orbitPos, orbitLen, oBitMask );
//  }
//  static void putBunch( unsigned int x, char* dataSample ) {
//    putNumber( x, dataSample + bunchPos, bunchLen, bBitMask );
//  }
//  static unsigned int getNumber( const char* ptr, int f, int n, unsigned mask );
  static unsigned int getOrbit( const char* dataSample ) {
    static unsigned int byteShift = ( GetSignal::orbitPos >> 3 );
//    return getNumber( dataSample + byteShift, 0, orbitLen, oBitMask );
    return *reinterpret_cast<const unsigned int*>( dataSample + byteShift );
  }
  static unsigned int getBunch( const char* dataSample ) {
    static unsigned int byteShift = ( GetSignal::bunchPos >> 3 );
//    return getNumber( dataSample + byteShift, 0, bunchLen, bBitMask );
    return *reinterpret_cast<const unsigned short*>( dataSample + byteShift );
  }
//  static const std::vector<int>& channelMap();
//  static const std::vector<int>& dataPosMap();

};

#endif // DataSource_TrigLessSim_h

