#include "DataSource/EventSim.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "Utilities/TimeStamp.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>

using namespace std;

// concrete factory to create an EventDump analyzer
class EventSimFactory: public SourceFactory::AbsFactory {
 public:
  // assign "sim" as name for this analyzer and factory
  EventSimFactory(): SourceFactory::AbsFactory( "sim" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
/*
    stringstream sstr;
    // get number of events to generate
    const std::string& sim = userPar->getUserParameter( keyword() );
    sstr.str( sim );
    unsigned int nevt;
    sstr >> nevt;
    // set seed if available
    unsigned int seed = 1;
    const std::string& sees = userPar->getUserParameter( "seed" );
    if ( !sees.empty() ) {
      sstr.clear();
      sstr.str( sees );
      sstr >> seed;
    }
*/
//    unsigned int nevt = 0;
//    unsigned int seed = 1;
//    unsigned int evtp = 0;
//    TimeStamp::Unit unit = TimeStamp::milliseconds;
//    const string& key = keyword();
//    userPar->getUserParameter( key            , nevt );
//    userPar->getUserParameter( key + "_seed"  , seed );
//    userPar->getUserParameter( key + "_period", evtp );
//    const std::string& tu = userPar->getUserParameter( key + "_unit" );
//    if ( ( *tu.c_str() == 's' ) ||
//         ( *tu.c_str() == 'S' ) )     unit = TimeStamp::seconds;
//    if ( tu.substr( 0, 3 ) == "mil" ) unit = TimeStamp::milliseconds;
//    if ( tu.substr( 0, 3 ) == "mic" ) unit = TimeStamp::microseconds;
//
//    string kSPed = key + "_pedestal";
//    string kPNoi = key + "_pedNoise";
//    string kNRMS = key + "_noiseRMS";
//    string kLMax = key + "_laserMax";
//    string kLRMS = key + "_laserRMS";
//    string kLPos = key + "_laserPos";
//    string kLSig = key + "_laserSig";
//    string kLWid = key + "_laserWid";
//    EventSim::Mode mode = EventSim::noise;
//    EventSim::ConfigData* conf = EventSim::createConfigData();
//    unsigned int nChannel = conf->nChannel;
//    unsigned int iCha;
//    unsigned int mask = conf->chanMask;
//    for ( iCha = 0; iCha < nChannel; ++iCha ) {
//      if ( mask & 1 ) {
//        const std::string chs = "_" + std::to_string( iCha );
//        userPar->getUserParameter( kSPed      , conf->pedestal[iCha] );
//        userPar->getUserParameter( kSPed + chs, conf->pedestal[iCha] );
//        userPar->getUserParameter( kPNoi      , conf->pedNoise[iCha] );
//        userPar->getUserParameter( kPNoi + chs, conf->pedNoise[iCha] );
//        userPar->getUserParameter( kNRMS      , conf->noiseRMS[iCha] );
//        userPar->getUserParameter( kNRMS + chs, conf->noiseRMS[iCha] );
//        userPar->getUserParameter( kLMax      , conf->laserMax[iCha] );
//        userPar->getUserParameter( kLMax + chs, conf->laserMax[iCha] );
//        userPar->getUserParameter( kLRMS      , conf->laserRMS[iCha] );
//        userPar->getUserParameter( kLRMS + chs, conf->laserRMS[iCha] );
//        userPar->getUserParameter( kLPos      , conf->laserPos[iCha] );
//        userPar->getUserParameter( kLPos + chs, conf->laserPos[iCha] );
//        userPar->getUserParameter( kLSig      , conf->laserSig[iCha] );
//        userPar->getUserParameter( kLSig + chs, conf->laserSig[iCha] );
//        userPar->getUserParameter( kLWid      , conf->laserWid[iCha] );
//        userPar->getUserParameter( kLWid + chs, conf->laserWid[iCha] );
//      }
//      mask >>= 1;
//    }
//    return bindToThisFactory( EventSim::create( keyword(), userPar,
//                              nevt, seed, mode, conf,
//                              evtp, unit ) );
    return bindToThisFactory( EventSim::basic_type::create<EventSim>( keyword(), userPar, keyword() ) );
//    return bindToThisFactory( IdentifiableObject::create<EventSim>( keyword(), userPar, keyword() ) );
//    return bindToThisFactory( EventSim::create( keyword(), userPar ) );
//    return bindToThisFactory( new EventSim( nevt, seed, mode, conf,
//                                            evtp, unit ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventSimFactory will be available with name "sim".
static EventSimFactory esf;

// simulate data with random seed
//EventSim::EventSim( const UserParametersManager* userPar,
//                    unsigned int n, unsigned int s,
//                    EventSim::Mode m, EventSim::ConfigData* c,
//                    unsigned int p, TimeStamp::Unit u ):
// nMax( n ),
// evId( 0 ),
// evTS( 0 ),
// evtP( p ),
// unit( u ) {
EventSim::EventSim( const UserParametersManager* userPar,
                    const string& key ):
 nMax( 0 ),
 evId( 0 ),
 evTS( 1 ),
 evtP( 0 ),
 unit( TimeStamp::milliseconds ) {
  userPar->getUserParameter( key            , nMax );
  userPar->getUserParameter( key + "_seed"  , evTS );
  userPar->getUserParameter( key + "_period", evtP );
  const std::string& tu = userPar->getUserParameter( key + "_unit" );
  if ( ( *tu.c_str() == 's' ) ||
       ( *tu.c_str() == 'S' ) )     unit = TimeStamp::seconds;
  if ( tu.substr( 0, 3 ) == "mil" ) unit = TimeStamp::milliseconds;
  if ( tu.substr( 0, 3 ) == "mic" ) unit = TimeStamp::microseconds;
//  cout << "EventSim " << n << ' ' << s << ' ' << p << endl;
  srandom( evTS );
//  aHead.channel_mask = 0x1F0F;
//  aHead.sample_per_chan = 128;
  string kSPed = key + "_pedestal";
  string kPNoi = key + "_pedNoise";
  string kNRMS = key + "_noiseRMS";
  string kLMax = key + "_laserMax";
  string kLRMS = key + "_laserRMS";
  string kLPos = key + "_laserPos";
  string kLSig = key + "_laserSig";
  string kLWid = key + "_laserWid";
//  EventSim::Mode mode = EventSim::noise;
//  userPar->getUserParameter( key + "_mode", mode );
  confData = EventSim::createConfigData();
  unsigned int nChannel = confData->nChannel;
  unsigned int iCha;
  unsigned int mask = confData->chanMask;
  for ( iCha = 0; iCha < nChannel; ++iCha ) {
    if ( mask & 1 ) {
      const std::string chs = "_" + std::to_string( iCha );
      userPar->getUserParameter( kSPed      , confData->pedestal[iCha] );
      userPar->getUserParameter( kSPed + chs, confData->pedestal[iCha] );
      userPar->getUserParameter( kPNoi      , confData->pedNoise[iCha] );
      userPar->getUserParameter( kPNoi + chs, confData->pedNoise[iCha] );
      userPar->getUserParameter( kNRMS      , confData->noiseRMS[iCha] );
      userPar->getUserParameter( kNRMS + chs, confData->noiseRMS[iCha] );
      userPar->getUserParameter( kLMax      , confData->laserMax[iCha] );
      userPar->getUserParameter( kLMax + chs, confData->laserMax[iCha] );
      userPar->getUserParameter( kLRMS      , confData->laserRMS[iCha] );
      userPar->getUserParameter( kLRMS + chs, confData->laserRMS[iCha] );
      userPar->getUserParameter( kLPos      , confData->laserPos[iCha] );
      userPar->getUserParameter( kLPos + chs, confData->laserPos[iCha] );
      userPar->getUserParameter( kLSig      , confData->laserSig[iCha] );
      userPar->getUserParameter( kLSig + chs, confData->laserSig[iCha] );
      userPar->getUserParameter( kLWid      , confData->laserWid[iCha] );
      userPar->getUserParameter( kLWid + chs, confData->laserWid[iCha] );
    }
    mask >>= 1;
  }
//  confData = ( c == nullptr ? EventSim::createConfigData() : c );
  aHead.channel_mask    = confData->chanMask;
  aHead.sample_per_chan = confData->nSamples;
  evTS = TimeStamp::sinceEpoch( unit );
}


EventSim::~EventSim() {
}


// create a configuration data structure with default values
EventSim::ConfigData* EventSim::createConfigData() {
  EventSim::ConfigData* conf = new EventSim::ConfigData;
  unsigned int nChannel =
  conf->nChannel = Event::maxChannelNumber;
  conf->chanMask = 0xFFF01FFF;//0x1F0F;
  conf->nSamples = 128;
  conf->pedestal = new float[nChannel];
  conf->pedNoise = new float[nChannel];
  conf->noiseRMS = new float[nChannel];
  conf->laserMax = new float[nChannel];
  conf->laserRMS = new float[nChannel];
  conf->laserPos = new float[nChannel];
  conf->laserSig = new float[nChannel];
  conf->laserWid = new float[nChannel];
  unsigned int iCha;
  for ( iCha = 0; iCha < nChannel; ++iCha ) {
    conf->pedestal[iCha] = 23800.0;
    conf->pedNoise[iCha] =     5.0;
    conf->noiseRMS[iCha] =     1.0;
    conf->laserMax[iCha] =  1000.0;
    conf->laserRMS[iCha] =     5.0;
    conf->laserPos[iCha] =    40.0;
    conf->laserSig[iCha] =     0.5;
    conf->laserWid[iCha] =     2.5;
  }
  return conf;
}


// get an event
const Event* EventSim::get() {
  if ( nMax-- ) return generate();
  return nullptr;
}


// generate an event
const Event* EventSim::generate() {

  TimeStamp::sleepUntil( evTS + evtP, unit );
  
  // set event id
  evId += ceil( randE( 10.0 ) );

  Event::evtHeader eHead;
  eHead.trigger_number = evId;

  // create a new event
  Event* ev = new Event( &aHead, eHead, this );

  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; i++ ) {
    Event::evtData eData;
    if ( !( ( aHead.channel_mask >> i ) & 0x1 ) ) continue;
    eData.chID_readPointer = 0;
    int n = aHead.sample_per_chan;
    eData.numOfValidData = n / 2;
    Event::data_type* data = reinterpret_cast<Event::data_type*>( eData.data );
//    float p = randG( 100.0,  5.0 );
//    float x = randG(  74.0,  5.0 );
//    float s = randG(   2.0,  0.5 );
//    float a = randG( 955.0,  9.0 );
    float p = confData->pedestal[i];
    float x = randG( confData->laserPos[i], confData->laserSig[i] );
    float s = confData->laserWid[i];
    float a = randG( confData->laserMax[i], confData->laserRMS[i] );
    float r = randG( confData->pedNoise[i], confData->noiseRMS[i] );
    while ( n-- ) data[n] = ( a * exp( -pow( ( n - x ) / s, 2 ) ) ) +
                              randG( p, r );
    ev->copy( i, eData );
  }

  evTS = TimeStamp::sinceEpoch( unit );
  return ev;

}

