#ifndef DataSource_FramesReadFromFile_h
#define DataSource_FramesReadFromFile_h

#include "DataSource/ReadFromFile.h"
#include "Framework/EventSource.h"
#include "Framework/Event.h"

#include <iostream>
#include <fstream>
#include <string>

class FramesReadFromFile: public ReadFromFile {

 public:

  // read data from file "name"
  FramesReadFromFile( const UserParametersManager* userPar,
                     const std::string& name );
  // deleted copy constructor and assignment to prevent unadvertent copy
  FramesReadFromFile           ( const FramesReadFromFile& x ) = delete;
  FramesReadFromFile& operator=( const FramesReadFromFile& x ) = delete;

  ~FramesReadFromFile() override;

 private:

  struct ClockPacket {
    uint32_t oc            : 32;
    uint32_t valid         :  1;
    uint32_t valid_all     : 27;
    uint32_t reserved1     :  2;
    uint32_t trigger_last  :  1;
    uint32_t trigger_start :  1;
    uint16_t bx            : 12;
    uint16_t spill         :  1;
    uint16_t reserved2     :  3;
    uint16_t adc24         : 16;
    uint32_t calib         : 32;
    uint16_t adcs[24];
  };

  // read and event
  const Event* readFile();

  bool readClockPacket( ClockPacket* cp );
  bool fillDataSample( char* dataSample );
//  static const Event* createEvent(
//                      const Event::acqHeader* ah, const Event::evtHeader& eh,
//                      const std::map<int, std::vector<Event::data_type>>& dMap );
  static void fillEvent( Event* ev,
                         const std::map<int,
                                        std::vector<Event::data_type>>& dMap );
  static bool contiguousClocks( unsigned int lo, unsigned int lb,
                                unsigned int ro, unsigned int rb );

};

#endif // DataSource_FramesReadFromFile_h

