#include "DataSource/EventReadFromFile.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>

using namespace std;

// concrete factory to create an EventDump analyzer
class EventReadFromFileFactory: public SourceFactory::AbsFactory {
 public:
  // assign "input" as name for this analyzer and factory
  EventReadFromFileFactory(): SourceFactory::AbsFactory( "input" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( EventReadFromFile::basic_type::
                                                 create<EventReadFromFile>(
                              keyword(), userPar,
                              userPar->getUserParameter( keyword() ) ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventReadFromFileFactory will be available with name "input".
static EventReadFromFileFactory erf;

// read data from file "name"
EventReadFromFile::EventReadFromFile( const UserParametersManager* userPar,
                                      const string& name ):
 ReadFromFile( userPar, name ) {
//  userPar->getUserParameter( "nread"            , evtRead );
////  cout << "EventReadFromFile  " << evtRead << endl;
//  userPar->getUserParameter( "nskip"            , evtSkip );
////  cout << "EventReadFromFile  " << evtSkip << endl;
//  userPar->getUserParameter( "chaId"            , channel );
////  cout << "EventReadFromFile  " << channel << endl;
//  userPar->getUserParameter( "eventNumberUpdate", evnUpdate );
////  cout << "EventReadFromFile  " << evnUpdate << endl;
//  userPar->getUserParameter( "inputVerbose"     , iVerb );
//  mode = 0;
//  userPar->getUserParameter( "inputMode"     , mode );
//  cout << "open file " << name << ' ' << channel << ' ' << evtSkip << ' ' << evtRead << endl;
//  file = new ifstream( name.c_str(), ios::binary );
//  if ( !mode )
  file->read( reinterpret_cast<char*>( &aHead ) , sizeof( Event::acqHeader ) );
}


EventReadFromFile::~EventReadFromFile() {
}

/*
// get an event
const Event* EventReadFromFile::get() {
  static int nread = evtRead;
  while ( evtSkip && evtSkip-- ) readFile();
  if ( ( evtRead >= 0 ) && ( evtRead-- <= 0 ) ) {
    if ( !evnUpdate ) return nullptr;
    int nnread;
    cout << "Enter new event number > " << flush;
    cin >> nnread;
    cin.clear();
    cin.ignore();
    evtRead = nnread - nread;
    if ( !evtRead-- ) return nullptr;
    nread = nnread;
  }
  return readFile();
}
*/


// read an event
const Event* EventReadFromFile::readFile() {

  // event pointer and identifier
  Event* ev;

  // try to read input file
  // on success create new event
  Event::evtHeader eHead;
  if ( file->read( reinterpret_cast<char*>( &eHead ),
		   sizeof( eHead ) ) ) ev = new Event( &aHead, eHead, this );
  // on failure return null pointer
  else return nullptr;

  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; i++ ) {

    Event::evtData eData;
    if ( ( ( aHead.channel_mask >> i ) & 0x1 ) == 0x1 ) {
      file->read( reinterpret_cast<char*>( &eData.chID_readPointer ),
                                   sizeof(  eData.chID_readPointer ) );
      file->read( reinterpret_cast<char*>( &eData.numOfValidData ),
                                   sizeof(  eData.numOfValidData ) );
      file->read( reinterpret_cast<char*>(  eData.data ),
            eData.numOfValidData * sizeof( *eData.data ));
      ev->copy( i, eData );
//      ev->copy( reinterpret_cast<Event::data_type*>( eData.data ),
//             eData.numOfValidData * sizeof( *eData.data ) /
//                                   sizeof( Event::data_type ) );
      if ( iVerb )
      cout << eData.numOfValidData << " read , "
           << eData.numOfValidData * sizeof( *eData.data ) /
                                     sizeof( Event::data_type ) << " written" << endl;
    }
//    delete[] eData.data;
  }

  return ev;

}

/*
// read an event
const Event* EventReadFromFile::readStream() {
//  static GetRawEventId* gi = GetRawEventId::instance();
//  static GetSignal    * gs = GetSignal    ::instance();
  static Event::evtHeader eHead = []() {
    static Event::evtHeader eh;
    eh.trigger_number = 0;
    return eh;
  }();
  static Event::acqHeader* aHead = GetSignal::getAcqHeader();
  static list<char*> sampleList;
  static map<int, vector<Event::data_type>> dMap;
  if ( sampleList.empty() ) {
    char* c;
    int n = 2;
    while ( n-- ) {
      c = new char[GetSignal::dataSize];
      sampleList.push_back( c );
      if ( !file->read( c, GetSignal::dataSize ) ) return nullptr;
    }
  }
  static auto nextStart = sampleList.begin();
  static auto saveStart = nextStart;
//  list<char*>::const_iterator ib = sampleList.begin();
  char* pp = sampleList.front();
  unsigned int prevOrbit = GetRawEventId::orbitCount( pp );
  unsigned int prevBunch = GetRawEventId::bunchCount( pp );
  char* cp = *nextStart;
//  char* cp = sampleList.back();
  unsigned int currOrbit = GetRawEventId::orbitCount( cp );
  unsigned int currBunch = GetRawEventId::bunchCount( cp );
  cout << ( *reinterpret_cast<unsigned int*>( pp + 50 ) & 0xff ) << endl;
  cout << ( *reinterpret_cast<unsigned int*>( pp + 51 ) & 0xff ) << endl;
  cout << ( *reinterpret_cast<unsigned int*>( pp + 52 ) & 0xff ) << endl;
  cout << ( *reinterpret_cast<unsigned int*>( pp + 53 ) & 0xff ) << endl;
  cout << *reinterpret_cast<unsigned int*>( pp + 50 ) << endl;
  cout << prevOrbit << '/' << prevBunch << ' '
       << currOrbit << '/' << currBunch << ' ' << sampleList.size() << endl;
  char* np = sampleList.back();
  unsigned int nextOrbit = GetRawEventId::orbitCount( np );
  unsigned int nextBunch = GetRawEventId::bunchCount( np );
//  unsigned int nextOrbit = currOrbit;
//  unsigned int nextBunch = currBunch;
//  GetSignal::bunchAdvance( nextOrbit, nextBunch );
//  while ( ( nextOrbit == currOrbit ) && ( nextBunch == currBunch ) ) {
  while ( contiguousClocks( currOrbit, currBunch, nextOrbit, nextBunch ) ) {
    cout << prevOrbit << '/' << prevBunch << ' '
         << currOrbit << '/' << currBunch << ' '
         << nextOrbit << '/' << nextBunch << ' ' << sampleList.size() << endl;
//    char* np;
//    if  ( ( prevOrbit == currOrbit ) && ( prevBunch == currBunch ) ) {
    if  ( nextStart == sampleList.begin() ) {
      cout << "create new buffer" << endl;
      sampleList.push_back( np = new char[GetSignal::dataSize] );
    }
    else {
      cout << "recycle oldest buffer" << endl;
      sampleList.push_back( np = sampleList.front() );
      sampleList.pop_front();
      pp = sampleList.front();
      prevOrbit = GetRawEventId::orbitCount( pp );
      prevBunch = GetRawEventId::bunchCount( pp );
    }
    cout << "read new buffer" << endl;
    if ( !file->read( np, GetSignal::dataSize ) ) {
      int ns = GetSignal::fillDataMap( sampleList.begin(),
                                       sampleList.end(), dMap );
      if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
      Event* ev = new Event( aHead, eHead, this );
      fillEvent( ev, dMap );
      sampleList.clear();
      nextStart = sampleList.end();
      return ev;
    }
    currOrbit = nextOrbit;
    currBunch = nextBunch;
    nextOrbit = GetRawEventId::orbitCount( np );
    nextBunch = GetRawEventId::bunchCount( np );
    cout << "saved counts " << nextOrbit << '/' << nextBunch << endl;
  }
//  while ( ( prevOrbit != currOrbit ) || ( prevBunch != currBunch ) ) {
  while ( saveStart != sampleList.begin() ) {
    cout << "delete oldest buffer" << endl;
    delete[] sampleList.front();
    sampleList.pop_front();
    pp = sampleList.front();
    prevOrbit = GetRawEventId::orbitCount( pp );
    prevBunch = GetRawEventId::bunchCount( pp );
  }
  cout << "save nextStart" << endl;
  saveStart = nextStart;
  nextStart = prev( sampleList.end() );
//  auto ib =       sampleList.begin();
//  auto il = prev( sampleList.end() );
//  cout << "fill data map" << endl;
//  int ns = GetSignal::fillDataMap( ib, il, dMap );
  cout << "fillDataMap" << endl;
  int ns = GetSignal::fillDataMap( sampleList.begin(), nextStart, dMap );
  if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
  Event* ev = new Event( aHead, eHead, this );
  cout << "fill event" << endl;
  fillEvent( ev, dMap );
  ++eHead.trigger_number;
  return ev;
}


void EventReadFromFile::fillEvent( Event* ev,
                                   const std::map<int,
				   std::vector<Event::data_type>>& dMap ) {
  for ( auto& e: dMap ) {
    int chan = e.first;
    vector<Event::data_type> data = e.second;
    if ( !data.empty() ) {
      unsigned int nd = data.size();
      cout << "fillEvent " << chan << ' ' << nd << endl;
      if ( nd & 1 ) {
        data.push_back( 0 );
	++nd;
      }
      nd >>= 1; // shift 1 bit to divide by 2
      Event::evtData eData;
      eData.chID_readPointer = 0;
      eData.numOfValidData = nd;
      memcpy( eData.data, data.data(), nd * Event::ioSize );
      ev->copy( chan, eData );
    }
  }
  return;
}


bool EventReadFromFile::contiguousClocks( unsigned int lo, unsigned int lb,
                                          unsigned int ro, unsigned int rb ) {
  GetSignal::bunchAdvance( lo, lb );
  return ( ( lo == ro ) && ( lb == rb ) );
}
*/
