#include "DataSource/IPbus/EventGetOnline.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"

#include "NtuTool/Common/interface/UserParametersManager.h"

//#include "tbstructs.h"
//#include "myproject.hpp"

#include "uhal/uhal.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <ctime>

using namespace std;

const int EventGetOnline::c_numOfImplementedChannels = 32;

// concrete factory to create an EventDump analyzer
class EventGetOnlineFactory: public SourceFactory::AbsFactory {
 public:
  // assign "online" as name for this analyzer and factory
  EventGetOnlineFactory(): SourceFactory::AbsFactory( "online" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
    cout << "EventGetOnline requested" << endl;
    return bindToThisFactory( EventGetOnline::basic_type::create<EventGetOnline>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<EventGetOnline>( keyword(), userPar ) );
//    return bindToThisFactory( EventGetOnline::create( keyword(), userPar );
//    return bindToThisFactory( new EventGetOnline( userPar ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventGetOnlineFactory will be available with name "online".
static EventGetOnlineFactory egf;

#define PRINTVAR(X) cout << #X << "=" << X << endl;

// read data from file "name"
EventGetOnline::EventGetOnline( const UserParametersManager* userPar ) {

//  cout << "EventGetOnline: " << userPar << endl;

  usTimeoutTot = userPar->getUserParameter<int>( "TIMEOUT_NOTRIG_MS" ) * 1000;
// = ConfigurationFile::Open()->GetIntParameterValue("TIMEOUT_NOTRIG_MS")*1000;
  lConnectionFilePath = userPar->getUserParameter( "CONNECTION_FILE_XML" );
// =  ConfigurationFile::Open()->GetParameterValue("CONNECTION_FILE_XML");
  lDeviceId = userPar->getUserParameter( "DEVICE_ID" );
// =  ConfigurationFile::Open()->GetParameterValue("DEVICE_ID");
  channelMask = 0x0;
// = 0x0;

  chDataPtrs = new event_t*[c_numOfImplementedChannels];
  for ( int i = 0; i < c_numOfImplementedChannels; i++ ) {
    string schan = "ENABLE_CHANNEL_";
    schan += to_string(i);
//    if ( ConfigurationFile::Open()->GetUIntParameterValue(schan.c_str()) == 1) {
    if ( userPar->getUserParameter<unsigned int>( schan ) == 1 ) {
      channelMask |= ( 0x1 << i );
	cout << "Channel " << i << ": enabled" << endl;
        chDataPtrs[i] = new event_t;
      }
      else {
	cout << "Channel " << i << ": disabled" << endl;
      }
    }
    cout << "Channel mask= 0x" << hex << channelMask << dec << endl;

  AutoTrgchannel = userPar->getUserParameter<unsigned int>( "AUTO_TRIGGER_CHANNEL" );
//ConfigurationFile::Open()->GetUIntParameterValue("AUTO_TRIGGER_CHANNEL");
  numOfData = userPar->getUserParameter<unsigned int>( "DATA_PER_CHANNEL" );
//ConfigurationFile::Open()->GetUIntParameterValue("DATA_PER_CHANNEL"); // to check less-equal than 8190
  trigDelay = userPar->getUserParameter<unsigned int>( "TRIGGER_DELAY" );
//ConfigurationFile::Open()->GetUIntParameterValue("TRIGGER_DELAY");
  nEvents = userPar->getUserParameter<unsigned int>( "EVENTS_PER_RUN" );
//ConfigurationFile::Open()->GetUIntParameterValue("EVENTS_PER_RUN");
  trigmode = userPar->getUserParameter<unsigned int>( "TRIGGER_MODE" );
//ConfigurationFile::Open()->GetUIntParameterValue("TRIGGER_MODE");
  printRunSummary = userPar->getUserParameter<bool>( "PRINT_RATE_RESULT" );
//std::strcmp(ConfigurationFile::Open()->GetParameterValue("PRINT_RATE_RESULT"),"true") == 0 ? true : false;

  wrongTrigger = !setTriggerConf( trigmode, triggerConf );

  threshold = userPar->getUserParameter<unsigned int>( "AUTO_TRIGGER_THRESHOLD" );
//ConfigurationFile::Open()->GetUIntParameterValue("AUTO_TRIGGER_THRESHOLD");
  isLess_b = userPar->getUserParameter<bool>( "AUTOTRG_BELOW_THRESHOLD" );
//std::strcmp(ConfigurationFile::Open()->GetParameterValue("AUTOTRG_BELOW_THRESHOLD"), "true") == 0 ? true : false;
  useSpill = userPar->getUserParameter<bool>( "TRIGGER_ONLY_IF_SPILL" );
//std::strcmp(ConfigurationFile::Open()->GetParameterValue("TRIGGER_ONLY_IF_SPILL"), "true") == 0 ? true : false;

  acqVersion = userPar->getUserParameter<unsigned int>( "SELECT_ACQ_VERSION" );
//ConfigurationFile::Open()->GetUIntParameterValue("SELECT_ACQ_VERSION");

  useSlimBuf = userPar->getUserParameter<bool>( "USE_SLIM_BUFSIZE" );
//std::strcmp(ConfigurationFile::Open()->GetParameterValue("USE_SLIM_BUFSIZE"), "true") == 0 ? true : false;
  slimBufDivider = userPar->getUserParameter<int>( "READ_SLIM_BUF_DIVIDER" );
//ConfigurationFile::Open()->GetIntParameterValue("READ_SLIM_BUF_DIVIDER");
    if((slimBufDivider > 32 || slimBufDivider < 1) && (slimBufDivider%2) != 0){
      std::cout << "Invalid Slim Buffer Divider " << slimBufDivider << ", set to default 4" << std::endl;
      slimBufDivider = 4;
    }

     eventSummary = userPar->getUserParameter<int>( "SHOW_SUMMARY_EVERY_N_EV" );
//ConfigurationFile::Open()->GetIntParameterValue("SHOW_SUMMARY_EVERY_N_EV");

    pulseMag = userPar->getUserParameter<unsigned int>( "CALIN_PULSE_MAGNITUDE" );
//ConfigurationFile::Open()->GetUIntParameterValue("CALIN_PULSE_MAGNITUDE");

    //parameters for summed triggers
    globSumChanMask = userPar->getUserParameter<uint32_t>( "GLOBAL_SUM_CHANNEL_MASK" );
//ConfigurationFile::Open()->GetExaParameterValue("GLOBAL_SUM_CHANNEL_MASK");
// global th to (0 to 4194303) 21 bits
    globSumTh = userPar->getUserParameter<unsigned int>("GLOBAL_SUM_TH");
//ConfigurationFile::Open()->GetUIntParameterValue("GLOBAL_SUM_TH");
//    cout << "SUM TRIGGER " << userPar->getUserParameter( "GLOBAL_SUM_CHANNEL_MASK" ) << ' ' << globSumChanMask << ' ' << globSumTh << endl;

    blockSumEna = userPar->getUserParameter<uint32_t>("BLOCKS_ENABLE");
// ConfigurationFile::Open()->GetExaParameterValue("BLOCKS_ENABLE");
// block th (0 to 524287 ) 18 bits
    blockSumTh = userPar->getUserParameter<unsigned int>("BLOCKS_TRIGGER_TH");
// ConfigurationFile::Open()->GetUIntParameterValue("BLOCKS_TRIGGER_TH");
    blockSumChanMask = userPar->getUserParameter<uint32_t>("BLOCKS_CHANNEL_MASK");
// ConfigurationFile::Open()->GetExaParameterValue("BLOCKS_CHANNEL_MASK");
//    cout << "GLOBAL SUM TRIGGER: "
//         << globSumChanMask << ' '
//         << globSumTh << ' '
//         << blockSumEna << ' '
//         << blockSumTh << ' '
//         << blockSumChanMask << endl;

    if(acqVersion > 2) {
      std::cout << "Error: unsupported Acquisition Version " << acqVersion << std::endl;
      wrongAcqVers = true;
    }
/*
  cout << "======= CONFIGURATION =======" << endl;
  PRINTVAR(usTimeoutTot);
  PRINTVAR(lConnectionFilePath);
  PRINTVAR(lDeviceId);
  PRINTVAR(channelMask);
  PRINTVAR(AutoTrgchannel);
  PRINTVAR(numOfData);
  PRINTVAR(trigDelay);
  PRINTVAR(nEvents);
  PRINTVAR(trigmode);
  PRINTVAR(printRunSummary);
  PRINTVAR(threshold);
  PRINTVAR(isLess_b);
  PRINTVAR(useSpill);

  PRINTVAR(acqVersion);

  PRINTVAR(useSlimBuf);
  PRINTVAR(slimBufDivider);

  PRINTVAR(eventSummary);
  PRINTVAR(pulseMag);
  cout << "======= ============= =======" << endl;
*/
   
  aHead.channel_mask    = channelMask ;
  aHead.sample_per_chan = numOfData ;

  uhal::setLogLevelTo( uhal::Error() );

  cout << "configureAcquisition" << endl;
  ptrHW = configureAcquisition( lConnectionFilePath,
                                lDeviceId,
                                c_numOfImplementedChannels,
                                triggerConf,
                                trigDelay,
                                numOfData,
                                AutoTrgchannel,
                                threshold,
                                isLess_b,
                                useSpill,
                                acqVersion,
                                globSumChanMask,
                                globSumTh,
                                blockSumEna,
                                blockSumTh,
                                blockSumChanMask );
  cout << "configured " << ptrHW << endl;

}


EventGetOnline::~EventGetOnline() {
//  delete file;
}


// get an event
const Event* EventGetOnline::get() {

  if ( wrongTrigger ||
       wrongAcqVers     ) return nullptr;
  if ( ptrHW == nullptr ) return nullptr;

//  uhal::HwInterface& lHW = *ptrHW;

  const int usSleep = 100;

  static auto startT = chrono::high_resolution_clock::now();

  static unsigned long long savedBytes = 0;
  static unsigned long long getBytes = 0;

  const Event* ev = ( nEvents-- ? readFromIPbus( //lHW,
                                  usSleep, savedBytes, getBytes ) : nullptr );

  static int nEp = 0;
  static auto prevT = startT;
  static int savedBprev = 0;
  static int getBprev = 0;
  if ( ev != nullptr ) {
//    int nEv = evCount() + 1;
//    if ( printRunSummary && !( ++nEv % eventSummary ) ) {
    static int nEv = 0;
    if ( printRunSummary && !( ++nEv % eventSummary ) ) {
      static bool headPrint = []{
        cout << "         ";
        printHeader();
        cout << "         *** average numbers written as negative to distinguish ***" << endl;
        return true; }();
      auto nowT = std::chrono::high_resolution_clock::now();
      auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>( nowT - startT );
      auto int_mi = std::chrono::duration_cast<std::chrono::milliseconds>( nowT -  prevT );
      if ( headPrint ) {// unnecessary condition, just to avoid warning for "unused variable"
      cout << "Instant: ";
      printSummary( nEv - nEp, int_mi.count(), savedBytes - savedBprev, getBytes - getBprev );
      cout << "Average: ";
      printSummary( -nEv, int_ms.count(), -savedBytes, -getBytes );
      }
      nEp = nEv;
      prevT = nowT;
      savedBprev = savedBytes;
      getBprev = getBytes;
    }
    return ev;
  }
  else {
    cfgToStartup( *ptrHW );
  }

  auto endT = chrono::high_resolution_clock::now();
  auto int_ms = chrono::duration_cast<std::chrono::milliseconds>( endT - startT );
  if ( printRunSummary ) {
    cout << "Total running time: " <<  int_ms.count()                      << " ms"   << endl;
    cout << "Events recorded: "    <<   evCount() << endl;
    cout << "Event rate: "         << ( evCount() * 1000. / int_ms.count() ) << " ev/s" << endl;
    cout << "Data saved: "         <<   savedBytes / ( 1024. * 1024. )     << " MB"   << endl;
    cout << "Data rate: "          << ( ( getBytes / ( 1024. * 1024. ) )
                                                * 1000. / int_ms.count() ) << " MB/s" << endl;
  }

  return nullptr;

}


// read an event
const Event* EventGetOnline::readFromIPbus( //uhal::HwInterface& lHW,
                                            const int usSleep,
                                            unsigned long long& savedBytes,
                                            unsigned long long& getBytes ) {

  static uint16_t readPtrs[c_numOfImplementedChannels];

// event pointer and identifier
  Event::evtHeader eHead;
  eHead.trigger_number = counter++;

  Event* ev = new Event( &aHead, eHead, this );

  uhal::HwInterface& lHW = *ptrHW;

  if ( timeoutTrigger( lHW, channelMask,
                       acqVersion, readPtrs, c_numOfImplementedChannels, triggerConf,
                       usSleep, usTimeoutTot ) ) return nullptr;

  struct full_slim_buffer_t slimbuf;
  if ( useSlimBuf ) {
    ReadSlimBuf( lHW, "MGPA.ringbuf", true,
                 acqVersion, slimBufDivider, slimbuf );
    getBytes += sizeof( slimbuf );
    //std::this_thread::sleep_for(std::chrono::microseconds(100));
  }
  for ( int i = 0; i < c_numOfImplementedChannels; i++ ) {
//    this_thread::sleep_for( chrono::microseconds( 1 ) );
    if ( ( ( channelMask >> i ) & 0x1 ) == 0x1 ) {
      bool isLast = false;
      if ( ( ( channelMask >> i ) & (~0x1) ) == 0x0 ) isLast = true;
//    @@@ data copy/refer
//      event_t& h = *chDataPtrs[i];
      event_t h;
      if ( !useSlimBuf ) {
        if ( acqVersion < 2 ) readDataBlock( lHW, i,
                              "MGPA.ringbuf", "MGPA.acq_stat",
                              numOfData,  isLast, acqVersion, h );
	else                  readDataBlock( lHW, i,
                              "MGPA.ringbuf", readPtrs[i],
                              numOfData, isLast, acqVersion, h );
        savedBytes += ( ( numOfData * 2 ) + 8 );
          getBytes +=   ( numOfData * 2 );
      }
      else {
        ReadSlimData( i, readPtrs[i], h, slimbuf );
        savedBytes += SINGLE_CHAN_SLIM_32BIT_SIZE * 4 + 8;
      }
//    @@@ data copy/refer
//      ev->refer( i, chDataPtrs[i] );
      ev->copy( i, h );
    }
  }

//  ++counter;
  return ev;

}

