#ifndef DataSource_IPbus_EventGetOnline_h
#define DataSource_IPbus_EventGetOnline_h

#include "Framework/EventSource.h"
#include "Framework/Event.h"

//#include "tbstructs.h"
#include "myproject.hpp"

//#include "uhal/uhal.hpp"

#include <iostream>
#include <fstream>
#include <string>

class EventGetOnline: public EventSource {

 public:

  static EventGetOnline* create( const std::string& key,
                                 const UserParametersManager* userPar ) {
    return nextInstance<EventGetOnline>( key, userPar );
  }

  // read data from file "name"
  EventGetOnline( const UserParametersManager* userPar );//( const std::string& name,
//                     int chann = 4,
//                     int nread = -1, int nskip = 0, bool verbose = false );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventGetOnline           ( const EventGetOnline& x ) = delete;
  EventGetOnline& operator=( const EventGetOnline& x ) = delete;

  ~EventGetOnline() override;

 private:

  static const int c_numOfImplementedChannels;

  Event::acqHeader aHead;
  event_t** chDataPtrs;

  // get an event
  const Event* get() override;

  int usTimeoutTot;
  std::string lConnectionFilePath;
  std::string lDeviceId;
  uint32_t channelMask;
  unsigned int AutoTrgchannel;
  unsigned int numOfData;
  unsigned int trigDelay;
  unsigned int nEvents;
  unsigned int trigmode;
  bool printRunSummary;
  unsigned int threshold;
  bool isLess_b;
  bool useSpill;

  unsigned int acqVersion;

  bool useSlimBuf;
  int slimBufDivider;

  int eventSummary;
  unsigned int pulseMag;

  //parameters for summed triggers
  uint32_t globSumChanMask;
// global th to (0 to 4194303) 21 bits
  unsigned int globSumTh;

  uint32_t blockSumEna;
// block th (0 to 524287 ) 18 bits
  unsigned int blockSumTh;
  uint32_t blockSumChanMask;

  enum trigtype_t triggerConf;
  bool wrongTrigger = false;
  bool wrongAcqVers = false;

  unsigned int counter = 0;

  uhal::HwInterface* ptrHW;

  // read and event
  const Event* readFromIPbus( //uhal::HwInterface& lHW,
                              const int usSleep,
                              unsigned long long& savedBytes,
                              unsigned long long& getBytes );

};

#endif // DataSource_IPbus_EventGetOnline_h
