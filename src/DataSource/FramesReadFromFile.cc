#include "DataSource/FramesReadFromFile.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>

using namespace std;

// concrete factory to create an EventDump analyzer
class FramesReadFromFileFactory: public SourceFactory::AbsFactory {
 public:
  // assign "input" as name for this analyzer and factory
  FramesReadFromFileFactory(): SourceFactory::AbsFactory( "infra" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( FramesReadFromFile::basic_type::
                                                  create<FramesReadFromFile>(
                              keyword(), userPar,
                              userPar->getUserParameter( keyword() ) ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an FramesReadFromFileFactory will be available with name "input".
static FramesReadFromFileFactory erf;

// read data from file "name"
FramesReadFromFile::FramesReadFromFile( const UserParametersManager* userPar,
                                        const string& name ):
 ReadFromFile( userPar, name ) {
//  userPar->getUserParameter( "nread"            , evtRead );
////  cout << "FramesReadFromFile  " << evtRead << endl;
//  userPar->getUserParameter( "nskip"            , evtSkip );
////  cout << "FramesReadFromFile  " << evtSkip << endl;
//  userPar->getUserParameter( "chaId"            , channel );
////  cout << "FramesReadFromFile  " << channel << endl;
//  userPar->getUserParameter( "eventNumberUpdate", evnUpdate );
////  cout << "FramesReadFromFile  " << evnUpdate << endl;
//  userPar->getUserParameter( "inputVerbose"     , iVerb );
//  mode = 0;
//  userPar->getUserParameter( "inputMode"     , mode );
//  cout << "open file " << name << ' ' << channel << ' ' << evtSkip << ' ' << evtRead << endl;
//  file = new ifstream( name.c_str(), ios::binary );
//  if ( !mode )
//  file->read( reinterpret_cast<char*>( &aHead ) , sizeof( Event::acqHeader ) );
  file->seekg( 16 );
}


FramesReadFromFile::~FramesReadFromFile() {
}


// read an event
const Event* FramesReadFromFile::readFile() {
//  cout << "FramesReadFromFile::readFile" << endl;
//  static GetRawEventId* gi = GetRawEventId::instance();
//  static GetSignal    * gs = GetSignal    ::instance();
  static Event::evtHeader eHead = []() {
    static Event::evtHeader eh;
    eh.trigger_number = 0;
    return eh;
  }();
  static Event::acqHeader* aHead = GetSignal::getAcqHeader();
  static list<char*> sampleList;
  static map<int, vector<Event::data_type>> dMap;
  if ( sampleList.empty() ) {
    char* c;
//    int n = 2;
//    while ( n-- ) {
      c = new char[GetSignal::dataSize];
      sampleList.push_back( c );
//      cout << "TRY FIRST READ" << endl;
//      if ( !file->read( c, GetSignal::dataSize ) ) return nullptr;
      if ( !fillDataSample( c ) ) return nullptr;
//      cout << "START: "
//           << GetSignal::bBytePos << ' '
//           << *reinterpret_cast<Event::data_type*>( c + GetSignal::bBytePos ) << ' '
//           << ( *reinterpret_cast<Event::data_type*>( c + 54 ) & 0xff ) << ' '
//           << ( *reinterpret_cast<Event::data_type*>( c + 55 ) & 0xff ) << ' '
//           << GetRawEventId::orbitCount( c ) << '/'
//           << GetRawEventId::bunchCount( c ) << endl;
//    }
  }
  static auto saveStart =       sampleList.begin();
  static auto nextStart =       sampleList.begin();
//  cout << "START DIST: " << distance( saveStart, nextStart ) << endl;
//  static auto nextEnd   = prev( sampleList.end() );
//  char* pp = sampleList.front();
//  unsigned int prevOrbit = GetRawEventId::orbitCount( pp );
//  unsigned int prevBunch = GetRawEventId::bunchCount( pp );
  char* cp = sampleList.back();
//  cout << "CURR SAMPLE: " << (void*)*saveStart << ' ' << (void*)cp << endl;
  unsigned int currOrbit;// = GetRawEventId::orbitCount( cp );
  unsigned int currBunch;// = GetRawEventId::bunchCount( cp );
  char* np;
  unsigned int nextOrbit = GetRawEventId::orbitCount( cp );//currOrbit;
  unsigned int nextBunch = GetRawEventId::bunchCount( cp );//currBunch;
  do {
    if ( nextStart == sampleList.begin() ) {
//      cout << "create new buffer" << endl;
      sampleList.push_back( np = new char[GetSignal::dataSize] );
    }
    else {
//      cout << "recycle oldest buffer " << distance( sampleList.begin(), nextStart ) << endl;
      sampleList.push_back( np = sampleList.front() );
      sampleList.pop_front();
    }
//    cout << "read next..." << endl;
//    if ( !file->read( np, GetSignal::dataSize ) ) {
    if ( !fillDataSample( np ) ) {
//      cout << "END OF FILE " << distance( sampleList.begin(), nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
//      cout << "END OF FILE " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
      int ns = GetSignal::fillDataMap( sampleList.begin(),
//                                       sampleList.end(), dMap );
                                 prev( sampleList.end() ), dMap );
      if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
      Event* ev = new Event( aHead, eHead, this );
      fillEvent( ev, dMap );
      sampleList.clear();
      nextStart = sampleList.end();
//      cout << "--------------> got last event" << endl;
      return ev;
    }
    currOrbit = nextOrbit;
    currBunch = nextBunch;
    nextOrbit = GetRawEventId::orbitCount( np );
    nextBunch = GetRawEventId::bunchCount( np );
//    cout << "CHECK FOR "
//         << currOrbit << '/' << currBunch << ' '
//         << nextOrbit << '/' << nextBunch << ' ' << sampleList.size() << endl;
  } while ( contiguousClocks( currOrbit, currBunch, nextOrbit, nextBunch ) );
//  cout << "CYCLE END " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << endl;
//  cout << "CYCLE END " << endl;
  while ( nextStart != sampleList.begin() ) {
//    cout << "delete oldest buffer" << endl;
    delete[] sampleList.front();
    sampleList.pop_front();
  }
  saveStart = nextStart;
  nextStart = prev( sampleList.end() );
//  cout << "fillDataMap " << distance( sampleList.begin(), nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
//  cout << "fillDataMap " << distance( sampleList.begin(), saveStart ) << ' ' << distance( saveStart, nextStart ) << ' ' << distance( nextStart, sampleList.end() ) << endl;
  int ns = GetSignal::fillDataMap( saveStart, nextStart, dMap );
  if ( aHead->sample_per_chan == 0 ) aHead->sample_per_chan = ns;
  Event* ev = new Event( aHead, eHead, this );
//  cout << "fill event" << endl;
  fillEvent( ev, dMap );
  ++eHead.trigger_number;
//  cout << "--------------> got event" << endl;
  return ev;
}


bool FramesReadFromFile::readClockPacket( ClockPacket* cp ) {
//  unsigned long long ttt;
//  file->read( reinterpret_cast<char*>( &ttt ), sizeof( ttt ) );
//  cout << ttt << endl;
  static int nClock = 0;
  static char* dum = new char[100];
//  static ClockPacket cp;
  static int nb = sizeof( *cp );
  if ( !nClock-- ) {
//    cout << "---> "  << "start sequence" << endl;
    if ( !file->read( dum, 8 ) ) return false;
    nClock = 15;
  }
//  cout << "---> clock " << nClock << ' ' << nb << endl; 
  return ( file->read( reinterpret_cast<char*>( cp ), nb ) ? true : false );
}


bool FramesReadFromFile::fillDataSample( char* dataSample ) {
  static ClockPacket cp;
  do {
    if ( !readClockPacket( &cp ) ) return false;
//    cout << "---> " << cp.oc << ' ' << cp.bx << ' ' << cp.spill << endl;
  } while ( !cp.spill );
  Event::data_type* channelData =
                    reinterpret_cast<Event::data_type*>( dataSample );
  int ic = 0;
  int jc = 23;
  while ( ic < 24 ) channelData[ic++] = cp.adcs[jc--];
  channelData[24] = cp.adc24;
  Event::data_type ol = GetRawEventId::pack( cp.oc, GetRawEventId::low );
  Event::data_type oh = GetRawEventId::pack( cp.oc, GetRawEventId::high );
  Event::data_type bc = GetRawEventId::pack( cp.bx );
  memcpy( dataSample + GetSignal::oBytePos,
          &ol, Event::edSize );
  memcpy( dataSample + GetSignal::oBytePos + Event::edSize,
          &oh, Event::edSize );
  memcpy( dataSample + GetSignal::bBytePos,
          &bc, Event::edSize );
//  cout << "---> filled" << endl;
  return true;
}


void FramesReadFromFile::fillEvent( Event* ev,
                                    const std::map<int,
                                    std::vector<Event::data_type>>& dMap ) {
  for ( auto& e: dMap ) {
    int chan = e.first;
    vector<Event::data_type> data = e.second;
    if ( !data.empty() ) {
      unsigned int nd = data.size();
//      cout << "fillEvent " << chan << ' ' << nd << endl;
      if ( nd & 1 ) {
//        cout << "add zero " << nd << endl;
        data.push_back( 0 );
	++nd;
      }
      nd >>= 1; // shift 1 bit to divide by 2
      Event::evtData eData;
      eData.chID_readPointer = 0;
      eData.numOfValidData = nd;
      memcpy( eData.data, data.data(), nd * Event::ioSize );
      ev->copy( chan, eData );
    }
//    else {
//      cout << "empty " << chan << endl;
//    }
  }
  return;
}


bool FramesReadFromFile::contiguousClocks( unsigned int lo, unsigned int lb,
                                          unsigned int ro, unsigned int rb ) {
  GetSignal::bunchAdvance( lo, lb );
  return ( ( lo == ro ) && ( lb == rb ) );
}

