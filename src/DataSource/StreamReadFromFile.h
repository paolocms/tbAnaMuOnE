#ifndef DataSource_StreamReadFromFile_h
#define DataSource_StreamReadFromFile_h

#include "DataSource/ReadFromFile.h"
#include "Framework/EventSource.h"
#include "Framework/Event.h"

#include <iostream>
#include <fstream>
#include <string>

class StreamReadFromFile: public ReadFromFile {

 public:

  // read data from file "name"
  StreamReadFromFile( const UserParametersManager* userPar,
                     const std::string& name );
  // deleted copy constructor and assignment to prevent unadvertent copy
  StreamReadFromFile           ( const StreamReadFromFile& x ) = delete;
  StreamReadFromFile& operator=( const StreamReadFromFile& x ) = delete;

  ~StreamReadFromFile() override;

 private:

  // read and event
  const Event* readFile();

//  static const Event* createEvent(
//                      const Event::acqHeader* ah, const Event::evtHeader& eh,
//                      const std::map<int, std::vector<Event::data_type>>& dMap );
  static void fillEvent( Event* ev,
                         const std::map<int,
                                        std::vector<Event::data_type>>& dMap );
  static bool contiguousClocks( unsigned int lo, unsigned int lb,
                                unsigned int ro, unsigned int rb );

};

#endif // DataSource_StreamReadFromFile_h

