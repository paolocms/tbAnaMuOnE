#include "DataSource/TrigLessSim.h"

#include "Framework/Event.h"
#include "Framework/SourceFactory.h"
#include "Framework/ConnectionMap.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <functional>

using namespace std;

// concrete factory to create an EventDump analyzer
class TrigLessSimFactory: public SourceFactory::AbsFactory {
 public:
  // assign "input" as name for this analyzer and factory
  TrigLessSimFactory(): SourceFactory::AbsFactory( "trigLessSim" ) {}
  // create an EventDump when builder is run
  EventSource* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( TrigLessSim::basic_type::create<TrigLessSim>( keyword(), userPar ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an TrigLessSimFactory will be available with name "input".
static TrigLessSimFactory tlsf;

//unsigned int TrigLessSim::dataNBit = 448          ; // data size in bits
//unsigned int TrigLessSim::dataSize = dataNBit / 8 ; // data size in bytes
//unsigned int TrigLessSim::orbitPos = 400; // bit position, left to right
//unsigned int TrigLessSim::bunchPos = 432;
//unsigned int TrigLessSim::orbitLen = 32;  // bits
//unsigned int TrigLessSim::bunchLen = 16;
//unsigned int TrigLessSim::oBitMask = bitMask( 32 - TrigLessSim::orbitLen,
//                                                   TrigLessSim::orbitLen );
//unsigned int TrigLessSim::bBitMask = bitMask( 32 - TrigLessSim::bunchLen,
//                                                   TrigLessSim::bunchLen );
//unsigned int TrigLessSim::bunchMax = 3563;

// read data from file "name"
TrigLessSim::TrigLessSim( const UserParametersManager* userPar ) {
  sFile = nullptr;
  nFile = nullptr;
  cFile = nullptr;
  string sName = userPar->getUserParameter( "signalData" );
  string nName = userPar->getUserParameter( "noiseData" );
  string cName = userPar->getUserParameter( "streamFile" );
  if ( !sName.empty() && !nName.empty() &&  cName.empty() ) {
    cout << "open signal file " << sName << endl;
    sFile = new ifstream( sName.c_str(), ios::binary );
    sFile->read( reinterpret_cast<char*>( &sHead ),
                 sizeof( Event::acqHeader ) );
    cout << "open noise  file " << nName << endl;
    nFile = new ifstream( nName.c_str(), ios::binary );
    nFile->read( reinterpret_cast<char*>( &nHead ),
                 sizeof( Event::acqHeader ) ) ;
  }
  else
  if (  sName.empty() &&  nName.empty() && !cName.empty() ) {
    cout << "open stream file " << cName << endl;
    cFile = new ifstream( cName.c_str(), ios::binary );
    cFile->read( reinterpret_cast<char*>( &sHead ),
                 sizeof( Event::acqHeader ) );
  }
  else {
    cout << "TrigLessSim wrong configuration:" << endl;
    cout << " enter signalData and noiseData" << endl;
    cout << "    or streamFile" << endl;
  }
  sSample = nullptr;
  nSample = nullptr;
  maxSelectedEvents = -1;
  userPar->GET_USER_PARAMETER( maxSelectedEvents );
  maxSignalEvents = -1;
  userPar->GET_USER_PARAMETER( maxSignalEvents );
  killSignal = false;
  userPar->GET_USER_PARAMETER( killSignal );
  randomClockCount = 0;
  userPar->GET_USER_PARAMETER( randomClockCount );
  orbitMDiff = 1000000;
  userPar->GET_USER_PARAMETER( orbitMDiff );
  unsigned int sampleBufferSize = 128;
  dataLCount = 40;
  dataRCount = sampleBufferSize - dataLCount;
  dataCenter = dataLCount;
  blindCount = sampleBufferSize;
  userPar->GET_USER_PARAMETER( sampleBufferSize );
  dataBuffer.resize( sampleBufferSize );
  streamDumpName = "";
  userPar->GET_USER_PARAMETER( streamDumpName );
  dataOldest = 0;
  fillStatus = true;
  unsigned int iData;
  for ( iData = 0; iData < sampleBufferSize; ++iData )
        nextDataSample( dataBuffer[iData] = new char[GetSignal::dataSize] );
  class DummyFilter: public Filter {
   public:
    DummyFilter() = default;
    ~DummyFilter() override = default;
    bool accept( const char* dataSample ) override { return true; }
  };
  class SumFilter: public Filter {
   public:
    SumFilter( const UserParametersManager* userPar ) {
      static ConnectionMap* connMap = ConnectionMap::instance();
      nChans = connMap->getNRows() * connMap->getNCols();
      channelSumThreshold = 0;
      userPar->GET_USER_PARAMETER( channelSumThreshold );
    }
    ~SumFilter() override = default;
    bool accept( const char* dataSample ) override {
      const Event::data_type* sig =
            reinterpret_cast<const Event::data_type*>( dataSample );
      const Event::data_type* end = sig + nChans;
      unsigned int sum = 0;
      while ( sig < end ) sum += *sig++;
      return sum > channelSumThreshold;
    }
   private:
    unsigned int nChans;
    unsigned int channelSumThreshold;
  };
  class SingleChannelSequenceFilter: public Filter {
   public:
    SingleChannelSequenceFilter( const UserParametersManager* userPar ) {
//      static ConnectionMap* connMap = ConnectionMap::instance();
//      nChans = connMap->getNRows() * connMap->getNCols();
      channelThreshold.resize( Event::maxChannelNumber, 0 );
      unsigned int i;
      for ( i = 0; i < Event::maxChannelNumber; ++i )
            userPar->getUserParameter( "channelThreshold_" + to_string( i ),
                                        channelThreshold[i] );
      triggerDepth = 3;
      userPar->GET_USER_PARAMETER( triggerDepth );
//      triggerSequence.resize( triggerDepth );
//      for ( auto& v: triggerSequence ) v.resize( Event::maxChannelNumber, 0 );
      triggerSequence.resize( Event::maxChannelNumber );
      for ( auto& v: triggerSequence ) v.resize( triggerDepth, 0 );
    }
    ~SingleChannelSequenceFilter() override = default;
    bool accept( const char* dataSample ) override {
      static const vector<int>& chMap = GetSignal::sampleChannelMap();
/*
      vector<bool> eT( Event::maxChannelNumber, true );
      unsigned int i;
      unsigned int j;
      for ( i = 1; i < triggerDepth; ++i )
        for ( j = 0; j < Event::maxChannelNumber; ++j )
              if ( !( triggerSequence[i-1][j] =
                      triggerSequence[i]  [j] ) ) eT[j] = false;
      const Event::data_type* channelData =
                              reinterpret_cast<const Event::data_type*>(
                              dataSample );
//      int& cT = triggerSequence.back() = 0;
      bool fT = false;
      for ( i = 0; i < Event::maxChannelNumber; ++i ) {
        int j = chMap[i];
        if ( j < 0 ) continue;
//        cout << "testing channel " << i << ' ' << j << ' ' << channelData[j] << ' ' <<  channelThreshold[i] << endl;
        int& cT = triggerSequence.back()[i] = 0;
        if ( channelData[j] > channelThreshold[i] ) cT = 1;
//        if ( channelData[j] > channelThreshold[i] ) {
//          cT = 1;
//          continue;
//        }
        if ( eT[i] && cT ) fT = true;
      }
//      triggerSequence.back()[i] = cT;
      return fT;
//      return ( eT && cT );
*/
      const Event::data_type* channelData =
                              reinterpret_cast<const Event::data_type*>(
                              dataSample );
      vector<bool> sT( Event::maxChannelNumber, true );
      bool eT = false;
      unsigned int i;
      unsigned int j;
      for ( i = 0; i < Event::maxChannelNumber; ++i ) {
        int k = chMap[i];
        if ( k < 0 ) continue;
        for ( j = 1; j < triggerDepth; ++j )
              if ( !( triggerSequence[i][j-1] =
                      triggerSequence[i][j]   ) ) sT[i] = false;
        int& cT = triggerSequence[i].back() = 0;
        if ( channelData[k] > channelThreshold[i] ) cT = 1;
        if ( sT[i] && cT ) eT = true;
      }
      return eT;
    }
   private:
    unsigned int nChans;
    unsigned int triggerDepth;
    vector<unsigned int> channelThreshold;
    vector<vector<int>> triggerSequence;
  };
  int filterMode = 0;
  userPar->GET_USER_PARAMETER( filterMode );
  switch ( filterMode ) {
  case 1:
    selector = new DummyFilter;
    break;
  case 2:
    selector = new SumFilter( userPar );
    break;
  case 3:
  default:
    selector = new SingleChannelSequenceFilter( userPar );
  }
  for ( iData = 0; iData < dataLCount; ++iData )
        selector->accept( dataBuffer[iData] );
}


TrigLessSim::~TrigLessSim() {
  delete sFile;
  delete nFile;
}


// get an event
const Event* TrigLessSim::get() {
  if ( ( maxSelectedEvents   >= 0 ) &&
       ( maxSelectedEvents-- <= 0 ) ) return nullptr;
  if ( ( sFile == nullptr ) &&
       ( nFile == nullptr ) &&
       ( cFile == nullptr ) ) return nullptr;
  while ( fillStatus &&
          !selector->accept( dataBuffer[dataCenter] ) ) fillDataBuffer();
  const Event* ev = eventBuild();
  unsigned int n = blindCount;
//  while ( n-- ) fillDataBuffer();
  while ( n-- > dataRCount ) fillDataBuffer();
  while ( n-- ) {
    fillDataBuffer();
    selector->accept( dataBuffer[dataCenter] );
  }
//  Event* ev = nullptr;
  return ev;
}


bool TrigLessSim::nextDataSample( char* dataSample ) {
  static int maxNoiseEvents = -1;
  if ( cFile != nullptr ) {
    return ( cFile->read( dataSample, GetSignal::dataSize ) ? true : false );
  }
  if ( nFile == nullptr ) return false;
  decltype( orbitCount ) orbitEvent = 0;
  decltype( bunchCount ) bunchEvent = 0;
  if ( killSignal ) {
    orbitEvent = orbitCount + 1;
  }
  else {
    if ( sSample == nullptr ) {
      sSample = new char[GetSignal::dataSize];
      if ( !fillDataSample( sFile, &sHead, sSample, randomClockCount,
                            false, maxSignalEvents ) )
           return false;
      sFailed = false;
//      orbitCount = ( randomClockCount ? 10000 : getOrbit( sSample ) );
      orbitCount = getOrbit( sSample );
      bunchCount = 0;
//      for ( int c: channelMap() ) cout << c << ' '; cout << endl;
//      for ( int c: dataPosMap() ) cout << c << ' '; cout << endl;
    }
    if ( sFailed ) return false;
      orbitEvent = getOrbit( sSample );
      bunchEvent = getBunch( sSample );
  }
  if ( nSample == nullptr ) {
    nSample = new char[GetSignal::dataSize];
    if ( !fillDataSample( nFile, &nHead, nSample, randomClockCount,
                          true, maxNoiseEvents ) )
         return false;
    nFailed = false;
  }
//  static unsigned int adcNByte = GetSignal::adcsNBit / 8;
//  static unsigned int oBytePos = GetSignal::orbitPos / 8;
//  static unsigned int bBytePos = GetSignal::bunchPos / 8;
  if ( ( orbitEvent == orbitCount ) && ( bunchEvent == bunchCount ) ) {
    if ( sFailed ) return false;
    memcpy( dataSample, sSample, GetSignal::adcsSize );
    memcpy( dataSample + GetSignal::oBytePos,
            &orbitCount, sizeof( orbitCount ) );
    memcpy( dataSample + GetSignal::bBytePos,
            &bunchCount, Event::edSize );
    sFailed = !fillDataSample( sFile, &sHead, sSample, randomClockCount,
                               false, maxSignalEvents );
  }
  else {
    if ( nFailed ) return false;
    memcpy( dataSample, nSample, GetSignal::adcsSize );
    memcpy( dataSample + GetSignal::oBytePos,
            &orbitCount, sizeof( orbitCount ) );
    memcpy( dataSample + GetSignal::bBytePos,
            &bunchCount, Event::edSize );
    nFailed = !fillDataSample( nFile, &nHead, nSample, randomClockCount,
                               true, maxNoiseEvents );
  }
  if ( GetSignal::bunchAdvance( orbitCount, bunchCount ) ) {
//  ++bunchCount;
//  if ( bunchCount > GetSignal::bunchMax ) {
//    ++orbitCount;
    unsigned int orbitMin = orbitEvent - orbitMDiff;
    if ( orbitCount < orbitMin ) orbitCount = orbitMin;
    cout << "orbitCount " << orbitCount << endl;
//    bunchCount = 0;
  }
  static ofstream* ofile = ( streamDumpName.empty() ? nullptr :
                             new ofstream( streamDumpName ) );
  static bool saveHeader = true;
  if ( ofile != nullptr ) {
    if ( saveHeader ) {
      saveHeader = false;
      ofile->write( reinterpret_cast<char*>( &sHead ),
                    sizeof( Event::acqHeader ) );
    }
    ofile->write( dataSample, GetSignal::dataSize );
  }
  return true;
}


void TrigLessSim::fillDataBuffer() {
  fillStatus = fillStatus && nextDataSample( dataBuffer[dataOldest++] );
  ++dataCenter;
  static unsigned int sampleBufferSize = dataLCount + dataRCount;
  if ( dataCenter >= sampleBufferSize ) dataCenter -= sampleBufferSize;
  if ( dataOldest >= sampleBufferSize ) dataOldest -= sampleBufferSize;
  return;
}


// read an event
const Event* TrigLessSim::eventBuild() {
  if ( !fillStatus ) return nullptr;
/*
  static GetRawEventId* eId = GetRawEventId::instance();
  static int olc = eId->getChannelOL();
  static int ohc = eId->getChannelOH();
  static int bcc = eId->getChannelBC();
*/
  static int count = 0;
  Event::evtHeader eHead;
  eHead.trigger_number = count++;
/*
  Event* ev = new Event( &sHead, eHead, this );
  static unsigned int sampleBufferSize = dataBuffer.size();
  unsigned int fSample = ( dataCenter + dataRCount ) % sampleBufferSize;
  static vector<Event::evtData>& dataPool = [this]()
                                            -> vector<Event::evtData>& {
    unsigned int n = Event::maxChannelNumber;
    static vector<Event::evtData> dp( n );
    while ( n-- ) {
      dp[n].chID_readPointer = 0;
      dp[n].numOfValidData = sampleBufferSize >> 1; // shift 1 bit
                                                    // to divide by 2
    }
    return dp;
  }();
  static vector<Event::data_type*>& dataPtrs = []()
                                               -> vector<Event::data_type*>& {
    unsigned int n = Event::maxChannelNumber;
    static vector<Event::data_type*> dp( n, nullptr );
    while ( n-- ) dp[n] =
                  reinterpret_cast<Event::data_type*>( dataPool[n].data );
    return dp;
  }();
  static const vector<int>& chMap = GetSignal::sampleChannelMap();
  unsigned int iSample;
  unsigned int iCha;
  for ( iSample = 0; iSample < sampleBufferSize; ++iSample ) {
    const char* dataSample = dataBuffer[fSample];
    const Event::data_type* channelData =
                            reinterpret_cast<const Event::data_type*>(
                            dataSample );
    for ( iCha = 0; iCha < Event::maxChannelNumber; ++iCha ) {
      int iPos = chMap[iCha];
      if ( iPos < 0 ) continue;
      dataPtrs[iCha][iSample] = channelData[iPos];
    }
    unsigned int oc = getOrbit( dataSample );
    dataPtrs[olc][iSample] =   oc         & 0x0000ffff;
    dataPtrs[ohc][iSample] = ( oc >> 16 ) & 0x0000ffff;
    dataPtrs[bcc][iSample] = getBunch( dataSample ) & 0x0000ffff;
    ++fSample;
    if ( fSample == sampleBufferSize ) fSample = 0;
  }
  for ( iCha = 0; iCha < Event::maxChannelNumber; ++iCha ) {
    if ( chMap[iCha] < 0 ) continue;
    ev->copy( iCha, dataPool[iCha] );
  }
  ev->copy( olc, dataPool[olc] );
  ev->copy( ohc, dataPool[ohc] );
  ev->copy( bcc, dataPool[bcc] );
  return ev;
*/
  return eventBuild( eHead, dataBuffer, ( dataCenter + dataRCount ) % dataBuffer.size(), this );
}


// read an event
const Event* TrigLessSim::readFile( ifstream* file,
                                    Event::acqHeader* aHead,
                                    bool restart ) {

//  cout << "readFile " << file << endl;
  // event pointer and identifier
  Event* ev;

  // try to read input file
  // on success create new event
  Event::evtHeader eHead;
  if ( !file->read( reinterpret_cast<char*>( &eHead ), sizeof( eHead ) ) ) {
    if ( restart ) {
      cout << "restart!" << endl;
      file->clear();
      file->seekg( sizeof( *aHead ) );
      file->read( reinterpret_cast<char*>( &eHead ), sizeof( eHead ) );
    }
    else {
      cout << "end of input" << endl;
      return nullptr;
    }
  }
  ev = new Event( aHead, eHead, nullptr );

  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; i++ ) {

    Event::evtData eData;
    if ( ( ( aHead->channel_mask >> i ) & 0x1 ) == 0x1 ) {
      file->read( reinterpret_cast<char*>( &eData.chID_readPointer ),
                                   sizeof(  eData.chID_readPointer ) );
      file->read( reinterpret_cast<char*>( &eData.numOfValidData ),
                                   sizeof(  eData.numOfValidData ) );
      file->read( reinterpret_cast<char*>(  eData.data ),
            eData.numOfValidData * sizeof( *eData.data ));
      ev->copy( i, eData );
    }
  }

  return ev;

}


bool TrigLessSim::fillDataSample( ifstream* file, Event::acqHeader* aHead,
                                  char* dataSample, unsigned int randomClock,
                                  bool restart, int& nmax ) {
//  static GetRawEventId* eId = GetRawEventId::instance();
//  static const vector<int>& chMap = channelMap();
  struct EventSample {
    const Event* evt = nullptr;
    map<int,vector<Event::data_type>> dMap;
    unsigned int sample = 0;
    int orbitId = 100000;
    Event::data_type bunchId = 0;
  };
  static map<ifstream*,EventSample> esMap;
  EventSample& es = esMap[file];
  const Event*& evt = es.evt;
  map<int,vector<Event::data_type>>& dMap = es.dMap;
  unsigned int& sample = es.sample;
  int& orbitId = es.orbitId;
  Event::data_type& bunchId = es.bunchId;
//  Event::data_type* channelData =
//                    reinterpret_cast<Event::data_type*>( dataSample );
  if ( evt == nullptr ) {
    if ( ( nmax   >= 0 ) &&
         ( nmax-- <= 0 ) ) return false;
    evt = readFile( file, aHead, restart );
    if ( evt == nullptr ) return false;
    GetSignal::fillSignalMap( *evt, dMap, true, true );
/*
    // @@@ DEBUG
    static const vector<int>& chMap = GetSignal::sampleChannelMap();
    list<const char*> streamChunk;
    int ns = dMap.begin()->second.size();
//    vector<char*> streamChunk( ns, nullptr );
//    for ( char*& c: streamChunk ) c = new vector<Event::data_type>(
    int is;
    for ( is = 0; is < ns; ++is ) {
      char* c = new char[GetSignal::dataSize];
      streamChunk.push_back( c );
//      char* c = streamChunk[is] = new char[GetSignal::dataSize];
      GetSignal::fillDataSample( dMap, is, c );
    }
    std::map<int, vector<Event::data_type>> cMap;
    GetSignal::fillDataMap( streamChunk.begin(), streamChunk.end(), cMap );
    for ( auto& e: dMap ) {
      int ic = e.first;
      if ( chMap[ic] < 0 ) continue;
//      cout << "CHECK CHANNEL: " << file << ' ' << ic << endl;
      const vector<Event::data_type>& v = e.second;
      const vector<Event::data_type>& w = cMap[ic];
//      cout << "CHECK CHANNEL: " << ic << ' ' << v.size() << ' ' << w.size() << endl;
      for ( is = 0; is < ns; ++is ) {
        if ( v[is] != w[is] ) cout << "WRONG FILL! " << ic << ',' << is << " : "
                                   << v[is] << ',' << w[is] << ',' <<endl;
      }
    }
    for ( const char* c: streamChunk ) delete[] c;
    // @@@
*/
    if ( randomClock ) {
      orbitId += ( 1 + ceil( randomClock * ( random() * 1.0 / RAND_MAX ) ) );
      bunchId = GetSignal::bunchMax * ( random() * 1.0 / RAND_MAX );
    }
    sample = 0;
  }
  if ( randomClock ) {
    GetSignal::bunchAdvance( orbitId, bunchId );
//    ++bunchId;
//    if ( bunchId > GetSignal::bunchMax ) {
//      ++orbitId;
//      bunchId = 0;
//    }
  }
  else {
    orbitId = 0;
    bunchId = 0;
  }
  if ( randomClock ) GetSignal::fillDataSample( dMap, sample, orbitId, bunchId,
                                                dataSample );
  else               GetSignal::fillDataSample( dMap, sample,
                                                dataSample );
/*
  for ( int i: evt->channels() ) {
    auto it = dMap.find( i );
    if ( it == dMap.end() ) continue;
    if ( it->second.empty() ) continue;
    if ( !randomClock ) {
      if ( i == eId->getChannelOL() ) orbitId |=   it->second[sample];
      if ( i == eId->getChannelOH() ) orbitId |= ( it->second[sample] << 16 );
      if ( i == eId->getChannelBC() ) bunchId  =   it->second[sample];
    }
    int pos = chMap[i];
    if ( pos < 0 ) continue;
    channelData[pos] = it->second[sample];
  }
  static unsigned int oBytePos = GetSignal::orbitPos / 8;
  static unsigned int bBytePos = GetSignal::bunchPos / 8;
  memcpy( dataSample + oBytePos, &orbitId, sizeof( orbitId ) );
  memcpy( dataSample + bBytePos, &bunchId, Event::edSize );
*/
  if ( ++sample >= aHead->sample_per_chan ) {
    delete evt;
    evt = nullptr;
  }
  return true;
}


Event* TrigLessSim::eventBuild( //Event::acqHeader  aHead,
                                const Event::evtHeader& eHead,
                                const vector<char*>& dataBuffer,
                                unsigned int start,
                                const EventSource* src ) {
  static const vector<int>& chMap = GetSignal::sampleChannelMap();
  static GetRawEventId* eId = GetRawEventId::instance();
  static int olc = eId->getChannelOL();
  static int ohc = eId->getChannelOH();
  static int bcc = eId->getChannelBC();
  unsigned int sampleBufferSize = dataBuffer.size();
  static Event::acqHeader* aHead = GetSignal::getAcqHeader();
/*
  static Event::acqHeader aHead = [sampleBufferSize]() {
    static Event::acqHeader h;
    h.channel_mask = 0;
//    unsigned int iCha;
//    for ( iCha = 0; iCha < Event::maxChannelNumber; ++iCha ) {
//      if ( chMap[iCha] >= 0 ) h.channel_mask |= ( 1 << iCha );
////      h.channel_mask <<= 1;
//    }
    unsigned int iCha = Event::maxChannelNumber;
    while ( iCha-- ) {
      h.channel_mask <<= 1;
      if ( chMap[iCha] >= 0 ) h.channel_mask |= 1;
    }
    h.channel_mask |= ( 1 << olc );
    h.channel_mask |= ( 1 << ohc );
    h.channel_mask |= ( 1 << bcc );
    h.sample_per_chan = sampleBufferSize;
    return h;
  }();
*/
  aHead->sample_per_chan = sampleBufferSize;
  vector<char*>::const_iterator ib = dataBuffer.begin();
  vector<char*>::const_iterator is = ib + start;
  vector<char*>::const_iterator ie = dataBuffer.end();
  vector<char*> sortBuffer;
  sortBuffer.reserve( sampleBufferSize );
  sortBuffer.insert( sortBuffer.end(), is, ie );
  sortBuffer.insert( sortBuffer.end(), ib, is );
  Event* ev = new Event( aHead, eHead, src );
  unsigned int n = Event::maxChannelNumber;
  vector<Event::evtData> dataPool( n );
  vector<Event::data_type*> dataPtrs( n );
  while ( n-- ) {
    dataPool[n].chID_readPointer = 0;
    dataPool[n].numOfValidData = sampleBufferSize >> 1; // shift 1 bit
                                                        // to divide by 2
    dataPtrs[n] = reinterpret_cast<Event::data_type*>( dataPool[n].data );
  }
  unsigned int iSample;
  unsigned int iCha;
  for ( iSample = 0; iSample < sampleBufferSize; ++iSample ) {
    const char* dataSample = sortBuffer[iSample];
    const Event::data_type* channelData =
                            reinterpret_cast<const Event::data_type*>(
                            dataSample );
    for ( iCha = 0; iCha < Event::maxChannelNumber; ++iCha ) {
      int iPos = chMap[iCha];
      if ( iPos < 0 ) continue;
      dataPtrs[iCha][iSample] = channelData[iPos];
    }
    unsigned int oc = getOrbit( dataSample );
//    dataPtrs[olc][iSample] =   oc         & 0x0000ffff;
//    dataPtrs[ohc][iSample] = ( oc >> 16 ) & 0x0000ffff;
//    dataPtrs[bcc][iSample] = getBunch( dataSample ) & 0x0000ffff;
    dataPtrs[olc][iSample] = GetRawEventId::pack( oc, GetRawEventId::low  );
    dataPtrs[ohc][iSample] = GetRawEventId::pack( oc, GetRawEventId::high );
    dataPtrs[bcc][iSample] = GetRawEventId::pack( getBunch( dataSample ) );
  }
  for ( iCha = 0; iCha < Event::maxChannelNumber; ++iCha )
      if ( chMap[iCha] >= 0 ) ev->copy( iCha, dataPool[iCha] );
  ev->copy( olc, dataPool[olc] );
  ev->copy( ohc, dataPool[ohc] );
  ev->copy( bcc, dataPool[bcc] );
  return ev;
}


//unsigned int TrigLessSim::getNumber( const char* ptr, int f, int n,
//                                     unsigned int mask ) {
//  static const int posDiff = ( sizeof( unsigned int ) << 3 ) - ( f + n );
//  return ( *reinterpret_cast<const unsigned int*>( ptr ) & mask ) >> posDiff;
//}


//const vector<int>& TrigLessSim::channelMap() {
//  static ConnectionMap* connMap = ConnectionMap::instance();
////  static GetRawEventId* eId = GetRawEventId::instance();
//  static int nCry = ( connMap->getNRows() * connMap->getNCols() );
//  static const vector<int>& chMap = []( int cFeb ) -> const vector<int>& {
//    static vector<int> cm( Event::maxChannelNumber, -1 );
//    int i;
//    for ( i = 0; i < cFeb; ++i ) cm[i] = i;
//    int j = 1 + Event::maxChannelNumber - cFeb;
//    while ( i < nCry ) cm[j++] = i++;
//    return cm;
//  }( 13 );
//  return chMap;
//}


//const vector<int>& TrigLessSim::dataPosMap() {
//  static int nCha = Event::maxChannelNumber;
//  static const vector<int>& dpMap = []() {
//    static const vector<int>& chMap = GetSignal::sampleChannelMap();
//    static vector<int> im( ( GetSignal::dataSize + Event::edSize - 1 ) /
//                                                   Event::edSize, -1 );
//    int i;
//    for ( i = 0; i < nCha; ++i ) {
//      int j = chMap[i];
//      if ( j < 0 ) continue;
//      im[j] = i;
//    }
//    return im;
//  }();
//  return dpMap;
//}

