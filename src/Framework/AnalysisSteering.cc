#include "Framework/AnalysisSteering.h"
#include "Framework/UserParametersCxxComment.h"

#include <cstdlib>

using namespace std;

AnalysisSteering::AnalysisSteering( const UserParametersManager* userPar ):
 uPar( userPar ) {
  obsMap()[this] = this;
}


AnalysisSteering::~AnalysisSteering() {
  auto& m = obsMap();
  auto i = m.find( this );
  if ( i != m.end() ) m.erase( i );
}


// function to be called at execution start / end
void AnalysisSteering::update( const EventSource::AnalysisStatus& status ) {
  switch ( status ) {
  case EventSource::begin:
    counter = 0;
    factor = 1;
    uPar->
    getUserParameter    ( "scaleFactor", factor );
    getAnalysisParameter( "scaleFactor", factor, IdentifiableObject::byReplica );
//    uPar->getUserParameter( "scaleFactor", factor );
//    uPar->getUserParameter( keyword() +
//                            "ScaleFactor", factor );
//    uPar->getUserParameter( keyword() +
//                            "ScaleFactor_" + to_string( replicaId() ), factor );
    time = 0;
    uPar->
    getUserParameter    ( "samplingTime", time );
    getAnalysisParameter( "samplingTime", time, IdentifiableObject::byReplica );
//    uPar->getUserParameter( "samplingTime", time );
//    uPar->getUserParameter( keyword() +
//                            "SamplingTime", time );
//    uPar->getUserParameter( keyword() +
//                            "SamplingTime_" + to_string( replicaId() ), time );
    beginJob();
    break;
  case EventSource::end:
    endJob();
    break;
  }
  return;
}

