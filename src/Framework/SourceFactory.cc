#include "Framework/SourceFactory.h"

#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <sstream>
#include <set>

using namespace std;

SourceFactory::SourceFactory() {
}


SourceFactory::~SourceFactory() {
}


// create event source
EventSource* SourceFactory::create( const UserParametersManager* userPar ) {
/*
*/
/*
  // loop over analysis object factories
  static map<string,AbsFactory*>* fm = factoryMap();
  set<string> ess;
  AbsFactory* af = nullptr;
  for ( const auto& element: *fm ) {
    // create analysis object if its name is listed in the command line
    if ( ( !userPar->   getUserParameter( element.first ).empty() ) ||
         (  userPar->hasNoValueParameter( element.first )         ) ) {
//      return element.second->create( userPar );
      ess.insert( element.first );
      af = element.second;
    }
  }
  switch ( ess.size() ) {
  case 1:
    return af->create( userPar );
  case 0:
    cout << "Invalid configuration, no data source given" << endl;
    return nullptr;
  default:
    cout << "Invalid configuration, multiple data source given:" << endl;
    for ( const string& s: ess ) cout << s << endl;
    return nullptr;
  }
*/
/*
*/
//  static int count = 0;
//  cout << "recursion count " << count++ << endl;
  return ObjectFactory<EventSource>::createSingle( userPar, "data source" );
/*
  std::vector<EventSource*> ess = ObjectFactory<EventSource>::create( userPar );
//  cout << "created " << endl;
  switch ( ess.size() ) {
  case 1:
    return ess.front();
  case 0:
    cout << "Invalid configuration, no data source given" << endl;
    return nullptr;
  default:
    cout << "Invalid configuration, multiple data source given:" << endl;
    for ( EventSource* s: ess ) cout << ObjectFactory<EventSource>::
                                        getFactory( s )->keyword() << endl;
  }
*/
/*
*/
//  return nullptr;
}

