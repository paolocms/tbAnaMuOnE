#include "Framework/EventSource.h"
#include "Framework/Event.h"
#include "Framework/AnalysisSteering.h"
#include "Framework/AnalysisFactory.h"
#include "Utilities/Dispatcher.h"
#include "Utilities/PolymorphicSingleton.h"
#include "Utilities/TimeStamp.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <thread>
#include <iostream>
#include <map>
#include <cmath>

#include <chrono>

using namespace std;

template <>
void Dispatcher<Event>::notify( const Event& x ) {

  last = &x;

  lazyNotify( x );

  static auto aol = activeObserverList();
  long long evTime = TimeStamp::sinceEpoch( TimeStamp::milliseconds );
  static auto m =
              EventSource::obsTimeMap<ActiveObserver<Event>*,long long>( *aol );
  for ( auto o: *aol ) {
    auto oMap = AnalysisSteering::obsMap();
    auto iter = oMap.find( o );
    if ( iter != oMap.end() ) { 
      AnalysisSteering* a = iter->second;
      if (   a->counter++ % a->factor   ) continue;
      if (   a->time &&
           ( evTime < m[o] + a->time  ) ) continue;
      else m[o] = evTime;
    }
    o->update( x );
  }

  return;

}


EventSource::EventSource():
 evtPrintFrequency( 0 ),
 interactiveTrigger( false ) {
  PolymorphicSingleton<UserParametersManager>::instance()->
    GET_USER_PARAMETER( interactiveTrigger );
  PolymorphicSingleton<UserParametersManager>::instance()->
    GET_USER_PARAMETER( evtPrintFrequency );
  // set status to "run" to allow analysis threads to stay on wait
  runStatus = status::run;
}


EventSource::~EventSource() {
  cout << *nEvtProd << " events read" << endl;
}


// get events
void EventSource::run() {
  PolymorphicSingleton<UserParametersManager>::instance()->
                       GET_USER_PARAMETER( eventBufferSize );
  if ( eventBufferSize ) {
    nEvtProd = new int( 0 );
    nEvtCons = new int( 0 );
    thread s( []( EventSource* s ){ s->runSend(); }, this );
    thread f( []( EventSource* s, unsigned int n ){ s->runFill( n ); },
              this, eventBufferSize );
    s.join();
    f.join();
    sumBufferSize /= numBufferSize;
    sqrBufferSize /= numBufferSize;
    cout << "event buffer size for " << numBufferSize
         <<        " events : mean=" << sumBufferSize
         <<                " , rms=" << sqrt( sqrBufferSize -
                                            ( sumBufferSize * sumBufferSize ) )
         << " ," << endl
         << "                  max=" << maxBufferSize
         <<   " ; full occurrences=" << numBufferFull
         <<  " ; empty occurrences=" << numBufferWait << endl;
  }
  else {
    nEvtProd = nEvtCons = new int( 0 );
    const Event* ev;
    while ( ( ev = get() ) != nullptr ) {
      sendToDispatcher( ev );
      delete ev;
      if ( interactiveTrigger ) cin.ignore();
    }
  }
  return;
}


unsigned int EventSource::bufferSize() const {
  mBuf.lock();
  unsigned int l = eBuf.size();
  mBuf.unlock();
  return l;
}


const function<bool()>& EventSource::getDropFunction(
                                     const std::string& name,
                                     unsigned int maxBufferSize,
                                     unsigned int minBufferSize,
                                     ostream* outStat ) const {
  static function<bool()> dumFunc( []()->bool{ return false; } );
  if ( !eventBufferSize ) return dumFunc;
  typedef function<bool()>* funcPtr;
  static map<string,funcPtr> m;
  static set<unique_ptr<EventDrop>> s;
  auto mchk = m.insert( map<string,funcPtr>::value_type( name, nullptr ) );
  auto iter = mchk.first;
  if ( !mchk.second ) return *iter->second;
  if ( !minBufferSize ) minBufferSize = eventBufferSize / 3;
  if ( !maxBufferSize ) maxBufferSize = 2 * minBufferSize;
  EventDrop* edp = new EventDrop( name, this,
                                  maxBufferSize, minBufferSize, outStat );
  s.insert( set<unique_ptr<EventDrop>>::value_type( edp ) );
  funcPtr bfp = new function<bool()>( [edp]()->bool{ return ( *edp )(); } );
  iter->second = bfp;
  return *bfp;
}


int EventSource::evCount( Process p ) const {
  return *( p == producer ? nEvtProd : nEvtCons );
}


EventSource::EventDrop::EventDrop( const std::string& name,
                                   const EventSource* evSource,
                                   unsigned int maxBufferSize,
                                   unsigned int minBufferSize,
                                   ostream* outStat ):
 n( name ),
 es( evSource ),
 os( outStat ),
 maxBSize( maxBufferSize ),
 minBSize( minBufferSize ) {
  if ( minBSize > maxBSize ) minBSize = maxBSize;
}


EventSource::EventDrop::~EventDrop() {
  if ( os != nullptr ) *os << " skip/keep/switch statistic for " << n
                           << " : skip "   << nSkip
                           << " , keep "   << nKeep
                           << " , switch " << nKSsw << endl; 
}


bool EventSource::EventDrop::operator()() {
  static bool keep = true;
  unsigned int bSize = es->bufferSize();
  if ( keep ) {
    if ( bSize < maxBSize ) {
      ++nKeep;
      return false;
    }
    keep = false;
    ++nKSsw;
    ++nSkip;
  }
  else {
    if ( bSize > minBSize ) {
      ++nSkip;
      return true;
    }
    keep = true;
//    ++nKSsw;
    ++nKeep;
  }
  return !keep;
/*
  if ( keep ) {
    if ( bSize < maxBSize ) return false;
    keep = false;
    ++nSkip;
  }
  else {
    if ( bSize > minBSize ) return true;
    keep = true;
    ++nKeep;
  }
  return !keep;
*/
/*
  if ( keep ) {
    if ( bSize > maxBSize ) {
      keep = false;
      ++nSkip;
      return true;
    }
  }
  else {
    if ( bSize > minBSize ) return true;
    keep = true;
    ++nKeep;
  }
  return false;
*/
}


// fill event buffer
void EventSource::runFill( unsigned int t ) {
//  if ( interactiveTrigger ) t = 1;
  const Event* ev;
  unsigned int l;
  // set buffer flag to true to allow continuous loop until buffer is full
  evget = true;
  // go on getting events until end-of-source is reached
  while ( runStatus == status::run ) {
    // get current event buffer size
    mBuf.lock();
    l = eBuf.size();
    mBuf.unlock();
    if ( l > maxBufferSize ) maxBufferSize = l;
    numBufferSize += 1;
    sumBufferSize += l;
    sqrBufferSize += l * l;
    // if the buffer is not full get one event
    if ( l < t ) {
      if ( ( ev = get() ) == nullptr ) {
        // at end of input set status to "end" to allow analysis 
        // threads stop looping
        runStatus = status::end;
        // awake threads waiting to avoid them being stuck
        // a lock_guard is an object locking the given mutex and
        // unlocking it when destroyed
        std::lock_guard<std::mutex> lg( cms );
        ready = true;
        cvs.notify_all();
        break;
      }
      // store event into buffer
      mBuf.lock();
      // increase the event count
//      ++nEvt;
      ++*nEvtProd;
      eBuf.push_back( ev );
      mBuf.unlock();
      // set event flag to true
      std::lock_guard<std::mutex> lg( cms );
      ready = true;
      // notify analysis threads
      cvs.notify_all();
      continue;
    }
    else {
    //  cout << "******* buffer full *******" << endl;
      // if the buffer is full set the flag to false, to avoid
      // wasting cpu time going on looping and checking continuously
      // for buffer size and instead wait until some event is actually
      // picked out from the buffer by some analysis thread
      std::lock_guard<std::mutex> lg( cmg );
      evget = false;
      ++numBufferFull;
    }
    // notify analysis threads waiting for events, to avoid deadlocks
    cms.lock();
    ready = true;
    cms.unlock();
    cvs.notify_all();
    // until "evget" is false, that means the event buffer is currently
    // full, wait for notification:
    // the "wait" function does the following:
    // - releases the lock
    // - blocks the execution until notified
    // - when notified reacquires the lock and exits
    std::unique_lock<std::mutex> lk( cmg );
    while ( !evget ) cvg.wait( lk );
    // alternate expression:
    // "evget" is re-evaluated after receiving each notification,
    // and if found to be "false" again a new notification is waited;
    // the value is to be got by a function or a functor, i.e. an object
    // with a function-call operator (), so a lambda-function is used here:
    // the current class instance is captured with [this] and the
    // "evget" value is returned
//    if ( !evget ) cvg.wait( lk, [this]{ return evget; } );
  }
  return;
}


// send events in the buffer to observers
void EventSource::runSend() {
//unsigned int EventSource::runSend() {
  // set event flag to false to avoid continuous loop when looking events
  // in the buffer (see "next" function)
  ready = false;
  // set event counter
//  unsigned int n = 0;
  const Event* ev;
  // pick up events from the buffer: "next" function takes care of
  // querying the buffer and wait, if necessary, for new events
  // to be filled
  while ( ( ev = next() ) != nullptr ) {
    // label this event with the sequential number of the thread
//    mMap.lock();
//    tMap[ev] = t;
//    mMap.unlock();
//    // increase the event count
//    ++nEvt;
    // dispatch event to observer
//: all observers get the event, so a
//    // patch must be applied to avoid processing of the same event by
//    // more than one thread, look at AnalysisObjects/ObjectBase.h for details
    sendToDispatcher( ev );
    // the "notify" functions calls in its turn the functions of the 
    // observers, so when it returns the processing of the event is 
    // completed
    // remove event from the label map after processing
//    mMap.lock();
//    tMap.erase( ev );
//    mMap.unlock();
    // actually delete the event
    delete ev;
    if ( interactiveTrigger ) cin.ignore();
    // give back control to the process manager (not necessary)
//    this_thread::yield();
  }
  // return the number of processed events
  return;
//  return nEvt;
}


// get one event from buffer
const Event* EventSource::next() {
  const Event* ev = nullptr;
  // go on iterating until an event is found in the buffer
  while ( true ) {
    mBuf.lock();
    // check if the buffer does contain any event
    unsigned int l = eBuf.size();
    if ( l ) {
      // if the buffer is not empty pick up the first event
      // and remove it from the buffer
      auto it = eBuf.begin();
      ev = *it;
      eBuf.erase( it );
      mBuf.unlock();
      // set buffer flag to true
      std::lock_guard<std::mutex> lg( cmg );
      evget = true;
      // notify source thread
      cvg.notify_all();
      // return the event
      return ev;
    }
    else {
      // if the buffer is empty set the flag to false, to avoid
      // wasting cpu time going on looping and checking continuously
      // for buffer size and instead wait until some event is actually
      // stored into the buffer, unless the end of input has been reached
      std::lock_guard<std::mutex> lg( cms );
      ready = ( runStatus == status::end );
      ++numBufferWait;
    }
    mBuf.unlock();
    // exit the loop if the end of input has been reached
    if ( runStatus == status::end ) break;
    // notify source thread waiting for buffer space, to avoid deadlocks
    cmg.lock();
    evget = true;
    cmg.unlock();
    cvg.notify_all();
    // until "ready" is false, that means the event buffer is currently
    // empty but new events are to come, wait for notification:
    // the "wait" function does the following:
    // - releases the lock
    // - blocks the execution until notified
    // - when notified reacquires the lock and exits
    std::unique_lock<std::mutex> lk( cms );
    while ( !ready ) cvs.wait( lk );
    // alternate expression:
    // "ready" is re-evaluated after receiving each notification,
    // and if found to be "false" again a new notification is waited;
    // the value is to be got by a function or a functor, i.e. an object
    // with a function-call operator (), so a lambda-function is used here:
    // the current class instance is captured with [this] and the
    // "ready" value is returned
//    if ( !ready ) cvs.wait( lk, [this]{ return ready; } );
  }
  return nullptr;
}

void EventSource::sendToDispatcher( const Event* ev ) {
  Dispatcher<Event>::notify( *ev );
  int& nEvt = *nEvtCons;
  ++nEvt;
  if ( evtPrintFrequency && !( nEvt % evtPrintFrequency ) )
       cout << nEvt << " events" << endl;
  return;
}

