#ifndef Framework_EqualSignParser_h
#define Framework_EqualSignParser_h

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "NtuTool/Common/interface/CommandLineParser.h"
#include <string>
#include <iostream>
#include <fstream>

class UserParametersEqualSign: public virtual UserParametersManager {
 public:
  void setConfiguration( const std::string& file ) override {
    std::ifstream cfg( file.c_str() );
    int lenMax = 1000;
    char* line = new char[lenMax];
    const char* lptr;
    std::string::size_type length;
    while ( cfg.getline( line, lenMax ) ) {
      lptr = line;
      while ( *lptr == ' ' ) ++lptr;
      if    ( *lptr == '#' ) continue;
      std::string key( lptr );
      std::string val;
      length = key.find( "=" );
      if ( length < 0 ) {
        length = key.find( " " );
        if ( length >= 0 ) key = key.substr( 0, length );
        addNoValueParameter( key );
        continue;
      }
      else {
        key = key.substr( 0, length );
        val = key.c_str() + length + 1;
      }
      lptr = val.c_str();
      while ( *lptr == ' ' ) ++lptr;
      val = lptr;
      length = key.find( " " );
      if ( length >= 0 ) key = key.substr( 0, length );
      length = val.find( " " );
      if ( length >= 0 ) val = val.substr( 0, length );
      setUserParameter( key, val );
    }
    delete[] line;
    return;
  }
};

class EqualSignParser: public CommandLineParser {

 public:

  void parse( UserParametersManager* uPar, int argc, char* argv[] ) override {
    int iarg;
    for ( iarg = 1; iarg < argc; ++iarg ) {
      const std::string args = argv[iarg];
      if ( args == "-c" ) {
        uPar->setConfiguration( argv[++iarg] );
        std::cout << "configured from " << argv[iarg] << std::endl;
        continue;
      }
      std::string::size_type argp = args.find( "=" );
      if ( argp != std::string::npos ) uPar->setUserParameter(
                                             args.substr( 0, argp ),
                                             args.substr( argp + 1,
                                             args.length() - argp ) );
      else                             uPar->addNoValueParameter( args );
    }
    return;
  }

};

#endif // Framework_EqualSignParser_h
