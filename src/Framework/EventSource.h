#ifndef Framework_EventSource_h
#define Framework_EventSource_h

#include <string>
#include <map>
#include <set>
#include <list>
#include <functional>
#include <mutex>
#include <condition_variable>

#include "Framework/SourceFactory.h"
#include "Utilities/IdentifiableObject.h"

class Event;

class EventSource: public IdentifiableObject {

  friend class AnalysisSteering;

 public:

  enum AnalysisStatus { begin, end };
  enum Process { producer, consumer };

  EventSource();
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventSource           ( const EventSource& x ) = delete;
  EventSource& operator=( const EventSource& x ) = delete;

  virtual ~EventSource();

  // get events
  virtual void run();

  unsigned int bufferSize() const;

  const std::function<bool()>& getDropFunction(
                               const std::string& name,
                               unsigned int maxBufferSize = 0,
                               unsigned int minBufferSize = 0,
                               std::ostream* outStat = nullptr ) const ;

  template <class O, class T>
  static std::map<O,T>& obsTimeMap( const std::set<O>& v ) {
    static std::map<O,T> m;
    for ( const O& o: v ) m[o] = 0;
    return m;
  }

 protected:

  int evCount( Process p = producer ) const;

 private:

  unsigned int eventBufferSize = 2000;

  class EventDrop {
   public:
    EventDrop( const std::string& name, const EventSource* evSource,
               unsigned int maxBufferSize, unsigned int minBufferSize,
               std::ostream* outStat = nullptr );
    ~EventDrop();
    bool operator()();
   private:
    std::string n;
    const EventSource* es;
    std::ostream* os;
    unsigned int maxBSize;
    unsigned int minBSize;
    int nKeep = 0;
    int nSkip = 0;
    int nKSsw = 0;
  };

  // get an event
  virtual const Event* get() = 0;

  // flag to signal end of input, to be used by threads consuming 
  // events to avoid never ending wait at end of input
  enum class status { run, end };
  status runStatus;

  // fill event buffer and send them to observers
  void runFill( unsigned int t );
  void runSend();
//  virtual void         runFill( unsigned int t );
//  virtual unsigned int runSend();

  // event buffer and corresponding mutex to avoid concurrent access
  std::list<const Event*> eBuf;
  mutable
  std::mutex              mBuf;

  // get one event from buffer
  virtual const Event* next();
  void sendToDispatcher( const Event* ev );

  // event-thread map and corresponding mutex to avoid concurrent access
  // declared static to be usable by "thrId" function (see above)
//  static std::map<const Event*,unsigned int> tMap;
//  static std::mutex                          mMap;

  // mutex and condition variable to make analysis threads wait
  // for events when buffer is empty
  std::mutex cms;
  std::condition_variable cvs;
  bool ready; // true when there are data in the buffer, or at end-of-source

  // mutex and condition variable to make event source thread wait
  // when buffer if full
  std::mutex cmg;
  std::condition_variable cvg;
  bool evget; // true when the buffer is not full
  unsigned int maxBufferSize = 0;
  unsigned int numBufferFull = 0;
  unsigned int numBufferWait = 0;
  double numBufferSize = 0;
  double sumBufferSize = 0;
  double sqrBufferSize = 0;

  // event counters
  int* nEvtProd;
  int* nEvtCons;

  // periodic event print frequency
  int evtPrintFrequency;

  // interactive trigger
  bool interactiveTrigger;

};

#endif // Framework_EventSource_h

