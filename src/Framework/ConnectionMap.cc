#include "Framework/ConnectionMap.h"
#include "Framework/Event.h"

#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <cstring>

using namespace std;


ConnectionMap::ConnectionMap(): missingMap( true ) {
}


ConnectionMap::~ConnectionMap() {
}


// get size
unsigned int ConnectionMap::getNRows() const {
  if ( missingMap ) fillMap();
  return nRows;
}


unsigned int ConnectionMap::getNCols() const {
  if ( missingMap ) fillMap();
  return nCols;
}


// get channel
int ConnectionMap::getChannel( unsigned int row, unsigned int col ) const {
  if ( missingMap ) fillMap();
  if ( ( --row < nRows ) &&
       ( --col < nCols ) ) return channelMap[( row * nCols ) + col];
  return -1;
}


// get row and col
int ConnectionMap::getRow( unsigned int channel ) const {
  if ( missingMap ) fillMap();
  if ( channel < Event::maxChannelNumber ) return rowMap[channel];
  return -1;
}


int ConnectionMap::getCol( unsigned int channel ) const {
  if ( missingMap ) fillMap();
  if ( channel < Event::maxChannelNumber ) return colMap[channel];
  return -1;
}


void ConnectionMap::fillMap() const {

  static const UserParametersManager*
               uPar = PolymorphicSingleton<UserParametersManager>::instance();
  string mapName = "febChannelForCrystal";
  nRows = 5;
  nCols = 5;
  uPar->getUserParameter( "nDetectorRows", nRows );
  uPar->getUserParameter( "nDetectorCols", nCols );
  channelMap.resize( nRows * nCols );
  rowMap.resize( Event::maxChannelNumber, -1 );
  colMap.resize( Event::maxChannelNumber, -1 );
  bool debug = false;
  uPar->getUserParameter( "debugConnectionMap", debug );

  unsigned int ir;
  unsigned int ic;
  unsigned int ih = 0;
  for ( ir = 0; ir < nRows; ++ir ) {
    for ( ic = 0; ic < nCols; ++ic ) {
      int j = ( ir * nCols ) + ic;
      int k = j;
//      int k = -1;
      unsigned int jr = ir + 1;
      unsigned int jc = ic + 1;
      uPar->getUserParameter( mapName + to_string( jr ) + to_string( jc ), k );
//      if ( k < 0 ) continue;
      rowMap[k] = jr;
      colMap[k] = jc;
      channelMap[ih++] = k;
      if ( debug ) cout << jr << ' ' << jc << ' ' << ih << ' ' << k << endl;
    }
  }
  if ( debug ) {
    for ( ir = 0; ir < nRows; ++ir ) {
      for ( ic = 0; ic < nCols; ++ic ) {
        cout << ir + 1 << ' ' << ic + 1 << " ---> "
             << channelMap[( ir * nCols ) + ic] << endl;
      }
    }
  }
  missingMap = false;

}


