#ifndef Framework_SourceFactory_h
#define Framework_SourceFactory_h

#include <string>
#include <map>

#include "Framework/UserParametersCxxComment.h"
#include "Utilities/ObjectFactory.h"
//#include "Framework/ObjectFactory.h"

class EventSource;
class UserParametersManager;

class SourceFactory: public ObjectFactory<EventSource> {

 public:

  SourceFactory();
  // deleted copy constructor and assignment to prevent unadvertent copy
  SourceFactory           ( const SourceFactory& x ) = delete;
  SourceFactory& operator=( const SourceFactory& x ) = delete;

  virtual ~SourceFactory();

  // create event source
  static EventSource* create( const UserParametersManager* info );

//  // analysis object abstract factory
//  class AbsFactory {
//   public:
//    // Analyzers are registered with a name so that they are actually 
//    // created only if, at runtime, their name is listed in the command line
//    AbsFactory( const std::string& name ) { registerFactory( name, this ); }
//    virtual ~AbsFactory() {}
//    virtual EventSource* create( const UserParametersManager* userPar ) = 0;
//  };
//
// private:
//
//  // function to add analyzer concrete factories
//  static void registerFactory( const std::string& name, AbsFactory* f );
//  // map to associate analyzer names with corresponding factories
//  static std::map<std::string,AbsFactory*>* factoryMap();

};

#endif // Framework_SourceFactory_h

