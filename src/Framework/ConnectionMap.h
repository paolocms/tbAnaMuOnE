#ifndef Framework_ConnectionMap_h
#define Framework_ConnectionMap_h

#include "Utilities/Singleton.h"

#include <vector>

class ConnectionMap: public Singleton<ConnectionMap> {

  friend class Singleton<ConnectionMap>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  ConnectionMap           ( const ConnectionMap& x ) = delete;
  ConnectionMap& operator=( const ConnectionMap& x ) = delete;

  ~ConnectionMap();

  // get size
  unsigned int getNRows() const;
  unsigned int getNCols() const;

  // get channel
  int getChannel( unsigned int row, unsigned int col ) const;

  // get row and col
  int getRow( unsigned int channel ) const;
  int getCol( unsigned int channel ) const;

 private:

  // private constructor being a singleton
  ConnectionMap();

  mutable unsigned int nRows;
  mutable unsigned int nCols;
  mutable bool missingMap;
  mutable std::vector<int> channelMap;
  mutable std::vector<int>     rowMap;
  mutable std::vector<int>     colMap;
  void fillMap() const;

};

#endif // Framework_ConnectionMap_h

