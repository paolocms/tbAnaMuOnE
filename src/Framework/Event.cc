#include "Framework/Event.h"

#include <iostream>
#include <cstring>

using namespace std;

const unsigned int Event::nWords         = NWORDS; //8192;
const unsigned int Event::ioSize         = sizeof( *Event::evtData::data ); //4;
const unsigned int Event::edSize         = sizeof(  Event::data_type );     //2;
const unsigned int Event::dataSizeFactor = Event::ioSize / Event::edSize;   //2
                                           
const unsigned int Event::maxChannelNumber =
                   8 * sizeof( Event::acqHeader::channel_mask );

int Event::evCounter = 0;

Event::Event( const acqHeader* ah, const evtHeader& eh,
              const EventSource* src ):
 // initializations
 evNumber( evCounter++ ),
 aHead( ah ),
 evSource( src ),
 oldChans( true ) {
  eHead.trigger_number = eh.trigger_number;
}


Event::~Event() {
  for ( auto& e: dMap ) {
    ChannelData* cd = e.second;
    if ( cd->copied ) delete cd->dataPtr;
    delete cd;
  }
}


void Event::copy( int channel, const Event::evtData& ed ) {
  auto it = dMap.find( channel );
  if ( it != dMap.end() ) cout << "channel " << channel
                               << " already present" << endl;
  ChannelData* cd = dMap[channel] = new ChannelData;
  evtData* eData = new evtData;
  eData->chID_readPointer = ed.chID_readPointer;
  eData->numOfValidData   = ed.numOfValidData;
  memcpy( eData->data, ed.data, ed.numOfValidData * ioSize );
  cd->dataPtr = eData;
  cd->copied   = true;
  oldChans = true;
  return;
}


void Event::refer( int channel, const Event::evtData* ed ) {
  auto it = dMap.find( channel );
  if ( it != dMap.end() ) cout << "channel " << channel
                               << " already present" << endl;
  ChannelData* cd = dMap[channel] = new ChannelData;
  cd->dataPtr = ed;
  cd->copied = false;
  oldChans = true;
  return;
}


const Event::acqHeader* Event::acquisitionHeader() const {
  return aHead;
}


const EventSource* Event::eventSource() const {
  return evSource;
}


int Event::eventNumber() const {
  return evNumber;
}


const Event::evtHeader& Event::eventHeader() const {
  return eHead;
}


const vector<int>& Event::channels() const {
  if ( oldChans ) {
    int nc = dMap.size();
    chans.resize( nc );
    int ic = 0;
    for ( auto& e: dMap ) chans[ic++] = e.first;
    oldChans = false;
  }
  return chans;
}


const Event::evtData* Event::eventData( int channel ) const {
  auto it = dMap.find( channel );
  if ( it != dMap.end() ) return it->second->dataPtr;
  return nullptr;
}

