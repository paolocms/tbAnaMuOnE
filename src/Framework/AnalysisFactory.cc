#include "Framework/AnalysisFactory.h"
#include "Framework/AnalysisSteering.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>

using namespace std;

AnalysisFactory::AnalysisFactory() {
}


AnalysisFactory::~AnalysisFactory() {
}

/*
// create all requested analysis objects
vector<AnalysisSteering*> AnalysisFactory::create(
                          const UserParametersManager* userPar ) {
  vector<AnalysisSteering*> aList;
  // loop over analysis object factories
  static map<string,AbsFactory*>* fm = factoryMap();
  for ( const auto& element: *fm ) {
    // create analysis object if its name is listed in the command line
    if ( ( userPar->   getUserParameter( element.first ) != "" ) ||
         ( userPar->hasNoValueParameter( element.first )       ) ) {
      int n = 1;
      PolymorphicSingleton<UserParametersManager>::instance()->
          getUserParameter( element.second->keyword() + "_replica", n );
      if ( n <= 0 ) continue;
      auto a = element.second->create( userPar );
      aList.push_back( a );
      if ( !a->isReplicable() ) continue;
      while ( --n )
      aList.push_back( element.second->create( userPar ) );
    }
  }
  return aList;
}
*/
