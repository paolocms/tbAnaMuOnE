#ifndef Framework_Event_h
#define Framework_Event_h

#include <vector>
#include <map>
#include <cstdint>

//#include "../../tbjuly/sw/ipbus/include/tbstructs.h"
#include "tbstructs.h"

class EventSource;

class Event {

 public:

  typedef ::acqHeader_t acqHeader;
  typedef ::evHeader_t  evtHeader;
  typedef ::event_t     evtData;

  typedef unsigned short data_type;

  // create event from header and source
  Event( const acqHeader* ah, const evtHeader& eh,
         const EventSource* src = nullptr );
  // deleted copy constructor and assignment to prevent unadvertent copy
  Event           ( const Event& x ) = delete;
  Event& operator=( const Event& x ) = delete;

  ~Event();

  // function to add a point
//  void copy ( const data_type* data, unsigned int n );
  void copy ( int channel, const evtData& ed );
  void refer( int channel, const evtData* ed );

  // get event-specific informations:
  const acqHeader* acquisitionHeader() const;
  const EventSource* eventSource() const;
  int              eventNumber() const;
  const evtHeader& eventHeader() const;
  const std::vector<int>& channels() const;
  const evtData*   eventData( int channel ) const;

  static const unsigned int nWords;
  static const unsigned int ioSize;
  static const unsigned int edSize;
  static const unsigned int dataSizeFactor;
  static const unsigned int maxChannelNumber;

 private:

  static int evCounter;
  int evNumber;
  const
  acqHeader* aHead;
  evtHeader  eHead;
  const EventSource* evSource;

  struct ChannelData {
    const evtData* dataPtr;
    bool copied;
  };
  std::map<int,ChannelData*> dMap;

  mutable bool oldChans;
  mutable std::vector<int> chans;

};

#endif // Framework_Event_h

