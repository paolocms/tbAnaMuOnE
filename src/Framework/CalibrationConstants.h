#ifndef Framework_CalibrationConstants_h
#define Framework_CalibrationConstants_h

#include "Utilities/Singleton.h"

#include <vector>

class CalibrationConstants: public Singleton<CalibrationConstants> {

  friend class Singleton<CalibrationConstants>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  CalibrationConstants           ( const CalibrationConstants& x ) = delete;
  CalibrationConstants& operator=( const CalibrationConstants& x ) = delete;

  ~CalibrationConstants();

  // get calibration
  float getCalibration( unsigned int channel ) const;
  float getCalibration( unsigned int row, unsigned int col ) const;

 private:

  // private constructor being a singleton
  CalibrationConstants();

  unsigned int nRows;
  unsigned int nCols;
  mutable bool missingMap;
  mutable std::vector<float> calMap;
  mutable std::vector<float> calCha;
  void fillMap() const;

};

#endif // Framework_CalibrationConstants_h

