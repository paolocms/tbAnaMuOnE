#include "Framework/CalibrationConstants.h"
#include "Framework/ConnectionMap.h"
#include "Framework/Event.h"

#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <cstring>

using namespace std;


CalibrationConstants::CalibrationConstants(): missingMap( true ) {
  ConnectionMap* cm = ConnectionMap::instance();
  nRows = cm->getNRows();
  nCols = cm->getNCols();
}


CalibrationConstants::~CalibrationConstants() {
}


// get calibration
float CalibrationConstants::getCalibration( unsigned int channel ) const {
  if ( missingMap ) fillMap();
  if ( channel < Event::maxChannelNumber ) return calCha[channel];
  return -1;
}


float CalibrationConstants::getCalibration( unsigned int row, unsigned int col ) const {
  if ( missingMap ) fillMap();
  if ( ( --row < nRows ) &&
       ( --col < nCols ) ) return calMap[( row * nCols ) + col];
  return -1;
}


void CalibrationConstants::fillMap() const {

  static const UserParametersManager*
               uPar = PolymorphicSingleton<UserParametersManager>::instance();
  string mapName = "calibrationForCrystal";
  calMap.resize( nRows * nCols );
  calCha.resize( Event::maxChannelNumber );
  bool debug = false;
  uPar->getUserParameter( "debugCalibrationConstants", debug );

  ConnectionMap* cm = ConnectionMap::instance();
  unsigned int ir;
  unsigned int ic;
  unsigned int ih = 0;
  for ( ir = 0; ir < nRows; ++ir ) {
    for ( ic = 0; ic < nCols; ++ic ) {
      float k = 1.0;
      unsigned int jr = ir + 1;
      unsigned int jc = ic + 1;
      uPar->getUserParameter( mapName + to_string( jr ) + to_string( jc ), k );
      calMap[ih++] = calCha[cm->getChannel( jr, jc )] = k;
      if ( debug ) cout << jr << ' ' << jc << ' ' << ih << ' ' << k << endl;
    }
  }
  if ( debug ) {
    for ( ir = 0; ir < nRows; ++ir ) {
      for ( ic = 0; ic < nCols; ++ic ) {
        cout << ir + 1 << ' ' << ic + 1 << " ---> "
             << calMap[( ir * nCols ) + ic] << endl;
      }
    }
  }
  missingMap = false;

}


