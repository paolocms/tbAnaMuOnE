#ifndef Framework_AnalysisFactory_h
#define Framework_AnalysisFactory_h

#include "Framework/UserParametersCxxComment.h"
#include "Framework/AnalysisSteering.h"
#include "Utilities/ObjectFactory.h"
//#include "Framework/ObjectFactory.h"

#include <string>
#include <vector>
#include <map>

//class AnalysisSteering;
//class UserParametersManager;
//template <class T> class Dispatcher;
//template <class T> class ActiveObserver;
//class Event;

class AnalysisFactory: public ObjectFactory<AnalysisSteering> {

//  friend class Dispatcher<Event>;

 public:

  AnalysisFactory();
  // deleted copy constructor and assignment to prevent unadvertent copy
  AnalysisFactory           ( const AnalysisFactory& x ) = delete;
  AnalysisFactory& operator=( const AnalysisFactory& x ) = delete;

  virtual ~AnalysisFactory();

  // create all requested analysis objects
//  static std::vector<AnalysisSteering*> create( const UserParametersManager* userPar );

// private:
//
//  static std::map<const ActiveObserver<Event>*,const AnalysisSteering*>& obsMap() {
//    static std::map<const ActiveObserver<Event>*,const AnalysisSteering*> m;
//    return m;
//  }

};

#endif // Framework_AnalysisFactory_h

