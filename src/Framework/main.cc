#include <vector>
#include <iostream>
#include <string>

#include "Framework/UserParametersCxxComment.h"
#include "Framework/EventSource.h"
#include "Framework/SourceFactory.h"
#include "Framework/AnalysisFactory.h"
#include "Utilities/Dispatcher.h"
#include "Utilities/Singleton.h"
#include "Utilities/PolymorphicSingleton.h"
#include "Utilities/TimeStamp.h"

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "NtuTool/Common/interface/CommandLineParser.h"

using namespace std;
//static EqualSignParser esp;

int main( int argc, char* argv[] ) {

  // store command line parameters
  UserParametersManager* userPar =
                         PolymorphicSingleton<UserParametersManager>::instance(
                         new UserParametersCxxComment );
//  userPar->setUserParameter( "main_argc", argc );
//  for ( int iarg = 0; iarg < argc; ++iarg )
//  userPar->setUserParameter( "main_argv" + to_string( iarg ), argv[iarg] );
  CommandLineParser* cl = CommandLineParser::getInstance();
  cl->parse( userPar, argc, argv );
  userPar->setUserParameter( "exe_name", argv[0] );
  userPar->setUserParameter( "exe_time", TimeStamp::fullStamp() );
//  cout << userPar << endl;
//  userPar->dumpAll( cout );
/*
  if ( userPar->hasNoValueParameter( "online" ) &&
       userPar->hasNoValueParameter( "dump"   ) ) {
    string configName = "data";
    bool isOutToStdOut = true;
    bool isOutToBinary = false;
    std::string extensionOutFile = "txt";
    bool appendTimestamp = true;
    userPar->getUserParameter( "OUT_TO_STDOUT", isOutToStdOut );
    userPar->getUserParameter( "OUT_TO_BINARY_FILE", isOutToBinary );
    userPar->getUserParameter( "APPEND_TIMESTAMP_BINARY_FILE", appendTimestamp );
    if ( isOutToBinary ) {
      userPar->getUserParameter( "OUT_BINARY_FILENAME", configName );
//      userPar->getUserParameter( "OUT_BINARY_FILE_TYPE", extensionOutFile );
      if ( appendTimestamp ) {
	long long ts = TimeStamp::packedDateHMS();
	string date = to_string(   ts / TimeStamp::dayFactor );
	string hour = to_string( ( ts % TimeStamp::dayFactor ) /
                                        TimeStamp::secFactor );
	( ( ( configName += "_" ) += date ) += "_" ) += hour;
      }
      ( configName += "." ) += extensionOutFile;
    }
    ofstream cfgFile( configName );
    userPar->dumpAll( cfgFile );
  }
*/
  // create data source
  EventSource* es = SourceFactory::create( userPar );
  if ( es == nullptr ) return 0;

  // create a list of analyzers
  AnalysisFactory::create( userPar );

  // initialize all analyzers
  Dispatcher<EventSource::AnalysisStatus>::notify( EventSource::begin );

  // loop over events
  es->run();

  // finalize all analyzers
  Dispatcher<EventSource::AnalysisStatus>::notify( EventSource::end );

  delete es;

  return 0;

}

