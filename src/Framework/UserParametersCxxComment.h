#ifndef Framework_UserParametersCxxComment_h
#define Framework_UserParametersCxxComment_h

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "NtuTool/Common/interface/CommandLineParser.h"
#include <string>
#include <iostream>
#include <fstream>

// class derived from UserParametersManager (documented in its own header file)
// to override the parsing of configuration files: lines are neglected when
// beginning with "//" instead of "#"

class UserParametersCxxComment: public virtual UserParametersManager {
 public:
  void setConfiguration( const std::string& file ) override {
    std::cout << "UserParametersCxxComment::setConfiguration " << file << std::endl;
    std::ifstream cfg( file.c_str() );
    int lenMax = 1000;
    char* line = new char[lenMax];
    char* lptr;
    std::string::size_type length;
    while ( cfg.getline( line, lenMax ) ) {
      truncateLineString( line, cfg.gcount() );
      lptr = line;
      while ( *lptr == ' ' ) ++lptr;
      if    ( *lptr == '/' && *++lptr == '/' ) continue;
      std::string key( lptr );
      length = key.find( "//" );
      if ( length != std::string::npos ) {
        key = key.substr( 0, length );
        lptr[length] = '\0';
      }
      if ( key.empty() ) continue;
      length = key.find( " " );
      if ( length == std::string::npos ) {
        addNoValueParameter( key );
        continue;
      }
      key = key.substr( 0, length );
      lptr += length;
      while ( *lptr == ' ' ) ++lptr;
      std::string val( lptr );
      if ( val.empty() ) {
        addNoValueParameter( key );
        continue;
      }
      length = val.find( " " );
      if ( length != std::string::npos )
      val = val.substr( 0, length );
      if ( key == "-c" ) setConfiguration( val );
      else               setUserParameter( key, val );
    }
    delete[] line;
    return;
  }
};

#endif // Framework_UserParametersCxxComment_h

