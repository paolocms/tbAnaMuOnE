#ifndef Framework_AnalysisSteering_h
#define Framework_AnalysisSteering_h

#include "Framework/EventSource.h"
#include "Framework/UserParametersCxxComment.h"
#include "Utilities/ActiveObserver.h"
#include "Utilities/IdentifiableObject.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

class Event;

class AnalysisSteering: public ActiveObserver<EventSource::AnalysisStatus>,
                        public ActiveObserver<Event>,
                        public virtual IdentifiableObject {

  friend void Dispatcher<Event>::notify( const Event& x );

 public:

  AnalysisSteering( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  AnalysisSteering           ( const AnalysisSteering& x ) = delete;
  AnalysisSteering& operator=( const AnalysisSteering& x ) = delete;

  ~AnalysisSteering() override;

//  enum ParameterReplicaGranularity { common, byReplica };

  // function to be called at execution start / end
  void update( const EventSource::AnalysisStatus& status ) override;

 protected:

  const UserParametersManager* uPar;

  const std::string& keyword() const {
    return ObjectFactory<AnalysisSteering>::getFactory( this )->keyword();
  }

  template<class V>
  void getAnalysisParameter( const std::string& k, V& v,
                             IdentifiableObject::ParameterReplicaGranularity g = common ) {
    switch ( g ) {
    default:
    case IdentifiableObject::common:
      uPar->
      getUserParameter(       keyword() + '_' + k, v );
      break;
    case IdentifiableObject::byReplica:
      this->
      getUserParameter( uPar, keyword() + '_' + k, v );
      break;
    }
  }

 private:

  // function to be called at execution start
  virtual void beginJob() = 0;
  // function to be called at execution end
  virtual void   endJob() = 0;

  static std::map<ActiveObserver<Event>*,AnalysisSteering*>& obsMap() {
    static std::map<ActiveObserver<Event>*,AnalysisSteering*>* m =
       new std::map<ActiveObserver<Event>*,AnalysisSteering*>;
    return *m;
  }

  unsigned int counter;
  unsigned int factor;
  unsigned int time;

};

#endif // Framework_AnalysisSteering_h

