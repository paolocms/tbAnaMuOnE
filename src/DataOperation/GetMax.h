#ifndef DataOperation_GetMax_h
#define DataOperation_GetMax_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
#include <vector>
#include <utility>
#include <tuple>
#include <functional>

//class Event;
#include "Framework/Event.h"

// class to find the maximum signal for any channel, giving it's position
// and value, i.e. the number of the sample having the largest number of
// counts and the number of counts itself

class GetMax: public Singleton<GetMax>,
              public LazyObserver<Event> {

  friend class Singleton<GetMax>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetMax           ( const GetMax& x ) = delete;
  GetMax& operator=( const GetMax& x ) = delete;

  ~GetMax() override;

  typedef Event::data_type data_type;
  int       getPos( int channel );
  data_type getVal( int channel );

  // function to be called for each event
  void update( const Event& ev ) override;

//  typedef std::function<std::pair<int,double>( int )> function;
  typedef std::function<std::pair<int,data_type>( int )> function;
//  typedef std::function<std::pair<int,int>( int )> function;
//  std::function<std::pair<int,double>( int )>& functional() {
  function& getFunction() {
    static
    function f = [this]( int i ) {
//    std::function<std::pair<int,double>( int )> f = [this]( int i ) {
      return std::make_pair( getPos( i ), getVal( i ) );
    };
    return f;
  };

//  typedef std::function<std::tuple<int,int,int>( int )> funcSide;
  typedef ArrayStatistic::funcSide funcSide;
//  std::function<std::tuple<int,int,int>( int )> functional( int w, int s ) {
  funcSide getFunction( int w, int s ) {
    funcSide f = [this,w,s]( int i ) {
//    std::function<std::tuple<int,int,int>( int )> f = [this,w,s]( int i ) {
      return std::make_tuple( getPos( i ), w, s );
    };
    return f;
  };

 private:

  // private constructor being a singleton
  GetMax();
  std::vector<      int> pos;
  std::vector<data_type> val;

};

#endif // DataOperation_GetMax_h

