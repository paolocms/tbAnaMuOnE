#include "DataOperation/GetXYPos.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetLaserMonitor.h"
#include "DataOperation/GetPedestal.h"

#include "Framework/Event.h"
#include "Framework/ConnectionMap.h"
#include "Framework/CalibrationConstants.h"

#include "Utilities/PolymorphicSingleton.h"
#include "Utilities/ArrayStatistic.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <cmath>
#include <iostream>

using namespace std;

GetXYPos::GetXYPos() {
  ConnectionMap* cm = ConnectionMap::instance();
  energy = new double[Event::maxChannelNumber];
  xWidth = cm->getNCols() * xSize;
  yWidth = cm->getNRows() * ySize;
  xHalfW = xWidth / 2;
  yHalfW = yWidth / 2;
  UserParametersManager* uPar =
    PolymorphicSingleton<UserParametersManager>::instance();
  uPar->GET_USER_PARAMETER( xSize );
  uPar->GET_USER_PARAMETER( ySize );
  uPar->GET_USER_PARAMETER( maxWeight );
}


GetXYPos::~GetXYPos() {
  delete[] energy;
}


// function to be called for each event to compute the result(s),
// it's actually called (automatically) the first time any of the results
// is requested
void GetXYPos::update( const Event& ev ) {

  // make use of other "operation" classes, in this case:
  // - "GetMax" that find the number of the sample having the largest
  //    number of counts and the number of counts itself;
  // - "GetPedestal" that finds the "pedestal" value, and its RMS,
  //   for all channels
  static GetMax         * gm = GetMax         ::instance();
  static GetPedestal    * gp = GetPedestal    ::instance();
//  static GetSignal      * gs = GetSignal      ::instance();
  static GetRawEventId  * gi = GetRawEventId  ::instance();
  static GetLaserMonitor* gl = GetLaserMonitor::instance();
  static const ConnectionMap       * connMap = ConnectionMap       ::instance();
  static const CalibrationConstants* cCalMap = CalibrationConstants::instance();


  set<int> chSkip = gi->getChannels();
  const set<int>& chSLas = gl->getChannels();
  chSkip.insert( chSLas.begin(), chSLas.end() );
  auto cm = ArrayStatistic::getChannelMax( ev.channels(), chSkip,
                                           gm->getFunction(),
                                           gp->getFunction() );
  int mCha = std::get<0>( cm );
//  pos = std::get<1>( cm );
//  val = std::get<2>( cm );
//  sum = std::get<3>( cm );

  xPos = 0;
  yPos = 0;

  double sSum = 0;
  int iCol;
  int iRow;
  int nCol = connMap->getNCols();
  int nRow = connMap->getNRows();
  int hRow = connMap->getRow( mCha );
  int hCol = connMap->getCol( mCha );
  int rMin = hRow - 1;
  int rMax = hRow + 1;
  int cMin = hCol - 1;
  int cMax = hCol + 1;
  if ( rMin <    1 ) rMin = 1;
  if ( rMax > nRow ) rMax = nRow;
  if ( cMin <    1 ) cMin = 1;
  if ( cMax > nCol ) cMax = nCol;
//  for ( iCol = 1; iCol <= nCol; ++iCol ) {
//    for ( iRow = 1; iRow <= nRow; ++iRow ) {
  for ( iCol = cMin; iCol <= cMax; ++iCol ) {
    for ( iRow = rMin; iRow <= rMax; ++iRow ) {
      unsigned int iCha = connMap->getChannel( iRow, iCol );
      double s = ( energy[iCha] = ( ( gm->getVal( iCha ) - gp->getPedestal( iCha ) ) * cCalMap->getCalibration( iCha ) ) );
      sSum += s;
    }
  }
  double wSum = 0;
  double zCol = ( nCol + 1 ) / 2.0;
  double zRow = ( nRow + 1 ) / 2.0;

//  for ( iCol = 1; iCol <= nCol; ++iCol ) {
//    for ( iRow = 1; iRow <= nRow; ++iRow ) {
  for ( iCol = cMin; iCol <= cMax; ++iCol ) {
    for ( iRow = rMin; iRow <= rMax; ++iRow ) {
      int iCha = connMap->getChannel( iRow, iCol );
      double s = energy[iCha];
      double w = weight( s, sSum, maxWeight );
      wSum += w;
      xPos += ( xSize * ( iCol - zCol ) * w );
      yPos += ( ySize * ( zRow - iRow ) * w );
//      cout << ev.eventNumber() << " = " << iRow << ' ' << iCol << ' ' << s << " : " << xSize * ( iCol - zCol ) << ' ' << ySize * ( zRow - iRow ) << ' ' << w << ' ' << wSum << endl;
    }
  }
//  while ( true ) { cout << "enter r, c " << flush; cin >> iRow >> iCol; cout << connMap->getChannel( iRow, iCol ) << ' ' << xSize * ( iCol - zCol ) << ' ' << ySize * ( zRow - iRow ) << endl; }
  xPos /= wSum;
  yPos /= wSum;
//  cout << ev.eventNumber() << " = " << xPos << ' ' << yPos << endl;

  return;

}

// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
double GetXYPos::getX() {
  check();
  return xPos;
}


double GetXYPos::getY() {
  check();
  return yPos;
}

void GetXYPos::getPos( double& x, double& y ) {
  check();
  x = xPos;
  y = yPos;
  return;
}


double GetXYPos::weight( double eCry, double eSum, double w0 ) {
  double we = w0 + log( eCry / eSum );
  return ( we > 0 ? we : 0 );
}

