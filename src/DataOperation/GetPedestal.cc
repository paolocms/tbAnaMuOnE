#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"
#include "Utilities/ArrayStatistic.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

using namespace std;

GetPedestal::GetPedestal():
 mode( 'm' ), // default value for the criterion to exclude the peak:
              // 'm'=region around the maximum , 'f'=fixed
 pos( 40 ),   // default value for the exclusion region center position
              // ignored if mode='m'
 width( 10 ), // default value for the half/width of the window around
              // the maximum to exclude in computing the pedestal
 side( 0 ) {  // peak side to use: -1=left , 0=both , +1=right
  // get the actual value from at runtime, if specified in one of
  // the following ways:
  // - including "-v pedestalExclusionWidth NEW_VALUE" in the command line;
  // - including "pedestalExclusionWidth XXX" in a configuration file
  //   and giving the file name in the command line with
  //   "-c CONFIG_FILE_NAME"
  // other documentation can be found in the UserParametersManager header file;
  // a "PolymorphicSingleton" singleton is used here to actually 
  UserParametersManager* uPar =
    PolymorphicSingleton<UserParametersManager>::instance();
  uPar->getUserParameter( "pedestalExclusionMode" , mode  );
  uPar->getUserParameter( "pedestalExclusionPos"  , pos   );
  uPar->getUserParameter( "pedestalExclusionWidth", width );
  uPar->getUserParameter( "pedestalExclusionSide" , side  );
}


GetPedestal::~GetPedestal() {
}


// function to be called for each event
void GetPedestal::update( const Event& ev ) {

  static GetSignal    * gs = GetSignal    ::instance();
  static GetRawEventId* gi = GetRawEventId::instance();
  static GetMax       * gm = GetMax       ::instance();

  fEntries.clear();
  fTsumw  .clear();
  fTsumw2 .clear();
  fMean   .clear();
  fRMS    .clear();

  fEntries.resize( Event::maxChannelNumber, 0 );
  fTsumw  .resize( Event::maxChannelNumber, 0 );
  fTsumw2 .resize( Event::maxChannelNumber, 0 );
  fMean   .resize( Event::maxChannelNumber, 0 );
  fRMS    .resize( Event::maxChannelNumber, 0 );
//  for ( int i: ev.channels() ) {
//    const std::vector<Event::data_type>& signal = gs->signal( i );
/*
*/
/*
    int tmin = 0;
    int tmax = signal.size();
    int time;
    int cpos = gm->getPos( i );
    if ( side < 0 ) tmax = cpos;
    if ( side > 0 ) tmin = cpos;
    double offset = signal[tmin];
    for ( time = tmin; time < tmax; ++time ) {
      if ( abs( time - cpos ) < width ) continue;
      cout << "GETPED: " << time << ' ' << cpos << ' ' << width << endl;
      double x = signal[time] - offset;
      fEntries[i] += 1;
      fTsumw  [i] += x;
      fTsumw2 [i] += x * x;
    }
*/
/*
    int cpos = gm->getPos( i );
    auto sb = signal.begin();
    double offset;
    bool setoff = true;
    if ( side <= 0 ) {
      offset = signal.front();
      setoff = false;
      int n = 1 + cpos - width;
      if ( n > 0 ) {
        fEntries[i] += n;
        auto se = sb + n;
        fTsumw  [i]  = ArrayStatistic::getSum   ( sb, se, offset );
        fTsumw2 [i]  = ArrayStatistic::getSumSqr( sb, se, offset );
      }
    }
    if ( side >= 0 ) {
      int n = cpos + width;
      int m = signal.size();
      if ( n < m ) {
        fEntries[i] += m - n;
        sb += n;
        auto se = signal.end();
        if ( setoff )
        offset = *sb;
        fTsumw  [i] += ArrayStatistic::getSum   ( sb, se, offset );
        fTsumw2 [i] += ArrayStatistic::getSumSqr( sb, se, offset );
      }
    }
*/
/*
*/
//    int cpos = gm->getPos( i );
//    auto sb = signal.begin();
//    double offset;
//    if ( side <= 0 ) {
//      offset = signal.front();
//      auto se = sb + 1 + cpos - width;
//      auto st = ArrayStatistic::getSums( sb, se, offset );
//      fEntries[i]  = std::get<0>( st );
//      fTsumw  [i]  = std::get<1>( st );
//      fTsumw2 [i]  = std::get<2>( st );
//    }
//    if ( side >= 0 ) {
//      sb += cpos + width;
//      if ( side )
//      offset = *sb;
//      auto se = signal.end();
//      auto st = ArrayStatistic::getSums( sb, se, offset );
//      fEntries[i] += std::get<0>( st );
//      fTsumw  [i] += std::get<1>( st );
//      fTsumw2 [i] += std::get<2>( st );
//    }
/*
*/
//    double mea =   fTsumw [i] / fEntries[i];
//    double var = ( fTsumw2[i] / fEntries[i] ) - ( mea * mea );
//    fMean[i] = mea + offset;
//    fRMS [i] = ( var > 0 ? sqrt( var ) : 0.0 );
//  }
//  GetSignal* ggs = gs;
//  typedef const std::vector<Event::data_type>::const_iterator data_iter;
//  std::function<std::pair<data_iter,data_iter>( int )> fSign = [ggs]( int i ) {
//    const std::vector<Event::data_type>& signal = gs->signal( i );
//    return make_pair( signal.begin(), signal.end() );
//  };
//  GetMax* ggm = gm;
//  std::function<std::tuple<int,int,int>( int )> fPeak = [ggm,this]( int i ) {
//    return make_tuple( gm->getPos( i ), width, side );
//  };
//  ArrayStatistic::getStat( ev.channels(), gi->getChannels(),
//                           gs->getFunction(), gm->getFunction( width, side ),
//                           fEntries.begin(), fTsumw.begin(), fTsumw2.begin(),
//                           fMean   .begin(), fRMS  .begin() );
  static ArrayStatistic::funcSide fixedPeak = [this]( int i ) {
    tuple<int,int,int> t( pos, width, side );
    return t;
  };
  ArrayStatistic::getStat( ev.channels(), gi->getChannels(),
                           gs->getFunction(),
                           ( mode == 'm' ? gm->getFunction( width, side ) :
                                           fixedPeak ),
                           fEntries.begin(), fTsumw.begin(), fTsumw2.begin(),
                           fMean   .begin(), fRMS  .begin() );
  return;

}

double GetPedestal::getPedestal( int channel ) {
  check();
  return fMean[channel];
}

double GetPedestal::getPedestalRMS( int channel ) {
  check();
  return fRMS[channel];
}

