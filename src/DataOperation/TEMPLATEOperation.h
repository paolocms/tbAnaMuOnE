#ifndef DataOperation_TEMPLATEOperation_h
#define DataOperation_TEMPLATEOperation_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <cmath>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static TEMPLATEOperation* op = TEMPLATEOperation::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.


class TEMPLATEOperation: public Singleton<TEMPLATEOperation>,
                         public LazyObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<TEMPLATEOperation>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  TEMPLATEOperation           ( const TEMPLATEOperation& x ) = delete;
  TEMPLATEOperation& operator=( const TEMPLATEOperation& x ) = delete;

  ~TEMPLATEOperation() override;

  // function returning the result, more functions may exist, to return
  // different results related together, i.e. coming from a single
  // set of instructions; in this example the channel giving the max
  // signal and its position and value are found
  int getMaxChannel();
  int getMaxPos();
  double getMaxValue();

  // function to be called for each event to compute the result(s)
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  TEMPLATEOperation();

  // results of the operation
  int cMax;
  int cPos;
  double cVal;

};

#endif // DataOperation_TEMPLATEOperation_h

