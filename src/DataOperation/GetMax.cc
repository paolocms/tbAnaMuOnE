#include "DataOperation/GetMax.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"
#include "Utilities/ArrayStatistic.h"

using namespace std;

GetMax::GetMax() {
}


GetMax::~GetMax() {
}


// function to be called for each event
void GetMax::update( const Event& ev ) {

  static GetSignal    * gs = GetSignal    ::instance();
  static GetRawEventId* gi = GetRawEventId::instance();

  pos.clear();
  val.clear();
  pos.resize( Event::maxChannelNumber, -1 );
  val.resize( Event::maxChannelNumber,  0 );
//  for ( int i: ev.channels() ) {
//    const std::vector<Event::data_type>& signal = gs->signal( i );
/*
*/
/*
    int tmin = 0;
    int tmax = signal.size();
    int time;
    int cpos = -1;
    int cval = -1;
    for ( time = tmin; time < tmax; ++time ) {
      int tsig = signal[time];
      if ( tsig > cval ) {
	cpos = time;
	cval = signal[time];
      }
    }
    pos[i] = cpos;
    val[i] = cval;
*/
/*
*/
//    auto mv = ArrayStatistic::getMax( signal.begin(), signal.end() );
//    pos[i] = mv.first;
//    val[i] = mv.second;
/*
*/
//  }
//  GetSignal* gg = gs;
//  typedef const std::vector<Event::data_type>::const_iterator data_iter;
//  std::function<std::pair<data_iter,data_iter>( int )> f = [gg]( int i ) {
//    const std::vector<Event::data_type>& signal = gs->signal( i );
//    return make_pair( signal.begin(), signal.end() );
//  };
  ArrayStatistic::getMax( ev.channels(), gi->getChannels(), gs->getFunction(),
                          pos.begin(), val.begin() );
  return;

}


int GetMax::getPos( int channel ) {
  check();
  return pos[channel];
}


GetMax::data_type GetMax::getVal( int channel ) {
  check();
  return val[channel];
}

