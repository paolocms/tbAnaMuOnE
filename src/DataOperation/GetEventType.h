#ifndef DataOperation_GetEventType_h
#define DataOperation_GetEventType_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <set>
#include <cmath>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static GetEventType* op = GetEventType::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.

#define EVENT_TYPES { unclassified, noise, laser, beam }
#define CLASS_MODES { bySignal, byDelay }
class GetEventType: public Singleton<GetEventType>,
                    public LazyObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<GetEventType>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetEventType           ( const GetEventType& x ) = delete;
  GetEventType& operator=( const GetEventType& x ) = delete;

  ~GetEventType() override;

  enum type EVENT_TYPES;
  enum mode CLASS_MODES;
  static const std::vector<type>& typeList();
  static const std::vector<mode>& modeList();
  static const std::map<type,std::string>& typeName();
  static const std::map<mode,std::string>& modeName();

  // function returning the result, more functions may exist, to return
  // different results related together, i.e. coming from a single
  // set of instructions; in this example the channel giving the max
  // signal and its position and value are found
  type getType();

  // function to be called for each event to compute the result(s)
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetEventType();

  // classification mode
  mode classMode;

  // skipped channels
  std::set<int> skippedChannels;

  // thresholds
  float maxMinRatioThreshold;
  float minSigToRMSThreshold;
  float maxSigToRMSThreshold;

  std::vector<bool> maskChannel;

  // results of the operation
  type eventType;

};

#endif // DataOperation_GetEventType_h

