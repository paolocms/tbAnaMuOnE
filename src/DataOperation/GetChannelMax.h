#ifndef DataOperation_GetChannelMax_h
#define DataOperation_GetChannelMax_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>

class Event;

// class to find the maximum signal for any channel, giving it's position
// and value, i.e. the number of the sample having the largest number of
// counts and the number of counts itself

class GetChannelMax: public Singleton<GetChannelMax>,
                     public LazyObserver<Event> {

  friend class Singleton<GetChannelMax>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetChannelMax           ( const GetChannelMax& x ) = delete;
  GetChannelMax& operator=( const GetChannelMax& x ) = delete;

  ~GetChannelMax() override;

  int getCha();
  int getPos();
  double getVal();
  double getSum();

  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetChannelMax();
  int cha;
  int pos;
  double val;
  double sum;

};

#endif // DataOperation_GetChannelMax_h

