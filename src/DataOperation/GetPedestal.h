#ifndef DataOperation_GetPedestal_h
#define DataOperation_GetPedestal_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <cmath>
#include <utility>
#include <functional>

class Event;

class GetPedestal: public Singleton<GetPedestal>,
                   public LazyObserver<Event> {

  friend class Singleton<GetPedestal>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetPedestal           ( const GetPedestal& x ) = delete;
  GetPedestal& operator=( const GetPedestal& x ) = delete;

  ~GetPedestal() override;

  double getPedestal   ( int channel );
  double getPedestalRMS( int channel );

  // function to be called for each event
  void update( const Event& ev ) override;

  typedef std::function<std::pair<double,double>( int )> function;
//  std::function<std::pair<double,double>( int )>& functional() {
  function& getFunction() {
    static
    function f = [this]( int i ) {
//    std::function<std::pair<double,double>( int )> f = [this]( int i ) {
      return std::make_pair( getPedestal( i ), getPedestalRMS( i ) );
    };
    return f;
  };

 private:

  // private constructor being a singleton
  GetPedestal();

  char mode;
  int pos;
  int width;
  int side;

  std::vector<double> fEntries; ///< Number of entries
  std::vector<double> fTsumw;   ///< Total Sum of weights
  std::vector<double> fTsumw2;  ///< Total Sum of squares of weights
  std::vector<double> fMean;    ///< Mean
  std::vector<double> fRMS;     ///< RMS

};

#endif // DataOperation_GetPedestal_h

