#ifndef DataOperation_GetXYPos_h
#define DataOperation_GetXYPos_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <cmath>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static GetXYPos* op = GetXYPos::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.


class GetXYPos: public Singleton<GetXYPos>,
                public LazyObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<GetXYPos>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetXYPos           ( const GetXYPos& x ) = delete;
  GetXYPos& operator=( const GetXYPos& x ) = delete;

  ~GetXYPos() override;

  float getXSize()  { return xSize; }
  float getYSize()  { return ySize; }
  float getXWidth() { return xWidth; }
  float getYWidth() { return yWidth; }
  float getXHalfW() { return xHalfW; }
  float getYHalfW() { return yHalfW; }

  // function returning the result, more functions may exist, to return
  // different results related together, i.e. coming from a single
  // set of instructions; in this example the channel giving the max
  // signal and its position and value are found
  double getX();
  double getY();
  void getPos( double& x, double& y );

  // function to be called for each event to compute the result(s)
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetXYPos();

  static double weight( double eCry, double eSum, double w0 );

  float xSize = 3;
  float ySize = 3;
  float xWidth;
  float yWidth;
  float xHalfW;
  float yHalfW;

  double* energy;
  float maxWeight = 5.5;

  // results of the operation
  double xPos;
  double yPos;

};

#endif // DataOperation_GetXYPos_h

