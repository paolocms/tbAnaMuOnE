#ifndef DataOperation_GetLaserMonitor_h
#define DataOperation_GetLaserMonitor_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <set>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static GetLaserMonitor* op = GetLaserMonitor::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.


class GetLaserMonitor: public Singleton<GetLaserMonitor>,
                       public LazyObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<GetLaserMonitor>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetLaserMonitor           ( const GetLaserMonitor& x ) = delete;
  GetLaserMonitor& operator=( const GetLaserMonitor& x ) = delete;

  ~GetLaserMonitor() override;

  // function returning the result, more functions may exist, to return
  // different results related together, i.e. coming from a single
  // set of instructions; in this example the channel giving the max
  // signal and its position and value are found
  int getLaserChannel();
  int getFiberChannel();
  const std::set<int>& getChannels();

  // function to be called for each event to compute the result(s)
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetLaserMonitor();

  int channelLM;
  int channelFM;
  std::set<int> channels;

};

#endif // DataOperation_GetLaserMonitor_h

