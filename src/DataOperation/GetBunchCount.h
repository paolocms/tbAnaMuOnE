#ifndef DataAnalysis_GetBunchCount_h
#define DataAnalysis_GetBunchCount_h

#include "Utilities/ActiveObserver.h"
#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>

class Event;

// This class is deigned and implemented according to considerations
// similar to the ones for "EventAccumulation", see comments therein
class GetBunchCount: public Singleton<GetBunchCount>,
                     public ActiveObserver<Event> {

  friend class Singleton<GetBunchCount>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetBunchCount           ( const GetBunchCount& x ) = delete;
  GetBunchCount& operator=( const GetBunchCount& x ) = delete;

  ~GetBunchCount() override;

  unsigned long long current();
  unsigned long long fromFirst();
  unsigned long long fromLast();

  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetBunchCount();

  class CountSave: public Singleton<GetBunchCount::CountSave>,
                   public LazyObserver<Event> {
    friend class Singleton<GetBunchCount::CountSave>;
   public:
    unsigned long long current();
    unsigned long long fromFirst();
    unsigned long long fromLast();
    void update( const Event& ev ) override;
   private:
    CountSave();
    static unsigned long long absCount( unsigned int oc, unsigned int bc );
    static unsigned long long countDiff( unsigned long long o,
                                         unsigned long long n );
    unsigned long long firstCount = 0xffffffffffffffff;
    unsigned long long lastCount  = 0;
    unsigned long long currCount;
    unsigned long long fDiff;
    unsigned long long lDiff;
  };
  CountSave* count;

  unsigned int minOC;
  unsigned int maxBC;
  unsigned long long firstCount = 0xffffffffffffffff;
  unsigned long long lastCount  = 0;
  unsigned long long absCount ( unsigned int oc, unsigned int bc );
  unsigned long long countDiff( unsigned long long o, unsigned long long n );


};

#endif // DataAnalysis_GetBunchCount_h

