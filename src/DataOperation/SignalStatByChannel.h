#ifndef DataOperation_SignalStatByChannel_h
#define DataOperation_SignalStatByChannel_h

#include "Framework/Event.h"

#include "Utilities/ActiveObserver.h"
#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <map>
#include <cmath>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static SignalStatByChannel* op = SignalStatByChannel::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.


class SignalStatByChannel: public Singleton<SignalStatByChannel>,
                           public ActiveObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<SignalStatByChannel>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SignalStatByChannel           ( const SignalStatByChannel& x ) = delete;
  SignalStatByChannel& operator=( const SignalStatByChannel& x ) = delete;

  ~SignalStatByChannel() override;

  unsigned int eventCount( int channel );
  const std::vector<double>& signalMean( int channel );
  const std::vector<double>& signalRMS ( int channel );
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  SignalStatByChannel();

  class ChannelScalers: public Singleton<SignalStatByChannel::ChannelScalers>,
                        public LazyObserver<Event> {
    friend class Singleton<SignalStatByChannel::ChannelScalers>;
   public:
    // activate/deactivate channels
    void setActiveChan( int channel, bool active );
    // function to get the results
    unsigned int eventCount( int channel );
    const std::vector<double>& signalOff( int channel );
    const std::vector<double>& signalSum( int channel );
    const std::vector<double>& signalSqr( int channel );
    // function to be called for each event
    void update( const Event& ev ) override;
   private:
    ChannelScalers();
    // results of the operation
    std::vector<bool> activeChan;
    std::vector<int> sPos;
    std::vector<Event::data_type> sMin;
    std::vector<Event::data_type> sMax;
    std::vector<unsigned int> counter;
    std::vector<std::vector<double>> sOff;
    std::vector<std::vector<double>> sSum;
    std::vector<std::vector<double>> sSqr;
  };
  ChannelScalers* scaler;
  class ChannelScalSta: public Singleton<SignalStatByChannel::ChannelScalSta>,
                        public LazyObserver<Event> {
    friend class Singleton<SignalStatByChannel::ChannelScalSta>;
   public:
    // function to get the results
    const std::vector<double>& signalMean( int channel );
    const std::vector<double>& signalRMS ( int channel );
    // function to be called for each event
    void update( const Event& ev ) override;
   private:
    ChannelScalSta();
    std::vector<std::vector<double>> sigMea;
    std::vector<std::vector<double>> sigRMS;
  };
  ChannelScalSta* scstat;

};

#endif // DataOperation_SignalStatByChannel_h

