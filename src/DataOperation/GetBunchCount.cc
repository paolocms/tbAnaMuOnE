#include "DataOperation/GetBunchCount.h"

#include "DataOperation/GetSignal.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"

#include <iostream>

using namespace std;

GetBunchCount::GetBunchCount() {
  count = GetBunchCount::CountSave::instance();
}


GetBunchCount::~GetBunchCount() {
}


unsigned long long GetBunchCount::current() {
  return count->current();
}


unsigned long long GetBunchCount::fromFirst() {
  return count->fromFirst();
}


unsigned long long GetBunchCount::fromLast() {
  return count->fromLast();
}


// function to be called for each event
void GetBunchCount::update( const Event& ev ) {
  count->current();
  return;
}


unsigned long long GetBunchCount::absCount( unsigned int oc,
                                            unsigned int bc ) {
  static unsigned long long bn = GetSignal::bunchMax + 1;
  return ( oc * bn ) + bc;
}


unsigned long long GetBunchCount::countDiff( unsigned long long o,
                                             unsigned long long n ) {
  if ( n > o ) return n - o;
  static const unsigned long long m = absCount( 0xffffffff,
                                                GetSignal::bunchMax );
  unsigned long long d = m - o;
  return d + n;
}


GetBunchCount::CountSave::CountSave() {
  fDiff = lDiff = 0;
}


unsigned long long GetBunchCount::CountSave::current() {
  check();
  return currCount;
}


unsigned long long GetBunchCount::CountSave::fromFirst() {
  check();
  return fDiff;
}


unsigned long long GetBunchCount::CountSave::fromLast() {
  check();
  return lDiff;
}


void GetBunchCount::CountSave::update( const Event& ev ) {
  static GetRawEventId* gId = GetRawEventId::instance();
  const std::vector<unsigned int>& oc = gId->getOC();
  const std::vector<unsigned int>& bc = gId->getBC();
  if ( oc.empty() || bc.empty() ) return;
  unsigned long long ac = absCount( oc.front(), bc.front() );
  if ( firstCount > 0xffffffffffff ) firstCount = ac;
  else                                lastCount = currCount;
  currCount = ac;
  fDiff = countDiff( firstCount, currCount );
  lDiff = countDiff(  lastCount, currCount );
  return;
}


unsigned long long GetBunchCount::CountSave::absCount( unsigned int oc,
                                                       unsigned int bc ) {
  static unsigned long long bn = GetSignal::bunchMax + 1;
  return ( oc * bn ) + bc;
}


unsigned long long GetBunchCount::CountSave::countDiff( unsigned long long o,
                                                        unsigned long long n ) {
  static const unsigned long long m = absCount( 0xffffffff,
                                                GetSignal::bunchMax );
  return ( n > o ? n - o : n + ( m - o ) );
}


