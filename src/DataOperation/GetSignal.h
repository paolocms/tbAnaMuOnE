#ifndef DataOperation_GetSignal_h
#define DataOperation_GetSignal_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include "Framework/Event.h"

#include <iostream>
#include <vector>
#include <list>
#include <utility>
#include <functional>

class GetSignal: public Singleton<GetSignal>,
                 public LazyObserver<Event> {

  friend class Singleton<GetSignal>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetSignal           ( const GetSignal& x ) = delete;
  GetSignal& operator=( const GetSignal& x ) = delete;

  ~GetSignal() override;

  // function to be called for each event
  void update( const Event& ev ) override;
  static int fillSignalMap( const Event& ev,
                            std::map<int,std::vector<Event::data_type>>& m,
                            bool stab, bool odds = false );
  static void fillDataSample( const std::map<int,
                                    std::vector<Event::data_type>>& dMap,
                              unsigned int sample,
                              char* dataSample );
  static void fillDataSample( const std::map<int,
                                    std::vector<Event::data_type>>& dMap,
                              unsigned int sample,
                              unsigned int orbitCount, unsigned int bunchCount,
                              char* dataSample );
  static int  fillDataMap( std::list<char*>::const_iterator ib,
                           std::list<char*>::const_iterator ie,
                           std::map<int, std::vector<Event::data_type>>& dMap );
  template <class O, class B>
  static bool bunchAdvance( O& oc, B& bc ) {
    static B bm = bunchMax;
    ++bc;
    if ( bc > bm ) {
      ++oc;
      bc = 0;
      return true;
    }
    return false;
  }

  static const std::vector<int>& sampleChannelMap();
  static const std::vector<int>& sampleDataPosMap();
  static Event::acqHeader* getAcqHeader();

  const std::vector<Event::data_type>& signal( int channel );
  const std::vector<Event::data_type>& signal();

  typedef const std::vector<Event::data_type>::const_iterator data_iter;
  typedef std::function<std::pair<data_iter,data_iter>( int )> function;
//  std::function<std::pair<data_iter,data_iter>( int )>& functional() {
  function& getFunction() {
    static
    function f = [this]( int i ) {
//    std::function<std::pair<data_iter,data_iter>( int )> f = [this]( int i ) {
      const std::vector<Event::data_type>& s = signal( i );
      return std::make_pair( s.begin(), s.end() );
    };
    return f;
  };

  static const unsigned int dataNBit;
  static const unsigned int dataSize;
  static const unsigned int adcsNBit;
  static const unsigned int adcsSize;
  static const unsigned int orbitPos;
  static const unsigned int oBytePos;
  static const unsigned int orbitLen;
  static const unsigned int bunchPos;
  static const unsigned int bBytePos;
  static const unsigned int bunchLen;
  static const unsigned int bunchMax;

 private:

  // private constructor being a singleton
  GetSignal();

  bool signalTailAtBegin;
  bool oddDataSize;
  std::map<int,std::vector<Event::data_type>> dMap;
  std::vector<Event::data_type> dTot;

};

#endif // DataOperation_GetSignal_h

