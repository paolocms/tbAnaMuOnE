#include "DataOperation/EventAccumulation.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"
#include "Framework/EventSource.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>

using namespace std;

EventAccumulation::EventAccumulation() {
  scaler = EventAccumulation::ChannelScalers::instance();
  GetRawEventId* gi = GetRawEventId::instance();
  for ( int id: gi->getChannels() ) scaler->setActiveChan( id, false );
}


EventAccumulation::~EventAccumulation() {
}


// function to be called for each event
void EventAccumulation::update( const Event& ev ) {
  // call to ensure that the nested "LazyObserver" is actually updated
  scaler->eventCount( GetEventType::unclassified );
  return;
}

// return the results
unsigned int EventAccumulation::eventCount( GetEventType::type evtType ) {
  return scaler->eventCount( evtType );
}


const std::vector<double>& EventAccumulation::signal( int channel,
                           GetEventType     ::type evtType,
                           EventAccumulation::type sumType ) {
  return scaler->signal( channel, evtType, sumType );
}


const std::vector<double>& EventAccumulation::signalSum(
                           GetEventType     ::type evtType,
                           EventAccumulation::type sumType ) {
  return scaler->signalSum( evtType, sumType );
}


void EventAccumulation::ChannelScalers::update( const Event& ev ) {
  static bool init = true;
  if ( init ) {
    int samplePerChan = ev.acquisitionHeader()->sample_per_chan;
    for ( GetEventType::type evtType: GetEventType::typeList() ) {
      counter[evtType] = 0;
      yAcc[evtType].resize( Event::maxChannelNumber );
      zAcc[evtType].resize( Event::maxChannelNumber );
      for ( auto& v: yAcc[evtType] ) v.resize( samplePerChan, 0 );
      for ( auto& v: zAcc[evtType] ) v.resize( samplePerChan, 0 );
      yAccSum[evtType].resize( samplePerChan, 0 );
      zAccSum[evtType].resize( samplePerChan, 0 );
    }
    init = false;
  }
  static UserParametersManager* upm =
         PolymorphicSingleton<UserParametersManager>::instance();
  static unsigned int maxBufferLength = []() {
    unsigned int mbl = 0;
    upm->getUserParameter( "EventAccumulation_maxBufferLength", mbl );
    return mbl;
  }();
  static unsigned int minBufferLength = []() {
    unsigned int mbl = 0;
    upm->getUserParameter( "EventAccumulation_minBufferLength", mbl );
    return mbl;
  }();
  static const function<bool()>& dropEvent =
               ev.eventSource()->getDropFunction(
                                 "EventAccumulationDropFunction",
                                 maxBufferLength,
                                 minBufferLength,
                                 &cout );
  if ( dropEvent() ) return;
  const vector<int>& channels = ev.channels();
  static GetPedestal * gp = GetPedestal ::instance();
  static GetSignal   * gs = GetSignal   ::instance();
  static GetEventType* gt = GetEventType::instance();
  GetEventType::type evtType = gt->getType();
  ++counter[evtType];
  if ( evtType != GetEventType::unclassified )
       ++counter[ GetEventType::unclassified];
  for ( int i: channels ) {
    if ( !activeChan[i] ) continue;
    const std::vector<Event::data_type>& signal = gs->signal( i );
    int n = signal.size();
    int j;
    for ( j = 0; j < n; ++j ) {
      float sig = signal[j];
      float eff = sig - gp->getPedestal( i );
      zAcc   [GetEventType::unclassified][i][j] += sig;
      zAccSum[GetEventType::unclassified]   [j] += sig;
      yAcc   [GetEventType::unclassified][i][j] += eff;
      yAccSum[GetEventType::unclassified]   [j] += eff;
      if ( evtType == GetEventType::unclassified ) continue;
      zAcc   [evtType                   ][i][j] += sig;
      zAccSum[evtType                   ]   [j] += sig;
      yAcc   [evtType                   ][i][j] += eff;
      yAccSum[evtType                   ]   [j] += eff;
    }
  }
  return;
}


EventAccumulation::ChannelScalers::ChannelScalers() {
  activeChan.resize( Event::maxChannelNumber, true );
}


void EventAccumulation::ChannelScalers::setActiveChan( int channel,
                                                       bool active ) {
  activeChan[channel] = active;
  return;
}


unsigned int EventAccumulation::ChannelScalers::eventCount(
                                                GetEventType::type evtType ) {
  check();
  return counter[evtType];
}


const std::vector<double>& EventAccumulation::ChannelScalers::signal(
                           int channel,
                           GetEventType     ::type evtType,
                           EventAccumulation::type sumType ) {
  check();
  switch ( sumType ) {
  case raw:
    return zAcc[evtType][channel];
  default:
  case pedestalSubtracted:
    return yAcc[evtType][channel];
  }
  return yAcc[evtType][channel];
}


const std::vector<double>& EventAccumulation::ChannelScalers::signalSum(
                           GetEventType     ::type evtType,
                           EventAccumulation::type sumType ) {
  check();
  switch ( sumType ) {
  case raw:
    return zAccSum[evtType];
  default:
  case pedestalSubtracted:
    return yAccSum[evtType];
  }
  return yAccSum[evtType];
}

