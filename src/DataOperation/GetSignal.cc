#include "DataOperation/GetSignal.h"

#include "DataOperation/GetRawEventId.h"
#include "Framework/ConnectionMap.h"
#include "Framework/Event.h"
#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/PolymorphicSingleton.h"

#include <cstring>
#include <iostream>

using namespace std;

const unsigned int GetSignal::dataNBit = 448          ; // data size in bits
const unsigned int GetSignal::dataSize = GetSignal::dataNBit / 8;
                                                        // data size in bytes
const unsigned int GetSignal::adcsNBit = 400;           // adc  size in bits
const unsigned int GetSignal::adcsSize = GetSignal::adcsNBit / 8;
                                                        // adc  size in bytes
const unsigned int GetSignal::orbitPos = GetSignal::adcsNBit;
                                              // bit position, left to right
const unsigned int GetSignal::oBytePos = GetSignal::orbitPos / 8;
const unsigned int GetSignal::orbitLen = 32;  // bits
const unsigned int GetSignal::bunchPos = GetSignal::orbitPos +
                                         GetSignal::orbitLen;
const unsigned int GetSignal::bBytePos = GetSignal::bunchPos / 8;
const unsigned int GetSignal::bunchLen = 16;
const unsigned int GetSignal::bunchMax = 3563;

GetSignal::GetSignal():
 signalTailAtBegin( true ),
 oddDataSize( false ) {
  static const UserParametersManager*
               uPar = PolymorphicSingleton<UserParametersManager>::instance();
  uPar->getUserParameter( "signalTailAtBegin", signalTailAtBegin );
  uPar->getUserParameter( "oddDataSize", oddDataSize );
}


GetSignal::~GetSignal() {
}


// function to be called for each event
void GetSignal::update( const Event& ev ) {
  int totalSize = fillSignalMap( ev, dMap, signalTailAtBegin, oddDataSize );
  dTot.resize( totalSize );
  auto it = dTot.begin();
  for ( auto& e: dMap ) {
    const auto& v = e.second;
    int nd = v.size();
    memcpy( &(*it), &(v.front()), nd * Event::edSize );
    it += nd;
  }
  return;

}


// function to unpack data
int GetSignal::fillSignalMap( const Event& ev,
                              map<int,vector<Event::data_type>>& m,
                              bool stab, bool odds ) {
  m.clear();
  int totalSize = 0;
  for ( int i: ev.channels() ) {
    const Event::evtData* dataPtr = ev.eventData( i );
    int dataNumber = dataPtr->numOfValidData;
    const Event::data_type* chData = reinterpret_cast<const Event::data_type*>(
                                     dataPtr->data );
    const Event::data_type* chTail;
    if ( dataPtr->chID_readPointer & 1 ) {
      if ( !stab )
           chTail = chData + ( --dataNumber * Event::dataSizeFactor );
      else chTail = chData;
      ++chData;
    }
    else {
      chTail = chData + ( ( dataNumber * Event::dataSizeFactor ) - 1 );
    }
    int nw = dataNumber * Event::dataSizeFactor;
    std::vector<Event::data_type>& sig = m[i];
    sig.resize( nw );
    memcpy( &sig.front(), chData, ( nw - 1 ) * Event::edSize );
    memcpy( &sig.back (), chTail,              Event::edSize );
    if ( odds ) sig.resize( --nw );
    totalSize += nw;
  }
  return totalSize;
}


void GetSignal::fillDataSample( const map<int, vector<Event::data_type>>& dMap,
                                unsigned int sample,
                                char* dataSample ) {
  static GetRawEventId* eId = GetRawEventId::instance();
  static int ol = eId->getChannelOL();
  static int oh = eId->getChannelOH();
  static int bc = eId->getChannelBC();
  static vector<Event::data_type> dum( Event::maxChannelNumber, 0 );
  map<int,vector<Event::data_type>>::const_iterator it;
  const vector<Event::data_type>& ols = ( ( it = dMap.find( ol ) )
                                          == dMap.end() ? dum : it->second );
  const vector<Event::data_type>& ohs = ( ( it = dMap.find( oh ) )
                                          == dMap.end() ? dum : it->second );
  const vector<Event::data_type>& bcs = ( ( it = dMap.find( bc ) )
                                          == dMap.end() ? dum : it->second );
  fillDataSample( dMap, sample,
                  GetRawEventId::unpack( ols[sample],
                                         ohs[sample] ),
                  GetRawEventId::unpack( bcs[sample] ),
                  dataSample );
  return;
}


void GetSignal::fillDataSample( const map<int,vector<Event::data_type>>& dMap,
                                unsigned int sample,
                                unsigned int orbitCount,
                                unsigned int bunchCount,
                                char* dataSample ) {
  static unsigned int oBytePos = orbitPos / 8;
  static unsigned int bBytePos = bunchPos / 8;
  static unsigned int oByteLen = orbitLen / 8;
  static unsigned int bByteLen = bunchLen / 8;
  static const vector<int>& chMap = sampleChannelMap();
  Event::data_type* channelData =
                    reinterpret_cast<Event::data_type*>( dataSample );
  for ( auto& e: dMap ) {
    int i = e.first;
    const vector<Event::data_type>& s = e.second;
    if ( s.empty() ) continue;
    int pos = chMap[i];
    if ( pos < 0 ) continue;
    channelData[pos] = s[sample];
  }
  memcpy( dataSample + oBytePos, &orbitCount, oByteLen );
  memcpy( dataSample + bBytePos, &bunchCount, bByteLen );
}


int  GetSignal::fillDataMap( list<char*>::const_iterator ib,
                             list<char*>::const_iterator ie,
                             map<int, vector<Event::data_type>>& dMap ) {
  int n = distance( ib, ie );
  if ( n < 0 ) return 0;
  static GetRawEventId* eId = GetRawEventId::instance();
  for ( auto& e: dMap ) e.second.clear();
  static const vector<int>& dpMap = sampleDataPosMap();
  for ( int i: dpMap ) if ( i >= 0 ) dMap[i].resize( n );
  vector<Event::data_type>& ol = dMap[eId->getChannelOL()];
  vector<Event::data_type>& oh = dMap[eId->getChannelOH()];
  vector<Event::data_type>& bc = dMap[eId->getChannelBC()];
  ol.resize( n );
  oh.resize( n );
  bc.resize( n );
  int is = 0;
//  int ns = distance( ib, ie );
//  cout << "nsamples: " << ns << endl;
//  if ( ns < 0 ) return 0;
  while ( ib != ie ) {
    const char* c = *ib++;
    const Event::data_type* channelData =
                            reinterpret_cast<const Event::data_type*>( c );
    int ic;
    int jc;
    static int nc = dpMap.size();
    for ( ic = 0; ic < nc; ++ic ) {
//      jc = dpMap.at( ic );
      jc = dpMap[ic];
      if ( jc < 0 ) continue;
      vector<Event::data_type>& s = dMap[jc];
//      cout << "fill " << is << ' ' << ic << endl;
      s[is] = channelData[ic];
//      s.at( is ) = channelData[ic];
    }
    ol[is] = GetRawEventId::orbitCount( c, GetRawEventId::low  );
    oh[is] = GetRawEventId::orbitCount( c, GetRawEventId::high );
    bc[is] = GetRawEventId::bunchCount( c );
//    ol.at( is ) = GetRawEventId::orbitCount( c, GetRawEventId::low  );
//    oh.at( is ) = GetRawEventId::orbitCount( c, GetRawEventId::high );
//    bc.at( is ) = GetRawEventId::bunchCount( c );
//    ol[is] = *reinterpret_cast<const Event::data_type*>(
//              c + GetSignal::oBytePos );
//    oh[is] = *reinterpret_cast<const Event::data_type*>(
//              c + GetSignal::oBytePos + sizeof( Event::data_type ) );
//    bc[is] = *reinterpret_cast<const Event::data_type*>(
//              c + GetSignal::bBytePos );
    ++is;
  }
  return n;
}


//bool GetSignal::bunchAdvance( unsigned int& oc, unsigned int& bc ) {
//  ++bc;
//  if ( bc > bunchMax ) {
//    ++oc;
//    bc = 0;
//    return true;
//  }
//  return false;
//}


const std::vector<int>& GetSignal::sampleChannelMap() {
  static ConnectionMap* connMap = ConnectionMap::instance();
  static int nCry = ( connMap->getNRows() * connMap->getNCols() );
  static const vector<int>& chMap = []( int cFeb ) {
    static vector<int> cm( Event::maxChannelNumber, -1 );
    int i;
    for ( i = 0; i < cFeb; ++i ) cm[i] = i;
    int j = 1 + Event::maxChannelNumber - cFeb;
    while ( i < nCry ) cm[j++] = i++;
    return cm;
  }( 13 );
  return chMap;
}


const std::vector<int>& GetSignal::sampleDataPosMap() {
  static int nCha = Event::maxChannelNumber;
  static const vector<int>& dpMap = []() {
    static const vector<int>& chMap = sampleChannelMap();
    static vector<int> im( ( GetSignal::dataSize + Event::edSize - 1 ) /
                                                   Event::edSize, -1 );
    int i;
    for ( i = 0; i < nCha; ++i ) {
      int j = chMap[i];
      if ( j < 0 ) continue;
      im[j] = i;
    }
    return im;
  }();
  return dpMap;
}


const std::vector<Event::data_type>& GetSignal::signal() {
  check();
  return dTot;
}


Event::acqHeader* GetSignal::getAcqHeader() {
  static Event::acqHeader aHead = []() {
    static const vector<int>& chMap = GetSignal::sampleChannelMap();
    static GetRawEventId* eId = GetRawEventId::instance();
    static int olc = eId->getChannelOL();
    static int ohc = eId->getChannelOH();
    static int bcc = eId->getChannelBC();
    static Event::acqHeader h;
    h.channel_mask = 0;
    unsigned int iCha = Event::maxChannelNumber;
    while ( iCha-- ) {
      h.channel_mask <<= 1;
      if ( chMap[iCha] >= 0 ) h.channel_mask |= 1;
    }
    h.channel_mask |= ( 1 << olc );
    h.channel_mask |= ( 1 << ohc );
    h.channel_mask |= ( 1 << bcc );
    h.sample_per_chan = 0;
    return h;
  }();
  return &aHead;
}


const std::vector<Event::data_type>& GetSignal::signal( int channel ) {
  check();
  static vector<Event::data_type> dum;
  auto it = dMap.find( channel );
  if ( it == dMap.end() ) return dum;
  else                    return it->second;
}

