#include "DataOperation/TEMPLATEOperation.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetPedestal.h"

#include "Framework/Event.h"

using namespace std;

TEMPLATEOperation::TEMPLATEOperation() {
}


TEMPLATEOperation::~TEMPLATEOperation() {
}


// function to be called for each event to compute the result(s),
// it's actually called (automatically) the first time any of the results
// is requested
void TEMPLATEOperation::update( const Event& ev ) {

  // make use of other "operation" classes, in this case:
  // - "GetMax" that find the number of the sample having the largest
  //    number of counts and the number of counts itself;
  // - "GetPedestal" that finds the "pedestal" value, and its RMS,
  //   for all channels
  static GetMax     * gm = GetMax     ::instance();
  static GetPedestal* gp = GetPedestal::instance();

  cMax = -1;
  cPos = -1;
  cVal = -1;

  for ( int i: ev.channels() ) { // loop over channels
    // get the max. value, pedestal subtracted, for this channel
    double s = gm->getVal( i ) - gp->getPedestal( i );
    if ( s > cVal ) {
      cMax = i;
      cPos = gm->getPos( i );
      cVal = gm->getVal( i );
    }
  }

  return;

}

// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
int TEMPLATEOperation::getMaxChannel() {
  check();
  return cMax;
}

int TEMPLATEOperation::getMaxPos() {
  check();
  return cPos;
}

double TEMPLATEOperation::getMaxValue() {
  check();
  return cVal;
}

