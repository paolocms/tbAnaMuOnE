#include "DataOperation/GetLaserMonitor.h"

#include "DataOperation/GetSignal.h"

#include "Framework/Event.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

using namespace std;

GetLaserMonitor::GetLaserMonitor() {
  channelLM = 18;
  channelFM = 19;
  UserParametersManager* uPar =
    PolymorphicSingleton<UserParametersManager>::instance();
  uPar->GET_USER_PARAMETER( channelLM );
  uPar->GET_USER_PARAMETER( channelFM );
  channels.insert( channelLM );
  channels.insert( channelFM );
}


GetLaserMonitor::~GetLaserMonitor() {
}


// function to be called for each event to compute the result(s),
// it's actually called (automatically) the first time any of the results
// is requested
void GetLaserMonitor::update( const Event& ev ) {
  return;
}

// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
int GetLaserMonitor::getLaserChannel() {
  check();
  return channelLM;
}

int GetLaserMonitor::getFiberChannel() {
  check();
  return channelFM;
}

const set<int>& GetLaserMonitor::getChannels() {
  return channels;
}

