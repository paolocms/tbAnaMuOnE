#include "DataOperation/SignalStatByChannel.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"
#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/PolymorphicSingleton.h"

#include <iostream>

using namespace std;

SignalStatByChannel::SignalStatByChannel() {
  scaler = SignalStatByChannel::ChannelScalers::instance();
  scstat = SignalStatByChannel::ChannelScalSta::instance();
  GetRawEventId* gi = GetRawEventId::instance();
  for ( int id: gi->getChannels() ) scaler->setActiveChan( id, false );
}


SignalStatByChannel::~SignalStatByChannel() {
}


// function to be called for each event
void SignalStatByChannel::update( const Event& ev ) {
//  cout << "SignalStatByChannel::update" << endl;
  scaler->eventCount( 0 );
  return;
}

// return the results
unsigned int SignalStatByChannel::eventCount( int channel ) {
  return scaler->eventCount( channel );
}


const std::vector<double>& SignalStatByChannel::signalMean( int channel ) {
  return scstat->signalMean( channel );
}


const std::vector<double>& SignalStatByChannel::signalRMS( int channel ) {
  return scstat->signalRMS( channel );
}


void SignalStatByChannel::ChannelScalers::update( const Event& ev ) {
//  cout << "SignalStatByChannel::ChannelScalers::update" << endl;
  const vector<int>& channels = ev.channels();
  static bool init = true;
  if ( init ) {
    int samplePerChan = ev.acquisitionHeader()->sample_per_chan;
//    cout << "samplePerChan " << samplePerChan << endl;
    counter.resize( Event::maxChannelNumber, 0 );
    sOff.resize( Event::maxChannelNumber );
    sSum.resize( Event::maxChannelNumber );
    sSqr.resize( Event::maxChannelNumber );
    unsigned int i;
    for ( i = 0; i < Event::maxChannelNumber; ++i ) {
      sOff[i].resize( samplePerChan, -1 );
      sSum[i].resize( samplePerChan, 0 );
      sSqr[i].resize( samplePerChan, 0 );
    }
    init = false;
  }
  static GetMax      * gm = GetMax      ::instance();
  static GetPedestal * gp = GetPedestal ::instance();
  static GetSignal   * gs = GetSignal   ::instance();
  for ( int i: channels ) {
    if ( !activeChan[i] ) continue;
    int posMax = sPos[i];
    if ( posMax && ( gm->getPos( i ) != posMax ) ) continue;
    Event::data_type sigMax = gm->getVal( i ) - gp->getPedestal( i );
    if ( sigMax < sMin[i] ) continue;
    if ( sigMax > sMax[i] ) continue;
    const std::vector<Event::data_type>& signal = gs->signal( i );
    std::vector<double>& sigOff = sOff[i];
    std::vector<double>& sigSum = sSum[i];
    std::vector<double>& sigSqr = sSqr[i];
    ++counter[i];
    int n = signal.size();
    int j;
    for ( j = 0; j < n; ++j ) {
      double sig = signal[j];
      if ( sigOff[j] < 0 ) {
        sigOff[j] = sig;
        continue;
      }
      sig -= sigOff[j];
      sigSum[j] += sig;
      sigSqr[j] += sig * sig;
    }
  }
  return;
}


SignalStatByChannel::ChannelScalers::ChannelScalers() {
  activeChan.resize( Event::maxChannelNumber, true );
  sPos.resize( Event::maxChannelNumber,  0 );
  sMin.resize( Event::maxChannelNumber,  0 );
  sMax.resize( Event::maxChannelNumber, -1 );
  UserParametersManager* uPar =
    PolymorphicSingleton<UserParametersManager>::instance();
  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; ++i ) {
    uPar->getUserParameter( "signalStatByChannelPos_" + to_string( i ),
                            sPos[i] );
    uPar->getUserParameter( "signalStatByChannelMin_" + to_string( i ),
                            sMin[i] );
    uPar->getUserParameter( "signalStatByChannelMax_" + to_string( i ),
                            sMax[i] );
  }
}


void SignalStatByChannel::ChannelScalers::setActiveChan( int channel, bool active ) {
  activeChan[channel] = active;
  return;
}


// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
unsigned int SignalStatByChannel::ChannelScalers::eventCount( int channel ) {
  check();
  return counter[channel];
}


const std::vector<double>& SignalStatByChannel::ChannelScalers::signalOff( int channel ) {
  return sOff[channel];
}



const std::vector<double>& SignalStatByChannel::ChannelScalers::signalSum( int channel ) {
  return sSum[channel];
}


const std::vector<double>& SignalStatByChannel::ChannelScalers::signalSqr( int channel ) {
  return sSqr[channel];
}


void SignalStatByChannel::ChannelScalSta::update( const Event& ev ) {
//  cout << "SignalStatByChannel::ChannelScalSta::update" << endl;
  static SignalStatByChannel::ChannelScalers* scaler =
         SignalStatByChannel::ChannelScalers::instance();
  sigMea.resize( Event::maxChannelNumber );
  sigRMS.resize( Event::maxChannelNumber );
  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; ++i ) {
    const std::vector<double>& sOff = scaler->signalOff( i );
    const std::vector<double>& sSum = scaler->signalSum( i );
    const std::vector<double>& sSqr = scaler->signalSqr( i );
    static int samplePerChan = sOff.size();
    std::vector<double>& sMea = sigMea[i];
    std::vector<double>& sRMS = sigRMS[i];
    sMea.resize( samplePerChan, 0 );
    sRMS.resize( samplePerChan, 0 );
    int count = scaler->eventCount( i );
    if ( !count ) continue;
    int j;
    for ( j = 0; j < samplePerChan; ++j ) {
      double mea =   sSum[j] / count;
      double var = ( sSqr[j] / count ) - ( mea * mea );
      sMea[j] = mea + sOff[j];
      sRMS[j] = ( var > 0 ? sqrt( var ) : 0 );
    }
  }
  return;
}


SignalStatByChannel::ChannelScalSta::ChannelScalSta() {
}


const std::vector<double>& SignalStatByChannel::ChannelScalSta::signalMean( int channel ) {
  check();
  return sigMea[channel];
}


const std::vector<double>& SignalStatByChannel::ChannelScalSta::signalRMS( int channel ) {
//  cout << "SignalStatByChannel::ChannelScalers::signalSqr" << endl;
  check();
  return sigRMS[channel];
}

