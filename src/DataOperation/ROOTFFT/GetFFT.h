#ifndef DataOperation_ROOTFFT_GetFFT_h
#define DataOperation_ROOTFFT_GetFFt_h

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <TFFTComplex.h>

#include <iostream>
#include <vector>
#include <memory>

class UserParametersManager;
class Event;

class GetFFT: public Singleton<GetFFT>,
              public LazyObserver<Event> {

  friend class Singleton<GetFFT>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetFFT           ( const GetFFT& x ) = delete;
  GetFFT& operator=( const GetFFT& x ) = delete;

  ~GetFFT() override;

  const std::vector<double>& getFFT( int channel );
  const std::vector<double>& getFFTOfSum();

  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  GetFFT();
  
  std::unique_ptr<TFFTComplex> fft;
  std::vector<std::vector<double>> fftVec;
  std::vector<double> fftSum;
  
  std::string windowFunction;

};

#endif // DataOperation_ROOTFFT_GetFFT_h

