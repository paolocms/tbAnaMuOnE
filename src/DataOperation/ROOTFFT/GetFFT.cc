#include "DataOperation/ROOTFFT/GetFFT.h"
#include "DataOperation/GetSignal.h"

#include "Framework/Event.h"

#include "Utilities/PolymorphicSingleton.h"

#include "NtuTool/Common/interface/UserParametersManager.h"

#include <TMath.h>

using namespace std;

GetFFT::GetFFT() {
  auto uPar = PolymorphicSingleton<UserParametersManager>::instance();

  windowFunction = "none";
  windowFunction = uPar->getUserParameter("fftWindowFunction");
  if (windowFunction != "none" and windowFunction != "hann" and windowFunction != "hamming") {
    throw std::invalid_argument("Invalind fft window function name " + windowFunction);
  }
  
  
  fftVec.resize(Event::maxChannelNumber);
}


GetFFT::~GetFFT() {
}


// function to be called for each event
void GetFFT::update( const Event& ev ) {  

  static GetSignal* gs = GetSignal::instance();

  unsigned int count = gs->signal( ev.channels().front() ).size();
  int icount = count;

  if ( !fft || ( fft->GetN()[0] != icount ) ) {
    fft.reset(new TFFTComplex(gs->signal( ev.channels().front() ).size(), false));
    fft->Init("measure", 1, nullptr);
  }
  
  vector<double> zero(count, 0);
  vector<double> sumY(count, 0);
  for (int iFeb: ev.channels()) {
    
    vector<double> chanSignal(gs->signal(iFeb).size());
    // why r-value reference?
//    auto&& signal = gs->signal(iFeb);
    auto& signal = gs->signal(iFeb);
    size_t nSamp = chanSignal.size();
    for( size_t iSamp = 0; iSamp < nSamp; iSamp++) {
      chanSignal[iSamp] = signal[iSamp];
      if (windowFunction == "hann") {
        chanSignal[iSamp] *= 0.5*(1 - cos(2*TMath::Pi()*iSamp/nSamp));
      }
      else if (windowFunction == "hamming") {
        chanSignal[iSamp] *= (25./46 - 21./46*cos(2*TMath::Pi()*iSamp/nSamp));
      }
      sumY[iSamp] += signal[iSamp];
    }
  
    fftVec[iFeb].clear();
    fftVec[iFeb].resize(gs->signal(iFeb).size());
    
    fft->SetPointsComplex(chanSignal.data(), zero.data());
    fft->Transform();
    
    vector<double> fftRe(count);
    vector<double> fftIm(count);
    
    fft->GetPointsComplex(fftRe.data(), fftIm.data());
    
    for (size_t iSamp = 0; iSamp < count; iSamp++) {
      fftVec[iFeb][iSamp] = hypot(fftRe[iSamp], fftIm[iSamp]);
    }
  }
  
  fftSum.clear();
  fftSum.resize(count);
  
  if (windowFunction == "hann") {
    size_t nSamp = sumY.size();
    for (size_t iSamp = 0; iSamp < count; iSamp++) {
      sumY[iSamp] *= 0.5*(1 - cos(2*TMath::Pi()*iSamp/nSamp));
    }
  }
  else if (windowFunction == "hamming") {
    size_t nSamp = sumY.size();
    for (size_t iSamp = 0; iSamp < count; iSamp++) {
      sumY[iSamp] *= (25./46 - 21./46*cos(2*TMath::Pi()*iSamp/nSamp));
    }
  }
  
  fft->SetPointsComplex(sumY.data(), zero.data());
  fft->Transform();
  
  vector<double> fftRe(count);
  vector<double> fftIm(count);
  
  fft->GetPointsComplex(fftRe.data(), fftIm.data());
  
  for (size_t iSamp = 0; iSamp < count; iSamp++) {
    fftSum[iSamp] = hypot(fftRe[iSamp], fftIm[iSamp]);
  }
   

}


const vector<double>& GetFFT::getFFT( int channel ) {
  check();
  return fftVec[channel];
}


const vector<double>& GetFFT::getFFTOfSum() {
  check();
  return fftSum;
}

