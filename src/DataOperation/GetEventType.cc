#include "DataOperation/GetEventType.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetLaserMonitor.h"
#include "DataOperation/GetBunchCount.h"

#include "Framework/Event.h"
#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/PolymorphicSingleton.h"

using namespace std;

#define FILL_EVTMAP(M,T) M[T]=#T

GetEventType::GetEventType():
 classMode( bySignal ),
 maxMinRatioThreshold( 10 ),
 minSigToRMSThreshold( 20 ),
 maxSigToRMSThreshold(  5 ) {
  static GetRawEventId  * gi = GetRawEventId::instance();
  static GetLaserMonitor* gl = GetLaserMonitor::instance();
  skippedChannels.insert( gi->getChannels().begin(), gi->getChannels().end() );
  skippedChannels.insert( gl->getChannels().begin(), gl->getChannels().end() );
  static const UserParametersManager*
               uPar = PolymorphicSingleton<UserParametersManager>::instance();
  uPar->getUserParameter( "evType_maxMinRatioThreshold", maxMinRatioThreshold );
  uPar->getUserParameter( "evType_minSigToRMSThreshold", minSigToRMSThreshold );
  uPar->getUserParameter( "evType_maxSigToRMSThreshold", maxSigToRMSThreshold );
  maskChannel.resize( Event::maxChannelNumber, false );
  unsigned int iChan;
  for ( iChan = 0; iChan < Event::maxChannelNumber; ++iChan ) {
    bool cm = maskChannel[iChan];
    uPar->getUserParameter( string( "evType_maskChannel_" ) +
                         to_string( iChan ), cm );
    maskChannel[iChan] = cm;
  }
  map<string,mode> m;
  for ( auto& e: modeName() ) m[e.second] = e.first;
  string ecm = uPar->getUserParameter( "evType_classificationMode" );
  if ( ecm != "" ) {
    auto e = m.find( ecm );
    if ( e != m.end() ) classMode = e->second;
  }
//  auto mmm = modeName();
//  cout << "CLASSIFICATION MODE: " << mmm[classMode] << endl;
}


GetEventType::~GetEventType() {
}


const vector<GetEventType::type>& GetEventType::typeList() {
  static vector<type> v EVENT_TYPES;
  return v;
}


const vector<GetEventType::mode>& GetEventType::modeList() {
  static vector<mode> v CLASS_MODES;
  return v;
}


const map<GetEventType::type,string>& GetEventType::typeName() {
  static map<type,string>* m = [](){
    map<type,string>* p = new map<type,string>;
    map<type,string>& r = *p;
    FILL_EVTMAP( r, unclassified );
    FILL_EVTMAP( r, noise );
    FILL_EVTMAP( r, laser );
    FILL_EVTMAP( r, beam );
    return p;
  }();
  return *m;
}


const map<GetEventType::mode,string>& GetEventType::modeName() {
  static map<mode,string>* m = [](){
    map<mode,string>* p = new map<mode,string>;
    map<mode,string>& r = *p;
    FILL_EVTMAP( r, bySignal );
    FILL_EVTMAP( r, byDelay );
    return p;
  }();
  return *m;
}


// function to be called for each event to compute the result(s),
// it's actually called (automatically) the first time any of the results
// is requested
void GetEventType::update( const Event& ev ) {
//  cout << "GetEventType::update begin" << endl;
  static GetBunchCount* getBC = GetBunchCount::instance();
  eventType = unclassified;
//  return;

  if ( classMode == byDelay ) {

    unsigned long long delay = getBC->fromLast() / 40;
    if ( ( delay > 90000 ) && ( delay < 110000 ) ) eventType = laser;
    else
    if ( ( delay >   500 ) && ( delay <   1000 ) ) eventType = beam;
    else                                           eventType = unclassified;

  }
  else
  if ( classMode == bySignal ) {

    // make use of other "operation" classes, in this case:
    // - "GetMax" that find the number of the sample having the largest
    //    number of counts and the number of counts itself;
    // - "GetPedestal" that finds the "pedestal" value, and its RMS,
    //   for all channels
    static GetMax     * gm = GetMax     ::instance();
    static GetPedestal* gp = GetPedestal::instance();

    eventType = unclassified;
    double minSignal = 1.0e+9;
    double maxSignal = 0.0;
    double minSToRMS = 1.0e+9;
    double maxSToRMS = 0.0;
    static set<int>::const_iterator iend = skippedChannels.end();
    for ( int i: ev.channels() ) { // loop over channels
      if ( maskChannel[i] ) continue;
      if ( skippedChannels.find( i ) != iend ) continue;
     // get the max. value, pedestal subtracted, for this channel
       double s = gm->getVal( i ) - gp->getPedestal( i );
      // get the signal to rms ratio
      double r = s / gp->getPedestalRMS( i );
      // get min and max values
      if ( s < minSignal ) minSignal = s;
      if ( s > maxSignal ) maxSignal = s;
      if ( r < minSToRMS ) minSToRMS = r;
      if ( r > maxSToRMS ) maxSToRMS = r;
    }

    double signRatio = maxSignal / minSignal;

    if ( ( signRatio < maxMinRatioThreshold ) &&
         ( minSToRMS > minSigToRMSThreshold ) &&
         ( maxSToRMS > maxSigToRMSThreshold ) ) eventType = laser;
    else
    if ( ( signRatio > maxMinRatioThreshold ) &&
         ( minSToRMS < minSigToRMSThreshold ) &&
         ( maxSToRMS > maxSigToRMSThreshold ) ) eventType = beam;
    else
    if (   maxSToRMS < maxSigToRMSThreshold   ) eventType = noise;
  }

//  auto mmm = typeName();
//  cout << "EVENT TYPE: " << mmm[eventType] << endl;
//  cout << "GetEventType::update end" << endl;
  return;

}

// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
GetEventType::type GetEventType::getType() {
  check();
  return eventType;
}

