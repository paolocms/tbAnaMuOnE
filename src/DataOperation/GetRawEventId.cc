#include "DataOperation/GetRawEventId.h"

#include "DataOperation/GetSignal.h"

#include "Framework/Event.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

using namespace std;

GetRawEventId::GetRawEventId() {
  channelOL = 13;
  channelOH = 14;
  channelBC = 15;
  UserParametersManager* uPar =
    PolymorphicSingleton<UserParametersManager>::instance();
  uPar->GET_USER_PARAMETER( channelOL );
  uPar->GET_USER_PARAMETER( channelOH );
  uPar->GET_USER_PARAMETER( channelBC );
  channels.insert( channelOL );
  channels.insert( channelOH );
  channels.insert( channelBC );
}


GetRawEventId::~GetRawEventId() {
}


// function to be called for each event to compute the result(s),
// it's actually called (automatically) the first time any of the results
// is requested
void GetRawEventId::update( const Event& ev ) {

  // make use of other "operation" classes, in this case:
  // - "GetSignal" that realigns data for all channels
  static GetSignal* gs = GetSignal::instance();

  const std::vector<Event::data_type>& ols = gs->signal( channelOL );
  const std::vector<Event::data_type>& ohs = gs->signal( channelOH );
  const std::vector<Event::data_type>& bcs = gs->signal( channelBC );

  unsigned int nol = ols.size();
  unsigned int noh = ohs.size();
  unsigned int nbc = bcs.size();
  unsigned int nnn = max( nol, max( noh, nbc ) );

  ocId.resize( nnn, 0 );
  bcId.resize( nnn, 0 );

  unsigned int i;
  for ( i = 0; ( i < nol ) && ( i < noh ); ++i ) ocId[i] = unpack( ols[i],
                                                                   ohs[i] );
//  for ( i = 0; ( i < nol ) && ( i < noh ); ++i ) {
//    unsigned int l = ols[i];
//    unsigned int h = ohs[i];
//    ocId[i] = ( ( ( h & 0x0000ffff ) << 16 ) & 0xffff0000 ) |
//                  ( l & 0x0000ffff );
//  }
//  while ( i < nnn ) ocId[i++] = 0;
//  while ( i < nol ) ocId[i++] = 0;
//  while ( i < noh ) ocId[i++] = 0;
//  while ( i < nbc ) ocId[i++] = 0;

  for ( i = 0; i < nbc; ++i ) bcId[i] = unpack( bcs[i] );
//  for ( i = 0; i < nbc; ++i ) {
//    bcId[i] = bcs[i];
//    bcId[i] &= 0x0000ffff;
//  }
//  while ( i < nnn ) bcId[i++] = 0;
//  while ( i < nol ) bcId[i++] = 0;
//  while ( i < noh ) bcId[i++] = 0;
//  while ( i < nbc ) bcId[i++] = 0;

  return;

}

// return the results: the "check()" functions take care of automatically
// call the "update(...)" function when the cached results come from
// an Event that does not correspond any more to the current one
const vector<unsigned int>& GetRawEventId::getOC() {
  check();
  return ocId;
}

const vector<unsigned int>& GetRawEventId::getBC() {
  check();
  return bcId;
}

const set<int>& GetRawEventId::getChannels() {
  return channels;
}


unsigned int GetRawEventId::unpack( Event::data_type l, Event::data_type h ) {
  unsigned int ll = l;
  unsigned int hh = h;
  return ( ( ( hh & 0x0000ffff ) << 16 ) & 0xffff0000 ) |
             ( ll & 0x0000ffff );
}


unsigned int GetRawEventId::unpack( Event::data_type x ) {
  unsigned int xx = x;
  return ( xx & 0x0000ffff );
}


Event::data_type GetRawEventId::pack( unsigned int x ) {
  return x & 0x0000ffff;
}


Event::data_type GetRawEventId::pack( unsigned int x,
                                      GetRawEventId::wordPart p ) {
  switch ( p ) {
  case low:
    return   x         & 0x0000ffff;
  case high:
    return ( x >> 16 ) & 0x0000ffff;
  }
  return 0;
}


Event::data_type GetRawEventId::orbitCount( const char* dataSample,
                                            GetRawEventId::wordPart p ) {
  switch ( p ) {
  case low:
    return *reinterpret_cast<const Event::data_type*>(
            dataSample + GetSignal::oBytePos );
  case high:
    return *reinterpret_cast<const Event::data_type*>(
            dataSample + GetSignal::oBytePos
                       + sizeof( Event::data_type ) );
  }
  return 0;
}


Event::data_type GetRawEventId::bunchCount( const char* dataSample ) {
  return unpack( *reinterpret_cast<const Event::data_type*>(
                  dataSample + GetSignal::bBytePos ) );
}


unsigned int GetRawEventId::orbitCount( const char* dataSample ) {
  return unpack( *reinterpret_cast<const Event::data_type*>(
                  dataSample + GetSignal::oBytePos ),
                 *reinterpret_cast<const Event::data_type*>(
                  dataSample + GetSignal::oBytePos
                             + sizeof( Event::data_type ) ) );
}

