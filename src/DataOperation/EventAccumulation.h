#ifndef DataOperation_EventAccumulation_h
#define DataOperation_EventAccumulation_h

//#include "Framework/Event.h"

#include "DataOperation/GetEventType.h"
#include "Utilities/ActiveObserver.h"
#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <map>
#include <cmath>

class Event;

// The sums over events must be done for all events and not just for the
// ones chosen for a display, if any, so that it's implemented as an
// "ActiveObserver"; on the other side the sums must be updated before
// any other observer needs them, so that the actual sum is done in a
// nested class implemented as a "LazyObserver", notified before all the
// "ActiveObserver"s, and its actual update for all events is forced by a
// call in the containing class
class EventAccumulation: public Singleton<EventAccumulation>,
                         public ActiveObserver<Event> {

  friend class Singleton<EventAccumulation>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  EventAccumulation           ( const EventAccumulation& x ) = delete;
  EventAccumulation& operator=( const EventAccumulation& x ) = delete;

  ~EventAccumulation() override;

  enum type { raw, pedestalSubtracted };

  unsigned int eventCount( GetEventType::type evtType =
                           GetEventType::unclassified );
  const std::vector<double>& signal( int channel,
                                     GetEventType::type evtType =
                                     GetEventType::unclassified,
                                     type sumType = pedestalSubtracted );
  const std::vector<double>& signalSum( GetEventType::type evtType =
                                        GetEventType::unclassified,
                                        type sumType = pedestalSubtracted );
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // private constructor being a singleton
  EventAccumulation();

  // nested class actually doing the sums, being a "LazyObserver" to ensure
  // it's notified before all "ActiveObserver"s
  class ChannelScalers: public Singleton<EventAccumulation::ChannelScalers>,
                        public LazyObserver<Event> {
    friend class Singleton<EventAccumulation::ChannelScalers>;
   public:
    // activate/deactivate channels
    void setActiveChan( int channel, bool active );
    // function to get the results
    unsigned int eventCount( GetEventType::type evtType =
                             GetEventType::unclassified );
    const std::vector<double>& signal( int channel,
                                       GetEventType::type evtType,
                                       type sumType );
    const std::vector<double>& signalSum( GetEventType::type evtType,
                                          type sumType );
    // function to be called for each event
    void update( const Event& ev ) override;
   private:
    ChannelScalers();
    // results of the operation
    std::vector<bool> activeChan;
    std::map<GetEventType::type,unsigned int> counter;
    std::map<GetEventType::type,std::vector<std::vector<double>>> yAcc;
    std::map<GetEventType::type,std::vector<double>> yAccSum;
    std::map<GetEventType::type,std::vector<std::vector<double>>> zAcc;
    std::map<GetEventType::type,std::vector<double>> zAccSum;
  };
  ChannelScalers* scaler;

};

#endif // DataOperation_EventAccumulation_h

