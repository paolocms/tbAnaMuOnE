#ifndef DataOperation_GetRawEventId_h
#define DataOperation_GetRawEventId_h

#include "Framework/Event.h"
#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"

#include <iostream>
#include <vector>
#include <set>

class Event;

// Example of class doing an operation potentially needed by several parts
// of the analysis, designed as described in the following:
// - it's a "Singleton", i.e. an object existing with a single instance,
//   accessible everywhere throughout the program with the instruction
//   static GetRawEventId* op = GetRawEventId::instance();
// - it's a "LazyObserver", i.e. for each Event it computes its result
//   the first time it's actually needed, then it saves that results and
//   returns it if it's needed again in any other, or the same, part of
//   the program;
// - any operation can use the result(s) of other ones, and the evaluation
//   order is automaticall guaranteed.


class GetRawEventId: public Singleton<GetRawEventId>,
                     public LazyObserver<Event> {

  // being a singleton, the constructor is private but the "Singleton"
  // base class must access it, so it's declared friend
  friend class Singleton<GetRawEventId>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  GetRawEventId           ( const GetRawEventId& x ) = delete;
  GetRawEventId& operator=( const GetRawEventId& x ) = delete;

  ~GetRawEventId() override;

  enum wordPart { low, high };

  // function returning the result, more functions may exist, to return
  // different results related together, i.e. coming from a single
  // set of instructions; in this example the channel giving the max
  // signal and its position and value are found
  const std::vector<unsigned int>& getOC();
  const std::vector<unsigned int>& getBC();
  const std::set<int>& getChannels();

  int getChannelOL() const { return channelOL; }
  int getChannelOH() const { return channelOH; }
  int getChannelBC() const { return channelBC; }

  // function to be called for each event to compute the result(s)
  void update( const Event& ev ) override;

  static unsigned int unpack( Event::data_type l, Event::data_type h );
  static unsigned int unpack( Event::data_type x );
  static Event::data_type pack( unsigned int x );
  static Event::data_type pack( unsigned int x, wordPart p );
  static Event::data_type orbitCount( const char* dataSample, wordPart p );
  static Event::data_type bunchCount( const char* dataSample );
  static unsigned int orbitCount( const char* dataSample );


 private:

  // private constructor being a singleton
  GetRawEventId();

  int channelOL;
  int channelOH;
  int channelBC;
  std::set<int> channels;

  std::vector<unsigned int> ocId;
  std::vector<unsigned int> bcId;

};

#endif // DataOperation_GetRawEventId_h

