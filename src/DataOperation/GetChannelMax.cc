#include "DataOperation/GetChannelMax.h"
//#include "DataOperation/GetSignal.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetRawEventId.h"

#include "Framework/Event.h"
#include "Utilities/ArrayStatistic.h"

using namespace std;

GetChannelMax::GetChannelMax() {
}


GetChannelMax::~GetChannelMax() {
}


// function to be called for each event
void GetChannelMax::update( const Event& ev ) {

//  static GetSignal  * gs = GetSignal  ::instance();
  static GetPedestal  * gp = GetPedestal  ::instance();
  static GetMax       * gm = GetMax       ::instance();
  static GetRawEventId* gi = GetRawEventId::instance();
/*
  cha = -1;
  pos = -1;
  val = 0.0;
  for ( int i: ev.channels() ) {
    double cv = gm->getVal( i ) - gp->getPedestal( i );
    if ( cv > val ) {
      cha = i;
      pos = gm->getPos( i );
      val = cv;
    }
  }
*/
  auto cm = ArrayStatistic::getChannelMax( ev.channels(), gi->getChannels(),
                                                          gm->getFunction(),
                                                          gp->getFunction() );
  cha = std::get<0>( cm );
  pos = std::get<1>( cm );
  val = std::get<2>( cm );
  sum = std::get<3>( cm );
  return;

}


int GetChannelMax::getCha() {
  check();
  return cha;
}


int GetChannelMax::getPos() {
  check();
  return pos;
}


double GetChannelMax::getVal() {
  check();
  return val;
}


double GetChannelMax::getSum() {
  check();
  return sum;
}

