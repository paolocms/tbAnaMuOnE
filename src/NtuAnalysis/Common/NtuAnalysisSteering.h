#ifndef NtuAnalysis_Common_NtuAnalysisSteering_h
#define NtuAnalysis_Common_NtuAnalysisSteering_h

#include "Framework/EventSource.h"
#include "Utilities/ActiveObserver.h"
#include "Utilities/IdentifiableObject.h"
#include "NtuTool/Common/interface/UserParametersManager.h"
#include "NtuTool/Common/interface/TreeWrapper.h"

#include <functional>

class Event;

template<class T>
class NtuAnalysisSteering: //public ActiveObserver<T>,
                           public virtual IdentifiableObject {

//  friend void Dispatcher<Event>::notify( const T& x );

 public:

  typedef T ntuple_data;
  NtuAnalysisSteering( const UserParametersManager* userPar ):
   uPar( userPar ),
   refreshCount(    0 ),
   refreshScale( 1000 ) {
    uPar->getUserParameter( "refreshScale", refreshScale );
//  NtuAnalysisSteering( const UserParametersManager* userPar,
//                       TFile** filePtr ):
//   uPar( userPar ),
//   currentFPtr( filePtr ) {
//    obsMap()[this] = this;
  }
  // deleted copy constructor and assignment to prevent unadvertent copy
  NtuAnalysisSteering           ( const NtuAnalysisSteering& x ) = delete;
  NtuAnalysisSteering& operator=( const NtuAnalysisSteering& x ) = delete;

  ~NtuAnalysisSteering() override {
//    auto& m = obsMap();
//    auto i = m.find( this );
//    if ( i != m.end() ) m.erase( i );
  }

//  enum ParameterReplicaGranularity { common, byReplica };

  // function to be called at execution start / end
//  void update( const EventSource::AnalysisStatus& status ) override;

  // function to be called at execution start
  virtual void beginJob() = 0;
  // histograms creation
  virtual void book() = 0;
  // data analysis
//  void setCounters( int ientry, int ievent_file, int ievent_tot,
//                    TFile* file ) {
//    entry      = ientry;
//    event_file = ievent_file;
//    event_tot  = ievent_tot;
//    currentFile = file;
//  }
//  void update( const T& ntuData ) override {
//    currentFile = *currentFPtr;
//    analyze( ntuData );
//  }
//  virtual bool analyze( const T& ntuData ) = 0;
  virtual bool analyze( int ientry, int ievent_file, int ievent_tot,
                        TFile* file, const T& ntuData ) = 0;
  // function to be called at execution end
  virtual void endJob() = 0;
  // draw progress plots
  virtual void plotUpdate() {}
  // draw plots
  virtual void modulePlot( bool& noPlot ) {
    histoPlotted = true;
    plot();
    if ( histoPlotted ) noPlot = false;
  }
  virtual void plot() {
    histoPlotted = false;
  }
  // save histograms
//  virtual void save() { autoSave(); }

  typedef std::function<void( TObject* o )> saveFunc;
//  static
  template <class A>
  void setObjSaver( A* as ) {
    saveFunc* sf = new saveFunc ( [as]( TObject* o ) { *as = o; } );
    setObjSaver( *sf );
  }
  void setObjSaver( saveFunc& objSaver ) {
    autoSavedObject.setObjS( objSaver );
  }

 protected:

  const UserParametersManager* uPar;
  int refreshCount;
  int refreshScale;

//  static int entry;
//  static int event_file;
//  static int event_tot;
//  TFile* currentFile;

  const std::string& keyword() const {
    return ObjectFactory<NtuAnalysisSteering>::getFactory( this )->keyword();
  }

//  template<class V>
//  void getAnalysisParameter( const std::string& k, V& v,
//                             ParameterReplicaGranularity g = common ) {
//    switch ( g ) {
//    default:
//    case IdentifiableObject::common:
//      uPar->
//      getUserParameter(       keyword() + '_' + k, v );
//      break;
//    case IdentifiableObject::byReplica:
//      this->
//      getUserParameter( uPar, keyword() + '_' + k, v );
//      break;
//    }
//  }

  class AutoSavedObject {
   public:
    AutoSavedObject& operator=( TObject* obj ) {
      if ( objS != nullptr ) (*objS)( obj );
      return *this;
    }
    void setObjS( saveFunc& objSaver ) {
      objS = &objSaver;
    }
   private:
    saveFunc* objS = nullptr;
  };
//  static
  AutoSavedObject autoSavedObject;

 private:

  TFile** currentFPtr;

  bool histoPlotted;

//  static std::map<ActiveObserver<Event>*,NtuAnalysisSteering*>& obsMap() {
//    static std::map<ActiveObserver<Event>*,NtuAnalysisSteering*> m;
//    return m;
//  }
//
//  unsigned int counter;
//  unsigned int factor;
//  unsigned int time;

};

//template<class T> int NtuAnalysisSteering<T>::entry;
//template<class T> int NtuAnalysisSteering<T>::event_file;
//template<class T> int NtuAnalysisSteering<T>::event_tot;
//template<class T> typename NtuAnalysisSteering<T>::AutoSavedObject
//                           NtuAnalysisSteering<T>::autoSavedObject;

#endif // NtuAnalysis_Common_NtuAnalysisSteering_h

