#ifndef NtuAnalysis_Common_MultipleAnalysis_h
#define NtuAnalysis_Common_MultipleAnalysis_h

#include "Framework/UserParametersCxxComment.h"
#include "NtuAnalysisSteering.h"
#include "NtuAnalysisFactory.h"
#include "Utilities/Dispatcher.h"

//#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
#include "NtuTool/Common/interface/TreeReader.h"

//#include "TH1.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"

#include <iostream>
//#include <vector>

#include "main.h"

//class LaserNoiseModule;
template <class T>
class NtuAnalysisSteering;

template <class NtuFormat>
class MultipleAnalysis: public NtuFormat,   // ntuple definition,
                        public UserParametersCxxComment, // to use a different
                                            // UserParametersManager (optional)
                        public TreeReader { // create branches and
                                            // read ntuple from file

 public:

  MultipleAnalysis(){
    nms.reserve( 10 );
  }
  // deleted copy constructor and assignment to prevent unadvertent copy
  MultipleAnalysis           ( const MultipleAnalysis& x ) = delete;
  MultipleAnalysis& operator=( const MultipleAnalysis& x ) = delete;

  // Operations to be done at the execution begin
  void beginJob() override { // virtual function defined in TreeWrapper
    this->setup();           // define ntuple structure (in NtuFormat)
    nms = NtuAnalysisFactory<typename NtuFormat::Data>::create( this );
    std::cout << "MODULES: " << nms.size() << std::endl;
    for ( auto nmp: nms ) nmp->beginJob();
    return;
  }

  // Histograms creation
  void book() override { // virtual function defined in TreeWrapper
    for ( auto nmp: nms ) {
      nmp->setObjSaver( &autoSavedObject );
      nmp->book();
    }
  }

  // Data analysis
  bool analyze( int entry, int event_file, int event_tot ) override {
                // virtual function defined in TreeWrapper
    Dispatcher<typename NtuFormat::Data>::notify( *this );
    TFile* file = dynamic_cast<TFile*>( gROOT
                                        ->GetListOfFiles()
                                        ->FindObject( currentFile.c_str() ) );
    for ( auto nmp: nms ) nmp->analyze( entry, event_file, event_tot,
                                        file, *this );
    return true;
  }

  // Operations to be done at the execution end
  void endJob() override { // virtual function defined in TreeWrapper
    cout << "MultipleAnalysis::endJob" << endl;
    for ( auto nmp: nms ) nmp->endJob();
  }

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override {
    bool noPlot = true;
    for ( auto nmp: nms ) nmp->modulePlot( noPlot );
    if ( noPlot ) TreeReader::plot();
  }

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  std::vector<NtuAnalysisSteering<typename NtuFormat::Data>*> nms;

};

#endif // NtuAnalysis_Common_MultipleAnalysis_h
