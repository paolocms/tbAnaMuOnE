#ifndef NtuAnalysis_Common_NtupleBase_h
#define NtuAnalysis_Common_NtupleBase_h

template <class NtuData>
class NtupleBase: protected virtual NtuData {
 public:
  typedef NtuData Data;
};

#endif // NtuAnalysis_Common_NtupleBase_h
