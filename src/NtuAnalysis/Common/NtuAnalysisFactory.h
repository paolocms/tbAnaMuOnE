#ifndef NtuAnalysis_Common_NtuAnalysisFactory_h
#define NtuAnalysis_Common_NtuAnalysisFactory_h

#include "Utilities/ObjectFactory.h"
#include "Utilities/PolymorphicSingleton.h"
//#include "Framework/ObjectFactory.h"
#include "NtuAnalysisSteering.h"

#include <string>
#include <vector>
#include <map>

class AnalysisSteering;
//class UserParametersManager;
//template <class T> class Dispatcher;
//template <class T> class ActiveObserver;
//class Event;

template<class T>
class NtuAnalysisFactory: public ObjectFactory<NtuAnalysisSteering<T>> {

//  friend class Dispatcher<Event>;

 public:

  NtuAnalysisFactory() = default;
  // deleted copy constructor and assignment to prevent unadvertent copy
  NtuAnalysisFactory           ( const NtuAnalysisFactory& x ) = delete;
  NtuAnalysisFactory& operator=( const NtuAnalysisFactory& x ) = delete;

  virtual ~NtuAnalysisFactory() = default;

/*
  // create all requested analysis objects
  static std::vector<NtuAnalysisSteering<T>*> create( const UserParametersManager* userPar ) {
    std::vector<NtuAnalysisSteering<T>*> aList;
    // loop over analysis object factories
    static std::map<std::string,
                    typename ObjectFactory<NtuAnalysisSteering<T>>::AbsFactory*>* fm =
                             ObjectFactory<NtuAnalysisSteering<T>>::factoryMap();
    for ( const auto& element: *fm ) {
      // create analysis object if its name is listed in the command line
      if ( ( userPar->   getUserParameter( element.first ) != "" ) ||
           ( userPar->hasNoValueParameter( element.first )       ) ) {
        int n = 1;
        PolymorphicSingleton<UserParametersManager>::instance()->
            getUserParameter( element.second->keyword() + "_replica", n );
        if ( n <= 0 ) continue;
        auto a = element.second->create( userPar );
        aList.push_back( a );
        if ( !a->isReplicable() ) continue;
        while ( --n )
        aList.push_back( element.second->create( userPar ) );
      }
    }
    return aList;
  }
*/

};

#endif // NtuAnalysis_Common_NtuAnalysisFactory_h

