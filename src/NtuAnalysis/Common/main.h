#include "NtuTool/Common/interface/TreeStandardAnalyzer.h"

//#include "TH1.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TROOT.h"

#include <iostream>

using namespace std;
//static EqualSignParser esp;

class TreeCustomAnalyzer: public TreeStandardAnalyzer {
 public:
  TreeCustomAnalyzer() {}
  ~TreeCustomAnalyzer() override = default;
  int loop( TreeReader* tr, std::ifstream& treeListFile,
            int evtmax, int evskip, int accmax, bool anaexe = true ) override {
    const vector<string>& nvp = tr->getNoValueParameters();
    string treeFileName( nvp.empty() ? "ntu.root" : nvp[0] );
    TChain* c = tr->initRead( treeFileName );
    int evfile = tr->loop( evtmax, evskip, accmax, anaexe );
    delete c;
    return evfile;
  }
} tca;


int main( int argc, char* argv[] ) {

  gROOT->ProcessLine( "#include <vector>" );
  // Create an analyzer object
  cout << "create analyzer" << endl;
  TreeStandardAnalyzer* tsa = TreeStandardAnalyzer::getInstance();

  // Run the analyzer
  // No detailed instruction is needed, the sequence of call to the 
  // functions "beginJob()", "book()", "analyze(...)" and so on are 
  // done automatically inside the library.
  tsa->run( argc, argv );
  return 0;

}

