#include "../../Common/MultipleAnalysis.h"
#include "../Format/SimpleNtuple.h"

#include "../Modules/LaserNoiseModule.cc"
#include "../Modules/LaserMonitor.cc"
#include "../Modules/NoiseComponents.cc"
#include "../Modules/NoiseCorrelation.cc"

// It's essential to instantiate a global "MultipleAnalysis" object,
// so that it's created before the execution starts; it's then retrieved
// and used inside the library according to the needs.
static MultipleAnalysis<SimpleNtuple> reader;

