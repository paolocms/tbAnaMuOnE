#include "SimpleAnalysis.h"
#include "NtuTool/Common/interface/TreeStandardAnalyzer.h"

//#include "TH1.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"

#include <iostream>

#include "Common/main.h"

using namespace std;

SimpleAnalysis::SimpleAnalysis() {
  ASS_USER_PARAMETER( binPos, 100 );   // equivalent to
  ASS_USER_PARAMETER( minPos, 625 );   // setUserParameter("minPos",minPos=350);
  ASS_USER_PARAMETER( maxPos, 675 );   // (the value can be itself
  ASS_USER_PARAMETER( binValue, 100 ); //  an expression)
  ASS_USER_PARAMETER( minValue, 100 );
  ASS_USER_PARAMETER( maxValue, 200 );
}


// Operations to be done at the execution begin
void SimpleAnalysis::beginJob() {
  setup();                      // define ntuple structure (in SimpleNtuple)
  GET_USER_PARAMETER( binPos ); // equivalent to
  GET_USER_PARAMETER( minPos ); // getUserParameter( "minPos", minPos );
  GET_USER_PARAMETER( maxPos );
  GET_USER_PARAMETER( binValue );
  GET_USER_PARAMETER( minValue );
  GET_USER_PARAMETER( maxValue );
  return;
}

// Histograms creation
void SimpleAnalysis::book() { // virtual function defined in TreeWrapper
  // "autoSavedObject" is a tool to mark
  // histograms for automatic saving to output
  // file                    (in TreeWrapper )
  autoSavedObject =
  hMaxPos   = new TH1F( "maxPos"  , "maxPos"  ,
                        binPos   + 0.5, minPos   + 0.5, maxPos   + 0.5 );
  autoSavedObject =
  hMaxValue = new TH1F( "maxValue", "maxValue",
                        binValue + 0.5, minValue + 0.5, maxValue + 0.5 );
  return;
}

// Data analysis
bool SimpleAnalysis::analyze( int entry, int event_file, int event_tot ) {
                              // virtual function defined in TreeWrapper
  // Data are automatically reset and read from ntuple file for each
  // entry; no explicit call to GetEntry is needed here.
  // The 3 parameters are:
  //  - the entry number,
  //  - the number of entries actually processed in this ntuple file
  //  - the number of entries actually processed in the whole analysis
  int pos = 0;
  int value = -999999999;
  int sum = 0;
  unsigned int i;
  unsigned int n = nSig;
  for ( i = 0; i < n; ++i ) {
    int s = signal->at( i );
    if ( s > value ) {
      pos   = i;
      value = s;
    }
    sum += s;
  }
  hMaxPos  ->Fill( pos   );
  hMaxValue->Fill( value );
  return ( sum > 100 );
  // return "true" or "false" to accept or reject entries.
  // This value is essentially used to count events, but 
  // it can be used in other libraries implementing additional
  // functionalities.
}

// Operations to be done at the execution end
void SimpleAnalysis::endJob() { // virtual function defined in TreeWrapper
  // Any other operation to be done at the execution end can be put
  // in this function, apart on histogram saving to file that is to be put in
  // a dedicated function (unless relying on automatic saving, in that case
  // nothing is to be done).
  // A similar function "endFile()" can be declared and implemented
  // as overriding a virtual function defined in TreeWrapper, for any
  // operation to do when starting the analysis of a new ntuple file.
  return;
}

// Optional function to draw some plots at the execution end: see the
// INSTRUCTIONS file for details about running options.
// This function is executed before saving histograms, so any operation 
// done to them through the graphic interface will be persistent.
// This function can be simply left not declared and not implemented, but
// do not declare an empty function, otherwise execution could get stuck
// in some situations.
void SimpleAnalysis::plot() {
  TCanvas* can = new TCanvas( "can", "can", 800, 600 );
  can->Divide( 2, 1 );
  can->cd( 1 ); hMaxPos  ->Draw();
  can->cd( 2 ); hMaxValue->Draw();
  can->Print( "can.pdf" );
  return;
}

// It's essential to instantiate a global "SimpleAnalysis" object,
// so that it's created before the execution starts; it's then retrieved
// and used inside the library according to the needs.
static SimpleAnalysis reader;

