#ifndef NtuAnalysis_SimpleNtuple_Read_SimpleAnalysis_h
#define NtuAnalysis_SimpleNtuple_Read_SimpleAnalysis_h

#include "Framework/UserParametersCxxComment.h"

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
#include "NtuTool/Common/interface/TreeReader.h"

#include "TH1.h"

#include <iostream>
#include <vector>

class SimpleAnalysis: public SimpleNtuple,  // ntuple definition,
                      public UserParametersCxxComment, // to use a different
                                            // UserParametersManager (optional)
                      public TreeReader {   // create branches and
                                            //   read ntuple from file

 public:

  SimpleAnalysis();
  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleAnalysis           ( const SimpleAnalysis& x ) = delete;
  SimpleAnalysis& operator=( const SimpleAnalysis& x ) = delete;

  // Operations to be done at the execution begin
  void beginJob() override; // virtual function defined in TreeWrapper

  // Histograms creation
  void book() override;

  //  Optional preliminary processing
//  bool getEntry( int ientry ) override {
                               // virtual function defined in TreeWrapper:
                               // some preliminary processing can be done here,
                               // e.g. read just some branch and choose if
                               // this entry is to be kept or skipped without 
                               // spending the time to read the whole data
                               // structure
//    b_n_arr->GetEntry( ientry );       // read only n_arr from file
//    if ( n_arr < nCut ) return false;  // if less than a cut skip this entry
//    currentTree()->GetEntry( ientry ); // otherwise read the full data
//    return true;                       // and go on with the analysis
//  }

  // Data analysis
  bool analyze( int entry, int event_file, int event_tot ) override;
                // virtual function defined in TreeWrapper

  // Operations to be done at the execution end
  void endJob() override;

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override;

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  int   binPos;
  float minPos;
  float maxPos;
  int   binValue;
  float minValue;
  float maxValue;

  TH1F* hMaxPos;
  TH1F* hMaxValue;

};

#endif // NtuAnalysis_SimpleNtuple_Read_SimpleAnalysis_h
