#ifndef NtuAnalysis_SimpleNtuple_Modules_NoiseCorrelation_h
#define NtuAnalysis_SimpleNtuple_Modules_NoiseCorrelation_h

#include "Utilities/ActiveObserver.h"
#include "Utilities/Singleton.h"

//#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
//#include "NtuAnalysis/SimpleNtuple/Util/NtuChannelMap.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetPedestal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetChannelMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSums.h"

#include "../Format/SimpleNtuple.h"
#include "../Util/NtuChannelMap.h"
#include "../Util/SimpleNtupleGetSignal.h"
#include "../Util/SimpleNtupleGetRawEventId.h"
#include "../Util/SimpleNtupleGetMax.h"
#include "../Util/SimpleNtupleGetPedestal.h"
#include "../Util/SimpleNtupleGetChannelMax.h"
#include "../Util/SimpleNtupleGetSums.h"

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/ArrayStatistic.h"
#include "Utilities/PeakFitter.h"
//#include "Framework/Event.h"

#include "../../Common/NtuAnalysisSteering.h"
#include "../../Common/NtuAnalysisFactory.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TSystem.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <string>

class NoiseCorrelation: public NtuAnalysisSteering<SimpleNtupleData> {

 public:

//  static NoiseCorrelation* create( const std::string& key,
//                                   const UserParametersManager* userPar ) {
//    return nextInstance<NoiseCorrelation>( key, userPar );
//  }

  NoiseCorrelation( const UserParametersManager* userPar ):
   NtuAnalysisSteering<SimpleNtupleData>( userPar ),
   nChannel( SimpleNtupleData::nChannels ),
   nFEB( SimpleNtupleData::nFEB ),
   chFEB( lround( ceil( nChannel / nFEB ) ) ),
   febBounds( nFEB ),
   febMap( nFEB * chFEB ),
   colMap( nullptr ),
   rowMap( nullptr ) {
    std::cout << "NCHANNELS: " << nChannel << ' ' << nFEB  << ' ' << chFEB << std::endl;
//    unsigned int chFEB = lround( ceil( nChannel * 1.0 / chFEB ) );
    unsigned int fChan = 0;
    unsigned int lChan = chFEB;
    unsigned int iFEB;
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      auto& b = febBounds[iFEB];
      b.first  = fChan;
      b.second = lChan;
//      int iChan;
//      for ( iChan = fChan; iChan < lChan; ++iChan ) ;
//      fChan = lChan;
      while ( fChan < lChan ) febMap[fChan++] = iFEB;
      lChan = fChan + chFEB;
    }
    for ( auto& e: febBounds ) std::cout << "FEB bounds: " << e.first << ' ' << e.second << std::endl;
    for ( unsigned int i = 0; i < nChannel; ++i ) std::cout << "FEB: " << i << ' ' << febMap[i] << std::endl;
  };
  // deleted copy constructor and assignment to prevent unadvertent copy
  NoiseCorrelation           ( const NoiseCorrelation& x ) = delete;
  NoiseCorrelation& operator=( const NoiseCorrelation& x ) = delete;
  ~NoiseCorrelation() override = default;

  // Operations to be done at the execution begin
  void beginJob() override {
    colMap = rowMap = nullptr;
  }

  // Histograms creation
  void book() override {
    unsigned int iFEB;
    unsigned int iCha;
    unsigned int jCha;
    std::string nBase = "FEB";
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      auto& b = febBounds[iFEB];
      unsigned int fCha = b.first;
      unsigned int lCha = b.second;
      corrHist.resize( lCha );
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        for ( jCha = iCha + 1; jCha < lCha; ++jCha ) {
          std::string hName = nBase +
                              std::to_string( iFEB + 1 ) + "_" +
                              std::to_string( iCha     ) + "_" +
                              std::to_string( jCha     );
          std::cout << hName << std::endl;
          autoSavedObject = corrHist[iCha][jCha] =
          new TH1D( hName.c_str(), hName.c_str(), 128, -1.1, 1.1 );
        }
      }
    }
    return;
  }

  // Data analysis
  bool analyze( int ientry, int ievent_file, int ievent_tot,
                TFile* file, const SimpleNtupleData& ntuData ) override {
//    std::cout << "NoiseCorrelation::analyze " << ientry << std::endl;
    if ( ( colMap == nullptr ) ||
         ( rowMap == nullptr ) ) {
      file->GetObject( "colMap", colMap );
      file->GetObject( "rowMap", rowMap );
      NtuChannelMap::get( colMap, rowMap, cryMap, adcMap );
      nCrystal = adcMap.size();
      std::cout << adcMap.size() << " ADCMAP:";
      for ( int x: adcMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << cryMap.size() << " CRYMAP:";
      for ( int x: cryMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << adcMap.size() << " XXXMAP:";
      for ( int x: adcMap ) std::cout << ' ' << cryMap[x];
      std::cout << std::endl;
    }

   static SimpleNtupleGetSignal    * gs =
          SimpleNtupleGetSignal    ::instance() ;
//    static SimpleNtupleGetRawEventId* gi =
//           SimpleNtupleGetRawEventId::instance();
//    static SimpleNtupleGetMax       * gm =
//           SimpleNtupleGetMax       ::instance();
    static SimpleNtupleGetPedestal  * gp =
           SimpleNtupleGetPedestal  ::instance();
//    static SimpleNtupleGetChannelMax* gc =
//           SimpleNtupleGetChannelMax::instance();
    static SimpleNtupleGetSums      * gt =
           SimpleNtupleGetSums      ::instance();
//    static auto fSig = gs->getFunction();

    unsigned int fPos =  60;
    unsigned int lPos = 128;

//    std::vector<double> fEntries( nChannel );
//    std::vector<double> fTsumw  ( nChannel );
//    std::vector<double> fTsumw2 ( nChannel );
//    std::vector<double> fMean   ( nChannel );
//    std::vector<double> fRMS    ( nChannel );
//    ArrayStatistic::getStat( gs->channels(), gi->getChannels(),
//                             gs->getFunction(), fPos, lPos,
//                             fEntries.begin(), fTsumw.begin(), fTsumw2.begin(),
//                             fMean   .begin(), fRMS  .begin() );

    const std::vector<double>& sqSum = gt->getSumSqr( fPos, lPos );
//    std::vector<double> sqSum( nChannel, 0.0 );

//    std::vector<std::map<unsigned int,double>> signalProd;
    unsigned int iFEB;
    unsigned int iCha;
    unsigned int jCha;
/*
    for ( iCha = 0; iCha < nChannel; ++iCha ) {
      auto iBegin = gs->begin( iCha );
      sqSum[iCha] = ArrayStatistic::getSumSqr( iBegin + fPos, iBegin + lPos,
                                               gp->getPedestal( iCha ) );
    }
*/
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      auto& b = febBounds[iFEB];
      unsigned int fCha = b.first;
      unsigned int lCha = b.second;
//      signalProd.resize( lCha );
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        if ( cryMap[iCha] < 0 ) continue;
        for ( jCha = iCha + 1; jCha < lCha; ++jCha ) {
          if ( cryMap[jCha] < 0 ) continue;
          auto iBegin = gs->begin( iCha ) + fPos;
          auto iEnd   = gs->begin( iCha ) + lPos;
          double c = ArrayStatistic::getProd( iBegin, iEnd,
                                              gs->begin( jCha ) + fPos,
                                              gp->getPedestal( iCha ),
                                              gp->getPedestal( jCha ) );
//	  std::cout << iCha << ' ' << jCha << ' '
//                    << c / sqrt( fTsumw2[iCha] * fTsumw2[jCha] ) << std::endl;
          corrHist[iCha][jCha]->Fill( c / sqrt( sqSum[iCha] * sqSum[jCha] ) );
        }
      }
    }

    return true;

  }

  // Operations to be done at the execution end
  void endJob() {
  }

  void plotUpdate() override {
/*
    static int count = 0;
    if ( count++ % 2000 ) return;
    static TCanvas* cRMS = nullptr;
    if ( cRMS == nullptr ) {
      cRMS = new TCanvas( "cCor", "cCor", 800, 600 );
      int nCol = ceil( sqrt( nCrystal ) );
      int nRow = ceil( nCrystal * 1.0 / nCol );
      cRMS->Divide( nCol, nRow );
      cRMS->Draw();
    }
    static std::vector<TH1D*> hNoiClone( nHis, nullptr );
    for ( i = 0; i < nHis; i++ ) {
      cryId = cryMap[i];
      if ( cryId < 0 ) continue;
      cSig->cd( cryId + 1 );
      delete hNoiClone[i];
      hNoiClone[i] = dynamic_cast<TH1D*>( hNoiArray[i]->Clone() );
      hNoiClone[i]->GetXaxis()->SetRangeUser( 0, 100 );
      hNoiClone[i]->DrawCopy();
    }
    cRMS->Modified();
    cRMS->Update();
    gSystem->ProcessEvents();
*/
  }

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override {
    TCanvas* can = new TCanvas( "can", "can", 800, 600 );
    can->cd();
    int i = 0;
    int j;
    int n = corrHist.size();
//    int p = ( chFEB * ( chFEB - 1 ) ) / 2;
//    TH2F* h2Corr = new TH2F( "correlPlot", "correlPlot", p, -0.5, p - 0.5,
//                                                         p, -0.5, p - 0.5 );
    TH2F* h2Corr = new TH2F( "correlPlot", "correlPlot",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    TH2F* h2CNum = new TH2F( "correlNums", "correlNums",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    corrMean.clear();
    corrMean.resize( febBounds.front().second );
    for ( i = 0; i < n; ++i ) {
      if ( cryMap[i] < 0 ) continue;
      std::map<unsigned int,TH1D*>& cMap = corrHist[i];
      for ( auto& e: cMap ) {
        j = e.first;
        if ( cryMap[j] < 0 ) continue;
        TH1D* h = e.second;
//        if ( h->GetEntries() < 5 ) continue;
        h->Draw();
        can->Draw();
        can->Modified();
        can->Update();
//        std::cin.ignore();
	std::tuple<double,double,double> fPeak = PeakFitter::fit( *h );
//        double mean  = fPeak.first;
//        double sigma = fPeak.second;
        double mean  = std::get<1>( fPeak );
        double sigma = std::get<2>( fPeak );
        double lFactor = 1;
        double rFactor = 1;
        double rangeMin = mean - ( lFactor * sigma );
        double rangeMax = mean + ( rFactor * sigma );
        TF1* func = new TF1( "f1", "gaus", rangeMin, rangeMax );
        func->SetParameter( 0, std::get<0>( fPeak ) );
        func->SetParameter( 1, mean  );
        func->SetParameter( 2, sigma );
        h->Fit( func, "R" );
        h->Draw();
        can->Draw();
        can->Modified();
        can->Update();
//        h->Fit( func, "RSQN" );
//        std::cin.ignore();
        int fChan = febBounds[febMap[i]].first;
        int ci = i - fChan;
        int cj = j - fChan;
//        int cj = e.first - fChan;
        if ( febMap[i] ) std::swap( ci, cj );
        double cm = corrMean[ci][cj] = func->GetParameter( 1 );
        h2Corr->SetBinContent( ci + 1, cj + 1, cm );
        h2CNum->SetBinContent( ci + 1, cj + 1, ( i *100 ) + j );
	std::cout << "CORREL: " << i << '(' << ci << ") " << e.first << '(' << cj << ") ---> " << cm << std::endl;
        
        delete func;
      }
    }
/*
    TH2F* h2Corr = new TH2F( "correlNums", "correlNums",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    int i = iFEB;
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      if ( iFEB > 1 ) break;
      auto& b = febBounds[iFEB];
      unsigned int fChan = b.first;
      unsigned int lChan = b.second;
      for ( int i = 0; i < n; ++i ) {
        for ( int j = i; i < n; ++i ) {
        }
      }
    }
*/
    TCanvas* ct = new TCanvas( "ct", "ct", 800, 600 );
    ct->cd();
    h2Corr->SetStats( kFALSE );
    h2Corr->SetBarOffset(  0.2 );
    h2Corr->Draw( "colz0TEXT" );
    h2CNum->SetBarOffset( -0.2 );
    h2CNum->Draw( "TEXT SAME" );
    ct->Draw();
    ct->Modified();
    ct->Update();
  }

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  const
  unsigned int nChannel;
  unsigned int nCrystal;

  const
  unsigned int nFEB;
  const
  unsigned int chFEB;
  std::vector<std::pair<unsigned int,unsigned int>> febBounds;
  std::vector<unsigned int> febMap;

  std::vector<int>* colMap;
  std::vector<int>* rowMap;
  std::vector<int>  cryMap;
  std::vector<int>  adcMap;

  std::vector<std::map<unsigned int,TH1D*>> corrHist;
  std::vector<std::map<unsigned int,double>> corrMean;

};

// concrete factory to create a  analyzer
class NoiseCorrelationFactory: public NtuAnalysisFactory<SimpleNtupleData>::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
 NoiseCorrelationFactory(): NtuAnalysisFactory<SimpleNtupleData>::AbsFactory( "noiseCorrelation" ) {std::cout<<"NoiseCorrelationFactory" << std::endl;}
  // create an EventDump when builder is run
  NtuAnalysisSteering<SimpleNtupleData>* create( const UserParametersManager* userPar ) override {
    std::cout << "create NoiseCorrelation" << std::endl;
    return bindToThisFactory( NoiseCorrelation::basic_type::create<NoiseCorrelation>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<NoiseCorrelation>( keyword(), userPar ) );
//    return bindToThisFactory( NoiseCorrelation::create( keyword(), userPar ) );
//    return bindToThisFactory( new NoiseCorrelation( userPar ) );
  }
};
static NoiseCorrelationFactory ncrf;


#endif // NtuAnalysis_SimpleNtuple_Modules_NoiseCorrelation_h
