#ifndef NtuAnalysis_SimpleNtuple_Modules_LaserMonitor_h
#define NtuAnalysis_SimpleNtuple_Modules_LaserMonitor_h

#include "Utilities/ActiveObserver.h"
#include "Utilities/Singleton.h"

//#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
//#include "NtuAnalysis/SimpleNtuple/Util/NtuChannelMap.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetPedestal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetChannelMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSums.h"

#include "../Format/SimpleNtuple.h"
#include "../Util/NtuChannelMap.h"
#include "../Util/SimpleNtupleGetSignal.h"
#include "../Util/SimpleNtupleGetRawEventId.h"
#include "../Util/SimpleNtupleGetMax.h"
#include "../Util/SimpleNtupleGetPedestal.h"
#include "../Util/SimpleNtupleGetChannelMax.h"
#include "../Util/SimpleNtupleGetSums.h"

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/ArrayStatistic.h"
#include "Utilities/PeakFitter.h"
//#include "Framework/Event.h"

#include "../../Common/NtuAnalysisSteering.h"
#include "../../Common/NtuAnalysisFactory.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TSystem.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <string>

class LaserMonitor: public NtuAnalysisSteering<SimpleNtupleData> {

 public:

//  static LaserMonitor* create( const std::string& key,
//                               const UserParametersManager* userPar ) {
//    return nextInstance<LaserMonitor>( key, userPar );
//  }

  LaserMonitor( const UserParametersManager* userPar ):
   NtuAnalysisSteering<SimpleNtupleData>( userPar ) {
  };
  // deleted copy constructor and assignment to prevent unadvertent copy
  LaserMonitor           ( const LaserMonitor& x ) = delete;
  LaserMonitor& operator=( const LaserMonitor& x ) = delete;
  ~LaserMonitor() override = default;

  // Operations to be done at the execution begin
  void beginJob() override {
    uPar->getUserParameter( keyword() + "_refreshScale", refreshScale );
    uPar->getUserParameter( "laserChannel", laserCha, 18 );
    uPar->getUserParameter( "fiberChannel", fiberCha, 19 );
  }

  // Histograms creation
  void book() override {
    int    nBin;
    double rMin;
    double rMax;
    uPar->getUserParameter( keyword() + "_histBin"     , nBin, 80000   );
    uPar->getUserParameter( keyword() + "_laserHistBin", nBin          );
    uPar->getUserParameter( keyword() + "_histMin"     , rMin,     0.0 );
    uPar->getUserParameter( keyword() + "_laserHistMin", rMin          );
    uPar->getUserParameter( keyword() + "_histMax"     , rMax, 80000.0 );
    uPar->getUserParameter( keyword() + "_laserHistMax", rMax          );
    autoSavedObject = laserHist =
    new TH1D( "laserSignal", "laserSignal", nBin, rMin, rMax );
    uPar->getUserParameter( keyword() + "_histBin"     , nBin, 80000   );
    uPar->getUserParameter( keyword() + "_fiberHistBin", nBin          );
    uPar->getUserParameter( keyword() + "_histMin"     , rMin,     0.0 );
    uPar->getUserParameter( keyword() + "_fiberHistMin", rMin          );
    uPar->getUserParameter( keyword() + "_histMax"     , rMax, 80000.0 );
    uPar->getUserParameter( keyword() + "_fiberHistMax", rMax          );
    autoSavedObject = fiberHist =
    new TH1D( "fiberSignal", "fiberSignal", nBin, rMin, rMax );
    return;
  }

  // Data analysis
  bool analyze( int ientry, int ievent_file, int ievent_tot,
                TFile* file, const SimpleNtupleData& ntuData ) override {

    static SimpleNtupleGetMax     * gm =
           SimpleNtupleGetMax     ::instance() ;
    static SimpleNtupleGetPedestal* gp =
           SimpleNtupleGetPedestal::instance();

    laserHist->Fill( gm->getVal( laserCha ) - gp->getPedestal( laserCha ) );
    fiberHist->Fill( gm->getVal( fiberCha ) - gp->getPedestal( fiberCha ) );

    return true;

  }

  // Operations to be done at the execution end
  void endJob() {
  }

  void plotUpdate() override {
//    static int count = 0;
    if ( refreshCount++ % refreshScale ) return;
    static TCanvas* cSig = nullptr;
    if ( cSig == nullptr ) {
      cSig = new TCanvas( "LaserMonitor", "LaserMonitor", 800, 600 );
      cSig->Divide( 2, 1 );
      cSig->Draw();
    }
    cSig->cd( 1 );
    laserHist->Draw();
    cSig->cd( 2 );
    fiberHist->Draw();
    cSig->Modified();
    cSig->Update();
    gSystem->ProcessEvents();
  }

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override {
    plotUpdate();
    return;
  }

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  int laserCha;
  int fiberCha;

  TH1D* laserHist;
  TH1D* fiberHist;

};

// concrete factory to create a  analyzer
class LaserMonitorFactory: public NtuAnalysisFactory<SimpleNtupleData>::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  LaserMonitorFactory(): NtuAnalysisFactory<SimpleNtupleData>::AbsFactory( "laserMonitor" ) {}
  // create an EventDump when bx1uilder is run
  NtuAnalysisSteering<SimpleNtupleData>* create( const UserParametersManager* userPar ) override {
    std::cout << "create LaserMonitor" << std::endl;
    return bindToThisFactory( LaserMonitor::basic_type::create<LaserMonitor>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<LaserMonitor>( keyword(), userPar ) );
//    return bindToThisFactory( LaserMonitor::create( keyword(), userPar ) );
//    return bindToThisFactory( new LaserMonitor( userPar ) );
  }
};
static LaserMonitorFactory lmf;



#endif // NtuAnalysis_SimpleNtuple_Modules_LaserMonitor_h
