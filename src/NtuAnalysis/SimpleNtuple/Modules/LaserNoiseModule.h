#ifndef NtuAnalysis_SimpleNtuple_Modules_LaserNoiseModule_h
#define NtuAnalysis_SimpleNtuple_Modules_LaserNoiseModule_h

#include "Utilities/ActiveObserver.h"
#include "Utilities/Singleton.h"

//#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
//#include "NtuAnalysis/SimpleNtuple/Util/NtuChannelMap.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetPedestal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetChannelMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSums.h"
//#include "NtuAnalysis/SimpleNtuple/Util/plot.h"

#include "../Format/SimpleNtuple.h"
#include "../Util/NtuChannelMap.h"
#include "../Util/SimpleNtupleGetSignal.h"
#include "../Util/SimpleNtupleGetEventType.h"
#include "../Util/SimpleNtupleGetRawEventId.h"
#include "../Util/SimpleNtupleGetMax.h"
#include "../Util/SimpleNtupleGetPedestal.h"
#include "../Util/SimpleNtupleGetChannelMax.h"
#include "../Util/SimpleNtupleGetSums.h"
#include "../Util/plot.h"

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/ArrayStatistic.h"
//#include "Framework/Event.h"

#include "../../Common/NtuAnalysisSteering.h"
#include "../../Common/NtuAnalysisFactory.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TSystem.h"

#include <iostream>
#include <fstream>
#include <vector>

class LaserNoiseModule: public NtuAnalysisSteering<SimpleNtupleData> {

 public:

//  static LaserNoiseModule* create( const std::string& key,
//                                   const UserParametersManager* userPar ) {
//    return nextInstance<LaserNoiseModule>( key, userPar );
//  }

  LaserNoiseModule( const UserParametersManager* userPar ):
   NtuAnalysisSteering<SimpleNtupleData>( userPar ),
//  LaserNoiseModule( const UserParametersManager* userPar, TFile** currentFile ):
//   NtuAnalysisSteering<SimpleNtupleData>( userPar, currentFile ),
//   nCrystal( 25 ),
   nChannel( SimpleNtupleData::nChannels ),
//   nChannel( Event::maxChannelNumber ),
   colMap( nullptr ),
   rowMap( nullptr ) {
//   ASS_USER_PARAMETER( maxValue, 200 );
  };
  // deleted copy constructor and assignment to prevent unadvertent copy
  LaserNoiseModule           ( const LaserNoiseModule& x ) = delete;
  LaserNoiseModule& operator=( const LaserNoiseModule& x ) = delete;
  ~LaserNoiseModule() override = default;

  // Operations to be done at the execution begin
  void beginJob() override {
    colMap = rowMap = nullptr;
    uPar->getUserParameter( keyword() + "_refreshScale", refreshScale );
  }

  // Histograms creation
  void book() override {
    autoSavedObject =
    hSumADC   = new TH1D(   "hSumADC",    "Sum ADC Counts", 100,  0   , 80000 );
    autoSavedObject =
    hPeakADC  = new TH2D(  "hPeakADC",   "Peak ADC Counts",  25,  0   ,    25,
                                                            100,  0   , 20000 );
    autoSavedObject =
    hPeakFrac = new TH2D( "hPeakFrac", "Peak ADC Fraction",  25,  0   ,    25,
                                                            100,  0   ,     1 );
    autoSavedObject =
    h3x3Sum   = new TH2D(   "h3x3Sum",           "3x3 Sum",  25,  0   ,    25,
                                                            100,  0   , 20000 );
    autoSavedObject =
    h3x3Frac  = new TH2D(  "h3x3Frac",      "3x3 Fraction",  25,  0   ,    25,
                                                            100,  0.85,     1 );
    autoSavedObject =
    maxchan   = new TH1D(   "maxchan", "channel with peak",  30, 20   ,    50 );

//    histBin  .resize( nChannel );
//    histMin  .resize( nChannel );
//    histMax  .resize( nChannel );
    histRMin .resize( nChannel );
    histRMax .resize( nChannel );
    histArray.resize( nChannel );
//     rmsBin  .resize( nChannel );
//     rmsMin  .resize( nChannel );
//     rmsMax  .resize( nChannel );
     rmsRMin .resize( nChannel );
     rmsRMax .resize( nChannel );
     rmsArray.resize( nChannel );
//    histArray.resize( nCrystal );
//     rmsArray.resize( nCrystal );
    const char* histA;
    const char* histB;
//    char* histA = new char[20];
//    char* histB = new char[20];
    int    nBin;
    double rMin;
    double rMax;
    unsigned int i;
    for ( i = 0; i < nChannel; i++ ) {
//       sprintf( histA, "channel_%d", i );
//       sprintf( histB, "chanRMS_%d", i );
       std::string hNCha = std::to_string( i );
       std::string hNA = "channel_" + hNCha;
       std::string hNB = "chanRMS_" + hNCha;
       histA = hNA.c_str();
       histB = hNB.c_str();
//    for ( i = 0; i < nCrystal; i++ ) {
//       sprintf( histA, "crystal_%d", i );
//       sprintf( histB,  "cryrms_%d", i );
//       cout << "***" << histA << "***" << endl;
//       cout << "***" << histB << "***" << endl;
//       int  & nhBin = histBin[i] = 80000;
//       float& rhMin = histMin[i] =     0.0;
//       float& rhMax = histMax[i] = 80000.0;
       uPar->getUserParameter( keyword() + "_chanSignalBin"        , nBin, 80000   );
       uPar->getUserParameter( keyword() + "_chanSignalBin" + hNCha, nBin );
       uPar->getUserParameter( keyword() + "_chanSignalMin"        , rMin,     0.0 );
       uPar->getUserParameter( keyword() + "_chanSignalMin" + hNCha, rMin );
       uPar->getUserParameter( keyword() + "_chanSignalMax"        , rMax, 80000.0 );
       uPar->getUserParameter( keyword() + "_chanSignalMax" + hNCha, rMax );
       double& hrMin = histRMin[i];
       double& hrMax = histRMax[i];
       uPar->getUserParameter( keyword() + "_chanSPlotMin"        , hrMin, rMin );
       uPar->getUserParameter( keyword() + "_chanSPlotMin" + hNCha, hrMin );
       uPar->getUserParameter( keyword() + "_chanSPlotMax"        , hrMax, rMax );
       uPar->getUserParameter( keyword() + "_chanSPlotMax" + hNCha, hrMax );
       autoSavedObject =
       histArray[i] = new TH1D( histA, histA, nBin, rMin, rMax );
//       int  & nrBin = histBin[i] = 500;
//       float& rrMin = histMin[i] =   0.0;
//       float& rrMax = histMax[i] = 100.0;
       uPar->getUserParameter( keyword() + "_chanNoiseBin"        , nBin, 500   );
       uPar->getUserParameter( keyword() + "_chanNoiseBin" + hNCha, nBin );
       uPar->getUserParameter( keyword() + "_chanNoiseMin"        , rMin,   0.0 );
       uPar->getUserParameter( keyword() + "_chanNoiseMin" + hNCha, rMin );
       uPar->getUserParameter( keyword() + "_chanNoiseMax"        , rMax, 500.0 );
       uPar->getUserParameter( keyword() + "_chanNoiseMax" + hNCha, rMax );
       double& rrMin = rmsRMin[i];
       double& rrMax = rmsRMax[i];
       uPar->getUserParameter( keyword() + "_chanRPlotMin"        , rrMin, rMin );
       uPar->getUserParameter( keyword() + "_chanRPlotMin" + hNCha, rrMin );
       uPar->getUserParameter( keyword() + "_chanRPlotMax"        , rrMax, rMax );
       uPar->getUserParameter( keyword() + "_chanRPlotMax" + hNCha, rrMax );
       autoSavedObject =
        rmsArray[i] = new TH1D( histB, histB, nBin, rMin, rMax );
    }
    return;
  }

  // Data analysis
//  bool analyze( const SimpleNtupleData& ntuData ) override {
  bool analyze( int ientry, int ievent_file, int ievent_tot,
                TFile* file, const SimpleNtupleData& ntuData ) override {
//    std::cout << "LaserNoiseModule::analyze " << ientry << std::endl;
//    std::cout << "analyze: " << ientry << ' ' << ievent_file << ' ' << ievent_tot << std::endl;
//    return true;
    if ( ( colMap == nullptr ) ||
         ( rowMap == nullptr ) ) {
      file->GetObject( "colMap", colMap );
      file->GetObject( "rowMap", rowMap );
/*
      unsigned int i = colMap->size();
      unsigned int j = rowMap->size();
      unsigned int n = ( i < j ? i : j );
      adcMap.clear();
      cryMap.clear();
//      adcMap.resize( nChannel, -1 );
      cryMap.resize( nChannel, -1 );
      nCrystal = 0;
      for ( i = 0; i < n; ++i ) {
        if ( (*rowMap)[i] < 0 ) continue;
        j = ( ( (*rowMap)[i] - 1 ) * 5 ) + (*colMap)[i] - 1;
        if ( j >= adcMap.size() ) adcMap.resize( j + 1, -1 );
        adcMap.at( j ) = i;
        cryMap.at( i ) = j;
//        if ( j >= nCrystal ) nCrystal = j + 1;
      }
*/
      NtuChannelMap::get( colMap, rowMap, cryMap, adcMap );
      nCrystal = adcMap.size();
//      filterWithCry( histArray, histArCry );
//      filterWithCry( rmsArray, rmsArCry );
      std::cout << adcMap.size() << " ADCMAP:";
      for ( int x: adcMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << cryMap.size() << " CRYMAP:";
      for ( int x: cryMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << adcMap.size() << " XXXMAP:";
      for ( int x: adcMap ) std::cout << ' ' << cryMap[x];
      std::cout << std::endl;
    }

//    static SimpleNtupleGetSignal    * gs =
//           SimpleNtupleGetSignal    ::instance() ;
//    static SimpleNtupleGetRawEventId* gi =
//           SimpleNtupleGetRawEventId::instance();
    static SimpleNtupleGetMax       * gm =
           SimpleNtupleGetMax       ::instance();
    static SimpleNtupleGetPedestal  * gp =
           SimpleNtupleGetPedestal  ::instance();
    static SimpleNtupleGetChannelMax* gc =
           SimpleNtupleGetChannelMax::instance();
    static SimpleNtupleGetSums      * gt =
           SimpleNtupleGetSums      ::instance();
//    static auto fSig = gs->getFunction();

    int    chaMax = gc->getCha();
    double sigMax = gc->getVal();
    double sigSum = gc->getSum();
    int    cryMax = cryMap[chaMax];

/*
    std::vector<double> fEntries( nChannel ); ///< Number of entries
    std::vector<double> fTsumw  ( nChannel ); ///< Total Sum of weights
    std::vector<double> fTsumw2 ( nChannel ); ///< Total Sum of squares
    std::vector<double> fMean   ( nChannel ); ///< Mean
    std::vector<double> fRMS    ( nChannel ); ///< RMS
    SimpleNtupleGetMax::funcSide fSide = [this]( int i ) {
      return std::make_tuple( 0, 60, +1 );
    };
    ArrayStatistic::getStat( gs->channels(), gi->getChannels(),
                             gs->getFunction(), fSide,
                             fEntries.begin(), fTsumw.begin(), fTsumw2.begin(),
                             fMean   .begin(), fRMS  .begin() );
*/
    int fPos = 60;
    int lPos = ntuData.signalCounts->front();
    int nPos = lPos - fPos;
    const std::vector<double>& fTsumw  = gt->getSum   ( fPos, lPos );
    const std::vector<double>& fTsumw2 = gt->getSumSqr( fPos, lPos );

    unsigned int i;
    for ( i = 0; i < nCrystal; ++i ) {
      int a = adcMap[i];
      double mean = fTsumw[a] / nPos;
      double var = ( fTsumw2[a] / nPos ) - ( mean * mean );
      int posMax = gm->getPos( a );
      maxchan->Fill( posMax );
//      if ( ( posMax < 35 ) || ( posMax > 45 ) ) continue;
      int k = a;
      if ( k < 0 ) continue;
      histArray[k]->Fill( gm->getVal( a ) - gp->getPedestal( a ) );
      rmsArray[k]->Fill( var > 0 ? sqrt( var ) : 0 );
//      rmsArray[k]->Fill( fRMS[a] );
    }
    hSumADC  ->Fill( sigSum ) ;
    hPeakADC ->Fill( cryMax, sigMax );
    hPeakFrac->Fill( cryMax, sigMax / sigSum );
    double sum3x3 = get3x3Sum( cryMax );
//    cout << sum3x3 << endl;
    h3x3Sum ->Fill( cryMax, sum3x3 );
    h3x3Frac->Fill( cryMax, sum3x3 / sigSum );

    return true;
  }

  // Operations to be done at the execution end
  void endJob() {
//    filterWithCry( histArray, histArCry );
//    filterWithCry( rmsArray, rmsArCry );
  }

  void plotUpdate() override {
//    static int count = 0;
    if ( refreshCount++ % refreshScale ) return;
    auto& hSigArray = histArray;
    auto& hNoiArray =  rmsArray;
//    auto& hSigArray = histArCry;
//    auto& hNoiArray =  rmsArCry;
    int nHis = hSigArray.size();
    static TCanvas* cSig = nullptr;
    static TCanvas* cRMS = nullptr;
//    int nCry = 0;
    if ( cSig == nullptr ) {
//      for ( TH1D* hh: hSigArray ) if ( hh != nullptr ) nCry++;
      cSig = new TCanvas( "cSig", "cSig", 200, 100, 1200, 900 );
      int nCol = ceil( sqrt( nCrystal ) );
      int nRow = ceil( nCrystal * 1.0 / nCol );
      cSig->Divide( nCol, nRow );
      cSig->Draw();
    }
    if ( cRMS == nullptr ) {
      cRMS = new TCanvas( "cRMS", "cRMS", 400, 200, 1200, 900 );
      int nCol = ceil( sqrt( nCrystal ) );
      int nRow = ceil( nCrystal * 1.0 / nCol );
      cRMS->Divide( nCol, nRow );
      cRMS->Draw();
    }
    int i;
    int cryId = 0;
    static std::vector<TH1D*> hSigClone( nHis, nullptr );
    static std::vector<TH1D*> hNoiClone( nHis, nullptr );
    for ( i = 0; i < nHis; i++ ) {
      cryId = cryMap[i];
      if ( cryId < 0 ) continue;
//      if ( hSigArray[i] == nullptr ) continue;
//      cSig->cd( ++cryId );
      cSig->cd( cryId + 1 );
//      hSigArray[i]->Draw();//Copy();
      delete hSigClone[i];
      hSigClone[i] = dynamic_cast<TH1D*>( hSigArray[i]->Clone() );
//      hSigClone[i]->GetXaxis()->SetRangeUser( 0, 2000 );
      hSigClone[i]->GetXaxis()->SetRangeUser( histRMin[i], histRMax[i] );
//      float xSize = 0.08;
//      uPar->getUserParameter( "sigCloneSize", xSize );
//      hSigClone[i]->GetXaxis()->SetLabelSize( xSize );
      hSigClone[i]->GetXaxis()->SetLabelSize( 0.07 );
      hSigClone[i]->GetYaxis()->SetLabelSize( 0.07 );
//      hSigClone[i]->DrawCopy();
      hSigClone[i]->Draw();
//      cRMS->cd(   cryId );
      cRMS->cd( cryId + 1 );
//      hNoiArray[i]->Draw();//Copy();
      delete hNoiClone[i];
      hNoiClone[i] = dynamic_cast<TH1D*>( hNoiArray[i]->Clone() );
//      hNoiClone[i]->GetXaxis()->SetRangeUser( 0, 100 );
      hNoiClone[i]->GetXaxis()->SetRangeUser( rmsRMin[i], rmsRMax[i] );
      hNoiClone[i]->GetXaxis()->SetLabelSize( 0.07 );
      hNoiClone[i]->GetYaxis()->SetLabelSize( 0.07 );
      hNoiClone[i]->DrawCopy();
    }
    cSig->Modified();
    cSig->Update();
    cRMS->Modified();
    cRMS->Update();
    gSystem->ProcessEvents();
  }

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override {
    std::cout << "LaserNoiseModule::plot" << std::endl;
    plotUpdate();
    return;
//    ::plot( rmsArray, 0.1 );
//    ::plot( rmsArCry, 0.1, cryMap );
//    ::plot( rmsArray, 0.5, cryMap );
    double lRangeSignal;
    double rRangeSignal;
    double fracSignal;
    double lRangeRMS;
    double rRangeRMS;
    double fracRMS;
    uPar->getUserParameter( keyword() + "_lRangeSignal", lRangeSignal, 0.5 );
    uPar->getUserParameter( keyword() + "_rRangeSignal", rRangeSignal, 0.5 );
    uPar->getUserParameter( keyword() + "_fracSignal"  , fracSignal  , 0.5 );
    uPar->getUserParameter( keyword() + "_lRangeRMS"   , lRangeRMS   , 2.0 );
    uPar->getUserParameter( keyword() + "_rRangeRMS"   , rRangeRMS   , 0.5 );
    uPar->getUserParameter( keyword() + "_fracRMS"     , fracRMS     , 0.5 );
    ::plot( rmsArray, 0.5, fracRMS, cryMap, "rms", lRangeRMS, rRangeRMS );
    std::cout << "SUMMARY FITS: " << fracSignal << ' ' << lRangeSignal << ' ' << rRangeSignal << std::endl;
    ::plot( histArray, 0.5, fracSignal, cryMap, "signal", lRangeSignal, rRangeSignal );
  }

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  const
  unsigned int nChannel;
  unsigned int nCrystal;

//  bool missingMap;
  std::vector<int>* colMap;
  std::vector<int>* rowMap;
  std::vector<int>  cryMap;
  std::vector<int>  adcMap;

  TH1D* hSumADC;
  TH2D* hPeakADC;
  TH2D* hPeakFrac;
  TH2D* h3x3Sum;
  TH2D* h3x3Frac;
//  std::vector<int  > histBin;
//  std::vector<float> histMin;
//  std::vector<float> histMax;
  std::vector<double> histRMin;
  std::vector<double> histRMax;
  std::vector<TH1D*> histArray;//[25];
//  std::vector<int  >  rmsBin;
//  std::vector<float>  rmsMin;
//  std::vector<float>  rmsMax;
  std::vector<double>  rmsRMin;
  std::vector<double>  rmsRMax;
  std::vector<TH1D*>  rmsArray;//[25];
//  std::vector<TH1D*> histArCry;//[25];
//  std::vector<TH1D*> rmsArCry;//[25];
  TH1D* maxchan;

  double get3x3Sum( int iCry ) {

    static SimpleNtupleGetSignal  * gs = SimpleNtupleGetSignal  ::instance();
    static SimpleNtupleGetPedestal* gp = SimpleNtupleGetPedestal::instance();
    int yCry = iCry/5;
    int xCry = iCry%5;
    if( yCry == 0 || yCry == 4 ) return -1;
    if( xCry == 0 || xCry == 4 ) return -1;

    static int pos = 39;
    double sum = 0;
    for( int ix=xCry-1 ; ix<xCry+2 ; ix++ ) {
      for( int iy=yCry-1 ; iy<yCry+2 ; iy++ ) {
        int jCry = ix+5*iy ;
        int jADC=adcMap[jCry];
        sum += *( gs->begin( jADC ) + pos ) - gp->getPedestal( jADC );
      }
    }
    return sum ;
  }

/*
  void filterWithADC( std::vector<TH1D*>& hArr,
                      std::vector<TH1D*>& hADC ) {
    int i;
    int n = hArr.size();
//    std::vector<TH1D*> hTmp;
//    hTmp.reserve( n );
    hADC.reserve( n );
    for ( i = 0; i < n; ++i ) if ( adcMap[ i ] >= 0 ) {
      std::cout << "hist " << i << " selected" << std::endl;
      hADC.push_back( hArr[i] );
//      hTmp.push_back( hArr[i] );
    }
//    hArr = hTmp;
    return;
  }

  void filterWithCry( std::vector<TH1D*>& hArr,
                      std::vector<TH1D*>& hCry ) {
    int i;
    int n = hArr.size();
//    std::vector<TH1D*> hTmp;
//    hTmp.reserve( n );
//    hTmp.resize( nCrystal, nullptr );
//    hTmp.resize( n, nullptr );
    hCry.resize( n, nullptr );
    for ( i = 0; i < n; ++i ) if ( cryMap[ i ] >= 0 ) {
      std::cout << "hist " << i << " selected" << std::endl;
//      hTmp.push_back( hArr[i] );
//      hTmp[cryMap[i]] = hArr[i];
//      hTmp[i] = hArr[i];
      hCry[i] = hArr[i];
    }
//    hArr = hTmp;
    return;
  }
*/
};

// concrete factory to create a LaserNoiseModule analyzer
class LaserNoiseModuleFactory: public NtuAnalysisFactory<SimpleNtupleData>::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  LaserNoiseModuleFactory(): NtuAnalysisFactory<SimpleNtupleData>::AbsFactory( "simpleNoise" ) {}
  // create an EventDump when builder is run
  NtuAnalysisSteering<SimpleNtupleData>* create( const UserParametersManager* userPar ) override {
    std::cout << "create LaserNoiseModule" << std::endl;
    return bindToThisFactory( LaserNoiseModule::basic_type::create<LaserNoiseModule>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<LaserNoiseModule>( keyword(), userPar ) );
//    return bindToThisFactory( LaserNoiseModule::create( keyword(), userPar ) );
//    return bindToThisFactory( new LaserNoiseModule( userPar ) );
  }
};
static LaserNoiseModuleFactory lnmf;


#endif // NtuAnalysis_SimpleNtuple_Modules_LaserNoiseModule_h
