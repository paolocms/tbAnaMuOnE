#ifndef NtuAnalysis_SimpleNtuple_Modules_NoiseComponents_h
#define NtuAnalysis_SimpleNtuple_Modules_NoiseComponents_h

#include "Utilities/ActiveObserver.h"
#include "Utilities/Singleton.h"

//#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
//#include "NtuAnalysis/SimpleNtuple/Util/NtuChannelMap.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetPedestal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetChannelMax.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSums.h"

#include "../Format/SimpleNtuple.h"
#include "../Util/NtuChannelMap.h"
#include "../Util/SimpleNtupleGetSignal.h"
#include "../Util/SimpleNtupleGetRawEventId.h"
#include "../Util/SimpleNtupleGetMax.h"
#include "../Util/SimpleNtupleGetPedestal.h"
#include "../Util/SimpleNtupleGetChannelMax.h"
#include "../Util/SimpleNtupleGetSums.h"

#include "NtuTool/Common/interface/UserParametersManager.h"
#include "Utilities/ArrayStatistic.h"
#include "Utilities/PeakFitter.h"
//#include "Framework/Event.h"

#include "../../Common/NtuAnalysisSteering.h"
#include "../../Common/NtuAnalysisFactory.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TSystem.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <string>

class NoiseComponents: public NtuAnalysisSteering<SimpleNtupleData> {

 public:

//  static NoiseComponents* create( const std::string& key,
//                               const UserParametersManager* userPar ) {
//    return nextInstance<NoiseComponents>( key, userPar );
//  }

  NoiseComponents( const UserParametersManager* userPar ):
   NtuAnalysisSteering<SimpleNtupleData>( userPar ),
   nChannel( SimpleNtupleData::nChannels ),
   nFEB( SimpleNtupleData::nFEB ),
   chFEB( lround( ceil( nChannel / nFEB ) ) ),
   febBounds( nFEB ),
   febMap( nFEB * chFEB ),
   colMap( nullptr ),
   rowMap( nullptr ) {
    std::cout << "NCHANNELS: " << nChannel << ' ' << nFEB  << ' ' << chFEB << std::endl;
//    unsigned int chFEB = lround( ceil( nChannel * 1.0 / chFEB ) );
    unsigned int fCha = 0;
    unsigned int lCha = chFEB;
    unsigned int iFEB;
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      auto& b = febBounds[iFEB];
      b.first  = fCha;
      b.second = lCha;
//      int iChan;
//      for ( iChan = fChan; iChan < lChan; ++iChan ) ;
//      fChan = lChan;
      while ( fCha < lCha ) febMap[fCha++] = iFEB;
      lCha = fCha + chFEB;
    }
    for ( auto& e: febBounds ) std::cout << "FEB bounds: " << e.first << ' ' << e.second << std::endl;
    for ( unsigned int i = 0; i < nChannel; ++i ) std::cout << "FEB: " << i << ' ' << febMap[i] << std::endl;
    chanMean.resize( nChannel, -1 );
    febMean. resize( nFEB    , -1 );
  };
  // deleted copy constructor and assignment to prevent unadvertent copy
  NoiseComponents           ( const NoiseComponents& x ) = delete;
  NoiseComponents& operator=( const NoiseComponents& x ) = delete;
  ~NoiseComponents() override = default;

  // Operations to be done at the execution begin
  void beginJob() override {
    colMap = rowMap = nullptr;
    uPar->getUserParameter( keyword() + "_rangeFPos", rangeFPos );
    uPar->getUserParameter( keyword() + "_rangeLPos", rangeLPos );
  }

  // Histograms creation
  void book() override {
    unsigned int iFEB;
    unsigned int iCha;
    unsigned int jCha;
    std::string nBase = "chan_";
    std::string nBFEB = "feb_";
//    febBin .resize( nFEB );
//    febMin .resize( nFEB );
//    febMax .resize( nFEB );
    int    nBin;
    double rMin;
    double rMax;
    febHist.resize( nFEB );
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      auto& b = febBounds[iFEB];
      unsigned int fCha = b.first;
      unsigned int lCha = b.second;
//      chanBin .resize( lCha );
//      chanMin .resize( lCha );
//      chanMax .resize( lCha );
      chanHist.resize( lCha );
//      corrBin .resize( lCha );
//      corrMin .resize( lCha );
//      corrMax .resize( lCha );
      corrHist.resize( lCha );
      std::string hNFeb = std::to_string( iFEB + 1 );
      std::string hName = nBFEB + hNFeb;
//      int  & nBin =  febBin[iFEB] = 200;
//      float& rMin =  febMin[iFEB] =   0.0;
//      float& rMax =  febMax[iFEB] = 200.0;
//      nBin = 200;
//      rMin =   0.0;
//      rMax = 200.0;
      uPar->getUserParameter( keyword() + "_febNoiseBin"        , nBin, 200   );
      uPar->getUserParameter( keyword() + "_febNoiseBin" + hNFeb, nBin );
      uPar->getUserParameter( keyword() + "_febNoiseMin"        , rMin,   0.0 );
      uPar->getUserParameter( keyword() + "_febNoiseMin" + hNFeb, rMin );
      uPar->getUserParameter( keyword() + "_febNoiseMax"        , rMax, 200.0 );
      uPar->getUserParameter( keyword() + "_febNoiseMax" + hNFeb, rMax );
      autoSavedObject = febHist[iFEB] =
      new TH1D( hName.c_str(), hName.c_str(), nBin, rMin, rMax );
//      new TH1D( hName.c_str(), hName.c_str(), 200, 0, 200 );
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        std::string hNCha = std::to_string( iCha );
        std::string hName = nBase + hNCha;
//        int  & nBin = chanBin[iCha] = 200;
//        float& rMin = chanMin[iCha] =  0.0;
//        float& rMax = chanMax[iCha] = 40.0;
//        nBin = 200;
//        rMin =  0.0;
//        rMax = 40.0;
        uPar->getUserParameter( keyword() + "_chanNoiseBin"        , nBin, 200   );
        uPar->getUserParameter( keyword() + "_chanNoiseBin" + hNCha, nBin );
        uPar->getUserParameter( keyword() + "_chanNoiseMin"        , rMin,   0.0 );
        uPar->getUserParameter( keyword() + "_chanNoiseMin" + hNCha, rMin );
        uPar->getUserParameter( keyword() + "_chanNoiseMax"        , rMax,  40.0 );
        uPar->getUserParameter( keyword() + "_chanNoiseMax" + hNCha, rMax );
        autoSavedObject = chanHist[iCha] =
        new TH1D( hName.c_str(), hName.c_str(), nBin, rMin, rMax );
        for ( jCha = iCha + 1; jCha < lCha; ++jCha ) {
          std::string hNCor = std::to_string( iCha ) + "_" +
                              std::to_string( jCha );
          std::string hName = nBFEB + hNFeb + "_" + hNCor;
//          std::string hName = nBFEB +
//                              std::to_string( iFEB + 1 ) + "_" +
//                              std::to_string( iCha     ) + "_" +
//                              std::to_string( jCha     );
          std::cout << hName << std::endl;
//          int  & nBin = corrBin[iCha][jCha] = 102;
//          float& rMin = corrMin[iCha][jCha] = -1.02;
//          float& rMax = corrMax[iCha][jCha] =  1.02;
//          nBin = 102;
//          rMin = -1.02;
//          rMax =  1.02;
          uPar->getUserParameter( keyword() + "_correlationBin"        , nBin,   102 );
          uPar->getUserParameter( keyword() + "_correlationBin" + hNCor, nBin );
          uPar->getUserParameter( keyword() + "_correlationMin"        , rMin, -1.02 );
          uPar->getUserParameter( keyword() + "_correlationMin" + hNCor, rMin );
          uPar->getUserParameter( keyword() + "_correlationMin"        , rMax,  1.02 );
          uPar->getUserParameter( keyword() + "_correlationMin" + hNCor, rMax );
          autoSavedObject = corrHist[iCha][jCha] =
          new TH1D( hName.c_str(), hName.c_str(), nBin, rMin, rMax );
        }
      }
    }
    return;
  }

  // Data analysis
  bool analyze( int ientry, int ievent_file, int ievent_tot,
                TFile* file, const SimpleNtupleData& ntuData ) override {

    if ( ( colMap == nullptr ) ||
         ( rowMap == nullptr ) ) {
      file->GetObject( "colMap", colMap );
      file->GetObject( "rowMap", rowMap );
      NtuChannelMap::get( colMap, rowMap, cryMap, adcMap );
      nCrystal = adcMap.size();
      std::cout << adcMap.size() << " ADCMAP:";
      for ( int x: adcMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << cryMap.size() << " CRYMAP:";
      for ( int x: cryMap ) std::cout << ' ' << x;
      std::cout << std::endl;
      std::cout << adcMap.size() << " XXXMAP:";
      for ( int x: adcMap ) std::cout << ' ' << cryMap[x];
      std::cout << std::endl;
    }

    static SimpleNtupleGetSignal    * gs =
           SimpleNtupleGetSignal    ::instance() ;
//    static SimpleNtupleGetRawEventId* gi =
//           SimpleNtupleGetRawEventId::instance();
//    static SimpleNtupleGetMax       * gm =
//           SimpleNtupleGetMax       ::instance();
    static SimpleNtupleGetPedestal  * gp =
           SimpleNtupleGetPedestal  ::instance();
//    static SimpleNtupleGetChannelMax* gc =
//           SimpleNtupleGetChannelMax::instance();
    static SimpleNtupleGetSums      * gt =
           SimpleNtupleGetSums      ::instance();
//    static auto fSig = gs->getFunction();

    unsigned int fPos =  60;
    unsigned int lPos = 128;
    int nPos = lPos - fPos;
    double xPos = nPos;

    const std::vector<double>& sqSum = gt->getSumSqr( fPos, lPos );
//    std::vector<double> sqSumCha( nChannel, 0 );
    unsigned int iFEB;
    unsigned int iCha;
    unsigned int jCha;
//    for ( iCha = 0; iCha < nChannel; ++iCha ) {
//      auto iBegin = gs->begin( iCha );
//      chanHist[iCha]->Fill( sqrt(
//                      ArrayStatistic::getSumSqr( iBegin + fPos, iBegin + lPos,
//                                                 gp->getPedestal( iCha ) ) /
//                      lPos - fPos ) );
//    }
//    std::vector<double> sqSumFEB( nFEB, 0 );
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      std::vector<double> febSum( nPos, 0 );
      auto fBegin = febSum.begin();
      auto fEnd   = febSum.end();
      auto& b = febBounds[iFEB];
      unsigned int fCha = b.first;
      unsigned int lCha = b.second;
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        if ( cryMap[iCha] < 0 ) continue;
        chanHist[iCha]->Fill( sqrt( sqSum[iCha] / xPos ) );
//        auto iBegin = gs->begin( iCha );
//        chanHist[iCha]->Fill( sqrt( ArrayStatistic::getSumSqr(
//                                    iBegin + fPos, iBegin + lPos,
//                                    gp->getPedestal( iCha ) ) / xPos ) );
        ArrayStatistic::accumulate( fBegin, fEnd,
                                    gs->begin( iCha ) + fPos,
                                    gp->getPedestal( iCha ) );
        for ( jCha = iCha + 1; jCha < lCha; ++jCha ) {
          if ( cryMap[jCha] < 0 ) continue;
          auto iBegin = gs->begin( iCha ) + fPos;
          auto iEnd   = gs->begin( iCha ) + lPos;
          double c = ArrayStatistic::getProd( iBegin, iEnd,
                                              gs->begin( jCha ) + fPos,
                                              gp->getPedestal( iCha ),
                                              gp->getPedestal( jCha ) );
          corrHist[iCha][jCha]->Fill( c / sqrt( sqSum[iCha] * sqSum[jCha] ) );
        }
      }
      febHist[iFEB]->Fill( sqrt( ArrayStatistic::getSumSqr( fBegin, fEnd ) /
                                 xPos ) );
    }

    return true;

  }

  // Operations to be done at the execution end
  void endJob() {
  }

  void plotUpdate() override {
/*
    static int count = 0;
    if ( count++ % 2000 ) return;
    static TCanvas* cRMS = nullptr;
    if ( cRMS == nullptr ) {
      cRMS = new TCanvas( "cCor", "cCor", 800, 600 );
      int nCol = ceil( sqrt( nCrystal ) );
      int nRow = ceil( nCrystal * 1.0 / nCol );
      cRMS->Divide( nCol, nRow );
      cRMS->Draw();
    }
    static std::vector<TH1D*> hNoiClone( nHis, nullptr );
    for ( i = 0; i < nHis; i++ ) {
      cryId = cryMap[i];
      if ( cryId < 0 ) continue;
      cSig->cd( cryId + 1 );
      delete hNoiClone[i];
      hNoiClone[i] = dynamic_cast<TH1D*>( hNoiArray[i]->Clone() );
      hNoiClone[i]->GetXaxis()->SetRangeUser( 0, 100 );
      hNoiClone[i]->DrawCopy();
    }
    cRMS->Modified();
    cRMS->Update();
    gSystem->ProcessEvents();
*/
  }

  // Optional function to draw some plots at the execution end: see the
  // INSTRUCTIONS file for details about running options.
  // This function is executed before saving histograms, so any operation 
  // done to them through the graphic interface will be persistent.
  // This function can be simply left not declared and not implemented, but
  // do not declare an empty function, otherwise execution could get stuck
  // in some situations.
  void plot() override {
    static const std::string cName = keyword() + "Fit";
    TCanvas* can = new TCanvas( cName.c_str(), cName.c_str(), 800, 600 );
    can->cd();
    unsigned int iFEB;
    unsigned int iCha;
    unsigned int jCha;
    float* cNum = new float[nCrystal];
    float* cWid = new float[nCrystal];
    float* nMea = new float[nCrystal];
    float* nRMS = new float[nCrystal];
    float* iMea = new float[nCrystal];
//    float* iRMS = new float[nCrystal];
    float* cMea = new float[nCrystal];
//    float* cRMS = new float[nCrystal];
    TH2F* h2CNum = new TH2F( "correlNums", "correlNums",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    TH2F* h2Corr = new TH2F( "correlPlot", "correlPlot",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    TH2F* h2CNoi = new TH2F( "corNoiPlot", "corNoiPlot",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    TH2F* h2IMin = new TH2F( "intNoiLCha", "intNoiLCha",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    TH2F* h2IMax = new TH2F( "intNoiRCha", "intNoiRCha",
                             chFEB, -0.5, chFEB - 0.5,
                             chFEB, -0.5, chFEB - 0.5 );
    corrMean.clear();
    corrMean.resize( febBounds.front().second );
    for ( iFEB = 0; iFEB < nFEB; ++iFEB ) {
      TH1D* h = febHist[iFEB];
      std::tuple<double,double,double> fPeak = PeakFitter::fit( *h );
      double mean  = std::get<1>( fPeak );
      double sigma = std::get<2>( fPeak );
      double lFactor = 1;
      double rFactor = 1;
      double rangeMin = mean - ( lFactor * sigma );
      double rangeMax = mean + ( rFactor * sigma );
      TF1* func = new TF1( "f1", "gaus", rangeMin, rangeMax );
      func->SetParameter( 0, std::get<0>( fPeak ) );
      func->SetParameter( 1, mean  );
      func->SetParameter( 2, sigma );
      h->Fit( func, "R" );
      h->Draw();
      can->Draw();
      can->Modified();
      can->Update();
      double cn =
      febMean[iFEB] = func->GetParameter( 1 );
      double qn = cn * cn;
//      std::cin.ignore();
      auto& b = febBounds[iFEB];
      unsigned int fCha = b.first;
      unsigned int lCha = b.second;
      int nc = 0;
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        if ( cryMap[iCha] < 0 ) continue;
        ++nc;
      }
      int n2 = nc * nc;
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        int cryId = cryMap[iCha];
        if ( cryId < 0 ) continue;
        TH1D* h = chanHist[iCha];
	std::tuple<double,double,double> fPeak = PeakFitter::fit( *h );
        double mean  = std::get<1>( fPeak );
        double sigma = std::get<2>( fPeak );
        double lFactor = 1;
        double rFactor = 1;
        double rangeMin = mean - ( lFactor * sigma );
        double rangeMax = mean + ( rFactor * sigma );
        TF1* func = new TF1( "f1", "gaus", rangeMin, rangeMax );
        func->SetParameter( 0, std::get<0>( fPeak ) );
        func->SetParameter( 1, mean  );
        func->SetParameter( 2, sigma );
        h->Fit( func, "R" );
        h->Draw();
        can->Draw();
        can->Modified();
        can->Update();
        double ct =
	chanMean[iCha] = func->GetParameter( 1 );
        double qt = ct * ct;
        double qr = qn / ( nc * qt );
        double kk = ( qr - 1 ) / ( nc - qr );
        double k1 = ( nc - 1 ) / ( nc - qr );
        std::cout << iFEB << " qn : " << qn << ' ' << cn << ' ' << nc << std::endl;
        std::cout << iCha << " q,R,k : " << qt << ' ' << ct << ' '
                  << qr << ' ' << kk << ' ' << 1 + kk - k1 << std::endl;
        double qi = qt / k1;
        double ki = ( ( n2 * qt ) - qn ) / ( n2 - nc );
        std::cout << iCha << " qi : " << qi << ' ' << ki << std::endl;
        double qc = kk * qi;
        double kc = ( qn - ( nc * qt ) ) / ( n2 - nc );
        std::cout << iCha << " qc : " << qc << ' ' << kc << ' '
                  << sqrt( qc ) << std::endl;
//        std::cin.ignore();
        cNum[cryId] = iCha;
        cWid[cryId] = 0.25;
        nMea[cryId] = ct;
        iMea[cryId] = ki > 0 ? sqrt( ki ) : -1;
        cMea[cryId] = kc > 0 ? sqrt( kc ) : -1;
        nRMS[cryId] = func->GetParameter( 2 );
      }
      for ( iCha = fCha; iCha < lCha; ++iCha ) {
        int cryId = cryMap[iCha];
        if ( cryId < 0 ) continue;
        std::map<unsigned int,TH1D*>& cMap = corrHist[iCha];
//        for ( jCha = fCha; jCha < lCha; ++jCha ) {
//          if ( cryMap[jCha] < 0 ) continue;
        for ( auto& e: cMap ) {
          jCha = e.first;
          if ( cryMap[jCha] < 0 ) continue;
          TH1D* h = e.second;
          h->Draw();
          can->Draw();
          can->Modified();
          can->Update();
          std::tuple<double,double,double> fPeak = PeakFitter::fit( *h );
          double mean  = std::get<1>( fPeak );
          double sigma = std::get<2>( fPeak );
          double lFactor = 1;
          double rFactor = 1;
          double rangeMin = mean - ( lFactor * sigma );
          double rangeMax = mean + ( rFactor * sigma );
          TF1* func = new TF1( "f1", "gaus", rangeMin, rangeMax );
          func->SetParameter( 0, std::get<0>( fPeak ) );
          func->SetParameter( 1, mean  );
          func->SetParameter( 2, sigma );
          h->Fit( func, "R" );
          h->Draw();
          can->Draw();
          can->Modified();
          can->Update();
          int iOff = febBounds[iFEB].first;
//          int iOff = febBounds[febMap[iCha]].first;
          int ci = iCha - iOff;
          int cj = jCha - iOff;
          if ( iFEB ) std::swap( ci, cj );
//          if ( febMap[iCha] ) std::swap( ci, cj );
          double cm = corrMean[ci][cj] = func->GetParameter( 1 );
          h2CNum->SetBinContent( ci + 1, cj + 1, ( iCha *100 ) + jCha );
          double cn = cm * chanMean[iCha] * chanMean[jCha];
          double ni = sqrt( pow( chanMean[iCha], 2 ) - cn );
          double nj = sqrt( pow( chanMean[jCha], 2 ) - cn );
          std::cout << "DISENTANGLE PAIR: " << iCha << ' ' << jCha << std::endl;
          h2Corr->SetBinContent( ci + 1, cj + 1, cm );
          h2CNoi->SetBinContent( ci + 1, cj + 1, cn > 0 ?  sqrt(  cn ) :
                                                          -sqrt( -cn ) );
          h2IMin->SetBinContent( ci + 1, cj + 1, ni );
          h2IMax->SetBinContent( ci + 1, cj + 1, nj );
          delete func;
        }
      }
    }
    TGraphErrors* nGra = new TGraphErrors( nCrystal, cNum, nMea, cWid, nRMS );
    TGraph*       iGra = new TGraph      ( nCrystal, cNum, iMea );
    TGraph*       cGra = new TGraph      ( nCrystal, cNum, cMea );
    nGra->SetMarkerStyle( 20 );
    iGra->SetMarkerStyle( 22 );
    iGra->SetMarkerColor(  2 );
    cGra->SetMarkerStyle( 23 );
    cGra->SetMarkerColor(  3 );
    TMultiGraph* mg = new TMultiGraph();
    mg->Add( nGra, "P" );
    mg->Add( iGra, "P" );
    mg->Add( cGra, "P" );
    TCanvas* dGra = new TCanvas( "noiseComponents",
                                 "noiseComponents", 800, 600 );
    dGra->cd();
    dGra->Draw();
    dGra->Modified();
    dGra->Update();
    mg->Draw( "A" );
//    nGra->Draw( "AP" );
//    cGra->Draw( "APsame" );
//    cGra->Draw( "AP" );
    TCanvas* ct = new TCanvas( "correlationCoefficients",
                               "correlationCoefficients",
                               800, 600 );
    ct->cd();
    h2Corr->SetStats( kFALSE );
    h2Corr->SetBarOffset(  0.2 );
    h2Corr->Draw( "colz0TEXT" );
    h2CNum->SetBarOffset( -0.2 );
    h2CNum->Draw( "TEXT SAME" );
    ct->Draw();
    ct->Modified();
    ct->Update();

    TCanvas* cn = new TCanvas( "correlatedNoise",
                               "correlatedNoise",
                               800, 600 );
    cn->cd();
    h2CNoi->SetStats( kFALSE );
    h2CNoi->SetBarOffset(  0.2 );
    h2CNoi->Draw( "colz0TEXT" );
    h2CNum->SetBarOffset( -0.2 );
    h2CNum->Draw( "TEXT SAME" );
    cn->Draw();
    cn->Modified();
    cn->Update();

    TCanvas* ni = new TCanvas( "uncorrelatedNoiseLeftChannel",
                               "uncorrelatedNoiseLeftChannel",
                               800, 600 );
    ni->cd();
    h2IMin->SetStats( kFALSE );
    h2IMin->SetBarOffset(  0.2 );
    h2IMin->Draw( "colz0TEXT" );
    h2CNum->SetBarOffset( -0.2 );
    h2CNum->Draw( "TEXT SAME" );
    ni->Draw();
    ni->Modified();
    ni->Update();

    TCanvas* nj = new TCanvas( "uncorrelatedNoiseRightChannel",
                               "uncorrelatedNoiseRightChannel", 800, 600 );
    nj->cd();
    h2IMax->SetStats( kFALSE );
    h2IMax->SetBarOffset(  0.2 );
    h2IMax->Draw( "colz0TEXT" );
    h2CNum->SetBarOffset( -0.2 );
    h2CNum->Draw( "TEXT SAME" );
    nj->Draw();
    nj->Modified();
    nj->Update();

  }

  // A function "save()" can be declared, but if relevant histograms are 
  // marked with "autoSavedObjects" (see the "book()" function) it can be 
  // left not declared and not implemented.
  // If some operation is to be done in addition to the automatic histogram 
  // saving, these operations can be put in this function together with
  // a call to "autoSave()".
//  void save() override {
// //    autoSave(); // automatic save of marked histograms (see above)
//    hMaxPos  ->Write(); // explicit save (alternative to "autoSave()")
//    hMaxValue->Write();
//    return;
//  }

 private:

  const
  unsigned int nChannel;
  unsigned int nCrystal;

  unsigned int rangeFPos =  60;
  unsigned int rangeLPos = 128;

  const
  unsigned int nFEB;
  const
  unsigned int chFEB;
  std::vector<std::pair<unsigned int,unsigned int>> febBounds;
  std::vector<unsigned int> febMap;

  std::vector<int>* colMap;
  std::vector<int>* rowMap;
  std::vector<int>  cryMap;
  std::vector<int>  adcMap;

//  std::vector<int  >  febBin;
//  std::vector<float>  febMin;
//  std::vector<float>  febMax;
  std::vector<TH1D*>  febHist;
//  std::vector<int  > chanBin;
//  std::vector<float> chanMin;
//  std::vector<float> chanMax;
  std::vector<TH1D*> chanHist;
  std::vector<double> chanMean;
  std::vector<double>  febMean;

//  std::vector<std::map<unsigned int,int  >> corrBin;
//  std::vector<std::map<unsigned int,float>> corrMin;
//  std::vector<std::map<unsigned int,float>> corrMax;
  std::vector<std::map<unsigned int,TH1D*>> corrHist;
  std::vector<std::map<unsigned int,double>> corrMean;

};

// concrete factory to create a  analyzer
class NoiseComponentsFactory: public NtuAnalysisFactory<SimpleNtupleData>::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  NoiseComponentsFactory(): NtuAnalysisFactory<SimpleNtupleData>::AbsFactory( "noiseComponents" ) {}
  // create an EventDump when builder is run
  NtuAnalysisSteering<SimpleNtupleData>* create( const UserParametersManager* userPar ) override {
    std::cout << "create NoiseComponents" << std::endl;
    return bindToThisFactory( NoiseComponents::basic_type::create<NoiseComponents>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<NoiseComponents>( keyword(), userPar ) );
//    return bindToThisFactory( NoiseComponents::create( keyword(), userPar ) );
//    return bindToThisFactory( new NoiseComponents( userPar ) );
  }
};
static NoiseComponentsFactory ncmf;



#endif // NtuAnalysis_SimpleNtuple_Modules_NoiseComponents_h
