#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSignal_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSignal_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

class SimpleNtupleGetSignal: public Singleton<SimpleNtupleGetSignal>,
                             public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetSignal>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetSignal           ( const SimpleNtupleGetSignal& x ) = delete;
  SimpleNtupleGetSignal& operator=( const SimpleNtupleGetSignal& x ) = delete;

  ~SimpleNtupleGetSignal() override = default;

  typedef std::remove_pointer<decltype(SimpleNtupleData::signal)>::
               type::const_iterator data_iter;

  const std::vector<int>& channels() {
    check();
    return activeChans;
  }

  const data_iter& begin( int channel ) {
    check();
    return signalBegin[channel];
  }

  const data_iter& end  ( int channel ) {
    check();
    return signalEnd  [channel];
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
//    currentData = &ev;
    activeChans.clear();
    signalBegin.clear();
    signalEnd  .clear();
    const std::vector<unsigned short>& signal = *ev.signal;
    const std::vector<           int>& offset = *ev.signalOffsets;
    const std::vector<           int>& counts = *ev.signalCounts;
//    const std::vector<unsigned short>& signal = *currentData->signal;
//    const std::vector<           int>& offset = *currentData->signalOffsets;
//    const std::vector<           int>& counts = *currentData->signalCounts;
    int i;
    int n = offset.size();
    activeChans.reserve( n );
    data_iter ie = signal.end();
    signalBegin.resize ( n, ie );
    signalEnd  .resize ( n, ie );
    for ( i = 0; i < n; ++i ) {
      int m = counts[i];
      if ( m <= 0 ) continue;
      activeChans.push_back( i );
      signalEnd[i] = ( signalBegin[i] = signal.begin() + offset[i] ) + m;
    }
    return;
  }

  typedef std::function<std::pair<data_iter,data_iter>( int )> function;
//  function getFunction( const SimpleNtupleData* ntuData ) {
  function& getFunction() {
    static
    function f = [this]( int i ) {
/*
      check();
      int n = currentData->signalCounts->at( i );
      if ( n > 0 ) {
        auto it = currentData->signal->begin() +
                  currentData->signalOffsets->at( i );
        return std::make_pair( it, it + n );
      }
      else {
        auto it = currentData->signal->end();
        return std::make_pair( it, it );
      }
*/
      return std::make_pair( begin( i ), end( i ) );
    };
    return f;
  };

 private:

  // private constructor being a singleton
  SimpleNtupleGetSignal() = default;

//  const SimpleNtupleData* currentData;
  std::vector<int> activeChans;
  std::vector<data_iter> signalBegin;
  std::vector<data_iter> signalEnd;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSignal_h

