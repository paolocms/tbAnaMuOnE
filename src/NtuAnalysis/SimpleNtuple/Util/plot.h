#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TStyle.h"
#include "TPaveText.h"
#include "TText.h"
#include "TMinuit.h"
#include "TVirtualFitter.h"
#include "TROOT.h"
#include "Utilities/QuadraticFitter.cc"
#include "Utilities/PeakFitter.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>

double round_up(double value, int decimal_places) {
    const double multiplier = std::pow(10.0, decimal_places);
    return std::ceil(value * multiplier) / multiplier;
}
//Recursive fitting function
// condition to meet: minimum relative difference between the peak of fitted function and the peak of the histogram
// input: histogram, initial parameters of the function, LHS and RHS range of the function
// play with the RHS range to meet the condition 
void fit(int nrec, int nmax, TH1D* &h,double &gmax,double &mu0,double &sig0,double &iosl, double &iosr,double num, double frac, double fact=0.5, double v=0.1, double iosri=1){
//void fit(TH1D* &h,double &mu0,double &sig0,double &iosl, double &iosr,double num, double fact=0.5, double v=0.1, double iosri=1){
    std::cout << h->GetName() << " : "
              << h->GetMaximum() << ' ' << nrec << ' '
              << mu0 << ' ' << sig0 << std::endl;
    TF1* fg = new TF1("g","gaus",mu0-iosl*sig0, mu0+iosr*sig0);
    fg->SetParameter(0,gmax);
    fg->SetParameter(1,mu0);
    fg->SetParameter(2,sig0);
    h->Fit(fg,"LER");
/*
//    return;
    TVirtualFitter::SetDefaultFitter("Minuit2");
     h->Fit("gaus","LE","",mu0-iosl*sig0, mu0+iosr*sig0);
    TF1 *f = h->GetFunction("gaus");
*/
    TF1 *f = fg;//h->GetFunction("g");
/*
//     std::cout << "functs: " << fg << ' ' << f << std::endl;
         std::cout << "funct: " << f << std::endl;
    f->SetParameter(0,gmax);
    f->SetParameter(1,mu0);
    f->SetParameter(2,sig0);
         std::cout << "refit: " << f << std::endl;
    h->Fit(f,"R");
*/
     gmax = f->GetParameter(0);
     double mu =f->GetParameter(1);
     double sig=f->GetParameter(2);
     std::cout << "pars: " << mu << ' ' << sig << " ( " << mu0 << ' ' << sig0 << " ) " << num << std::endl;
/*
         mu0=mu;
         sig0=sig;
         return;
     double hmaxX=h->GetMaximumBin();
     double fmaxX=f->GetMaximumX(); //peak of the signal distribution
     //double fmaxX=mu;
     double diffX = abs(hmaxX-fmaxX);

     //Calculate the relative difference with respect to the peak of the signal distribution 
     double relnum= num/hmaxX;
     if (diffX/hmaxX<relnum){
*/
//     if ( fabs( sig0 - sig ) < fabs( sig0 * num ) ) {
     if ( fabs( mu0 - mu ) < num ) {
         mu0=mu;
         sig0=sig;
         return;
     }
     else{
       if( nrec >= nmax ) {
         mu0=mu;
         sig0=sig;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****     MAX NUMBER OF ITERATIONS REACHED     *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "*****                                          *****"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 std::cout << "****************************************************"
                   << std::endl;
	 return;
       }
//       iosl=fact; //fix the LHS range
//       //iosr=fact; //fix the LHS range for rms_noise fitting
//       /*fact=abs(fact-v);
//       if(eps<fact<eps){
//            fact=v;
//            v=v/10;
//            fact=abs(fact -v);
//            iosl=fact;
//        }
//
//        if(iosr==3*iosri)
//           iosr=iosr;*/
//        //else
//          iosr=iosr+0.2; //increase the RHS range by 0.2 until the condition meets
//         //iosl=iosl+0.2; //for rms_noise fitting, increase the LHS range by 0.2 until the condition meets
//	  //fit the histogram with updated parameters 
//       mu=(mu+mu0)/2;  
//       sig=(sig+sig0)/2; 
       mu0=mu;  
       sig0=sig; 
       fit(nrec+1,nmax,h,gmax,mu,sig,iosl,iosr,num,fact,v);
     }
   }

  void DrawPlot( TH1D* h, double &mu0, double &sig0, std::string name_t, double num, double frac, const std::string& cName, double xmin=1, double xmax=1  )
{
    std::cout<<"I am here"<<std::endl;
  h->SetTitle(name_t.c_str());
  h->SetMarkerStyle(20) ;
  h->SetMarkerSize(0.75) ;
  h->SetLineWidth(3);
  h->SetLineColor(kBlack);
  h->GetXaxis()->SetLabelSize(0.04);
  h->GetYaxis()->SetLabelSize(0.04);


//  h->GetXaxis()->SetRangeUser( h->GetMean()-xmin*h->GetRMS(),
//                               h->GetMean()+xmax*h->GetRMS() ) ;
//  mu0=h->GetMean();
  //mu0=h->GetMaximumBin();
//  sig0=h->GetRMS();

/*
  QuadraticFitter q;
  int n = h->GetNbinsX();
  int i;
  int j = -1;
  double p = 0.0;
  for ( i = 1; i <= n; ++i ) {
    double c = h->GetBinContent( i );
    if ( c < p ) continue;
    j = i;
    p = c;
  }
  double k = h->GetBinCenter( j );
//  q.add( h->GetBinCenter( j     ), p );
//  q.add( h->GetBinCenter(  0 ), p );
  double z = p / 2;
  q.add( 0, p - z );
//  int k;
//  q.add( h->GetBinCenter( j - 1 ), h->GetBinContent( j - 1 ) );
  q.add( h->GetBinCenter( j - 1 ) - k, h->GetBinContent( j - 1 ) - z );
//  q.add( h->GetBinCenter( -1 ), h->GetBinContent( j - 1 ) );
  for ( i = j - 2; ( p = h->GetBinContent( i ) ) > z ; i-- )
        q.add( h->GetBinCenter( i ) - k, p - z );
//        q.add( h->GetBinCenter( i ), p );
//  for ( i = -2; ( p = h->GetBinContent( k = i + j ) ) > z ; i-- )
//        q.add( h->GetBinCenter( k ), p );
//  q.add( h->GetBinCenter( j + 1 ), h->GetBinContent( j + 1 ) );
  q.add( h->GetBinCenter( j + 1 ) - k, h->GetBinContent( j + 1 ) - z );
//  q.add( h->GetBinCenter( +1 ), h->GetBinContent( j + 1 ) );
  for ( i = j + 2; ( p = h->GetBinContent( i ) ) > z ; i++ )
        q.add( h->GetBinCenter( i ) - k, p - z );
//        q.add( h->GetBinCenter( i ), p );
//  for ( i = +2; ( p = h->GetBinContent( k = i + j ) ) > z ; i++ )
//        q.add( h->GetBinCenter( k ), p );
  double qa = q.a() + z;
  double qb = q.b();
  double qc = q.c();
    std::cout << h->GetName() << " : "
              << qa << ' '
              << qb << ' ' << qc << std::endl;
//  mu0 = -qb / ( 2*qc );
//  sig0 = sqrt( ( pow( qb / qc, 2 ) / 20 ) - ( qa / ( 5 * qc ) ) );
*/
  std::tuple<double,double,double> fPeak = PeakFitter::fit( *h, frac );
//    mu0 = k - ( qb / ( 2 * qc ) );
  mu0 = std::get<1>( fPeak );
//  mu0 = fPeak.first;
  double iosl=xmin;//2;//1.5;
  double iosr=xmax;//0.5;//2;//1.5;

  //  sig0 = fPeak.second;
  sig0 = std::get<2>( fPeak );
//  sig0 = sqrt( ( pow( qb / qc, 2 ) / 4 ) - ( qa / qc ) ) * 0.62727;
      //
       //    ( iosl + iosr );
  double gmax = std::get<0>( fPeak );//h->GetMaximum();
  ///call fitting function
    std::cout << h->GetName() << " : "
              << h->GetMaximum() << ' '
              << mu0 << ' ' << sig0 << std::endl;
    TF1* fg = new TF1("g","gaus",mu0-iosl*sig0, mu0+iosr*sig0);
    fg->SetParameter(0,gmax);
    fg->SetParameter(1,mu0);
    fg->SetParameter(2,sig0);
    h->GetXaxis()->SetRangeUser( mu0 - 3*sig0, mu0 + 3*sig0 );
//    h->DrawCopy() ;

//    fg->Draw("same");
//    return;
  fit(0,10,h,gmax,mu0,sig0,iosl,iosr,num,frac);
//TVirtualFitter::SetDefaultFitter("Minuit2");
//h->Fit("gaus","","",mu0-0.5*sig0, mu0+1*sig0);

  //TF1 *ft = new TF1("ft","gaus",h->GetMean()-0.1*h->GetRMS(), h->GetMean()+h->GetRMS());
 // ft->SetParameters(h->GetMean(),h->GetRMS());

//  h->GetFunction("gaus")->SetLineColor(kRed);
//  h->GetFunction("gaus")->SetLineStyle(7);
//  h->GetFunction("gaus")->SetLineWidth(3);
  h->GetFunction("g")->SetLineColor(kRed);
  h->GetFunction("g")->SetLineStyle(7);
  h->GetFunction("g")->SetLineWidth(3);

//  fg->SetLineColor(kRed);
//  fg->SetLineStyle(7);
//  fg->SetLineWidth(3);

//  h->GetXaxis()->SetRangeUser( h->GetMean()-1.5*h->GetRMS(),
//                               h->GetMean()+1.5*h->GetRMS() ) ;
//  double m = h->GetMean();
//  double r = h->GetRMS();
//  h->GetXaxis()->SetRangeUser( std::max( m-5*r, 0.0 ),
//                               std::min( m+5*r,
//                                      h->GetXaxis()->GetXmax()      ) ) ;
  double a=h->GetFunction("g")->GetParameter(0);
  double m=h->GetFunction("g")->GetParameter(1);
  double r=h->GetFunction("g")->GetParameter(2);
  h->GetXaxis()->SetRangeUser( std::max( m-5*r, 0.0 ),
                               std::min( m+5*r,
                                      h->GetXaxis()->GetXmax()      ) ) ;
  h->SetMaximum( std::max( h->GetMaximum(), a ) * 1.1 );
  h->DrawCopy() ;
}

///fitting a histogram
//input : histogram, name to save the output, the desired abosulte difference between fitted peak and the distribution's peak
 void fit_histo(TH1D* &h, std::string name, double num, double frac, const std::string& cName, double lRange, double rRange)
{
	std::cout<<name<<std::endl;
	std::string name_t = name;//+" voltage 375";
	TGaxis::SetMaxDigits(3);
	double mu0=h->GetMean();
	//double mu0=h->GetMaximumBin();
	double sig0=h->GetRMS();
	//h->Fit("gaus","VLE","",mu0-0.4*sig0, mu0+1*sig0);
	//h->GetXaxis()->SetRangeUser( h->GetMean()- 1*h->GetRMS(),
        //                       h->GetMean()+1*h->GetRMS() ) ;
	//h->Draw();
	//define the canvas
//    std::cin.ignore();
//	auto c2= (TCanvas*) gROOT->FindObject("gausfit");//"c2");
        static TCanvas* c2 = nullptr;
//        if (c2) delete c2;
        if ( c2 == nullptr )
        c2 = new TCanvas("gausfit","gausfit",800,800);
        c2->cd();
	c2->SetTitle(name_t.c_str());
	//common plotting style
	gStyle->SetStatFormat("6.4g");
	gStyle->SetOptFit(0);
	gStyle->SetOptStat(0);
	gPad->SetGridx();
	gPad->SetGridy();

	//call the function to plot the histogram
	DrawPlot(h,mu0,sig0,name_t,num,frac,cName,lRange,rRange);
//   c2->Update();
//	return;

	double peak=h->GetMaximumBin();
//	double fitmu=h->GetFunction("gaus")->GetParameter(1);
//	double fitsig=h->GetFunction("gaus")->GetParameter(2);
//	double fitmuer=h->GetFunction("gaus")->GetParError(1);
//	double fitsiger=h->GetFunction("gaus")->GetParError(2);
	double fitmu=h->GetFunction("g")->GetParameter(1);
	double fitsig=h->GetFunction("g")->GetParameter(2);
	double fitmuer=h->GetFunction("g")->GetParError(1);
	double fitsiger=h->GetFunction("g")->GetParError(2);
	int rndV=2;
	//TPaveTxt formatting//======================================================
        //following numbers are printed on the canvas
	peak=round_up(peak,rndV);
	fitmu=round_up(fitmu,rndV);
	fitmuer=round_up(fitmuer,rndV);
	fitsig=round_up(fitsig,rndV);
	fitsiger=round_up(fitsiger,rndV);
	mu0=round_up(mu0,rndV);
	sig0=round_up(sig0,rndV);
	//==========
	std::ostringstream oss1;
        oss1 << std::setprecision(8) << std::noshowpoint << peak;
        std::string Peak= oss1.str();
        Peak= "histo_Peak   "+Peak;

	std::ostringstream oss2;
        oss2 << std::setprecision(8) << std::noshowpoint << fitmu;
        std::string Mu= oss2.str();

	std::ostringstream oss2er;
        oss2er << std::setprecision(8) << std::noshowpoint << fitmuer;
        std::string Muer= oss2er.str();
        Mu= "fit_mean   "+Mu+ " +/- "+Muer;

	 std::ostringstream oss3;
        oss3 << std::setprecision(8) << std::noshowpoint << mu0;
        std::string histomu= oss3.str();
	histomu= "Histo_mean   "+histomu;

	 std::ostringstream oss4;
        oss4 << std::setprecision(8) << std::noshowpoint << sig0;
        std::string histosig= oss4.str();
	histosig= "histo_RMS   "+histosig;

	 std::ostringstream oss5;
        oss5 << std::setprecision(8) << std::noshowpoint << fitsig;
        std::string Sig= oss5.str();

	std::ostringstream oss5er;
        oss5er << std::setprecision(8) << std::noshowpoint << fitsiger;
        std::string Siger= oss5er.str();

	Sig= "fit_sigma   "+Sig+ " +/- "+Siger;
	//=====================================================================================
   TPaveText *pt = new TPaveText(.1,.9,.38,.7,"NDC");
   //TPaveText *pt = new TPaveText();
   TText *t1=pt->AddText("Fit paramters");
   t1->SetTextFont(22);
   t1->SetTextSize(0.03);
   //t1->SetTextFont(122);
   pt->AddLine(0,0.9,1,.9);
   pt->AddText(Peak.c_str());
   pt->AddText(histomu.c_str());
   pt->AddText(histosig.c_str());
   pt->AddText(Mu.c_str());
   pt->AddText(Sig.c_str());
   pt->SetFillColor(0);
   pt->SetBorderSize(1);
   pt->SetTextFont(132);
   pt->SetTextSize(0.028);
   pt->SetTextAlign(11);
   pt->SetMargin(0.01);
   pt->Draw();
   c2->Draw();
   c2->Update();
   c2->Modified();

   //save the plot
// DISABLED AT LEAST TEMPORARILY
/*
   std::string plot = name+"v375.png";
   c2->SaveAs(plot.c_str());
*/
}

/// fit all the histograms at the same time
// input : the desired absolute difference between peak of gaussian function and the peak of the signal distribution 
//void plot(const std::vector<TH1D*>& hArr, double num)
TGraphErrors* plot( const std::vector<TH1D*>& hArr, double num, double frac,
                    const std::vector<int>& cryMap, const std::string& cName, double lRange, double rRange )
{
  std::cout << "PLOT... " << std::endl;
  std::cout << "PLOT: " << hArr.size() << std::endl;
	TH1D *hist;
        const char *histA;
//        char *histA = new char[10];
	std::string fileName = "SaveResults"+cName;
//	TFile *f = new TFile("vap_375_0123.root"); //open the file
//	TFile *f = new TFile((fileName+".hist").c_str()); //open the file

	std::ofstream fout;
//	fout.open("vap375_0123.dat"); //save the fitting parameters in a txtfile
	fout.open((fileName+".dat").c_str()); //save the fitting parameters in a txtfile
	//call 25 histograms from the rootfile
//        for(int i=0;i<24;i++){
	int nHis = hArr.size();
        static std::map<std::string,TCanvas*> cTotMap;
        if ( cTotMap.find( cName ) == cTotMap.end() ) cTotMap[cName] = nullptr;
        TCanvas*& cTot = cTotMap[cName];
//	static TCanvas* cTot = nullptr;
	int nCry = 0;
        for ( int x: cryMap ) if ( x >= 0 ) ++nCry;
/*
	if ( cTot == nullptr ) {
//          for ( TH1D* hh: hArr ) if ( hh != nullptr ) nCry++;
          cTot = new TCanvas( "cTot", "cTot", 800, 600 );
          int nCol = ceil( sqrt( nCry ) );
          int nRow = ceil( nCry * 1.0 / nCol );
          cTot->Divide( nCol, nRow );
	}
*/
        static float* cNum = new float[nCry];
        static float* cWid = new float[nCry];
        static float* nMea = new float[nCry];
        static float* nRMS = new float[nCry];
        int cryId = 0;
	std::map<int,TH1D*> cloneMap;
	for(int i=0;i<nHis;i++){
          cryId = cryMap[i];
	  std::cout << "HIST: " << i << ' ' << hArr[i] << std::endl;
//          cNum[i] = nMea[i] = nRMS[i] = 0;
//        for(int i=0;i<25;i++){
//	 sprintf(histA,"crystal_%d",i);
//	 hist = (TH1D*)f->Get(histA);
////	 hist = (TH1D*)f->Get("crystal_1");
//          if ( hArr[i] == nullptr ) continue;
          if ( cryId < 0 ) continue;
          cloneMap[cryId] =
	  hist = dynamic_cast<TH1D*>( hArr[i]->Clone() );
//	  hist = dynamic_cast<TH1D*>( hArr[i]->Clone( ( std::string( hArr[i]->GetName() ) + "clone" ).c_str() ) );
	  std::cout << "....: " << i << ' ' << hist << std::endl;
//	 hist = hArr[i];
	 if ( hist == nullptr ) continue;
	 histA = hist->GetName();
//         cNum[

	 fit_histo(hist,histA,num,frac,cName,lRange,rRange);
//	 continue;
	
//	double fitmu=hist->GetFunction("gaus")->GetParameter(1);
//	double fitsig=hist->GetFunction("gaus")->GetParameter(2);
//	double fitmuer=hist->GetFunction("gaus")->GetParError(1);
//	double fitsiger=hist->GetFunction("gaus")->GetParError(2);
	double fitmu=hist->GetFunction("g")->GetParameter(1);
	double fitsig=hist->GetFunction("g")->GetParameter(2);
	double fitmuer=hist->GetFunction("g")->GetParError(1);
	double fitsiger=hist->GetFunction("g")->GetParError(2);
        

	fout<<i<<" "<<fitmu<<" "<<fitsig<<" "<<fitmuer<<" "<<fitsiger<<std::endl;

        cNum[cryId] = i;
        cWid[cryId] = 0.25;
        nMea[cryId] = fitmu;
        nRMS[cryId] = fitsig;
/*
//	cTot->cd( ++cryId );
	cTot->cd( cryId + 1 );
        std::cout << "Draw in cTot: " << i << ' ' << hArr[i]->GetName() << ' ' << hArr[i]->GetTitle() << ' ' << cryId << std::endl;
//	cTot->cd( i + 1 );
	hist->DrawCopy();
	delete hist;
*/
	}



	if ( cTot == nullptr ) {
//          for ( TH1D* hh: hArr ) if ( hh != nullptr ) nCry++;
//          cTot = new TCanvas( "cTot", "cTot", 800, 600 );
          std::string ncTot = cName + "cTot";
          cTot = new TCanvas( ncTot.c_str(), ncTot.c_str(), 800, 600 );
          int nCol = ceil( sqrt( nCry ) );
          int nRow = ceil( nCry * 1.0 / nCol );
          cTot->Divide( nCol, nRow );
	}
        for ( auto& e: cloneMap ) {
          cryId = e.first;
	  hist = e.second;
          cTot->cd( cryId + 1 );
          std::cout << "Draw in cTot: " << hist->GetName() << ' ' << hist->GetTitle() << ' ' << cryId << std::endl;
          hist->DrawCopy();
          delete hist;
	}

	cTot->Draw();
	cTot->Modified();
	cTot->Update();
	fout.close();
//	static TGraphErrors* nGra = new TGraphErrors( nCry, cNum, nMea, cWid, nRMS );
        TGraphErrors* nGra = new TGraphErrors( nCry, cNum, nMea, cWid, nRMS );
        static std::map<std::string,TCanvas*> cGraMap;
        if ( cGraMap.find( cName ) == cGraMap.end() ) cGraMap[cName] = nullptr;
        TCanvas*& cGra = cGraMap[cName];
//        static TCanvas* cGra = nullptr;
        if ( cGra == nullptr ) {
          std::string ncGra = cName + "cGra";
          cGra = new TCanvas( ncGra.c_str(), ncGra.c_str(), 800, 600 );
        }
        cGra->cd();
	cGra->Draw();
	cGra->Modified();
	cGra->Update();
        nGra->Draw( "AP" );
        //
        return nGra;
//        return nullptr;
}

/// fit all the histograms at the same time
// input : the desired absolute difference between peak of gaussian function and the peak of the signal distribution 
//void plot(const std::string& name, double num)
TGraphErrors* plot(const std::string& name, double num, double frac, const std::string& cName, double lRange, double rRange,
                   const std::vector<int>* rMap = nullptr,
                   const std::vector<int>* cMap = nullptr )
{
  unsigned int i;
  unsigned int n = cMap->size();
  std::vector<int> cryMap( n, -1 );
  for ( i = 0; i < n; ++i ) {
    if ( (*cMap)[i] < 0 ) continue;
    cryMap[i] = ( ( (*rMap)[i] - 1 ) * 5 ) + (*cMap)[i] - 1;
  }
  char *histA = new char[name.length() + 20];
  TH1D *hist;
//  int nMax = 25;
//  int nMax = 32;
  std::vector<TH1D*> hArr( n, nullptr );
  for ( i = 0; i < n; i++ ) {
    sprintf(histA,"%s%d",name.c_str(),i);
    gDirectory->GetObject( histA, hist );
//    hArr[i] = hist;
//    hArr[i] = ( hist->GetEntries() > 0.5 ? hist : nullptr );
    hArr[i] = ( cryMap[i] < 0 ? nullptr : hist );
  }
  return
    plot( hArr, num, frac, cryMap, cName, lRange, rRange );
//  return;
}

/// fit sing histogram
// input : 1. name of the hisogram to be fitted
// input : 2. the desired absolute difference between peak of gaussian function and the peak of the signal distribution
/*
void plot_single(std::string a, double num)
{
	std::string name=a;
        TH1D *hist;
        char *histA = new char[10];
        TFile *f = new TFile("vap_375_0123.root");
         //sprintf(histA,"crystal_0");
         //hist = (TH1D*)f->Get(histA);
         hist = (TH1D*)f->Get(name.c_str());

         fit_histo(hist,name,num);
        double fitmu=hist->GetFunction("gaus")->GetParameter(1);
        double fitsig=hist->GetFunction("gaus")->GetParameter(2);
        double fitmuer=hist->GetFunction("gaus")->GetParError(1);
        double fitsiger=hist->GetFunction("gaus")->GetParError(2);

        std::cout<<" "<<fitmu<<" "<<fitsig<<" "<<fitmuer<<" "<<fitsiger<<std::endl;

        }
*/
