#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetRawEventId_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetRawEventId_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/PolymorphicSingleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

class SimpleNtupleGetRawEventId: public Singleton<SimpleNtupleGetRawEventId>,
                                 public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetRawEventId>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetRawEventId           ( const SimpleNtupleGetRawEventId& x ) = delete;
  SimpleNtupleGetRawEventId& operator=( const SimpleNtupleGetRawEventId& x ) = delete;

  ~SimpleNtupleGetRawEventId() override = default;

  const std::vector<unsigned int>& getOC() {
    check();
    return ocId;
  }

  const std::vector<unsigned int>& getBC() {
    check();
    return bcId;
  }

  const std::set<int>& getChannels() {
    return channels;
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {

    const std::vector<unsigned short>& signal = *ev.signal;
    const std::vector<           int>& offset = *ev.signalOffsets;
    const std::vector<           int>& counts = *ev.signalCounts;

    unsigned int ool = offset[channelOL];
    unsigned int ooh = offset[channelOH];
    unsigned int obc = offset[channelBC];
    unsigned int nol = counts[channelOL];
    unsigned int noh = counts[channelOH];
    unsigned int nbc = counts[channelBC];
    unsigned int nnn = ( nol > noh ? nol : noh );
    if ( nbc > nnn ) nnn = nbc;

    ocId.resize( nnn );
    bcId.resize( nnn );

    unsigned int i;
    for ( i = 0; ( i < nol ) && ( i < noh ); ++i ) {
      unsigned int l = signal[ool + i];
      unsigned int h = signal[ooh + i];
      ocId[i] = ( ( ( h & 0x0000ffff ) << 16 ) & 0xffff0000 ) |
                    ( l & 0x0000ffff );
    }
    while ( i < nnn ) ocId[i++] = 0;
//    while ( i < nol ) ocId[i++] = 0;
//    while ( i < noh ) ocId[i++] = 0;
//    while ( i < nbc ) ocId[i++] = 0;

    for ( i = 0; i < nbc; ++i ) {
      bcId[i] = signal[obc + i];
      bcId[i] |= 0x0000ffff;
    }
    while ( i < nnn ) ocId[i++] = 0;
//    while ( i < nol ) bcId[i++] = 0;
//    while ( i < noh ) bcId[i++] = 0;
//    while ( i < nbc ) bcId[i++] = 0;

    return;

  }


 private:

  // private constructor being a singleton
  SimpleNtupleGetRawEventId() {
    channels.insert( channelOL = 13 );
    channels.insert( channelOH = 14 );
    channels.insert( channelBC = 15 );
    UserParametersManager* uPar =
      PolymorphicSingleton<UserParametersManager>::instance();
    uPar->GET_USER_PARAMETER( channelOL );
    uPar->GET_USER_PARAMETER( channelOH );
    uPar->GET_USER_PARAMETER( channelBC );
  }
  int channelOL;
  int channelOH;
  int channelBC;
  std::set<int> channels;

  std::vector<unsigned int> ocId;
  std::vector<unsigned int> bcId;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetRawEventId_h

