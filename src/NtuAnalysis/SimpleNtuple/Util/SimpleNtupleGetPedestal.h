#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetPedestal_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetPedestal_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
//#include <vector>
//#include <utility>
//#include <functional>

class SimpleNtupleGetPedestal: public Singleton<SimpleNtupleGetPedestal>,
                               public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetPedestal>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetPedestal           ( const SimpleNtupleGetPedestal& x ) = delete;
  SimpleNtupleGetPedestal& operator=( const SimpleNtupleGetPedestal& x ) = delete;

  ~SimpleNtupleGetPedestal() override = default;

  double getPedestal   ( int channel ) {
    check();
    return (*fMean)[channel];
  }
  double getPedestalRMS( int channel ) {
    check();
    return (*fRMS )[channel];
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
    fMean = ev.pedestal;
    fRMS  = ev.pedRMS;
    return;
  }

  typedef std::function<std::pair<double,double>( int )> function;
  function& getFunction() {
    static
    function f = [this]( int i ) {
      return std::make_pair( getPedestal( i ), getPedestalRMS( i ) );
    };
    return f;
  };

 private:

  // private constructor being a singleton
  SimpleNtupleGetPedestal() = default;
//  std::vector<double> fEntries; ///< Number of entries
//  std::vector<double> fTsumw;   ///< Total Sum of weights
//  std::vector<double> fTsumw2;  ///< Total Sum of squares of weights
//  std::vector<double> fMean;    ///< Mean
//  std::vector<double> fRMS;     ///< RMS
  decltype(SimpleNtupleData::pedestal) fMean;
  decltype(SimpleNtupleData::pedRMS  ) fRMS;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetPedestal_h

