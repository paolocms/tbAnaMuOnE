#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetChannelMax_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetChannelMax_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetPedestal.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
#include <tuple>
//#include <vector>
//#include <utility>
//#include <functional>

class SimpleNtupleGetChannelMax: public Singleton<SimpleNtupleGetChannelMax>,
                                 public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetChannelMax>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetChannelMax           ( const SimpleNtupleGetChannelMax& x ) = delete;
  SimpleNtupleGetChannelMax& operator=( const SimpleNtupleGetChannelMax& x ) = delete;

  ~SimpleNtupleGetChannelMax() override = default;

  int getCha() {
    check();
    return cha;
  }

  int getPos() {
    check();
    return pos;
  }

  double getVal() {
    check();
    return val;
  }

  double getSum() {
    check();
    return sum;
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
    static SimpleNtupleGetSignal    * gs =
           SimpleNtupleGetSignal  ::instance();
    static SimpleNtupleGetRawEventId* gi =
           SimpleNtupleGetRawEventId::instance();
    static SimpleNtupleGetPedestal  * gp =
           SimpleNtupleGetPedestal::instance();
    static SimpleNtupleGetMax       * gm =
           SimpleNtupleGetMax     ::instance();
    auto cm = ArrayStatistic::getChannelMax( gs->channels(), gi->getChannels(),
                                             gm->getFunction(),
                                             gp->getFunction() );
    cha = std::get<0>( cm );
    pos = std::get<1>( cm );
    val = std::get<2>( cm );
    sum = std::get<3>( cm );
    return;
  }

 private:

  // private constructor being a singleton
  SimpleNtupleGetChannelMax() = default;
  int cha;
  int pos;
  double val;
  double sum;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetChannelMax_h

