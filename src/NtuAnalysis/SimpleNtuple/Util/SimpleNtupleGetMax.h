#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetMax_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetMax_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
#include <vector>
//#include <utility>
//#include <functional>

class SimpleNtupleGetMax: public Singleton<SimpleNtupleGetMax>,
                          public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetMax>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetMax           ( const SimpleNtupleGetMax& x ) = delete;
  SimpleNtupleGetMax& operator=( const SimpleNtupleGetMax& x ) = delete;

  ~SimpleNtupleGetMax() override = default;

  typedef ArrayStatistic::from<SimpleNtupleGetSignal::data_iter>::pointed_type
                                                                  data_type;
//  typedef std::remove_pointer<decltype(SimpleNtupleData::signal)>::
//               type::const_iterator data_iter;

  int            getPos( int channel ) {
    check();
    return pos[channel];
  }
  unsigned short getVal( int channel ) {
    check();
    return val[channel];
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
    static SimpleNtupleGetSignal    * gs =
           SimpleNtupleGetSignal    ::instance();
    static SimpleNtupleGetRawEventId* gi =
           SimpleNtupleGetRawEventId::instance();
    const std::vector<int>& channels = gs->channels();
    int n = ev.signalOffsets->size();
    pos.clear();
    val.clear();
    pos.resize( n, -1 );
    val.resize( n,  0 );
    ArrayStatistic::getMax( channels, gi->getChannels(), gs->getFunction(),
                            pos.begin(), val.begin() );
    return;
  }

  typedef std::function<std::pair<int,data_type>( int )> function;
  function& getFunction() {
    static
    function f = [this]( int i ) {
      return std::make_pair( getPos( i ), getVal( i ) );
    };
    return f;
  };

//  typedef std::function<std::tuple<int,int,int>( int )> funcSide;
  typedef ArrayStatistic::funcSide funcSide;
  funcSide getFunction( int w, int s ) {
    funcSide f = [this,w,s]( int i ) {
      return std::make_tuple( getPos( i ), w, s );
    };
    return f;
  };

 private:

  // private constructor being a singleton
  SimpleNtupleGetMax() = default;
  std::vector<           int> pos;
  std::vector<unsigned short> val;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetMax_h

