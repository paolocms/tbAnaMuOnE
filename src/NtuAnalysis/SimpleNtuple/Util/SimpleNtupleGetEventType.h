#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetEventType_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetEventType_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
//#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
//#include <vector>
//#include <utility>
//#include <functional>

class SimpleNtupleGetEventType: public Singleton<SimpleNtupleGetEventType>,
                                public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetEventType>;

 public:

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetEventType           ( const SimpleNtupleGetEventType& x ) = delete;
  SimpleNtupleGetEventType& operator=( const SimpleNtupleGetEventType& x ) = delete;

  ~SimpleNtupleGetEventType() override = default;

  char getType() {
    check();
    return type;
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
    type = ev.evtType;
    return;
  }

 private:

  // private constructor being a singleton
  SimpleNtupleGetEventType() = default;
  char type;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetEventType_h

