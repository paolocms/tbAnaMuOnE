#ifndef NtuAnalysis_SimpleNtuple_Util_NtuChannelMap_h
#define NtuAnalysis_SimpleNtuple_Util_NtuChannelMap_h

#include "TFile.h"
#include <vector>

class NtuChannelMap {

 public:

  // deleted constructor and assignment, only static functions
  NtuChannelMap()                                    = delete;
  NtuChannelMap           ( const NtuChannelMap& x ) = delete;
  NtuChannelMap& operator=( const NtuChannelMap& x ) = delete;
  ~NtuChannelMap() = default;

  static void get( const std::vector<int>* colMap,
                   const std::vector<int>* rowMap,
                         std::vector<int>& cryMap,
                         std::vector<int>& adcMap ) {
    unsigned int i = colMap->size();
    unsigned int j = rowMap->size();
    unsigned int n = ( i < j ? i : j );
    adcMap.clear();
    cryMap.clear();
    cryMap.resize( n, -1 );
    for ( i = 0; i < n; ++i ) {
      if ( (*rowMap)[i] < 0 ) continue;
      j = ( ( (*rowMap)[i] - 1 ) * 5 ) + (*colMap)[i] - 1;
      if ( j >= adcMap.size() ) adcMap.resize( j + 1, -1 );
      adcMap[cryMap[i] = j] = i;
    }
    return;
  }

};

#endif // NtuAnalysis_SimpleNtuple_Util_NtuChannelMap_h
