#ifndef NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSums_h
#define NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSums_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetSignal.h"
#include "NtuAnalysis/SimpleNtuple/Util/SimpleNtupleGetRawEventId.h"

#include "Utilities/LazyObserver.h"
#include "Utilities/Singleton.h"
#include "Utilities/ArrayStatistic.h"

#include <iostream>
#include <map>
//#include <utility>
//#include <functional>

class SimpleNtupleGetSums: public Singleton<SimpleNtupleGetSums>,
                           public LazyObserver<SimpleNtupleData> {

  friend class Singleton<SimpleNtupleGetSums>;

 public:

  typedef decltype(SimpleNtupleData::signal) data_type;

  // deleted copy constructor and assignment to prevent unadvertent copy
  SimpleNtupleGetSums           ( const SimpleNtupleGetSums& x ) = delete;
  SimpleNtupleGetSums& operator=( const SimpleNtupleGetSums& x ) = delete;

  ~SimpleNtupleGetSums() override = default;

  const std::vector<double>& getSum( int fPos, int lPos ) {
//  double getSum( int channel, int fPos, int lPos ) {
    check();
    static SimpleNtupleGetSignal  * gs =
           SimpleNtupleGetSignal  ::instance() ;
    static SimpleNtupleGetPedestal* gp =
           SimpleNtupleGetPedestal::instance();
    std::map<int,std::map<int,bool>>::iterator
                                      itf = outdatedSum.find( fPos );
    bool flag = false;
    if ( itf == outdatedSum.end() ) {
      flag = outdatedSum[fPos][lPos] = true;
    }
    else {
      auto& m = itf->second;
      std::map<int,bool>::iterator itl = m.find( lPos );
      if ( itl == m.end() ) flag = m[lPos] = true;
    }
    std::vector<double>& s = sum[fPos][lPos];
    if ( flag ) {
      s.clear();
      s.resize( SimpleNtupleData::nChannels, 0 );
      unsigned int iCha;
      for ( iCha = 0; iCha < SimpleNtupleData::nChannels; ++iCha ) {
        auto iBegin = gs->begin( iCha );
        s[iCha] = ArrayStatistic::getSum( iBegin + fPos, iBegin + lPos,
                                          gp->getPedestal( iCha ) );
      }
      outdatedSum[fPos][lPos] = false;
    }
    return s;
//    return s[iCha];
  }

  const std::vector<double>& getSumSqr( int fPos, int lPos ) {
//  double getSumSqr( int channel, int fPos, int lPos ) {
    check();
    static SimpleNtupleGetSignal  * gs =
           SimpleNtupleGetSignal  ::instance() ;
    static SimpleNtupleGetPedestal* gp =
           SimpleNtupleGetPedestal::instance();
    std::map<int,std::map<int,bool>>::iterator
                                      itf = outdatedSumSqr.find( fPos );
    bool flag = false;
    if ( itf == outdatedSumSqr.end() ) {
      flag = outdatedSumSqr[fPos][lPos] = true;
    }
    else {
      auto& m = itf->second;
      std::map<int,bool>::iterator itl = m.find( lPos );
      if ( itl == m.end() ) flag = m[lPos] = true;
    }
    std::vector<double>& s = sumSqr[fPos][lPos];
    if ( flag ) {
//      std::cout << "compute sum sqr for " << fPos << ' ' << lPos << std::endl;
      s.clear();
      s.resize( SimpleNtupleData::nChannels, 0 );
      unsigned int iCha;
      for ( iCha = 0; iCha < SimpleNtupleData::nChannels; ++iCha ) {
        auto iBegin = gs->begin( iCha );
        s[iCha] = ArrayStatistic::getSumSqr( iBegin + fPos, iBegin + lPos,
                                             gp->getPedestal( iCha ) );
      }
      outdatedSumSqr[fPos][lPos] = false;
    }
//    else std::cout << "recover sum sqr for " << fPos << ' ' << lPos << std::endl;
    return s;
//    return s[iCha];
  }

  // function to be called for each event
  void update( const SimpleNtupleData& ev ) override {
    outdatedSum.clear();
    sum.clear();
    outdatedSumSqr.clear();
    sumSqr.clear();
    return;
  }

 private:

  // private constructor being a singleton
  SimpleNtupleGetSums() = default;

  std::map<int,std::map<int,bool>> outdatedSum;
  std::map<int,std::map<int,std::vector<double>>> sum;
  std::map<int,std::map<int,bool>> outdatedSumSqr;
  std::map<int,std::map<int,std::vector<double>>> sumSqr;

};

#endif // NtuAnalysis_SimpleNtuple_Util_SimpleNtupleGetSums_h

