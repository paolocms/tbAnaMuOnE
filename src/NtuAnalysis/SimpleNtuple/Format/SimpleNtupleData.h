#ifndef NtuAnalysis_SimpleNtuple_Format_SimpleNtupleData_h
#define NtuAnalysis_SimpleNtuple_Format_SimpleNtupleData_h

#include <vector>

class SimpleNtupleData {

 public:

  SimpleNtupleData() {
    signal        = new std::vector<unsigned short>;
    signalOffsets = new std::vector<int>;
    signalCounts  = new std::vector<int>;
    pedestal      = new std::vector<double>;
    pedRMS        = new std::vector<double>;
    signOverRMS   = new std::vector<double>;
  }
  virtual ~SimpleNtupleData() {
    delete signal;
    delete signalOffsets;
    delete signalCounts;
    delete pedestal;
    delete pedRMS;
    delete signOverRMS;
//    std::cout << "delete SimpleNtupleData " << this << std::endl;
  }

  // Data declarations

  int iEvt;                            // event number
  char evtType;                        // event type: b=beam
                                       //             l=laser
                                       //             n=noise
                                       //             u=unclassified
  unsigned int nSig;                   // total number of signal data
  std::vector<unsigned short>* signal; // signal data, with all channels
                                       // queued in a row
  std::vector<int           >* signalOffsets; // start point of data for
                                              // each channel
  std::vector<int           >* signalCounts;  // number of data for
                                              // each channel
  std::vector<double        >* pedestal;      // pedestal values for channels
  std::vector<double        >* pedRMS;        // pedestal RMS for each channel
  std::vector<double        >* signOverRMS;   // pedestal-subtracted max-value
                                              // over pedestal RMS for
                                              // each channel

  static constexpr unsigned int nChannels = 32;
  static constexpr unsigned int nFEB = 2;

 private:

  SimpleNtupleData           ( const SimpleNtupleData& x ) = delete;
  SimpleNtupleData& operator=( const SimpleNtupleData& x ) = delete;

};

#endif // NtuAnalysis_SimpleNtuple_Format_SimpleNtupleData_h

