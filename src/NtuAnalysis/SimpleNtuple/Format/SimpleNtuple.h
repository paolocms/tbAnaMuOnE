#ifndef NtuAnalysis_SimpleNtuple_Format_SimpleNtuple_h
#define NtuAnalysis_SimpleNtuple_Format_SimpleNtuple_h

#include "NtuAnalysis/Common/NtupleBase.h"
#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtupleData.h"
#include "NtuTool/Common/interface/TreeWrapper.h"

#include "TBranch.h"

class SimpleNtuple: protected virtual NtupleBase<SimpleNtupleData>,
                    //protected virtual SimpleNtupleData,
                    public virtual TreeWrapper {

 public:

//  typedef SimpleNtupleData Data;

  SimpleNtuple() {
//    signal        = new std::vector<unsigned short>;
//    signalOffsets = new std::vector<int>;
//    signalCounts  = new std::vector<int>;
//    pedestal      = new std::vector<double>;
//    pedRMS        = new std::vector<double>;
//    signOverRMS   = new std::vector<double>;
  }
  ~SimpleNtuple() override {
//    std::cout << "delete SimpleNtuple " << this << std::endl;
//    delete signal;
  }

  void beginJob() {
    setup();
  }

  void setup() {
    treeName = "simpleNtuple";
    // Associate variables to branches, the same calls can be used 
    // when writing and reading ntuples in place of different calls 
    // to "Branch" (when writing) or "SetBranchAddress" (when reading).
    setBranch( "iEvt"         , &  iEvt         ,
               "iEvt/I"       , &b_iEvt          ); // event number
    setBranch( "evtType"      , &  evtType      ,
               "evtType/B"    , &b_evtType       ); // event type
    setBranch( "nSig"         , &  nSig         ,
               "nSig/i"       , &b_nSig          ); // number of signal data
    setBranch( "signal"       , &  signal       ,
               1000, 99       , &b_signal        ); // signal data
    setBranch( "signalOffsets", &  signalOffsets,
               1000, 99       , &b_signalOffsets ); // signal offsets
    setBranch( "signalCounts" , &  signalCounts ,
               1000, 99       , &b_signalCounts  ); // signal counts
    setBranch( "pedestal"     , &  pedestal     ,
               1000, 99       , &b_pedestal      ); // channel pedestal
    setBranch( "pedRMS"       , &  pedRMS       ,
               1000, 99       , &b_pedRMS        ); // channel pedestal
    setBranch( "signOverRMS"  , &  signOverRMS  ,
               1000, 99       , &b_signOverRMS   ); // signal counts/pedRMS
    return;
  }

  void reset() override {
    autoReset(); // defined in TreeWrapper: it automatically
    return;      // reset to zero all numbers and clears all std::vectors
  }

 protected:

  // List of branches

  TBranch* b_iEvt;
  TBranch* b_evtType;
  TBranch* b_nSig;
  TBranch* b_signal;
  TBranch* b_pedestal;
  TBranch* b_pedRMS;
  TBranch* b_signOverRMS;
  TBranch* b_signalOffsets;
  TBranch* b_signalCounts;

 private:

  SimpleNtuple           ( const SimpleNtuple& x ) = delete;
  SimpleNtuple& operator=( const SimpleNtuple& x ) = delete;

};

#endif // NtuAnalysis_SimpleNtuple_Format_SimpleNtuple_h

