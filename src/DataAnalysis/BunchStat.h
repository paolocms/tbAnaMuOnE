#ifndef DataAnalysis_BunchStat_h
#define DataAnalysis_BunchStat_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class BunchStat: public AnalysisSteering {

 public:

  BunchStat( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  BunchStat           ( const BunchStat& x ) = delete;
  BunchStat& operator=( const BunchStat& x ) = delete;

  ~BunchStat() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

};

#endif // DataAnalysis_BunchStat_h

