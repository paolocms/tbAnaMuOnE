#ifndef DataAnalysis_PrintEvtId_h
#define DataAnalysis_PrintEvtId_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class PrintEvtId: public AnalysisSteering {

 public:

//  static PrintEvtId* create( const std::string& key,
//                                   const UserParametersManager* userPar ) {
//    return nextInstance<PrintEvtId>( key, userPar );
//  }

  PrintEvtId( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  PrintEvtId           ( const PrintEvtId& x ) = delete;
  PrintEvtId& operator=( const PrintEvtId& x ) = delete;

  ~PrintEvtId() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // class member example

//  std::string aStringFromCommandLine;
//  int         aValueFromCommandLine;

  // trivial example: number of processed events
//  int counter;

};

#endif // DataAnalysis_PrintEvtId_h

