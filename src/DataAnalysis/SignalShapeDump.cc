#include "DataAnalysis/SignalShapeDump.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"
#include "DataOperation/SignalStatByChannel.h"
#include "DataOperation/GetMax.h"

#include <iostream>

using namespace std;

// concrete factory to create an SignalShapeDump analyzer
class SignalShapeDumpFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  SignalShapeDumpFactory(): AnalysisFactory::AbsFactory( "sigShapeDump" ) {}
  // create an SignalShapeDump when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create SignalShapeDump" << endl;
    return bindToThisFactory( SignalShapeDump::basic_type::create<SignalShapeDump>( keyword(), userPar ) );
  }
};
// create a global SignalShapeDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an SignalShapeDumpFactory will be available with name "sigShapeDump".
static SignalShapeDumpFactory ssdf;

SignalShapeDump::SignalShapeDump( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


SignalShapeDump::~SignalShapeDump() {
}


// function to be called at execution start
void SignalShapeDump::beginJob() {

  maxPos.resize( Event::maxChannelNumber );
  SignalStatByChannel::instance();
  return;

}


// function to be called at execution end
void SignalShapeDump::endJob() {

  cout << "SignalShapeDump::endJob" << endl;
  SignalStatByChannel* ssbc = SignalStatByChannel::instance();
  const ConnectionMap* connMap = ConnectionMap::instance();
  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; ++i ) {
    if ( connMap->getRow( i ) < 0 ) continue;
    map<int,int>& cmp = maxPos[i];
    map<int,int>  inv;
    for ( auto& e: cmp ) inv[e.second] = e.first;
    int k = 0;
    for ( auto& f: inv ) cout << i << ' '
                              << f.first << ' ' << ( k = f.second ) << endl;
    cout << "signalStatByChannelPos_" << i << ' ' << k << endl;
    cout << "eventCount " << i << ' ' << ssbc->eventCount( i ) << endl;
    const vector<double>& signalMean = ssbc->signalMean( i );
    const vector<double>& signalRMS  = ssbc->signalRMS ( i );
    int n = signalMean.size();
    int j;
    for ( j = 0; j < n; ++j ) {
      cout << "meanSignal " << i << ' ' << j << ' '
           << signalMean[j] << ' '
           << signalRMS [j] << endl;
    }
  }
  return;

}


// function to be called for each event
void SignalShapeDump::update( const Event& ev ) {
  static GetMax* gm = GetMax::instance();
  const vector<int>& channels = ev.channels();
  for ( int i: channels ) {
    int j = gm->getPos( i );
    map<int,int>& cmp = maxPos[i];
    auto e = cmp.insert( map<int,int>::value_type( j, 1 ) );
    if ( !e.second ) ++(e.first->second);
  }
  return;
}

