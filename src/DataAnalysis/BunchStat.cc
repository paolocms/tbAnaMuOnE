#include "DataAnalysis/BunchStat.h"

#include "DataOperation/GetBunchCount.h"
#include "DataOperation/GetSignal.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetEventType.h"
#include "DataOperation/EventAccumulation.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include <iostream>

using namespace std;

// concrete factory to create an BunchStat analyzer
class BunchStatFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  BunchStatFactory(): AnalysisFactory::AbsFactory( "bxst" ) {}
  // create an BunchStat when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create BunchStat" << endl;
    return bindToThisFactory( BunchStat::basic_type::create<BunchStat>( keyword(), userPar ) );
  }
};
// create a global BunchStatFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an BunchStatFactory will be available with name "bxst".
static BunchStatFactory af;

BunchStat::BunchStat( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


BunchStat::~BunchStat() {
}


// function to be called at execution start
void BunchStat::beginJob() {
  return;
}


// function to be called at execution end
void BunchStat::endJob() {
  return;
}


// function to be called for each event
void BunchStat::update( const Event& ev ) {
  static GetBunchCount* gBC = GetBunchCount::instance();
  cout << ev.eventNumber() << ' ' << gBC->current() << ' '
       << gBC->fromFirst() / 40000000 << ' '
       << gBC->fromLast() / 40 << endl;

  return;
}

