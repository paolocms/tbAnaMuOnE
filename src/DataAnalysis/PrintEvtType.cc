#include "DataAnalysis/PrintEvtType.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include <iostream>

using namespace std;

// concrete factory to create an PrintEvtType analyzer
class PrintEvtTypeFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  PrintEvtTypeFactory(): AnalysisFactory::AbsFactory( "pet" ) {}
  // create an PrintEvtType when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create PrintEvtType" << endl;
    return bindToThisFactory( PrintEvtType::basic_type::create<PrintEvtType>( keyword(), userPar ) );
  }
};
// create a global PrintEvtTypeFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an PrintEvtTypeFactory will be available with name "pet".
static PrintEvtTypeFactory af;

PrintEvtType::PrintEvtType( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


PrintEvtType::~PrintEvtType() {
}


// function to be called at execution start
void PrintEvtType::beginJob() {

  for ( GetEventType::type t: GetEventType::typeList() ) count[t] = 0;

  return;

}


// function to be called at execution end
void PrintEvtType::endJob() {

  static const map<GetEventType::type,std::string>& name =
         GetEventType::instance()->typeName();
  for ( GetEventType::type t: GetEventType::typeList() )
        cout << "Event type " << name.at( t ) << " : " << count.at( t ) << endl;
  return;

}


// function to be called for each event
void PrintEvtType::update( const Event& ev ) {

  static GetEventType* get = GetEventType::instance();
  ++count[get->getType()];
  return;

}

