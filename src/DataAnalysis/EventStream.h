#ifndef DataAnalysis_EventStream_h
#define DataAnalysis_EventStream_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class EventStream: public AnalysisSteering {

 public:

  EventStream( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventStream           ( const EventStream& x ) = delete;
  EventStream& operator=( const EventStream& x ) = delete;

  ~EventStream() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  std::string name = "";
  std::ostream* os;

  bool signalTailAtBegin;
  bool oddDataSize;
  char* dataSample;

};

#endif // DataAnalysis_EventStream_h

