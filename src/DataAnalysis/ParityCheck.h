#ifndef DataAnalysis_ParityCheck_h
#define DataAnalysis_ParityCheck_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class ParityCheck: public AnalysisSteering {

 public:

//  static ParityCheck* create( const std::string& key,
//                              const UserParametersManager* userPar ) {
//    return nextInstance<ParityCheck>( key, userPar );
//  }

  ParityCheck( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  ParityCheck           ( const ParityCheck& x ) = delete;
  ParityCheck& operator=( const ParityCheck& x ) = delete;

  ~ParityCheck() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

};

#endif // DataAnalysis_ParityCheck_h

