#ifndef DataAnalysis_ROOT_EventDraw_h
#define DataAnalysis_ROOT_EventDraw_h

#include "DataAnalysis/ROOT/OnlinePlot.h"
#include "Framework/AnalysisSteering.h"
//#include "DataOperation/EventAccumulation.h"

#include "Utilities/ActiveObserver.h"
#include "Utilities/ReplicaCounter.h"

#include <string>
#include <vector>
//#include <memory>

class UserParametersManager;
class Event;
class TCanvas;
class TVirtualPad;
class TGraph;

class EventDraw: public OnlinePlot,
                 public ReplicaCounter<EventDraw> {

 public:

  static EventDraw* create( const std::string& key,
                            const UserParametersManager* userPar ) {
    return nextInstance<EventDraw>( key, userPar );
  }

  EventDraw( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventDraw           ( const EventDraw& x ) = delete;
  EventDraw& operator=( const EventDraw& x ) = delete;

  ~EventDraw() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  std::vector<TGraph*> graphChan;
  TGraph* grSumChan = nullptr;
  TGraph* graphFull = nullptr;
  TCanvas* can = nullptr;
  TVirtualPad* subPlotPad = nullptr;

  int mode = 0;

  std::vector<bool> activeChan;
  int nRow = -1;
  int nCol = -1;
//  int xSize = 800;
//  int ySize = 600;
//  int xPos  = 100;
//  int yPos  = 116;
//  int xOff  =   0;
//  int yOff  =   0;//42;
  int sMin = -1;
  int sMax = -1;
  float xLabelSize;
  float yLabelSize;
  std::vector<float> xLab_cSize;
  std::vector<float> yLab_cSize;
  float xLab_sSize;
  float yLab_sSize;

  void drawSignalsChain( const Event& ev );
  void drawSingleGraphs( const Event& ev );
//  void print();
//  void print( const std::string& name );

//  bool useGraphic;
//  bool saveToFile;

//  std::string name;
//  std::string ext;
//  int  addEvNumber;
  bool applyConnectionMap;

//  char eventType;
  bool channelSum;
//  bool subtractBaseline;
//  bool accumulateEvents;
//  bool averageOverEvents;

//  EventAccumulation* eAcc;
//  GetEventType::type evtType;
  bool accept;

};

#endif // DataAnalysis_ROOT_EventDraw_h

