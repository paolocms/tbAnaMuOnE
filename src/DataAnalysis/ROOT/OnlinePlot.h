#ifndef DataAnalysis_ROOT_OnlinePlot_h
#define DataAnalysis_ROOT_OnlinePlot_h

#include "Framework/AnalysisSteering.h"
//#include "DataOperation/EventAccumulation.h"
#include "DataOperation/GetEventType.h"

#include "Utilities/ActiveObserver.h"
#include "Utilities/ReplicaCounter.h"

#include <string>
#include <vector>

class UserParametersManager;
class EventAccumulation;
class Event;
class TCanvas;

class OnlinePlot: public AnalysisSteering {

 protected:

  OnlinePlot( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  OnlinePlot           ( const OnlinePlot& x ) = delete;
  OnlinePlot& operator=( const OnlinePlot& x ) = delete;

  ~OnlinePlot() override;

  void getParameters();
  bool evtAccept();
  unsigned int evCount();
  TCanvas* getCanvas();
  void updateCanvas( const Event& ev );
  void print();
  void print( const std::string& name );

  int xSize = 800;
  int ySize = 600;
  int xPos  = 100;
  int yPos  = 116;
  int xOff  =   0;
  int yOff  =   0;//42;

  bool useGraphic;
  bool saveToFile;

  std::string name;
  std::string ext;
  int  addEvNumber;

  char eventType;
  bool subtractBaseline;
  bool accumulateEvents;
  bool averageOverEvents;

  EventAccumulation* eAcc;

  GetEventType::type evtType;

 private:

  TCanvas* can = nullptr;

};

#endif // DataAnalysis_ROOT_OnlinePlot_h

