#ifndef DataAnalysis_ROOT_WriteAndAnalyzeTree_h
#define DataAnalysis_ROOT_WriteAndAnalyzeTree_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"
#include "NtuTool/Common/interface/TreeWriter.h"

class TFile;
class TApplication;

class UserParametersManager;
class Event;

//class LaserNoiseModule;
template <class T>
class NtuAnalysisSteering;
//class T;

class WriteAndAnalyzeTree: public AnalysisSteering, // generic data analysis interface
                 public SimpleNtuple,     // ntuple definition
                 public TreeWriter {      // create branches and
                                          //  write ntuple to file

 public:

//  static WriteAndAnalyzeTree* create( const std::string& key,
//                                      const UserParametersManager* userPar ) {
//    return nextInstance<WriteAndAnalyzeTree>( key, userPar );
//  }

  WriteAndAnalyzeTree( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  WriteAndAnalyzeTree           ( const WriteAndAnalyzeTree& x ) = delete;
  WriteAndAnalyzeTree& operator=( const WriteAndAnalyzeTree& x ) = delete;

  ~WriteAndAnalyzeTree() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  TDirectory* topDir;
  TFile* file;

  int ientry;
  int ievent_file;
  int ievent_tot;

//  LaserNoiseModule* lnm;
  std::vector<NtuAnalysisSteering<SimpleNtupleData>*> nms;

};

#endif // DataAnalysis_ROOT_WriteAndAnalyzeTree_h

