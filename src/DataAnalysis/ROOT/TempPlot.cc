#include "DataAnalysis/ROOT/TempPlot.h"
#include "DataAnalysis/ROOT/ApplicationManager.h"

#include "DataOperation/EventAccumulation.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "DataOperation/GetMax.h"
#include "DataOperation/GetPedestal.h"
//#include "DataOperation/GetEventType.h"
#include "Utilities/ArrayStatistic.h"

#include "TApplication.h"
#include "TGraph.h"
#include "TH2.h"
#include "TCanvas.h"

#include <iostream>
#include <sstream>
#include <ctime>


using namespace std;

// concrete factory to create an TempPlot analyzer
class TempPlotFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "tempPlot" as name for this analyzer and factory
  TempPlotFactory(): AnalysisFactory::AbsFactory( "tempPlot" ) {}
  // create an TempPlot when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "TempPlotFactory" << endl;
    return bindToThisFactory( TempPlot::basic_type::create<TempPlot>( keyword(), userPar ) );
//    return bindToThisFactory( ReplicaCounter::create<TempPlot>( keyword(), userPar ) );
//    return bindToThisFactory( TempPlot::create( keyword(), userPar ) );
//    return bindToThisFactory( new TempPlot( userPar ) );
  }
};
// create a global TempPlotFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// a TempPlotFactory will be available with name "tempPlot".
static TempPlotFactory ed;

TempPlot::TempPlot( const UserParametersManager* userPar ):
 OnlinePlot( userPar ) {
//  AnalysisSteering( userPar ) {
  cout << "TempPlot: " << this << endl;
}


TempPlot::~TempPlot() {
}


// function to be called at execution start
void TempPlot::beginJob() {
//  channel = 0;
//  uPar->getUserParameter("channel", channel);
  cout << "TempPlot::beginJob" << endl;
  getParameters();
//  useGraphic = false;
//  saveToFile = true;
//  name = "tpn";
//  if ( replicaCount() > 1 ) name += ( "_" + replicaLabel() );
//  ext = "pdf";
//  addEvNumber = true;
//  accumulateEvents = false;
//  averageOverEvents = false;
//  getAnalysisParameter( "useGraphic" ,
//                         useGraphic  , IdentifiableObject::common );
//  getAnalysisParameter( "saveToFile" ,
//                         saveToFile  , IdentifiableObject::byReplica );
//  getAnalysisParameter( "name"       ,
//                         name        , IdentifiableObject::byReplica );
//  getAnalysisParameter( "plotFormat" ,
//                         ext         , IdentifiableObject::byReplica );
  getAnalysisParameter( "mode"       ,
                         mode        , IdentifiableObject::byReplica );
//  getAnalysisParameter( "addEvNumber",
//                         addEvNumber , IdentifiableObject::byReplica );
//  getAnalysisParameter( "xSize", xSize, IdentifiableObject::byReplica );
//  getAnalysisParameter( "ySize", ySize, IdentifiableObject::byReplica );
//  getAnalysisParameter( "xPos" , xPos , IdentifiableObject::byReplica );
//  getAnalysisParameter( "yPos" , yPos , IdentifiableObject::byReplica );
//  getAnalysisParameter( "xOff" , xOff , IdentifiableObject::byReplica );
//  getAnalysisParameter( "yOff" , yOff , IdentifiableObject::byReplica );
//  getAnalysisParameter( "eventType"       ,
//                         eventType        , IdentifiableObject::byReplica );
//  getAnalysisParameter( "accumulateEvents",
//                         accumulateEvents , IdentifiableObject::byReplica );
//  getAnalysisParameter( "averageOverEvents",
//                         averageOverEvents , IdentifiableObject::byReplica );
//  uPar->getUserParameter<bool>( "tempPlot_addEvNumber", addEvNumber );
//  int argc = uPar->argc();
//  app = new TApplication( "TempPlot", &argc, uPar->argv() );
//  if ( useGraphic ) ApplicationManager::instance( uPar );
//  if ( averageOverEvents ) accumulateEvents = true;
  if ( accumulateEvents ) mode = 0;
//  evtType = GetEventType::unclassified;
//  if ( ( eventType >= 'A' ) && ( eventType <= 'Z' ) )
//         eventType += ( 'a' - 'A' );
//  switch ( eventType ) {
//  case 'n':
//    evtType = GetEventType::noise;
//    break;
//  case 'l':
//    evtType = GetEventType::laser;
//    break;
//  case 'b':
//    evtType = GetEventType::beam;
//    break;
//  case 'u':
//  default:
//    evtType = GetEventType::unclassified;
//    break;
//  }
//  eAcc = EventAccumulation::instance();
//  static ConnectionMap* connMap = ConnectionMap::instance();
  hId = "_" + replicaLabel();
  h2Channels = new TH2F(("channels"+hId).c_str(),"channels",5,-0.5,4.5,5,-0.5,4.5);
//  for (int ych=1; ych <= 5; ++ych) {
//   for (int xch=1; xch <= 5; ++xch) {
////    h2Channels->SetBinContent(xch,ych,(xch-1)+5*(ych-1));
//     h2Channels->SetBinContent(xch,ych,connMap->getChannel(6-ych,xch));
//   }
//  }
  h1TimeSpreadMax = new TH1F(("h1TimeSpreadMax"+hId).c_str(),("h1TimeSpreadMax"+hId).c_str(),20,-10,10);
  h2rawEnergy = ((TH2F*) h2Channels->Clone());
  h2rawEnergy->SetMarkerSize(2.0);
  h2rawEnergy->SetNameTitle(("h2rawEnergy"+hId).c_str(),("h2rawEnergy"+hId).c_str());
  h2PedSubtractedEnergy = ((TH2F*) h2rawEnergy->Clone());
  h2SignalOverNoise     = ((TH2F*) h2rawEnergy->Clone());
  h2PedSubtractedEnergy->SetNameTitle(("h2PedSubtractedEnergy"+hId).c_str(),("h2PedSubtractedEnergy"+hId).c_str());
  h2SignalOverNoise -> SetNameTitle(("h2SignalOverNoise"+hId).c_str(),("h2SignalOverNoise"+hId).c_str());
  return;
}


// function to be called at execution end
void TempPlot::endJob() {
  cout << "TempPlot::endJob" << endl;
//  app->Run( kFALSE );
  return;
}


// function to be called for each event
void TempPlot::update( const Event& ev ) {
  static ConnectionMap* connMap = ConnectionMap::instance();
//  static GetEventType* gt = GetEventType::instance();
//  GetEventType::type et = gt->getType();
//  accept = ( et == evtType ) || ( evtType == GetEventType::unclassified );
  accept = evtAccept();
//  stringstream sstr;
//  sstr << name;
//  if ( addEvNumber ) sstr << ev.eventNumber();
//  const string& cn = sstr.str();
////  TCanvas* can = new TCanvas( cn.c_str() );
  if ( can == nullptr ) {
    can = getCanvas();
//    can = new TCanvas( cn.c_str(), cn.c_str(),
//                       xPos - xOff, yPos - yOff,
//                       xSize      , ySize        );
    if ( mode ) can->Divide(2,2);
  }
//  delete h2Channels;
//  h2Channels = new TH2F("channels","channels",5,-0.5,4.5,5,-0.5,4.5);
  for (int ych=1; ych <= 5; ++ych) {
   for (int xch=1; xch <= 5; ++xch) {
//    h2Channels->SetBinContent(xch,ych,(xch-1)+5*(ych-1));
     h2Channels->SetBinContent(xch,ych,connMap->getChannel(6-ych,xch));
   }
  }

//  TH2F* h2rawEnergy ((TH2F*) h2Channels->Clone());
//  h2rawEnergy->SetMarkerSize(2.0);
//  h2rawEnergy->SetNameTitle(("h2rawEnergy"+hId).c_str(),"h2rawEnergy");
  h2rawEnergy->Reset();
//  TH2F* h2PedSubtractedEnergy((TH2F*) h2rawEnergy->Clone());
//  TH2F* h2SignalOverNoise    ((TH2F*) h2rawEnergy->Clone());
//  h2PedSubtractedEnergy->SetNameTitle(("h2PedSubtractedEnergy"+hId).c_str(),"h2PedSubtractedEnergy");
//  h2SignalOverNoise -> SetNameTitle(("h2SignalOverNoise"+hId).c_str(),"h2SignalOverNoise");
  h2PedSubtractedEnergy->Reset();
  h2SignalOverNoise->Reset();

  int firstChannelwSignal = -1;
//  delete h1TimeSpreadMax;
//  h1TimeSpreadMax = new TH1F("h1TimeSpreadMax","h1TimeSpreadMax",20,-10,10);
  h1TimeSpreadMax->Reset();
  static GetPedestal* gp = GetPedestal::instance();
  static GetMax* gm = GetMax::instance();
  for ( int i: ev.channels() ) {
//    int xch = i%5 + 1;
//    int ych = i/5 + 1;
    int xch =     connMap->getCol( i );
    int ych = 6 - connMap->getRow( i );
    if ( xch < 0 ) continue;
    if ( ych < 0 ) continue;
//    cout << "Active Channel: " << i << "\t xch:ych: " << xch << ":" << ych << endl;
    h2rawEnergy->SetBinContent(xch,ych,gm->getVal(i));
    unsigned int ec = evCount();
//    unsigned int evCount = ( averageOverEvents ? eAcc->eventCount() : 1 );
    if ( accumulateEvents ) {
      const std::vector<double>& s = eAcc->signal( i, evtType );
      h2PedSubtractedEnergy->SetBinContent(xch,ych,
        ArrayStatistic::getMax( s.begin(), s.end() ).second / ec );//evCount );
    }
    else if ( accept ) {
//    else {
      h2PedSubtractedEnergy->SetBinContent(xch,ych, gm->getVal(i)-gp->getPedestal(i));
    }
    double S_N = (gm->getVal(i)-gp->getPedestal(i))/gp->getPedestalRMS(i);
    h2SignalOverNoise    ->SetBinContent(xch,ych,(S_N));
    if (S_N>20) {
      if (firstChannelwSignal < 0) firstChannelwSignal = gm->getPos(i);
      else h1TimeSpreadMax->Fill(gm->getPos(i)-firstChannelwSignal);
    }
  }
  h2Channels->SetBarOffset(-0.2);
  if ( mode ) {
    can->cd(1);
    // h2rawEnergy.DrawClone("colz0");
    h2rawEnergy->SetBarOffset(0.0);
    h2rawEnergy->SetStats( kFALSE );
    h2rawEnergy->Draw("colz0TEXT");
    h2Channels->Draw("TEXT SAME");
    can->cd(2);
  }
  else {
    can->cd();
  }
  h2PedSubtractedEnergy->SetBarOffset(0.0);
  h2PedSubtractedEnergy->SetStats( kFALSE );
  h2PedSubtractedEnergy->Draw("colz0TEXT");
  h2Channels->Draw("TEXT SAME");
  if ( mode ) {
    can->cd(3);
    h2SignalOverNoise->SetBarOffset(0.0);
    h2SignalOverNoise->SetStats( kFALSE );
    h2SignalOverNoise->Draw("colz0TEXT");
    h2Channels->Draw("TEXT SAME");
    can->cd(4);
    h1TimeSpreadMax->Draw();
  }


  if ( !accept && !accumulateEvents ) return;
//  std::time_t result = std::time(nullptr);
//  std::cout << std::asctime(std::localtime(&result))
//            << result << " seconds since the Epoch\n";
  updateCanvas( ev );
//  can->SetTitle( cn.c_str() );
//  if ( useGraphic ) {
//    static ApplicationManager* app = ApplicationManager::instance();
//    can->SetTitle( cn.c_str() );
//    can->Draw();
//    int nReplica = 10 * replicaCount();
//    while ( nReplica-- )
//    app->update( can );
//  }
//  if ( saveToFile ) can->Print( ( cn + "." + ext ).c_str() );

//  cout << "TempPlot: " <<  gm->getPos(channel) 
//  << "\t" << gm->getVal(channel) 
//  // << "\t" << gm->getPedestal(channel)  
//  // << "\t" << gm->getPedestalRMS(channel)  
//  << endl;
  // write event number and number of points
  // const std::vector<Event::data_type>& signal = ev.signal(); //reading all channels and all samples/channel
  // unsigned int n = signal.size();

  // float* x = new float[n];
  // float* y = new float[n];
  // unsigned int i;
  // for ( i = 0; i < n; ++i ) {
  //   x[i] = i;
  //   y[i] = signal[i];
  // }
  // TGraph* g = new TGraph( n, x, y );

  // stringstream sstr;
  // sstr << fileName;
  // if ( addEvNumber ) sstr << ev.eventNumber();
  // const string& cn = sstr.str();
  // TCanvas* can = new TCanvas( cn.c_str() );
  // g->Draw( "AP" );
  // can->Print( ( cn + ".pdf" ).c_str() );

  return;

}

