#include "DataAnalysis/ROOT/PedestalStatistic.h"
#include "DataOperation/GetBunchCount.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetSignal.h"
//#include "DataOperation/GetMax.h"
//#include "DataOperation/GetRawEventId.h"
//#include "DataOperation/GetEventType.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"
#include "Utilities/ArrayStatistic.h"

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"

#include <iostream>
#include <cmath>

using namespace std;

// concrete factory to create an PedestalStatistic analyzer
class PedestalStatisticFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "draw" as name for this analyzer and factory
  PedestalStatisticFactory(): AnalysisFactory::AbsFactory( "pedstat" ) {}
  // create an PedestalStatistic when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( PedestalStatistic::basic_type::create<PedestalStatistic>( keyword(), userPar ) );
  }
};
// create a global PedestalStatisticFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an PedestalStatisticFactory will be available with name "pedstat".
static PedestalStatisticFactory af;

PedestalStatistic::PedestalStatistic( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
  connMap = ConnectionMap::instance();
  getPed  = GetPedestal  ::instance();
  getBC   = GetBunchCount::instance();
}


PedestalStatistic::~PedestalStatistic() {
}


// function to be called at execution start
void PedestalStatistic::beginJob() {

  cout << "PedestalStatistic::beginJob" << endl;

  minDelay = 0;
  getAnalysisParameter( "minDelay",
                         minDelay , IdentifiableObject::byReplica );
  maxDelay = 0;
  getAnalysisParameter( "maxDelay",
                         maxDelay , IdentifiableObject::byReplica );
  minULLD = static_cast<unsigned long long>( minDelay );
  maxULLD = static_cast<unsigned long long>( maxDelay );

  static unsigned int nChannels = Event::maxChannelNumber;
  eventCount.resize( nChannels, 0 );
  pedSum.resize( nChannels, 0 );
  varSum.resize( nChannels, 0 );
  pedHist.resize( nChannels, nullptr );
  sigHist.resize( nChannels, nullptr );
  sigVsRate.resize( nChannels, nullptr );
  int    nBin;
  double rMin;
  double rMax;
  unsigned int i;
  const string& keyW = keyword();
  const std::string& label = replicaLabel();
  const string keyR = keyW + "_" + label;
  cout << keyW << ' ' << keyR << endl;
  for ( i = 0; i < nChannels; ++i ) {
    if ( ( connMap->getRow( i ) < 0 ) ||
         ( connMap->getCol( i ) < 0 ) ) continue;
    std::string hNCha = std::to_string( i );
    string hisPed = "ped";
    string numPed = hisPed + "_" + hNCha;
    string labPed = hisPed + "_" + label + "_" + hNCha;
    uPar->getUserParameter( keyW + "_" + hisPed, nBin,  2000 );
    uPar->getUserParameter( keyW + "_" + hisPed, rMin, 23000.0d );
    uPar->getUserParameter( keyW + "_" + hisPed, rMax, 25000.0d );
    uPar->getUserParameter( keyW + "_" + numPed, nBin );
    uPar->getUserParameter( keyW + "_" + numPed, rMin );
    uPar->getUserParameter( keyW + "_" + numPed, rMax );
    uPar->getUserParameter( keyR + "_" + hisPed, nBin );
    uPar->getUserParameter( keyR + "_" + hisPed, rMin );
    uPar->getUserParameter( keyR + "_" + hisPed, rMax );
    uPar->getUserParameter( keyR + "_" + numPed, nBin );
    uPar->getUserParameter( keyR + "_" + numPed, rMin );
    uPar->getUserParameter( keyR + "_" + numPed, rMax );
    const char* hPed = labPed.c_str();
    pedHist[i] = new TH1F( hPed, hPed, nBin, rMin, rMax );
    string hisSig = "sig";
    string numSig = hisSig + "_" + hNCha;
    string labSig = hisSig + "_" + label + "_" + hNCha;
    uPar->getUserParameter( keyW + "_" + hisSig, nBin,   100 );
    uPar->getUserParameter( keyW + "_" + hisSig, rMin,     0.0d );
    uPar->getUserParameter( keyW + "_" + hisSig, rMax,    50.0d );
    uPar->getUserParameter( keyW + "_" + numSig, nBin );
    uPar->getUserParameter( keyW + "_" + numSig, rMin );
    uPar->getUserParameter( keyW + "_" + numSig, rMax );
    uPar->getUserParameter( keyR + "_" + hisSig, nBin );
    uPar->getUserParameter( keyR + "_" + hisSig, rMin );
    uPar->getUserParameter( keyR + "_" + hisSig, rMax );
    uPar->getUserParameter( keyR + "_" + numSig, nBin );
    uPar->getUserParameter( keyR + "_" + numSig, rMin );
    uPar->getUserParameter( keyR + "_" + numSig, rMax );
    const char* hSig = labSig.c_str();
    sigHist[i] = new TH1F( hSig, hSig, nBin, rMin, rMax );
    string name = "sigVsRate_" + label + "_" + hNCha;
    cout << "create " << name << endl;
    sigVsRate[i] = new TH2F( name.c_str(), name.c_str(), 1500, 0, 15000,
                                                          200, 0, 200 );
  }
  return;

}


// function to be called at execution end
void PedestalStatistic::endJob() {

  cout << "PedestalStatistic::endJob" << endl;

  string fileName = keyword() + "_hist.root";
  getAnalysisParameter( "fileName",
                         fileName , IdentifiableObject::byReplica );
  // see beginJob() for a description of these instructions

  TDirectory* curr = gDirectory; // save current ROOT directory
  TFile file( fileName.c_str(), "RECREATE" );
  unsigned int i;
  for ( i = 0; i < Event::maxChannelNumber; ++i ) {
    if ( ( connMap->getRow( i ) < 0 ) ||
         ( connMap->getCol( i ) < 0 ) ) continue;
    pedHist[i]->Write();
    sigHist[i]->Write();
    sigVsRate[i]->Write();
    int evn = eventCount[i];
    cout << "pedestal_" + replicaLabel() + ": " << i << ' ' << evn << ' '
         << pedSum[i] / evn << ' '
         << sqrt( varSum[i] / evn ) << endl;
  }
  curr->cd();                    // restore      ROOT directory
  
  return;
}


// function to be called for each event
void PedestalStatistic::update( const Event& ev ) {

  static GetSignal* gS = GetSignal::instance();

  static vector<vector<double>> readLists( Event::maxChannelNumber );
  static vector<vector<double>> eSigLists( Event::maxChannelNumber );

/*
  unsigned long long delay = getBC->fromLast() / 40;
//  static GetEventType* gET = GetEventType::instance();
//  static auto oldType = gET->getType();
  if ( (     0 < maxDelay  ) &&
       ( delay > maxULLD ) ) return;
  if ( (     0 < minDelay  ) &&
       ( delay < minULLD ) ) return;
*/
//  auto newType = gET->getType();
//  if ( ( newType != oldType ) && ( newType != GetEventType::unclassified ) ) cout << ev.eventNumber() << ' ' << delay << endl;
//  oldType = newType;
//  cout << ev.eventNumber() << ' ' << delay << ' ' << minDelay << ' ' << minULLD << ' ' << ( delay < minULLD ) << ' ' << maxDelay << ' ' << maxULLD << ' ' << ( delay > maxULLD ) << endl;
//  double ppp = getPed->getPedestal( 4 );
//  cout << replicaLabel() << ' ' << ev.eventNumber() << ' ' << ppp << endl;
  static int fClock = 50;
  for ( int i: ev.channels() ) {      // loop over channels
    if ( ( connMap->getRow( i ) < 0 ) ||
         ( connMap->getCol( i ) < 0 ) ) continue;
    double ped = getPed->getPedestal( i );
    double sig = getPed->getPedestalRMS( i );
    ++eventCount[i];
    pedSum[i] += ped;
    varSum[i] += ( sig * sig );
    pedHist[i]->Fill( ped );
    sigHist[i]->Fill( sig );
    const auto& signal = gS->signal( i );
    int n = signal.size();
    auto& rL = readLists[i];
    rL.resize( n );
    int j;
    double offset = rL[fClock];
    for( j = fClock; j < n; ++j ) rL[j] = signal[j];
    auto sumF = ArrayStatistic::getSums( rL.begin() + fClock, rL.end(), offset );
    double mSig = get<1>( sumF ) * 1.0d / get<0>( sumF );
    double sSig = sqrt( get<2>( sumF ) * 1.0d / get<0>( sumF ) - mSig * mSig );
//    cout << i << ' ' << get<0>( sumF ) << ' ' << get<1>( sumF ) << ' ' << get<2>( sumF ) << endl;
//    cout << i << ' ' << mean << ' ' << vari << endl;
    double sigT = offset + mSig + sSig;
    rL.clear();
    for( j = fClock; j < n; ++j ) {
      auto x = signal[j];
      if ( x < sigT ) rL.push_back( x );
    }
    auto sumB = ArrayStatistic::getSums( rL.begin(), rL.end(), offset );
    double mSel = get<1>( sumB ) * 1.0d / get<0>( sumB );
//    double sSel = sqrt( get<2>( sumS ) * 1.0d / get<0>( sumS ) - mSel * mSel );
    sigT = offset + mSel;
    rL.clear();
    for( j = fClock; j < n; ++j ) {
      auto x = signal[j];
      if ( x > sigT ) rL.push_back( x - sigT );
    }
//    auto sumS = ArrayStatistic::getSums( rL.begin(), rL.end() );
//    cout << i << ' ' << get<1>( sumS ) << endl;
//    sigVsRate[i]->Fill( get<1>( sumS ), sig );
    auto sumS = ArrayStatistic::getSum( rL.begin(), rL.end() );
    sigVsRate[i]->Fill( sumS, sig );
  }

  return;

}


