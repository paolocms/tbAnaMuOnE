#ifndef DataAnalysis_ROOT_TEMPLATEROOTAnalysis_h
#define DataAnalysis_ROOT_TEMPLATEROOTAnalysis_h

#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"

#include <string>
#include <vector>

class UserParametersManager;
class Event;
class TCanvas;

class TH1F;

class TEMPLATEROOTAnalysis: public AnalysisSteering {

 public:

//  static TEMPLATEROOTAnalysis* create( const std::string& key,
//                                       const UserParametersManager* userPar ) {
//    return nextInstance<TEMPLATEROOTAnalysis>( key, userPar );
//  }

  TEMPLATEROOTAnalysis( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  TEMPLATEROOTAnalysis           ( const TEMPLATEROOTAnalysis& x ) = delete;
  TEMPLATEROOTAnalysis& operator=( const TEMPLATEROOTAnalysis& x ) = delete;

  ~TEMPLATEROOTAnalysis() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  TH1F* hist;

};

#endif // DataAnalysis_ROOT_TEMPLATEROOTAnalysis_h

