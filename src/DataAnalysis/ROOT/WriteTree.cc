#include "DataAnalysis/ROOT/WriteTree.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "DataOperation/GetSignal.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetEventType.h"

#include "TFile.h"
//#include "TApplication.h"
#include "TGraph.h"
#include "TCanvas.h"

#include <iostream>
#include <sstream>

using namespace std;

// concrete factory to create an WriteTree analyzer
class WriteTreeFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "ntu" as name for this analyzer and factory
  WriteTreeFactory(): AnalysisFactory::AbsFactory( "ntu" ) {}
  // create an WriteTree when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( WriteTree::basic_type::create<WriteTree>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<WriteTree>( keyword(), userPar ) );
//    return bindToThisFactory( WriteTree::create( keyword(), userPar ) );
//    return bindToThisFactory( new WriteTree( userPar ) );
  }
};
// create a global WriteTreeFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an WriteTreeFactory will be available with name "ntu".
static WriteTreeFactory wt;

WriteTree::WriteTree( const UserParametersManager* userPar ):
  AnalysisSteering( userPar ) {
}


WriteTree::~WriteTree() {
}


// function to be called at execution start
void WriteTree::beginJob() {

  cout << "WriteTree::beginJob" << endl;
  bool recreate = uPar->getUserParameter<bool>( "ntuRecreateFile" );
  file = TFile::Open( uPar->getUserParameter( keyword() ).c_str(),
                      recreate ? "RECREATE" : "CREATE" );
  if ( !file && !recreate ) {
    throw std::invalid_argument( "Failed to create file " +
                                 uPar->getUserParameter( "ntu" ) +
                                 ( recreate ? "." :
                                   "(maybe it already exists)." ) );
  }
  
  setup();                   // define ntuple structure (in SimpleNtuple)
  initWrite( file );         // create tree branches    (in TreeWriter  )


  // "colMap" and "rowMap" vectors are stored in the same ROOT file
  // for convenience, they're NOT part of the ntuple
  vector<int> colMap( Event::maxChannelNumber, -1 );
  vector<int> rowMap( Event::maxChannelNumber, -1 );

  auto connMap = ConnectionMap::instance();
  for ( size_t i = 0; i < Event::maxChannelNumber; i++ ) {
    colMap[i] = connMap->getCol( i );
    rowMap[i] = connMap->getRow( i );
  }

  file->WriteObject( &colMap, "colMap" );
  file->WriteObject( &rowMap, "rowMap" );

  return;

}


// function to be called at execution end
void WriteTree::endJob() {
  cout << "WriteTree::endJob" << endl;
  close(); // save ntuple (in TreeWriter)
  delete file;
  return;
}


// function to be called for each event
void WriteTree::update( const Event& ev ) {

  static GetSignal   * gs = GetSignal   ::instance();
  static GetPedestal * gp = GetPedestal ::instance();
  static GetMax      * gm = GetMax      ::instance();
  static GetEventType* ge = GetEventType::instance();

  reset();                   // reset ntuple content    (in SimpleNtuple)
  iEvt = ev.eventNumber();
  switch ( ge->getType() ) {
  case GetEventType::beam:
    evtType='b';
    break;
  case GetEventType::laser:
    evtType='l';
    break;
  case GetEventType::noise:
    evtType='n';
    break;
  default:
  case GetEventType::unclassified:
    evtType='u';
    break;
  }
  nSig = gs->signal().size();
  *signal = gs->signal();
  
  pedestal     ->resize( Event::maxChannelNumber );
  pedRMS       ->resize( Event::maxChannelNumber );
  signOverRMS  ->resize( Event::maxChannelNumber );
  signalOffsets->resize( Event::maxChannelNumber, -1 );
  signalCounts ->resize( Event::maxChannelNumber, -1 );
  unsigned int offset = 0;
  for ( int i: ev.channels() ) {
    double chp =
    pedestal     ->at( i ) =   gp->getPedestal   ( i );
    double chr =
    pedRMS       ->at( i ) =   gp->getPedestalRMS( i );
    signOverRMS  ->at( i ) = ( gm->getVal        ( i ) - chp ) / chr;
    signalOffsets->at( i ) = offset;
    int chs =
    signalCounts ->at( i ) =   gs->signal        ( i ).size();
    offset += chs;
  }
  if ( offset != nSig ) {
    clog << offset << " vs " << nSig << endl;
  }
  
  fill();                    // write ntuple to file    (in TreeWriter  )

  return;

}

