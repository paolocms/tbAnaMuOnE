#ifndef DataAnalysis_ROOT_ApplicationManager_h
#define DataAnalysis_ROOT_ApplicationManager_h

//#include "Utilities/PolymorphicSingleton.h"
#include "Utilities/Singleton.h"
#include "NtuTool/Common/interface/UserParametersManager.h"

#include "TApplication.h"
#include "TSystem.h"
#include "TPad.h"

#include <string>
#include <iostream>

class ApplicationManager: public Singleton<ApplicationManager> {

//  friend class PolymorphicSingleton<ApplicationManager>;
//  friend class Singleton<ApplicationManager>;

 public:

  static ApplicationManager* instance(
                             const UserParametersManager* up = nullptr ) {
    static ApplicationManager* ptr = new ApplicationManager( up );
    return ptr;
  }

  static void update( TPad* p ) {
    p->Modified();
    p->Update();
    gSystem->ProcessEvents();
  }

  TApplication* getApplication() {
    return app;
  }

  // deleted copy constructor and assignment to prevent unadvertent copy
  ApplicationManager           ( const ApplicationManager& x ) = delete;
  ApplicationManager& operator=( const ApplicationManager& x ) = delete;

  ~ApplicationManager() override {}

 private:

  // private constructor being a singleton
  ApplicationManager( const UserParametersManager* up ) {
/*
    int argc = up->getUserParameter<int>( "main_argc" );
    char** argv = new char*[argc];
    for ( int iarg = 0; iarg < argc; ++iarg ) {
      const std::string& args =
            up->getUserParameter( "main_argv" + std::to_string( iarg ) );
      const
      char* argp = args.c_str();
      char* argq = argv[iarg] = new char[args.length() + 1];
      while (( *argq++ = *argp++ ));
    }
*/
    int argc = 1;
    char** argv = new char*;
    const std::string& name = up->getUserParameter( "exe_name" );
    const
    char* args = name.c_str();
    char* argt = *argv = new char[name.length() + 1];
    while (( *argt++ = *args++ ));
    app = new TApplication( "tApp", &argc, argv );
    app->SetReturnFromRun( kTRUE );
  }

  TApplication* app;

};

#endif // DataAnalysis_ROOT_ApplicationManager_h

