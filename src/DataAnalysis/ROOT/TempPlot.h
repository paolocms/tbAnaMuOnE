#ifndef DataAnalysis_ROOT_TempPlot_h
#define DataAnalysis_ROOT_TempPlot_h

#include "DataAnalysis/ROOT/OnlinePlot.h"
#include "Framework/AnalysisSteering.h"
//#include "DataOperation/EventAccumulation.h"

#include "Utilities/ActiveObserver.h"
#include "Utilities/ReplicaCounter.h"

#include <string>

class UserParametersManager;
class Event;
class TCanvas;
class TH1F;
class TH2F;

//class TApplication;

class TempPlot: public OnlinePlot,
                public ReplicaCounter<TempPlot> {

 public:

//  static TempPlot* create( const std::string& key,
//                           const UserParametersManager* userPar ) {
//    return nextInstance<TempPlot>( key, userPar );
//  }

  TempPlot( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  TempPlot           ( const TempPlot& x ) = delete;
  TempPlot& operator=( const TempPlot& x ) = delete;

  ~TempPlot() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

//  TApplication* app;

  TH1F* h1TimeSpreadMax = nullptr;
  TH2F* h2Channels = nullptr;
  TH2F* h2rawEnergy;
  TH2F* h2PedSubtractedEnergy;
  TH2F* h2SignalOverNoise;
  TCanvas* can = nullptr;

  int mode = 0;

//  int xSize = 800;
//  int ySize = 600;
//  int xPos  = 100;
//  int yPos  = 116;
//  int xOff  =   0;
//  int yOff  =   0;//42;

//  bool useGraphic;
//  bool saveToFile;

//  std::string name;
//  std::string ext;
  std::string hId;
//  bool addEvNumber;

//  char eventType;
//  bool accumulateEvents;
//  bool averageOverEvents;

//  EventAccumulation* eAcc;
//  GetEventType::type evtType;
  bool accept;

};

#endif // DataAnalysis_ROOT_TempPlot_h

