#include "DataAnalysis/ROOT/TEMPLATEROOTAnalysis.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetMax.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "TH1.h"
#include "TFile.h"

#include <iostream>

using namespace std;

// concrete factory to create an TEMPLATEROOTAnalysis analyzer
class TEMPLATEROOTAnalysisFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "draw" as name for this analyzer and factory
  TEMPLATEROOTAnalysisFactory(): AnalysisFactory::AbsFactory( "KEYWORD" ) {}
  // create an TEMPLATEROOTAnalysis when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( TEMPLATEROOTAnalysis::basic_type::create<TEMPLATEROOTAnalysis>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<TEMPLATEROOTAnalysis>( keyword(), userPar ) );
//    return bindToThisFactory( TEMPLATEROOTAnalysis::create( keyword(), userPar ) );
//    return bindToThisFactory( new TEMPLATEROOTAnalysis( userPar ) );
  }
};
// create a global TEMPLATEROOTAnalysisFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an TEMPLATEROOTAnalysisFactory will be available with name "KEYWORD".
static TEMPLATEROOTAnalysisFactory af;

TEMPLATEROOTAnalysis::TEMPLATEROOTAnalysis( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


TEMPLATEROOTAnalysis::~TEMPLATEROOTAnalysis() {
}


// function to be called at execution start
void TEMPLATEROOTAnalysis::beginJob() {

  cout << "TEMPLATEROOTAnalysis::beginJob" << endl;

  const string& hn = uPar->getUserParameter( "histoName" );
  const string hName = ( hn.empty() ? "hSignal" : hn );
  // hn is got from the command line, via
  // -v histoName HISTO_NAME
  // if missing in the command line, getUserParameter returns an empty string
  // and hName is assigned a default string ("hSignal")

  int nBins = 100;
  float sMin = 0.0;
  float sMax = 10000.0;
  uPar->getUserParameter( "histoBins", nBins );
  uPar->getUserParameter( "histoXMin", sMin  );
  uPar->getUserParameter( "histoXmax", sMax  );
  // if in the command line there are instructions
  // "-v histoBins 50"
  // "-v histoXMin 1000"
  // "-v histoXMax 2000"
  // then nBins, sMin and sMax are assigned 50, 1000 and 2000 respectively,
  // otherwise they stay unchanged

  hist = new TH1F( hName.c_str(), hName.c_str(), nBins, sMin, sMax );

  return;

}


// function to be called at execution end
void TEMPLATEROOTAnalysis::endJob() {

  cout << "TEMPLATEROOTAnalysis::endJob" << endl;

  const string& fn = uPar->getUserParameter( "fileName" );
  const string fName = ( fn.empty() ? "hist.root" : fn );
  // see beginJob() for a description of these instructions

  TDirectory* curr = gDirectory; // save current ROOT directory
  TFile file( fName.c_str(), "RECREATE" );
  hist->Write();
  curr->cd();                    // restore      ROOT directory
  
  return;
}


// function to be called for each event
void TEMPLATEROOTAnalysis::update( const Event& ev ) {

  // these objects allow retrieving pedestal and max value for all channels
  static GetPedestal* gp = GetPedestal::instance();
  static GetMax     * gm = GetMax     ::instance();

  float sum = 0;
  for ( int i: ev.channels() )       // loop over channels
    sum += ( gm->getVal( i ) -       // get max signal for channel "i"
             gp->getPedestal( i ) ); // subtract pedestal
  hist->Fill( sum );

  return;

}


