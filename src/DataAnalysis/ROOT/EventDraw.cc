#include "DataAnalysis/ROOT/EventDraw.h"
#include "DataAnalysis/ROOT/ApplicationManager.h"
#include "DataOperation/EventAccumulation.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetSignal.h"
//#include "DataOperation/GetEventType.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "TSystem.h"
#include "TGraph.h"
#include "TAxis.h"
#include "TCanvas.h"

#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>

using namespace std;

// concrete factory to create an EventDraw analyzer
class EventDrawFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "draw" as name for this analyzer and factory
  EventDrawFactory(): AnalysisFactory::AbsFactory( "draw" ) {}
  // create an EventDraw when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( EventDraw::basic_type::create<EventDraw>( keyword(), userPar ) );
//    return bindToThisFactory( ReplicaCounter::create<EventDraw>( keyword(), userPar ) );
//    return bindToThisFactory( EventDraw::create( keyword(), userPar ) );
//    return bindToThisFactory( new EventDraw( userPar ) );
  }
};
// create a global EventDrawFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventDrawFactory will be available with name "draw".
static EventDrawFactory ed;

EventDraw::EventDraw( const UserParametersManager* userPar ):
 OnlinePlot( userPar ) {
//  AnalysisSteering( userPar ) {
}


EventDraw::~EventDraw() {
}


// function to be called at execution start
void EventDraw::beginJob() {
  getParameters();
  xLabelSize = 0.035;
  yLabelSize = 0.035;
  xLab_sSize = 0.08;
  yLab_sSize = 0.08;
//  xLabelSize = 0.1;
//  yLabelSize = 0.08;
//  xLab_sSize = 0.1;
//  yLab_sSize = 0.08;

//  useGraphic = false;
//  saveToFile = true;
//  name = "evn";
//  if ( replicaCount() > 1 ) name += ( "_" + replicaLabel() );
//  ext = "pdf";
//  addEvNumber = 0;
  applyConnectionMap = false;
//  eventType = 'u';
  channelSum = true;
//  subtractBaseline = false;
//  accumulateEvents = false;
//  averageOverEvents = false;
//  getAnalysisParameter( "useGraphic" ,
//                         useGraphic  , IdentifiableObject::common );
//  getAnalysisParameter( "saveToFile" ,
//                         saveToFile  , IdentifiableObject::byReplica );
//  getAnalysisParameter( "name"       ,
//                         name        , IdentifiableObject::byReplica );
//  getAnalysisParameter( "plotFormat" ,
//                         ext         , IdentifiableObject::byReplica );
  getAnalysisParameter( "mode"       ,
                         mode        , IdentifiableObject::byReplica );
//  getAnalysisParameter( "addEvNumber",
//                         addEvNumber , IdentifiableObject::byReplica );
////  getAnalysisParameter( "xSize", xSize, IdentifiableObject::byReplica );
////  getAnalysisParameter( "ySize", ySize, IdentifiableObject::byReplica );
////  getAnalysisParameter( "xPos" , xPos , IdentifiableObject::byReplica );
////  getAnalysisParameter( "yPos" , yPos , IdentifiableObject::byReplica );
////  getAnalysisParameter( "xOff" , xOff , IdentifiableObject::byReplica );
////  getAnalysisParameter( "yOff" , yOff , IdentifiableObject::byReplica );
  getAnalysisParameter( "nRow" , nRow , IdentifiableObject::byReplica );
  getAnalysisParameter( "nCol" , nCol , IdentifiableObject::byReplica );
  getAnalysisParameter( "sMin" , sMin , IdentifiableObject::byReplica );
  getAnalysisParameter( "sMax" , sMax , IdentifiableObject::byReplica );
//  getAnalysisParameter( "subtractBaseline"  ,
//                         subtractBaseline   , IdentifiableObject::byReplica );
  getAnalysisParameter( "applyConnectionMap",
                         applyConnectionMap , IdentifiableObject::byReplica );
//  getAnalysisParameter( "eventType"         ,
//                         eventType          , IdentifiableObject::byReplica );
  getAnalysisParameter( "channelSum"        ,
                         channelSum         , IdentifiableObject::byReplica );
//  getAnalysisParameter( "accumulateEvents"  ,
//                         accumulateEvents   , IdentifiableObject::byReplica );
//  getAnalysisParameter( "averageOverEvents" ,
//                         averageOverEvents  , IdentifiableObject::byReplica );
  bool defaultFlag = true;
  getAnalysisParameter( "default", defaultFlag, IdentifiableObject::byReplica );
  activeChan.resize( Event::maxChannelNumber, defaultFlag );

  getAnalysisParameter( "xLabelSize", xLabelSize, IdentifiableObject::byReplica );
  getAnalysisParameter( "yLabelSize", yLabelSize, IdentifiableObject::byReplica );
  getAnalysisParameter( "xLab_sSize", xLab_sSize, IdentifiableObject::byReplica );
  getAnalysisParameter( "yLab_sSize", yLab_sSize, IdentifiableObject::byReplica );
  xLab_cSize.resize( Event::maxChannelNumber, xLabelSize );
  yLab_cSize.resize( Event::maxChannelNumber, yLabelSize );
  unsigned int iChan;
  for ( iChan = 0; iChan < Event::maxChannelNumber; ++iChan ) {
    bool cf = activeChan[iChan];
    getAnalysisParameter( string( "channel" ) +
                       to_string( iChan ), cf, IdentifiableObject::byReplica );
    activeChan[iChan] = cf;
    string chStr = to_string( iChan );
    getAnalysisParameter( "xLab_cSize"        , xLab_cSize[iChan],
                                                IdentifiableObject::byReplica );
    getAnalysisParameter( "yLab_cSize"        , yLab_cSize[iChan],
                                                IdentifiableObject::byReplica );
    getAnalysisParameter( "xLab_cSize" + chStr, xLab_cSize[iChan],
                                                IdentifiableObject::byReplica );
    getAnalysisParameter( "yLab_cSize" + chStr, yLab_cSize[iChan],
                                                IdentifiableObject::byReplica );
//    xLab_cSize[iChan] *= ( nCol > nRow ? nCol : nRow );
//    yLab_cSize[iChan] *= ( nCol > nRow ? nCol : nRow );
  }
  graphChan.resize( Event::maxChannelNumber, nullptr );
//  if ( useGraphic ) ApplicationManager::instance( uPar );
//  if ( averageOverEvents ) accumulateEvents = true;
//  eAcc = EventAccumulation::instance();
  if ( applyConnectionMap ) {
    const ConnectionMap* connMap = ConnectionMap::instance();
    nCol = connMap->getNCols();
    nRow = connMap->getNRows();
    for ( iChan = 0; iChan < Event::maxChannelNumber; ++iChan ) {
      int ir = connMap->getRow( iChan );
      int ic = connMap->getCol( iChan );
      if ( ( ir < 0 ) || ( ic < 0 ) ) activeChan[iChan] = false;
    }
  }
  unsigned int nCha = 0;
  for ( iChan = 0; iChan < Event::maxChannelNumber; ++iChan ) if ( activeChan[iChan] ) ++nCha;
  if ( nCol < 0 ) nCol = ceil( sqrt( nCha ) );
  if ( nRow < 0 ) nRow = ceil( nCha * 1.0 / nCol );
  float fact = sqrt( nCol < nRow ? nCol : nRow );
  for ( iChan = 0; iChan < Event::maxChannelNumber; ++iChan ) {
    xLab_cSize[iChan] *= fact;
    yLab_cSize[iChan] *= fact;
  }
//  yLabelSize /= ( nCol > nRow ? nCol : nRow );
//  cout << "EventDraw: eventType = " << eventType << endl;
//  evtType = GetEventType::unclassified;
//  if ( ( eventType >= 'A' ) && ( eventType <= 'Z' ) )
//         eventType += ( 'a' - 'A' );
//  switch ( eventType ) {
//  case 'n':
//    evtType = GetEventType::noise;
//    break;
//  case 'l':
//    evtType = GetEventType::laser;
//    break;
//  case 'b':
//    evtType = GetEventType::beam;
//    break;
//  case 'u':
//  default:
//    evtType = GetEventType::unclassified;
//    break;
//  }
  return;
}


// function to be called at execution end
void EventDraw::endJob() {
  return;
}


// function to be called for each event
void EventDraw::update( const Event& ev ) {

//  static GetEventType* gt = GetEventType::instance();
//  GetEventType::type et = gt->getType();
//  accept = ( et == evtType ) || ( evtType == GetEventType::unclassified );
  accept = evtAccept();

//  stringstream sstr;
//  sstr << name;
//  switch ( addEvNumber ) {
//  case 0:
//    break;
//  case 1:
//  default:
//    sstr << ev.eventNumber();
//    break;
//  case 2:
//    sstr << ev.eventHeader().trigger_number;
//    break;
//  }
//  const string cn = sstr.str();

////  if ( can != nullptr ) {
////    xPos = can->GetWindowTopX();
////    yPos = can->GetWindowTopY();
////    delete can;
////  }
//  if ( can == nullptr ) {
//    can = new TCanvas( cn.c_str(), cn.c_str(),
//                       xPos - xOff, yPos - yOff,
//                       xSize      , ySize        );
//  static TCanvas* can = nullptr;
  if ( can == nullptr ) {
    subPlotPad = can = getCanvas();
    if ( channelSum && mode ) {
      can->Divide( 1, 2 );
      can->GetPad( 1 )->SetPad( 0.0, 0.25, 1, 1 );
      can->GetPad( 2 )->SetPad( 0.0, 0.0, 1, 0.25 );
      subPlotPad = can->GetPad( 1 );
    }
    subPlotPad->Divide( nCol, nRow );
  }
/*
  if ( can == nullptr ) {
    can = new TCanvas( ( keyword() + "_" + to_string( replicaId() ) ).c_str(),
                       cn.c_str(),
                       xPos - xOff, yPos - yOff,
                       xSize      , ySize        );
  }
*/

/*
  unsigned int nCha = 0;
  for ( int c: ev.channels() ) if ( activeChan[c] ) ++nCha;
  if ( nCol < 0 ) nCol = ceil( sqrt( nCha ) );
  if ( nRow < 0 ) nRow = ceil( nCha * 1.0 / nCol );
*/

  if ( mode ) drawSingleGraphs( ev );
  else        drawSignalsChain( ev );
//  if ( saveToFile ) print();

  updateCanvas( ev );
//  can->SetTitle( cn.c_str() );
//  if ( useGraphic ) {
//    static ApplicationManager* app = ApplicationManager::instance();
//    can->SetTitle( cn.c_str() );
//    can->Draw();
//    int nReplica = 10 * replicaCount();
//    while ( nReplica-- )
//    app->update( can );
//  }
//  if ( saveToFile ) print( cn );

  return;

}


void EventDraw::drawSignalsChain( const Event& ev ) {

  static GetSignal* gs = GetSignal::instance();

  // write event number and number of points
  const std::vector<Event::data_type>& signal = gs->signal();
  unsigned int n = signal.size();
  unsigned int l = ( eAcc != nullptr ? eAcc->signalSum().size() : n );

  vector<int> chs = ev.channels();
  vector<float> x( n );
  vector<float> y( n );
  unsigned int ec = evCount();
//  unsigned int evCount = ( averageOverEvents ?
//                           eAcc->eventCount( evtType ) : 1 );
//  if ( !evCount ) evCount = 1;
  unsigned int i;
  if ( accumulateEvents ) {
    for ( i = 0; i < n; ++i ) {
      x[i] = i;
      y[i] = eAcc->signal( chs[i / l], evtType )[i % l] / ec;//evCount;
    }
  }
  else if ( accept ) {
    for ( i = 0; i < n; ++i ) {
      x[i] = i;
      y[i] = signal[i];
    }
  }

  if ( !accept && !accumulateEvents ) return;
  delete graphFull;
  graphFull = new TGraph( n, x.data(), y.data() );
  TGraph* g = graphFull;
  g->SetTitle( "Channels chain" );
  g->GetXaxis()->SetRangeUser( x.front() - 0.5, x.back() + 0.5 );
  if ( xLabelSize > 0 ) g->GetXaxis()->SetLabelSize( xLabelSize );
  if ( yLabelSize > 0 ) g->GetYaxis()->SetLabelSize( yLabelSize );
  can->cd();
  g->Draw( "APL" );

  return;

}


void EventDraw::drawSingleGraphs( const Event& ev ) {

  static GetPedestal* gp = GetPedestal::instance();
  static GetSignal  * gs = GetSignal  ::instance();

  static const ConnectionMap* connMap = ConnectionMap::instance();

  const vector<int>& channels = ev.channels();
  if ( sMin < 0 ) sMin = 0;
  if ( sMax < 0 ) {
    sMax = Event::nWords;
    for ( int i: channels ) {
      int cs = gs->signal( i ).size();
      if ( cs < sMax ) sMax = cs;
    }
  }
  if ( sMax < sMin ) {
    int sTmp = sMax;
    sMax = sMin;
    sMin = sTmp;
  }

//  TVirtualPad* subPlotPad = can;
//  if ( channelSum ) {
//    can->Divide( 1, 2 );
//    can->GetPad( 1 )->SetPad( 0.0, 0.25, 1, 1 );
//    can->GetPad( 2 )->SetPad( 0.0, 0.0, 1, 0.25 );
//    subPlotPad = can->GetPad( 1 );
//  }
//  subPlotPad->Divide( nCol, nRow );

  int j = 1;
  vector<float> xSum( sMax - sMin );
  vector<float> ySum( sMax - sMin );
  unsigned int ec = evCount();
//  unsigned int evCount = ( averageOverEvents ?
//                           eAcc->eventCount( evtType ) : 1 );
//  if ( !evCount ) evCount = 1;
  for ( int i: channels ) {
    if ( !activeChan[i] ) continue;
    // write event number and number of points
    const std::vector<Event::data_type>& signal = gs->signal( i );
    int nMax = signal.size();
    unsigned int n = ( nMax < sMax ? nMax : sMax ) - sMin;
    if ( n < 0 ) n = -n;
    if ( !n ) n = 1;
    vector<float> x( n );
    vector<float> y( n );
    unsigned int k;
    unsigned int l;
    int pedestal = 0;
    if ( subtractBaseline ) pedestal = gp->getPedestal( i );
    if ( accumulateEvents ) {
      const std::vector<double>& yAcc    = eAcc->signal( i, evtType );
      const std::vector<double>& yAccSum = eAcc->signalSum( evtType );
      for ( k = 0; k < n; ++k ) {
        l = k + sMin;
        x[k] = l;
        y[k] = yAcc[l] / ec;//evCount;
        xSum[k] = l;
        ySum[k] = yAccSum[l] / ec;//evCount;
      }
    }
//    else {
    else if ( accept ) {
      for ( k = 0; k < n; ++k ) {
        l = k + sMin;
        x[k] = l;
        y[k] = signal[l] - pedestal;
        xSum[k] = l;
        ySum[k] += signal[l] - pedestal;
      }
    }
    if ( !activeChan[i] ) {
      n = 1;
    }
    subPlotPad->cd( applyConnectionMap ?
                    ( ( connMap->getRow( i ) - 1 ) * nCol ) +
                        connMap->getCol( i ) :
                    j++ );
    TGraph*& g = graphChan[i];
    delete g;
    g = new TGraph( n, x.data(), y.data() );
    g->SetTitle( Form( "Channel FEB %d", i ) );
    g->GetXaxis()->SetRangeUser( x.front() - 0.5, x.back() + 0.5 );
    if ( xLab_cSize[i] > 0 ) g->GetXaxis()->SetLabelSize( xLab_cSize[i] );
    if ( yLab_cSize[i] > 0 ) g->GetYaxis()->SetLabelSize( yLab_cSize[i] );
    g->Draw( "APL" );
  }

  if ( channelSum ) {
    can->cd( 2 );
    delete grSumChan;
    grSumChan = new TGraph( sMax - sMin, xSum.data(), ySum.data() );
    TGraph* g = grSumChan;
    g->SetTitle( "Channel sum" );
    g->GetXaxis()->SetRangeUser( xSum.front() - 0.5, xSum.back() + 0.5 );
    if ( xLab_sSize > 0 ) g->GetXaxis()->SetLabelSize( xLab_sSize );
    if ( yLab_sSize > 0 ) g->GetYaxis()->SetLabelSize( yLab_sSize );
    g->Draw( "APL" );
  }

  return;

}

/*
// save plots to file
void EventDraw::print() {
  can->Print( ( string( can->GetName() ) + "." + ext ).c_str() );
  return;
}


// save plots to file
void EventDraw::print( const std::string& name ) {
  can->Print( ( name + "." + ext ).c_str() );
  return;
}
*/

