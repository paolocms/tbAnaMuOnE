#ifndef DataAnalysis_ROOT_WriteTree_h
#define DataAnalysis_ROOT_WriteTree_h

#include "NtuAnalysis/SimpleNtuple/Format/SimpleNtuple.h"
#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"
#include "NtuTool/Common/interface/TreeWriter.h"

class TFile;

class UserParametersManager;
class Event;

class WriteTree: public AnalysisSteering, // generic data analysis interface
                 public SimpleNtuple,     // ntuple definition
                 public TreeWriter {      // create branches and
                                          //  write ntuple to file

 public:

//  static WriteTree* create( const std::string& key,
//                            const UserParametersManager* userPar ) {
//    return nextInstance<WriteTree>( key, userPar );
//  }

  WriteTree( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  WriteTree           ( const WriteTree& x ) = delete;
  WriteTree& operator=( const WriteTree& x ) = delete;

  ~WriteTree() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  TFile* file;

};

#endif // DataAnalysis_ROOT_WriteTree_h

