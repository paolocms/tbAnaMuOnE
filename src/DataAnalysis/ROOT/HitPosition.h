#ifndef DataAnalysis_ROOT_HitPosition_h
#define DataAnalysis_ROOT_HitPosition_h

#include "DataAnalysis/ROOT/OnlinePlot.h"
#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"

#include <string>
#include <vector>

class UserParametersManager;
class Event;
class TCanvas;

class TH2F;

class HitPosition: public OnlinePlot,
                   public ReplicaCounter<HitPosition> {

 public:

//  static HitPosition* create( const std::string& key,
//                                       const UserParametersManager* userPar ) {
//    return nextInstance<HitPosition>( key, userPar );
//  }

  HitPosition( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  HitPosition           ( const HitPosition& x ) = delete;
  HitPosition& operator=( const HitPosition& x ) = delete;

  ~HitPosition() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  float xMin;
  float xMax;
  float yMin;
  float yMax;

  class HistoFiller: public ActiveObserver<Event> {
   public:
    TH2F* hist;
    void update( const Event& ev ) override;
  };

  HistoFiller hf;
  TCanvas* can = nullptr;
  std::string h2DrawOption;

};

#endif // DataAnalysis_ROOT_HitPosition_h

