#include "DataAnalysis/ROOT/HitPosition.h"
//#include "DataOperation/GetPedestal.h"
//#include "DataOperation/GetMax.h"
#include "DataOperation/GetXYPos.h"
#include "DataOperation/GetEventType.h"

#include "Framework/Event.h"
#include "Framework/EventSource.h"
#include "Utilities/PolymorphicSingleton.h"
#include "Framework/AnalysisFactory.h"
//#include "Framework/ConnectionMap.h"

#include "TH2.h"
#include "TLine.h"
#include "TCanvas.h"
//#include "TFile.h"

#include <iostream>

using namespace std;

// concrete factory to create an HitPosition analyzer
class HitPositionFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "draw" as name for this analyzer and factory
  HitPositionFactory(): AnalysisFactory::AbsFactory( "hPos" ) {}
  // create an HitPosition when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( HitPosition::basic_type::create<HitPosition>( keyword(), userPar ) );
//    return bindToThisFactory( ReplicaCounter::create<HitPosition>( keyword(), userPar ) );
//    return bindToThisFactory( HitPosition::create( keyword(), userPar ) );
//    return bindToThisFactory( new HitPosition( userPar ) );
  }
};
// create a global HitPositionFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an HitPositionFactory will be available with name "hPos".
static HitPositionFactory af;

HitPosition::HitPosition( const UserParametersManager* userPar ):
 OnlinePlot( userPar ) {
  evtType = GetEventType::beam;
}


HitPosition::~HitPosition() {
}


// function to be called at execution start
void HitPosition::beginJob() {

  cout << "HitPosition::beginJob" << endl;

  getParameters();

  static GetXYPos* gxy = GetXYPos::instance();
  float xHalfW = gxy->getXHalfW();
  float yHalfW = gxy->getYHalfW();
  int nBinsX = 150;
  int nBinsY = 150;
  xMin = -xHalfW;
  xMax =  xHalfW;
  yMin = -yHalfW;
  yMax =  yHalfW;
  getAnalysisParameter( "nBinsX", nBinsX );
  getAnalysisParameter( "nBinsY", nBinsY );
  h2DrawOption = "box";
  getAnalysisParameter( "h2DrawOption", h2DrawOption );

  string hName = "hitPosHist";
  if ( replicaCount() > 1 ) hName += ( "_" + replicaLabel() );
  hf.hist = new TH2F( hName.c_str(), hName.c_str(), nBinsX, xMin, xMax,
                                                    nBinsY, yMin, yMax );

  return;

}


// function to be called at execution end
void HitPosition::endJob() {

  cout << "HitPosition::endJob" << endl;
/*
  const string& fn = uPar->getUserParameter( "fileName" );
  const string fName = ( fn.empty() ? "hist.root" : fn );
  // see beginJob() for a description of these instructions

  TDirectory* curr = gDirectory; // save current ROOT directory
  TFile file( fName.c_str(), "RECREATE" );
  hist->Write();
  curr->cd();                    // restore      ROOT directory
*/  
  return;
}


// function to be called for each event
void HitPosition::update( const Event& ev ) {
  if ( can == nullptr ) can = getCanvas();
  can->cd();
  hf.hist->Draw( h2DrawOption.c_str() );
  GetXYPos* gxy = GetXYPos::instance();
  TLine* l = new TLine;
  float xS =  gxy->getXSize();
//  float xL = -gxy->getXHalfW();
//  float xR =  gxy->getXHalfW();
  float xM = xMax + ( xS / 10 );// xR + ( xS / 10 );
  float yS =  gxy->getYSize();
//  float yL = -gxy->getYHalfW();
//  float yH =  gxy->getYHalfW();
  float yM = yMax + ( yS / 10 );//  yH + ( yS / 10 );
  float xP;
  for ( xP = xMin; xP < xM; xP += xS ) l->DrawLine( xP, yMin, xP, yMax );
  float yP;
  for ( yP = yMin; yP < yM; yP += yS ) l->DrawLine( xMin, yP, xMax, yP );
  updateCanvas( ev );
  return;
}


void HitPosition::HistoFiller::update( const Event& ev ) {
  static GetEventType* get = GetEventType::instance();
  static UserParametersManager* upm =
         PolymorphicSingleton<UserParametersManager>::instance();
  static unsigned int maxBufferLength = []() {
    unsigned int mbl = 0;
    upm->getUserParameter( "HitPosition_maxBufferLength", mbl );
    return mbl;
  }();
  static unsigned int minBufferLength = []() {
    unsigned int mbl = 0;
    upm->getUserParameter( "HitPosition_minBufferLength", mbl );
    return mbl;
  }();
  static const function<bool()>& dropEvent =
               ev.eventSource()->getDropFunction(
                                 "HitPosDropFunction",
                                 maxBufferLength,
                                 minBufferLength,
                                 &cout );
  if ( dropEvent() ) return;
  if ( get->getType() != GetEventType::beam ) return;
  static GetXYPos* gxy = GetXYPos::instance();
  hist->Fill( gxy->getX(), gxy->getY() );
}



