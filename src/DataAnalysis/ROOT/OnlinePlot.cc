#include "DataAnalysis/ROOT/OnlinePlot.h"
#include "DataAnalysis/ROOT/ApplicationManager.h"
//#include "DataOperation/GetPedestal.h"
//#include "DataOperation/GetSignal.h"
//#include "DataOperation/GetEventType.h"
#include "Framework/Event.h"
#include "DataOperation/EventAccumulation.h"

//#include "Framework/Event.h"
//#include "Framework/AnalysisFactory.h"
//#include "Framework/ConnectionMap.h"

#include "TSystem.h"
//#include "TGraph.h"
//#include "TAxis.h"
#include "TCanvas.h"

#include <iostream>
#include <sstream>
//#include <cmath>
//#include <vector>

using namespace std;

OnlinePlot::OnlinePlot( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
//  EventAccumulation::instance();
}


OnlinePlot::~OnlinePlot() {
}


// function to get the runtime parameters
void OnlinePlot::getParameters() {
  useGraphic = false;
  saveToFile = true;
  name = "evn_" + keyword();
  if ( replicaCount() > 1 ) name += ( "_" + replicaLabel() );
  ext = "pdf";
  addEvNumber = 0;
//  applyConnectionMap = false;
  eventType = 'u';
//  channelSum = true;
  subtractBaseline = false;
  accumulateEvents = false;
  averageOverEvents = false;
  getAnalysisParameter( "useGraphic" ,
                         useGraphic  , IdentifiableObject::common );
  getAnalysisParameter( "saveToFile" ,
                         saveToFile  , IdentifiableObject::byReplica );
  getAnalysisParameter( "name"       ,
                         name        , IdentifiableObject::byReplica );
  getAnalysisParameter( "plotFormat" ,
                         ext         , IdentifiableObject::byReplica );
//  getAnalysisParameter( "mode"       ,
//                         mode        , IdentifiableObject::byReplica );
  getAnalysisParameter( "addEvNumber",
                         addEvNumber , IdentifiableObject::byReplica );
  getAnalysisParameter( "xSize", xSize, IdentifiableObject::byReplica );
  getAnalysisParameter( "ySize", ySize, IdentifiableObject::byReplica );
  getAnalysisParameter( "xPos" , xPos , IdentifiableObject::byReplica );
  getAnalysisParameter( "yPos" , yPos , IdentifiableObject::byReplica );
  getAnalysisParameter( "xOff" , xOff , IdentifiableObject::byReplica );
  getAnalysisParameter( "yOff" , yOff , IdentifiableObject::byReplica );
  getAnalysisParameter( "subtractBaseline"  ,
                         subtractBaseline   , IdentifiableObject::byReplica );
//  getAnalysisParameter( "applyConnectionMap",
//                         applyConnectionMap , IdentifiableObject::byReplica );
  getAnalysisParameter( "eventType"         ,
                         eventType          , IdentifiableObject::byReplica );
//  getAnalysisParameter( "channelSum"        ,
//                         channelSum         , IdentifiableObject::byReplica );
  getAnalysisParameter( "accumulateEvents"  ,
                         accumulateEvents   , IdentifiableObject::byReplica );
  getAnalysisParameter( "averageOverEvents" ,
                         averageOverEvents  , IdentifiableObject::byReplica );
  evtType = GetEventType::unclassified;
  if ( useGraphic ) ApplicationManager::instance( uPar );
  if ( averageOverEvents ) accumulateEvents = true;
  eAcc = ( accumulateEvents ? EventAccumulation::instance() : nullptr );
  cout << keyword() << " accumulate events: " << eAcc << endl;
  evtType = GetEventType::unclassified;
  if ( ( eventType >= 'A' ) && ( eventType <= 'Z' ) )
         eventType += ( 'a' - 'A' );
  switch ( eventType ) {
  case 'n':
    evtType = GetEventType::noise;
    break;
  case 'l':
    evtType = GetEventType::laser;
    break;
  case 'b':
    evtType = GetEventType::beam;
    break;
  case 'u':
  default:
    evtType = GetEventType::unclassified;
    break;
  }
  return;
}


bool OnlinePlot::evtAccept() {
  static GetEventType* gt = GetEventType::instance();
  return ( gt->getType() == evtType ) || ( evtType == GetEventType::unclassified );
}


unsigned int OnlinePlot::evCount() {
//  static EventAccumulation* eAcc = EventAccumulation::instance();
  unsigned int ec = ( averageOverEvents ? eAcc->eventCount( evtType ) : 1 );
  return ( ec ? ec : 1 );
}


TCanvas* OnlinePlot::getCanvas() {
//  static TCanvas* can = nullptr;
  if ( can == nullptr ) {
//    stringstream sstr;
//    sstr << name;
//    switch ( addEvNumber ) {
//    case 0:
//      break;
//    case 1:
//    default:
//      sstr << ev.eventNumber();
//      break;
//    case 2:
//      sstr << ev.eventHeader().trigger_number;
//      break;
//    }
//    const string cn = sstr.str();
//    can = new TCanvas( cn.c_str(), cn.c_str(),
    can = new TCanvas( name.c_str(), name.c_str(),
                       xPos - xOff, yPos - yOff,
                       xSize      , ySize        );
  }
  return can;
}


// update TCanvas
void OnlinePlot::updateCanvas( const Event& ev ) {
  stringstream sstr;
  sstr << name;
//  static EventAccumulation* eAcc = EventAccumulation::instance();
  if ( accumulateEvents && addEvNumber ) sstr << eAcc->eventCount( evtType );
  else
  switch ( addEvNumber ) {
  case 0:
    break;
  case 1:
  default:
    sstr << ev.eventNumber();
    break;
  case 2:
    sstr << ev.eventHeader().trigger_number;
    break;
  }
  const string cn = sstr.str();
//  getCanvas();
  can->SetTitle( cn.c_str() );
  if ( useGraphic ) {
    static ApplicationManager* app = ApplicationManager::instance();
    can->SetTitle( cn.c_str() );
    can->Draw();
    int nReplica = 10 * replicaCount();
    while ( nReplica-- )
    app->update( can );
  }
  if ( saveToFile ) print( cn );
}


// save plots to file
void OnlinePlot::print() {
//  static TCanvas* can = getCanvas();
  can->Print( ( string( can->GetName() ) + "." + ext ).c_str() );
  return;
}


// save plots to file
void OnlinePlot::print( const std::string& name ) {
//  static TCanvas* can = getCanvas();
  can->Print( ( name + "." + ext ).c_str() );
  return;
}


