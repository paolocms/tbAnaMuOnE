#ifndef DataAnalysis_ROOT_PedestalStatistic_h
#define DataAnalysis_ROOT_PedestalStatistic_h

#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"
#include "Utilities/ReplicaCounter.h"

#include <string>
#include <vector>

class UserParametersManager;
class Event;

class ConnectionMap;
class GetPedestal;
class GetBunchCount;

//class TCanvas;
class TH1F;
class TH2F;

class PedestalStatistic: public AnalysisSteering,
                         public ReplicaCounter<PedestalStatistic> {

 public:

  PedestalStatistic( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  PedestalStatistic           ( const PedestalStatistic& x ) = delete;
  PedestalStatistic& operator=( const PedestalStatistic& x ) = delete;

  ~PedestalStatistic() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  ConnectionMap* connMap;
  GetPedestal*   getPed;
  GetBunchCount* getBC;

  int minDelay;
  int maxDelay;
  unsigned long long minULLD;
  unsigned long long maxULLD;

  std::vector<int>    eventCount;
  std::vector<double> pedSum;
  std::vector<double> varSum;
  std::vector<TH1F*> pedHist;
  std::vector<TH1F*> sigHist;
  std::vector<TH2F*> sigVsRate;

};

#endif // DataAnalysis_ROOT_PedestalStatistic_h

