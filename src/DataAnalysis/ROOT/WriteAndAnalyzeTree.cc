#include "DataAnalysis/ROOT/WriteAndAnalyzeTree.h"
#include "DataAnalysis/ROOT/ApplicationManager.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "DataOperation/GetSignal.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetEventType.h"

#include "NtuAnalysis/Common/NtuAnalysisSteering.h"
#include "NtuAnalysis/Common/NtuAnalysisFactory.h"
#include "NtuAnalysis/SimpleNtuple/Modules/LaserNoiseModule.h"
#include "NtuAnalysis/SimpleNtuple/Modules/NoiseCorrelation.h"
#include "NtuAnalysis/SimpleNtuple/Modules/NoiseComponents.h"
#include "NtuAnalysis/SimpleNtuple/Modules/LaserMonitor.h"
#include "Utilities/TimeStamp.h"

#include "TFile.h"
#include "TApplication.h"
#include "TGraph.h"
#include "TCanvas.h"

#include <iostream>
#include <sstream>
#include <future>

using namespace std;

// concrete factory to create an WriteAndAnalyzeTree analyzer
class WriteAndAnalyzeTreeFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "ntu" as name for this analyzer and factory
  WriteAndAnalyzeTreeFactory(): AnalysisFactory::AbsFactory( "nWA" ) {}
  // create an WriteAndAnalyzeTree when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( WriteAndAnalyzeTree::basic_type::create<WriteAndAnalyzeTree>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<WriteAndAnalyzeTree>( keyword(), userPar ) );
//    return bindToThisFactory( WriteAndAnalyzeTree::create( keyword(),
//                              userPar ) );
//    return bindToThisFactory( new WriteAndAnalyzeTree( userPar ) );
  }
};
// create a global WriteAndAnalyzeTreeFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an WriteAndAnalyzeTreeFactory will be available with name "nWA".
static WriteAndAnalyzeTreeFactory wt;

WriteAndAnalyzeTree::WriteAndAnalyzeTree( const UserParametersManager* userPar ):
  AnalysisSteering( userPar ) {
  nms.reserve( 10 );
  include( *uPar );
//  nms.push_back( new LaserNoiseModule( this ) );
//  nms.push_back( new NoiseCorrelation( this ) );
//  nms.push_back( new NoiseComponents ( this ) );
}


WriteAndAnalyzeTree::~WriteAndAnalyzeTree() {
}


// function to be called at execution start
void WriteAndAnalyzeTree::beginJob() {

  cout << "WriteAndAnalyzeTree::beginJob" << endl;
  topDir = gDirectory;
  UserParametersManager* uPar = this;
  nms = NtuAnalysisFactory<SimpleNtupleData>::create( uPar );
  const std::string& name = uPar->getUserParameter( keyword() );
  bool recreate = uPar->getUserParameter<bool>( "ntuRecreateFile" );
  cout << "RECREATE: " << recreate << endl;
  file = TFile::Open( name.c_str(), recreate ? "RECREATE" : "CREATE" );
  if ( !file && !recreate ) {
    throw std::invalid_argument( "Failed to create file " + name +
                                 ( recreate ? "." :
                                   "(maybe it already exists)." ) );
  }
  
  cout << "setup" << endl;
  setup();                   // define ntuple structure (in SimpleNtuple)
  cout << "initWrite" << endl;
  initWrite( file );         // create tree branches    (in TreeWriter  )


  // "colMap" and "rowMap" vectors are stored in the same ROOT file
  // for convenience, they're NOT part of the ntuple
  vector<int> colMap( Event::maxChannelNumber, -1 );
  vector<int> rowMap( Event::maxChannelNumber, -1 );

  auto connMap = ConnectionMap::instance();
  for ( size_t i = 0; i < Event::maxChannelNumber; i++ ) {
    colMap[i] = connMap->getCol( i );
    rowMap[i] = connMap->getRow( i );
  }

  file->WriteObject( &colMap, "colMap" );
  file->WriteObject( &rowMap, "rowMap" );
  ientry = ievent_file = ievent_tot = 0;

//  cout << "new LaserNoiseModule" << endl;
//  lnm = new LaserNoiseModule( uPar );
//  cout << "lnm->beginJob" << endl;
//  lnm->beginJob();
  cout << "modules beginJob" << endl;
  for ( auto nmp: nms ) nmp->beginJob();
//  cout << "lnm->book" << endl;
  cout << "modules book" << endl;
  TDirectory* currentDir = gDirectory;
  topDir->cd();
//  NtuAnalysisSteering<SimpleNtupleData>::saveFunc objSaver =
//                                                  [this]( TObject* o ) {
//                                                    autoSavedObject = o;
//                                                  };
//  lnm->setObjSaver( objSaver );
//  lnm->setObjSaver( &autoSavedObject );
//  lnm->book();
  for ( auto nmp: nms ) {
    nmp->setObjSaver( &autoSavedObject );
    nmp->book();
  }
  currentDir->cd();
  cout << "ApplicationManager" << endl;
  ApplicationManager::instance( uPar );
  cout << "beginJob done" << endl;
//  static ApplicationManager* ptr =
  ApplicationManager::instance();
  cout << "application created" << endl;

 return;

}


// function to be called at execution end
void WriteAndAnalyzeTree::endJob() {
  cout << "WriteAndAnalyzeTree::endJob" << endl;
  for ( auto nmp: nms ) nmp->plotUpdate();
//  cin.ignore();
//  int count = 0;
//  while ( 1 )
//    if ( gSystem->ProcessEvents() ) cout << "********** " << ++count << endl;
  close(); // save ntuple (in TreeWriter)
  delete file;
  topDir->cd();
//  lnm->endJob();
  for ( auto nmp: nms ) nmp->endJob();
//  return;
//  static ApplicationManager* ptr = ApplicationManager::instance();
  bool noPlot = true;
//  lnm->modulePlot( noPlot );
//  for ( auto nmp: nms ) nmp->plotUpdate();
  for ( auto nmp: nms ) nmp->modulePlot( noPlot );
  if ( noPlot ) return;
//  ptr->getApplication()->Run( kTRUE );
  gSystem->ProcessEvents();
  string histFile;
  getAnalysisParameter( "histFile", histFile,
                        IdentifiableObject::byReplica );
  bool recreate = false;
  getAnalysisParameter( "hisRecreateFile", recreate,
                        IdentifiableObject::byReplica );
  cout << "SAVE TO: " << histFile << endl;
//  file = new TFile( histFile.c_str(), "CREATE" );
  file = new TFile( histFile.c_str(), recreate ? "RECREATE" : "CREATE" );
  if ( !file && !recreate ) {
    throw std::invalid_argument( "Failed to create file " + histFile +
                                 ( recreate ? "." :
                                   "(maybe it already exists)." ) );
  }
  autoSave();
  file->Close();
  delete file;

  std::future<void> asyncInput = std::async( std::launch::async,
                                 [](){ cout << "Press ENTER to exit "
                                            << flush;
                                       cin.ignore();
                                       return; } );
  while ( asyncInput.wait_for( std::chrono::milliseconds( 200 ) ) !=
          std::future_status::ready ) gSystem->ProcessEvents();

//  do {
//    gSystem->ProcessEvents();
//    cout << "press ENTER to refresh, CTRL_D to close" << endl;
//  }  while ( cin.ignore() );

  return;
}


// function to be called for each event
void WriteAndAnalyzeTree::update( const Event& ev ) {

  static GetSignal   * gs = GetSignal   ::instance();
  static GetPedestal * gp = GetPedestal ::instance();
  static GetMax      * gm = GetMax      ::instance();
  static GetEventType* ge = GetEventType::instance();

  reset();                   // reset ntuple content    (in SimpleNtuple)
  iEvt = ev.eventNumber();
  switch ( ge->getType() ) {
  case GetEventType::beam:
    evtType='b';
    break;
  case GetEventType::laser:
    evtType='l';
    break;
  case GetEventType::noise:
    evtType='n';
    break;
  default:
  case GetEventType::unclassified:
    evtType='u';
    break;
  }
  nSig = gs->signal().size();
  *signal = gs->signal();
  
  pedestal     ->resize( Event::maxChannelNumber );
  pedRMS       ->resize( Event::maxChannelNumber );
  signOverRMS  ->resize( Event::maxChannelNumber );
  signalOffsets->resize( Event::maxChannelNumber, -1 );
  signalCounts ->resize( Event::maxChannelNumber, -1 );
  unsigned int offset = 0;
  for ( int i: ev.channels() ) {
    double chp =
    pedestal     ->at( i ) =   gp->getPedestal   ( i );
    double chr =
    pedRMS       ->at( i ) =   gp->getPedestalRMS( i );
    signOverRMS  ->at( i ) = ( gm->getVal        ( i ) - chp ) / chr;
    signalOffsets->at( i ) = offset;
    int chs =
    signalCounts ->at( i ) =   gs->signal        ( i ).size();
    offset += chs;
  }
  if ( offset != nSig ) {
    clog << offset << " vs " << nSig << endl;
  }
  
  Dispatcher<SimpleNtupleData>::notify( *this );
//  lnm->analyze( ++ientry, ++ievent_file, ++ievent_tot, file, *this );
//  lnm->plotUpdate();
  ++ientry;
  ++ievent_file;
  ++ievent_tot;
  for ( auto nmp: nms ) {
    nmp->analyze( ientry, ievent_file, ievent_tot, file, *this );
    nmp->plotUpdate();
  }
  fill();                    // write ntuple to file    (in TreeWriter  )

  return;

}

