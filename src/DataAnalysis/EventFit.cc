#include "DataAnalysis/EventFit.h"
#include "Utilities/QuadraticFitter.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "DataOperation/GetMax.h"
#include "DataOperation/GetSignal.h"

#include <iostream>
#include <fstream>

using namespace std;

// concrete factory to create an EventFit analyzer
class EventFitFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "fit" as name for this analyzer and factory
  EventFitFactory(): AnalysisFactory::AbsFactory( "fit" ) {}
  // create an EventFit when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create EventFit" << endl;
    return bindToThisFactory( EventFit::basic_type::create<EventFit>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<EventFit>( keyword(), userPar ) );
//    return bindToThisFactory( EventFit::create( keyword(), userPar ) );
//    return bindToThisFactory( new EventFit( userPar ) );
  }
};
// create a global EventFitFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventFitFactory will be available with name "fit".
static EventFitFactory ed;

EventFit::EventFit( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


EventFit::~EventFit() {
}


// function to be called at execution start
void EventFit::beginJob() {
  sideWidth = 2;
  uPar->getUserParameter( "sideWidth", sideWidth );
  return;
}


// function to be called at execution end
void EventFit::endJob() {
  return;
}


// function to be called for each event
void EventFit::update( const Event& ev ) {

  static GetMax   * gm = GetMax   ::instance();
  static GetSignal* gs = GetSignal::instance();

  int channel = 14;
  const std::vector<Event::data_type>& signal = gs->signal( channel );
  int time;
  int pos = gm->getPos( channel );
  int val = gm->getVal( channel );

  int tmin = pos - sideWidth;
  int tmax = pos + sideWidth;
  QuadraticFitter q;

  double off = 0.0;
  for ( time = tmin; time <= tmax; ++time ) off += signal[time];
  off /= ( 1 + tmax - tmin );
  for ( time = tmin; time <= tmax; ++time ) q.add( time - pos,
                                                   signal[time] - off );

  double a = q.a();
  double b = q.b();
  double c = q.c();
  double maxPos = pos - ( 0.5 * b / c );
  double maxVal = off + a - ( 0.25 * b * b / c );
  cout << "eventFit: "
       << ev.eventNumber() << ' '
       << pos              << ' '
       << val              << " -> "
       << maxPos           << ' '
       << maxVal           << " -> "
       << maxVal - val     << endl;
  cout << a << ' ' << b << ' ' << c << endl;

  return;

}

