#ifndef DataAnalysis_SignalShapeDump_h
#define DataAnalysis_SignalShapeDump_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <map>
#include <iostream>

class UserParametersManager;
class Event;

class SignalShapeDump: public AnalysisSteering {

 public:

  SignalShapeDump( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  SignalShapeDump           ( const SignalShapeDump& x ) = delete;
  SignalShapeDump& operator=( const SignalShapeDump& x ) = delete;

  ~SignalShapeDump() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  std::vector<std::map<int,int>> maxPos;

};

#endif // DataAnalysis_SignalShapeDump_h

