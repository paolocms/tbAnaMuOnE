#include "DataAnalysis/EventStream.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "DataOperation/GetSignal.h"

#include <iostream>
#include <fstream>

using namespace std;

// concrete factory to create an EventStream analyzer
class EventStreamFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  EventStreamFactory(): AnalysisFactory::AbsFactory( "sample" ) {}
  // create an EventStream when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create EventStream" << endl;
    return bindToThisFactory( EventStream::basic_type::create<EventStream>( keyword(), userPar ) );
  }
};
// create a global EventStreamFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventStreamFactory will be available with name "sample".
static EventStreamFactory af;

EventStream::EventStream( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


EventStream::~EventStream() {
}


// function to be called at execution start
void EventStream::beginJob() {

  getAnalysisParameter( "signalTailAtBegin", signalTailAtBegin );
  getAnalysisParameter( "oddDataSize", oddDataSize );
  getAnalysisParameter( "name", name, IdentifiableObject::byReplica );

  if ( name != "" ) {
    os = new ofstream( name, std::ios::binary );
    dataSample = new char[GetSignal::dataSize];
  }
  else {
    cout << "EventStream: missing file name, no output" << endl;
    os = nullptr;
  }
  return;

}


// function to be called at execution end
void EventStream::endJob() {

  delete os;
  return;

}


// function to be called for each event
void EventStream::update( const Event& ev ) {

//  static GetSignal* gs = GetSignal::instance();
  if ( os == nullptr ) return;
  map<int,vector<Event::data_type>> dMap;
  GetSignal::fillSignalMap( ev, dMap, true, true );
  unsigned int i;
  unsigned int n = ev.acquisitionHeader()->sample_per_chan;
  for ( i = 0; i < n; ++i ) {
    GetSignal::fillDataSample( dMap, i, dataSample );
    os->write( dataSample, GetSignal::dataSize );
  }
  return;

}

