#include "DataAnalysis/ParityCheck.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include <iostream>

using namespace std;

// concrete factory to create an ParityCheck analyzer
class ParityCheckFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  ParityCheckFactory(): AnalysisFactory::AbsFactory( "pchk" ) {}
  // create an ParityCheck when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create ParityCheck" << endl;
    return bindToThisFactory( ParityCheck::basic_type::create<ParityCheck>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<ParityCheck>( keyword(), userPar ) );
//    return bindToThisFactory( ParityCheck::create( keyword(), userPar ) );
//    return bindToThisFactory( new ParityCheck( userPar ) );
  }
};
// create a global ParityCheckFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an ParityCheckFactory will be available with name "dump".
static ParityCheckFactory af;

ParityCheck::ParityCheck( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


ParityCheck::~ParityCheck() {
}


// function to be called at execution start
void ParityCheck::beginJob() {
  return;
}


// function to be called at execution end
void ParityCheck::endJob() {
  return;
}


// function to be called for each event
void ParityCheck::update( const Event& ev ) {
  cout << "evt " << ev.eventHeader().trigger_number;
  for ( int i: ev.channels() ) {
    const Event::evtData* ed = ev.eventData( i );
    cout << " - " << ( ed->chID_readPointer & 1 ) << ' ' <<  ed->numOfValidData;
  }
  cout << endl;
  return;
}

