#ifndef DataAnalysis_EventFit_h
#define DataAnalysis_EventFit_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class EventFit: public AnalysisSteering {
//class EventFit: public AnalysisSteering,
//                public ActiveObserver<Event> {

 public:

//  static EventFit* create( const std::string& key,
//                           const UserParametersManager* userPar ) {
//    return nextInstance<EventFit>( key, userPar );
//  }

  EventFit( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventFit           ( const EventFit& x ) = delete;
  EventFit& operator=( const EventFit& x ) = delete;

  ~EventFit() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  int sideWidth;
  std::string name;
  char mode;
  std::ostream* os;

};

#endif // DataAnalysis_EventFit_h

