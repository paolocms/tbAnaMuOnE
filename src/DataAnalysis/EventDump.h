#ifndef DataAnalysis_EventDump_h
#define DataAnalysis_EventDump_h

#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"
#include "Utilities/ReplicaCounter.h"

#include <iostream>

class UserParametersManager;
class Event;

class EventDump: public AnalysisSteering,
                 public ReplicaCounter<EventDump> {

 public:

//  static EventDump* create( const std::string& key,
//                            const UserParametersManager* userPar ) {
//    return nextInstance<EventDump>( key, userPar );
//  }

  EventDump( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventDump           ( const EventDump& x ) = delete;
  EventDump& operator=( const EventDump& x ) = delete;

  ~EventDump() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  std::string name = "";
  char mode = 'a';
  std::ostream* os;
  bool waitingHeader;

  bool getFromOnlineConfig = false;
  bool      saveFullConfig = false;
  bool     appendTimestamp = false;

};

#endif // DataAnalysis_EventDump_h

