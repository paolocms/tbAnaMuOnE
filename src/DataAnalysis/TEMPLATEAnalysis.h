#ifndef DataAnalysis_TEMPLATEAnalysis_h
#define DataAnalysis_TEMPLATEAnalysis_h

#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>

class UserParametersManager;
class Event;

class TEMPLATEAnalysis: public AnalysisSteering {

 public:

//  static TEMPLATEAnalysis* create( const std::string& key,
//                                   const UserParametersManager* userPar ) {
//    return nextInstance<TEMPLATEAnalysis>( key, userPar );
//  }

  TEMPLATEAnalysis( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  TEMPLATEAnalysis           ( const TEMPLATEAnalysis& x ) = delete;
  TEMPLATEAnalysis& operator=( const TEMPLATEAnalysis& x ) = delete;

  ~TEMPLATEAnalysis() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // class member example

  std::string aStringFromCommandLine;
  int         aValueFromCommandLine;

  // trivial example: number of processed events
  int counter;

};

#endif // DataAnalysis_TEMPLATEAnalysis_h

