#ifndef DataAnalysis_PrintEvtType_h
#define DataAnalysis_PrintEvtType_h

#include "DataOperation/GetEventType.h"
#include "Utilities/ActiveObserver.h"
#include "Framework/AnalysisSteering.h"

#include <iostream>
#include <map>

class UserParametersManager;
class Event;

class PrintEvtType: public AnalysisSteering {

 public:

  PrintEvtType( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  PrintEvtType           ( const PrintEvtType& x ) = delete;
  PrintEvtType& operator=( const PrintEvtType& x ) = delete;

  ~PrintEvtType() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

  // trivial example: number of processed events
  std::map<GetEventType::type,int> count;

};

#endif // DataAnalysis_PrintEvtType_h

