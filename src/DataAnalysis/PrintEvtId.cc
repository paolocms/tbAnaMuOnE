#include "DataAnalysis/PrintEvtId.h"
#include "DataOperation/GetRawEventId.h"
#include "DataOperation/GetSignal.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include <iostream>

using namespace std;

// concrete factory to create an PrintEvtId analyzer
class PrintEvtIdFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  PrintEvtIdFactory(): AnalysisFactory::AbsFactory( "pei" ) {}
  // create an PrintEvtId when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create PrintEvtId" << endl;
//    return bindToThisFactory( IdentifiableObject::create<PrintEvtId>( keyword(), userPar ) );
    return bindToThisFactory( PrintEvtId::basic_type::create<PrintEvtId>( keyword(), userPar ) );
//    return bindToThisFactory( PrintEvtId::create( keyword(), userPar ) );
//    return bindToThisFactory( new PrintEvtId( userPar ) );
  }
};
// create a global PrintEvtIdFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an PrintEvtIdFactory will be available with name "dump".
static PrintEvtIdFactory af;

PrintEvtId::PrintEvtId( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


PrintEvtId::~PrintEvtId() {
}


// function to be called at execution start
void PrintEvtId::beginJob() {

  // example of class member value assignment from the command line arguments
  // "uPar" is defined in some base class, the user can use it without
  // worrying about its provenance

//  aStringFromCommandLine = uPar->getUserParameter( "theString" );
  // if in the command line there's an instruction "-v theString pippo"
  // then "aStringFromCommandLine" is assigned the value "pippo",
  // otherwise "aStringFromCommandLine" is assigned an empty string

//  aValueFromCommandLine = 123;
//  uPar->getUserParameter( "theValue", aValueFromCommandLine );
  // if in the command line there's an instruction "-v theValue 234"
  // then "aValueFromCommandLine" is assigned the value "234",
  // otherwise "aValueFromCommandLine" stays unchanged

  // trivial example: initialize number of processed events
//  counter = 0;

  return;

}


// function to be called at execution end
void PrintEvtId::endJob() {

  // trivial example: print number of processed events
//  cout << "PrintEvtId processed " << counter << " events" << endl;
  return;

}


// function to be called for each event
void PrintEvtId::update( const Event& ev ) {
  static GetRawEventId* ge = GetRawEventId::instance();
  static GetSignal    * gs = GetSignal    ::instance();
  static char* buf = new char[48];
  static Event::data_type* chd =
                           reinterpret_cast<Event::data_type*>( buf );
  auto& oc = ge->getOC();
  auto& bc = ge->getBC();
  auto& sl = gs->signal( ge->getChannelOL() );
  auto& sh = gs->signal( ge->getChannelOH() );
  auto& sb = gs->signal( ge->getChannelBC() );
  cout << "******" << ev.eventNumber() << "******" << endl;
  int n = oc.size();
  int i;
  for ( i = 0; i < n; ++i ) {
    chd[0] =   sl[i];
    chd[1] =   sh[i];
    chd[2] = ( sb[i] << 4 ) & 0xfff0;
    cout << i << " : " << oc[i] << ',' << bc[i] << " ---> "
         << *reinterpret_cast<int*>( chd ) << ','
         << ( ( chd[2] >> 4 ) & 0xfff ) << " ("
         << chd[0] << ',' << chd[1] << ")"
         << ' ' << ( ( sl[i] >> 8 ) & 0xff ) << ' ' << ( sl[i] & 0xff )
         << endl;
  }
  // trivial example: increase number of processed events
//  ++counter;
  return;

}

