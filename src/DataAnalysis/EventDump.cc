#include "DataAnalysis/EventDump.h"

#include "DataOperation/GetSignal.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include "Utilities/TimeStamp.h"

#include <iostream>
#include <fstream>

using namespace std;

// concrete factory to create an EventDump analyzer
class EventDumpFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  EventDumpFactory(): AnalysisFactory::AbsFactory( "dump" ) {}
  // create an EventDump when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create EventDump" << endl;
    return bindToThisFactory( EventDump::basic_type::create<EventDump>( keyword(), userPar ) );
//    return bindToThisFactory( ReplicaCounter::create<EventDump>( keyword(), userPar ) );
//    return bindToThisFactory( EventDump::create( keyword(), userPar ) );
//    return bindToThisFactory( new EventDump( userPar ) );
  }
};
// create a global EventDumpFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventDumpFactory will be available with name "dump".
static EventDumpFactory ed;

EventDump::EventDump( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ),
 waitingHeader( true ) {
}


EventDump::~EventDump() {
}


// function to be called at execution start
void EventDump::beginJob() {

  getAnalysisParameter( "getFromOnlineConfig",
                         getFromOnlineConfig, IdentifiableObject::byReplica );
  getAnalysisParameter(      "saveFullConfig",
                              saveFullConfig, IdentifiableObject::byReplica );
  string base;
  string next;
  if ( getFromOnlineConfig ) {
    static string kw = keyword();
    string id = to_string( replicaId() );
    bool isOutToStdOut = true;
    bool isOutToBinary = false;
//    std::string extensionOutFile = ".dat";
    appendTimestamp = true;
    uPar->getUserParameter( "OUT_TO_STDOUT", isOutToStdOut );
    uPar->getUserParameter( "OUT_TO_BINARY_FILE", isOutToBinary );
    uPar->getUserParameter( "APPEND_TIMESTAMP_BINARY_FILE", appendTimestamp );
    if ( isOutToBinary ) {
      base = "data";
      next = "dat";
//      uPar->getUserParameter( "OUT_BINARY_FILENAME", name );
//      uPar->getUserParameter( "OUT_BINARY_FILE_TYPE", extensionOutFile );
//      if ( appendTimestamp ) ( name += "_" ) += uPar->getUserParameter( "exe_time" );
      uPar->getUserParameter( "OUT_BINARY_FILENAME", base );
      uPar->getUserParameter( "OUT_BINARY_FILE_TYPE", next );
      if ( appendTimestamp ) ( base += "_" ) += uPar->getUserParameter( "exe_time" );
      next = "." + next;
      name = base + next;
/*
      if ( appendTimestamp ) {
	long long ts = TimeStamp::packedDateHMS();
        string date = to_string(   ts / TimeStamp::dayFactor );
        string hour = to_string( ( ts % TimeStamp::dayFactor ) /
                                        TimeStamp::secFactor );
        while ( hour.length() < 6 ) hour = "0" + hour;
	( ( ( name += "_" ) += date ) += "_" ) += hour;
      }
*/
//      ( name += "." ) += extensionOutFile;
//      name = ( next.empty() ? base : base + "." + next );
      mode = 'b';
    }
    else
    if ( isOutToStdOut ) {
      name = "";
    }
  }
  else {
    getAnalysisParameter( "name",      name, IdentifiableObject::byReplica );
    getAnalysisParameter( "mode",      mode, IdentifiableObject::byReplica );
    getAnalysisParameter( "appendTimestamp",
                           appendTimestamp , IdentifiableObject::byReplica );
    auto dotPos = name.rfind( '.' );
    if ( dotPos == string::npos ) {
      base = name;
      next = "";
    }
    else {
      base = name.substr(      0, dotPos     );
      next = name.substr( dotPos, string::npos );
    }
    if ( appendTimestamp ) {
      base += "_";
      base += uPar->getUserParameter( "exe_time" );
//      name = ( next == "." ? name : base + "." + next );
    }
    name = ( next.empty() ? base : base + next );
  }
  if ( name == "" ) {
    os = &cout;
    mode = 'a';
    return;
  }
  if ( name != "" ) {
    cout << "Data dumped to: " << name << endl;
    if ( saveFullConfig ) {
//  if ( ( name != "" ) && saveFullConfig ) {
      string ncfg;
      ncfg = base + ( next.empty()   ? ".txt" :
                    ( next == "."    ? ".txt" :
                    ( next != ".txt" ? ".txt" : "_cfg.txt" ) ) );
      ofstream cfgFile( ncfg );
      uPar->dumpAll( cfgFile );
    }
  }
  if ( ( mode == 'b' ) || ( mode == 'B' ) )
       os = new ofstream( name, std::ios::binary );
  else os = new ofstream( name );

  return;

}


// function to be called at execution end
void EventDump::endJob() {
  if ( name != "" ) delete os;
  return;
}


// function to be called for each event
void EventDump::update( const Event& ev ) {

  static GetSignal* gs = GetSignal::instance();

  // write acquisition header
  if ( waitingHeader ) {
    const Event::acqHeader* aHead = ev.acquisitionHeader();
    if ( ( mode == 'b' ) || ( mode == 'B' ) )
         os->write( reinterpret_cast<const char*>( aHead ),
                    sizeof( Event::acqHeader ) );
    else
        *os << hex << aHead->channel_mask
            << dec << ' ' << aHead->sample_per_chan << endl;
    waitingHeader = false;
  }

  // write event number
  int e = ev.eventNumber();
  const Event::evtHeader& eHead = ev.eventHeader();
  const std::vector<int>& channels = ev.channels();

  // write channel data
  if ( ( mode == 'b' ) || ( mode == 'B' ) ) {
    os->write( reinterpret_cast<const char*>( &eHead ),
               sizeof( Event::evtHeader ) );
    for ( int i: channels ) {
      const Event::evtData* ecd = ev.eventData( i );
      auto chID_readPointer = &ecd->chID_readPointer ;
      auto numOfValidData   = &ecd->numOfValidData ;
      os->write( reinterpret_cast<const char*>( chID_readPointer ),
                 sizeof( *chID_readPointer ) );
      os->write( reinterpret_cast<const char*>( numOfValidData   ),
                 sizeof( *numOfValidData   ) );
      os->write( reinterpret_cast<const char*>( ecd->data ),
                 *numOfValidData * Event::ioSize );
    }
  }
  else {
    *os << e << ' ' << eHead.trigger_number << ' ' << channels.size() << endl;
    for ( int i: channels ) {
      const vector<Event::data_type>& sig = gs->signal( i );
      *os << i << ' ' << sig.size();
      for ( Event::data_type d: sig ) *os << ' ' << d;
      *os << endl;
    }
  }
  return;

}

