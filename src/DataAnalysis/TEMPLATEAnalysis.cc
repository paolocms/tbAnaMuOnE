#include "DataAnalysis/TEMPLATEAnalysis.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"

#include <iostream>

using namespace std;

// concrete factory to create an TEMPLATEAnalysis analyzer
class TEMPLATEAnalysisFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "dump" as name for this analyzer and factory
  TEMPLATEAnalysisFactory(): AnalysisFactory::AbsFactory( "KEYWORD" ) {}
  // create an TEMPLATEAnalysis when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    cout << "create TEMPLATEAnalysis" << endl;
    return bindToThisFactory( TEMPLATEAnalysis::basic_type::create<TEMPLATEAnalysis>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<TEMPLATEAnalysis>( keyword(), userPar ) );
//    return bindToThisFactory( TEMPLATEAnalysis::create( keyword(), userPar ) );
//    return bindToThisFactory( new TEMPLATEAnalysis( userPar ) );
  }
};
// create a global TEMPLATEAnalysisFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an TEMPLATEAnalysisFactory will be available with name "KEYWORD".
static TEMPLATEAnalysisFactory af;

TEMPLATEAnalysis::TEMPLATEAnalysis( const UserParametersManager* userPar ):
 AnalysisSteering( userPar ) {
}


TEMPLATEAnalysis::~TEMPLATEAnalysis() {
}


// function to be called at execution start
void TEMPLATEAnalysis::beginJob() {

  // example of class member value assignment from the command line arguments
  // "uPar" is defined in some base class, the user can use it without
  // worrying about its provenance

  aStringFromCommandLine = uPar->getUserParameter( "theString" );
  // if in the command line there's an instruction "-v theString pippo"
  // then "aStringFromCommandLine" is assigned the value "pippo",
  // otherwise "aStringFromCommandLine" is assigned an empty string

  aValueFromCommandLine = 123;
  uPar->getUserParameter( "theValue", aValueFromCommandLine );
  // if in the command line there's an instruction "-v theValue 234"
  // then "aValueFromCommandLine" is assigned the value "234",
  // otherwise "aValueFromCommandLine" stays unchanged

  // trivial example: initialize number of processed events
  counter = 0;

  return;

}


// function to be called at execution end
void TEMPLATEAnalysis::endJob() {

  // trivial example: print number of processed events
  cout << "TEMPLATEAnalysis processed " << counter << " events" << endl;
  return;

}


// function to be called for each event
void TEMPLATEAnalysis::update( const Event& ev ) {

  // trivial example: increase number of processed events
  ++counter;
  return;

}

