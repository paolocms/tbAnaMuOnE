#ifndef DataAnalysis_ROOTFFT_EventFFT_h
#define DataAnalysis_ROOTFFT_EventFFT_h

#include "Framework/AnalysisSteering.h"
#include "Utilities/ActiveObserver.h"

#include <string>
#include <vector>

class UserParametersManager;
class Event;
class TCanvas;

//class TApplication;

class EventFFT: public AnalysisSteering {

 public:

//  static EventFFT* create( const std::string& key,
//                           const UserParametersManager* userPar ) {
//    return nextInstance<EventFFT>( key, userPar );
//  }

  EventFFT( const UserParametersManager* userPar );
  // deleted copy constructor and assignment to prevent unadvertent copy
  EventFFT           ( const EventFFT& x ) = delete;
  EventFFT& operator=( const EventFFT& x ) = delete;

  ~EventFFT() override;

  // function to be called at execution start
  void beginJob() override;
  // function to be called at execution end
  void   endJob() override;
  // function to be called for each event
  void update( const Event& ev ) override;

 private:

//  TApplication* app;
  TCanvas* can;

  int mode = 0;

  std::vector<int> channelMap;
  int nRow = -1;
  int nCol = -1;
  int xSize = 800;
  int ySize = 600;
  int sMin = 1;
  int sMax = -1;

  void drawSignalsChain( const Event& ev );
  void drawSingleGraphs( const Event& ev );
  void drawAndSaveToFile();

  std::string fileName;
  std::string ext;
  bool addEvNumber;
  bool applyConnectionMap;
  
  bool drawChannelSum;

};

#endif // DataAnalysis_ROOTFFT_EventFFT_h

