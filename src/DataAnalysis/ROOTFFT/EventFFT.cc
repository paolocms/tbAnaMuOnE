#include "DataAnalysis/ROOTFFT/EventFFT.h"

#include "DataOperation/GetSignal.h"
#include "DataOperation/GetPedestal.h"
#include "DataOperation/ROOTFFT/GetFFT.h"

#include "Framework/Event.h"
#include "Framework/AnalysisFactory.h"
#include "Framework/ConnectionMap.h"

#include "TApplication.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TAxis.h"

#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

// concrete factory to create an EventFFT analyzer
class EventFFTFactory: public AnalysisFactory::AbsFactory {
 public:
  // assign "draw" as name for this analyzer and factory
  EventFFTFactory(): AnalysisFactory::AbsFactory( "fft" ) {}
  // create an EventFFT when builder is run
  AnalysisSteering* create( const UserParametersManager* userPar ) override {
    return bindToThisFactory( EventFFT::basic_type::create<EventFFT>( keyword(), userPar ) );
//    return bindToThisFactory( IdentifiableObject::create<EventFFT>( keyword(), userPar ) );
//    return bindToThisFactory( EventFFT::create( keyword(), userPar ) );
//    return bindToThisFactory( new EventFFT( userPar ) );
  }
};
// create a global EventFFTFactory, so that it is created and registered 
// before main execution start:
// when the AnalysisFactory::create function is run,
// an EventFFTFactory will be available with name "draw".
static EventFFTFactory ed;

EventFFT::EventFFT( const UserParametersManager* userPar ):
  AnalysisSteering( userPar ) {
}


EventFFT::~EventFFT() {
}


// function to be called at execution start
void EventFFT::beginJob() {
  cout << "EventFFT::beginJob" << endl;
  const string& f = uPar->getUserParameter( "fftFileName" );
  fileName = ( f.empty() ? "evnFFT" : f );
  const string& format = uPar->getUserParameter( "fftPlotFormat" );
  ext = ( format.empty() ? "pdf" : format );
  addEvNumber = false;
  uPar->getUserParameter( "fftAddEvNumber", addEvNumber );
  applyConnectionMap = false;
  uPar->getUserParameter( "drawApplyConnectionMap", applyConnectionMap );
  uPar->getUserParameter( "fftXSize", xSize );
  uPar->getUserParameter( "fftYSize", ySize );
  uPar->getUserParameter( "fftNRow", nRow );
  uPar->getUserParameter( "fftNCol", nCol );
  uPar->getUserParameter( "fftSMin", sMin );
  uPar->getUserParameter( "fftSMax", sMax );
  drawChannelSum = true;
  uPar->getUserParameter( "drawChannelSum", drawChannelSum );
  return;
}


// function to be called at execution end
void EventFFT::endJob() {
  cout << "EventFFT::endJob" << endl;
//  app->Run( kFALSE );
  return;
}


// function to be called for each event
void EventFFT::update( const Event& ev ) {

  stringstream sstr;
  sstr << fileName;
  if ( addEvNumber ) sstr << ev.eventNumber();
  const string cn = sstr.str();

  //cout << cn << ' ' << xSize << ' ' << ySize << endl;
  can = new TCanvas( cn.c_str(), cn.c_str(), xSize, ySize );
  unsigned nCha = ev.channels().size();
  if ( nCol < 0 ) nCol = ceil( sqrt( nCha ) );
  if ( nRow < 0 ) nRow = ceil( nCha * 1.0 / nCol );

  drawSingleGraphs( ev );

  return;

}

void EventFFT::drawSingleGraphs( const Event& ev ) {

  static GetSignal* gs = GetSignal::instance();

  static const ConnectionMap* connMap = ConnectionMap::instance();
  static const unsigned int nCols = connMap->getNCols();

  const vector<int>& channels = ev.channels();
  unsigned nCha = channels.size();
  if ( nCol < 0 ) nCol = ceil( sqrt( nCha ) );
  if ( nRow < 0 ) nRow = ceil( nCha * 1.0 / nCol );
  if ( sMin < 0 ) sMin = 0;
  auto maxTime = gs->signal( channels.front() ).size();
  if ( sMax < 0 ) sMax = maxTime;
  TVirtualPad* subPlotPad = can;
  if (drawChannelSum) {
    can->Divide(1, 2);
    can->GetPad(1)->SetPad(0.0, 0.25, 1, 1);
    can->GetPad(2)->SetPad(0.0, 0.0, 1, 0.25);
    subPlotPad = can->GetPad(1);
  }
  subPlotPad->Divide( nCol, nRow );
  
  auto fftCalc = GetFFT::instance();
  
  unsigned int n = sMax - sMin;
  vector<double> x(n);
  for ( unsigned int k = 0; k < n; ++k ) {
    unsigned int l = k + sMin;
    x[k] = l/(maxTime*0.025);
  }

  vector<unique_ptr<TGraph>> gg(Event::maxChannelNumber);
  int j = 1;
  for ( int i: channels ) {
    // write event number and number of points
    auto& fft = fftCalc->getFFT(i);
    vector<double> y(n);
    subPlotPad->cd( applyConnectionMap ? ( ( connMap->getRow( i ) - 1 ) * nCols ) +
                                      connMap->getCol( i ) :
                                    j++ );
    //gPad->SetLogy();
    gg[i].reset(new TGraph( n, x.data(), fft.data() + sMin ));
    TGraph* g = gg[i].get();
    g->GetXaxis()->SetRangeUser(sMin/(maxTime*0.025), sMax/(maxTime*0.025));
    g->SetTitle(Form("Channel FEB %d;frequency [MHz]", i+1));
    g->Draw( "APL" );
  }
  unique_ptr<TGraph> gSum;
  if (drawChannelSum) {
    can->cd(2);
    auto fftSum = fftCalc->getFFTOfSum();
    gSum.reset(new TGraph(sMax - sMin, x.data(), fftSum.data() + sMin));
    gSum->SetTitle("Channel sum;frequency [MHz]");
    gSum->GetXaxis()->SetRangeUser(sMin/(maxTime*0.025), sMax/(maxTime*0.025));
    gSum->Draw("APL");
  }
  
  drawAndSaveToFile();

  return;

}


// function to be called for each event
void EventFFT::drawAndSaveToFile() {
  can->Print( ( string (can->GetName()) + "." + ext ).c_str() );
  delete can;
  return;
}


