setenv CURRENT_DIR `/bin/pwd`
setenv SCRIPT_COM `/bin/echo $_ | /usr/bin/awk '{print $NF}'`
cd `/usr/bin/dirname ${SCRIPT_COM}`
setenv TBMUONE_OFFLINE_UTY_DIR `/bin/pwd`
cd ..
setenv TBMUONE_OFFLINE_TOP_DIR `/bin/pwd`
setenv TBMUONE_OFFLINE_DIRNAME `/usr/bin/basename ${TBMUONE_OFFLINE_TOP_DIR}`
setenv TBMUONE_OFFLINE_BIN_DIR ${TBMUONE_OFFLINE_TOP_DIR}/bin
setenv TBMUONE_OFFLINE_LIB_DIR ${TBMUONE_OFFLINE_TOP_DIR}/lib

cd ${CURRENT_DIR}

setenv TBMUONE_OFFLINE_GITNAME tbAnaMuOnE

setenv COUNT_PATH `/usr/bin/printenv | /usr/bin/awk -F= '($1=="PATH"){print $2}' | /usr/bin/wc | /usr/bin/awk '{print $1}'`
setenv COUNT_LD_LIBRARY_PATH `/usr/bin/printenv | /usr/bin/awk -F= '($1=="LD_LIBRARY_PATH"){print $2}' | /usr/bin/wc | /usr/bin/awk '{print $1}'`

if ( ${COUNT_PATH} != 0 ) then
    setenv PATH `/bin/echo $PATH | /usr/bin/awk -F: '{for(i=1;i<=NF;++i)print $i}' | grep -v ${TBMUONE_OFFLINE_GITNAME} | /usr/bin/awk -v dirname=${TBMUONE_OFFLINE_BIN_DIR} '{printf($0":")} END{print dirname}'`
else
    setenv PATH ${TBMUONE_OFFLINE_BIN_DIR}
endif
if ( ${COUNT_LD_LIBRARY_PATH} != 0 ) then
    setenv LD_LIBRARY_PATH `/bin/echo $LD_LIBRARY_PATH | /usr/bin/awk -F: '{for(i=1;i<=NF;++i)print $i}' | grep -v ${TBMUONE_OFFLINE_GITNAME} | /usr/bin/awk -v dirname=${TBMUONE_OFFLINE_LIB_DIR} '{printf($0":")} END{print dirname}'`
else
    setenv LD_LIBRARY_PATH ${TBMUONE_OFFLINE_LIB_DIR}
endif


