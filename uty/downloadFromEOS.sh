#!/bin/bash

echo "*****************************************************************************************"
echo "*****    THIS SCRIPT DOES NOT WORK ANY MORE DUE TO CHANGES IN THE CERNBOX SYSTEM    *****"
echo "***** IF POSSIBLE IT WILL BE UPDATED AND RESTORED, BUT THERE'S LITTLE HOPE FOR THAT *****"
echo "*****************************************************************************************"

export BASE_URL=$1
export FILENAME=$2
export DESTPATH=`/bin/pwd`

if [ $# -gt 2 ] ; then
  export DESTPATH=$3
fi

export URLP=${BASE_URL}"/download?path=%2F.&x-access-token=ACCESS_TOKEN&files="${FILENAME}
export URLL=urlTemp`date +%s`

#/usr/bin/wget -O ${URLL} ${BASE_URL}
#export ACCESS_TOKEN=`grep access-token ${URLL} | grep cernboxauthtoken | awk -F"access-token" '{print $NF}' | awk -F= '{print $NF}' | awk '{print $1}' | sed s/\"//g`

#/usr/bin/wget -O ${DESTPATH}/${FILENAME} `echo ${URLP} | sed s/ACCESS_TOKEN/${ACCESS_TOKEN}/`
#/bin/rm ${URLL}
