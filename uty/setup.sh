SOURCE=${BASH_ARGV[0]}
export CURRENT_DIR=`/bin/pwd`
export SCRIPT_COM=`/bin/echo ${SOURCE}`
cd `/usr/bin/dirname ${SCRIPT_COM}`
export TBMUONE_OFFLINE_UTY_DIR=`/bin/pwd`
cd ..
export TBMUONE_OFFLINE_TOP_DIR=`/bin/pwd`
export TBMUONE_OFFLINE_DIRNAME=`/usr/bin/basename ${TBMUONE_OFFLINE_TOP_DIR}`
export TBMUONE_OFFLINE_BIN_DIR=${TBMUONE_OFFLINE_TOP_DIR}/bin
export TBMUONE_OFFLINE_LIB_DIR=${TBMUONE_OFFLINE_TOP_DIR}/lib

cd ${CURRENT_DIR}

export TBMUONE_OFFLINE_GITNAME=tbAnaMuOnE

export COUNT_PATH=`/usr/bin/printenv | /usr/bin/awk -F= '($1=="PATH"){print $2}' | /usr/bin/wc | /usr/bin/awk '{print $1}'`
export COUNT_LD_LIBRARY_PATH=`/usr/bin/printenv | /usr/bin/awk -F= '($1=="LD_LIBRARY_PATH"){print $2}' | /usr/bin/wc | /usr/bin/awk '{print $1}'`

if [ ${COUNT_PATH} != 0 ] ; then
    export PATH=`/bin/echo $PATH | /usr/bin/awk -F: '{for(i=1;i<=NF;++i)print $i}' | /bin/grep -v ${TBMUONE_OFFLINE_GITNAME} | /usr/bin/awk -v dirname=${TBMUONE_OFFLINE_BIN_DIR} '{printf($0":")} END{print dirname}'`
else
    export PATH=${TBMUONE_OFFLINE_BIN_DIR}
fi
if [ ${COUNT_LD_LIBRARY_PATH} != 0 ] ; then
    export LD_LIBRARY_PATH=`/bin/echo $LD_LIBRARY_PATH | /usr/bin/awk -F: '{for(i=1;i<=NF;++i)print $i}' | /bin/grep -v ${TBMUONE_OFFLINE_GITNAME} | /usr/bin/awk -v dirname=${TBMUONE_OFFLINE_LIB_DIR} '{printf($0":")} END{print dirname}'`
else
    export LD_LIBRARY_PATH=${TBMUONE_OFFLINE_LIB_DIR}
fi


