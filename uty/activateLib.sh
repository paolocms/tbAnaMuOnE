#!/bin/bash

export LIBRARY_PATH=$1

source `/usr/bin/dirname $0`/setup.sh

export ACTIVE_MAKEFILE=${TBMUONE_OFFLINE_TOP_DIR}/src/${LIBRARY_PATH}/Makefile
export   LAZY_MAKEFILE=${TBMUONE_OFFLINE_TOP_DIR}/src/${LIBRARY_PATH}/Makefilelazy

if [ -f ${LAZY_MAKEFILE} ]; then
  /bin/cp ${LAZY_MAKEFILE} ${ACTIVE_MAKEFILE}
elif [ -f ${ACTIVE_MAKEFILE} ]; then
  /bin/echo ${LIBRARY_PATH}" already active"
  /bin/cp ${ACTIVE_MAKEFILE} ${LAZY_MAKEFILE}
else
  /bin/echo ${LIBRAY_PATH}" not a library, no Makefile found"
fi

