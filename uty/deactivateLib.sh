#!/bin/bash

export LIBRARY_PATH=$1

source `/usr/bin/dirname $0`/setup.sh

export ACTIVE_MAKEFILE=${TBMUONE_OFFLINE_TOP_DIR}/src/${LIBRARY_PATH}/Makefile
export   LAZY_MAKEFILE=${TBMUONE_OFFLINE_TOP_DIR}/src/${LIBRARY_PATH}/Makefilelazy

if [ -f ${ACTIVE_MAKEFILE} ]; then
  /bin/rm -f ${LAZY_MAKEFILE}
  /bin/mv ${ACTIVE_MAKEFILE} ${LAZY_MAKEFILE}
elif [ -f ${LAZY_MAKEFILE} ]; then
  /bin/echo ${LIBRARY_PATH}" already inactive"
else
  /bin/echo ${LIBRAY_PATH}" not a library, no Makefile found"
fi

