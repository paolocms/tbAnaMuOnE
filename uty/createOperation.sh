#!/bin/bash

source `/usr/bin/dirname $0`/setup.sh

export OPERATION_PATH=${TBMUONE_OFFLINE_TOP_DIR}"/src/DataOperation"

if [ $# -lt 1 ] ; then
  /bin/echo "operation class not specified, give a name for your class"
  exit
fi

export OPERATION_CLASS=$1

sed s/TEMPLATEOperation/${OPERATION_CLASS}/g ${OPERATION_PATH}/TEMPLATEOperation.h > ${OPERATION_PATH}/${OPERATION_CLASS}.h
sed s/TEMPLATEOperation/${OPERATION_CLASS}/g ${OPERATION_PATH}/TEMPLATEOperation.cc > ${OPERATION_PATH}/${OPERATION_CLASS}.cc
