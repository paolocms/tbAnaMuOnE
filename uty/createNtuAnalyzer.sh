#!/bin/bash

source `/usr/bin/dirname $0`/setup.sh

export NTUANALYZER_PATH=${TBMUONE_OFFLINE_TOP_DIR}"/src/NtuAnalysis"

export NTUANALYZER_CLASS=""
export NTUANALYZER_FORMAT=""

eval `echo $@ | /usr/bin/awk '{for(i=1;i<=NF;++i){j=index($i,"="); if(j!=0){key=substr($i,1,j-1); val=substr($i,j+1,length($i)-j); if(key=="class")print "export NTUANALYZER_CLASS="val" ;"; if(key=="ntuple")print "export NTUANALYZER_FORMAT="val" ;"}}}'`

if [ "==="${NTUANALYZER_CLASS}"===" == "======" ] ; then
  /bin/echo "ntuple analyzer class not specified, use \"class=CLASS_NAME\""
  exit
fi
if [ "==="${NTUANALYZER_FORMAT}"===" == "======" ] ; then
  /bin/echo "ntuple format not specified, \"SimpleNtuple\" used;"
  /bin/echo "otherwise use \"ntuple=NTUPLE_NAME\""
  export NTUANALYZER_FORMAT="SimpleNtuple"
fi

sed s/SimpleAnalysis/${NTUANALYZER_CLASS}/g ${NTUANALYZER_PATH}/SimpleAnalysis.h | sed s/SimpleNtuple/${NTUANALYZER_FORMAT}/g > ${NTUANALYZER_PATH}/${NTUANALYZER_CLASS}.h
sed s/SimpleAnalysis/${NTUANALYZER_CLASS}/g ${NTUANALYZER_PATH}/SimpleAnalysis.cc | sed s/SimpleNtuple/${NTUANALYZER_FORMAT}/g > ${NTUANALYZER_PATH}/${NTUANALYZER_CLASS}.cc
