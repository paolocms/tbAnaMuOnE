#!/bin/bash

source `/usr/bin/dirname $0`/setup.sh

export ANALYZER_PATH=${TBMUONE_OFFLINE_TOP_DIR}"/src/DataAnalysis"

export ANALYZER_CLASS=""
export ANALYZER_KEY=""

eval `echo $@ | /usr/bin/awk '{for(i=1;i<=NF;++i){j=index($i,"="); if(j!=0){key=substr($i,1,j-1); val=substr($i,j+1,length($i)-j); if(key=="class")print "export ANALYZER_CLASS="val" ;"; if(key=="key")print "export ANALYZER_KEY="val" ;"}}}'`

if [ "==="${ANALYZER_CLASS}"===" == "======" ] ; then
  /bin/echo "analyzer class not specified, use \"class=CLASS_NAME\""
  exit
fi
if [ "==="${ANALYZER_KEY}"===" == "======" ] ; then
  /bin/echo "analyzer key not specified, use \"key=KEYWORD\""
  exit
fi

sed s/TEMPLATEAnalysis/${ANALYZER_CLASS}/g ${ANALYZER_PATH}/TEMPLATEAnalysis.h > ${ANALYZER_PATH}/${ANALYZER_CLASS}.h
sed s/TEMPLATEAnalysis/${ANALYZER_CLASS}/g ${ANALYZER_PATH}/TEMPLATEAnalysis.cc | sed s/KEYWORD/${ANALYZER_KEY}/ > ${ANALYZER_PATH}/${ANALYZER_CLASS}.cc
