THIS_MAKEFILE := $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
PATH_MAKEFILE := $(shell /usr/bin/dirname $(THIS_MAKEFILE))
PACKAGE_TOP_DIR := $(shell cd $(PATH_MAKEFILE) && /bin/pwd)
PACKAGE_SRC_DIR := $(PACKAGE_TOP_DIR)/src
PACKAGE_LIB_DIR := $(PACKAGE_TOP_DIR)/lib
PACKAGE_BIN_DIR := $(PACKAGE_TOP_DIR)/bin
#CURRENT_LIBRARY := $(patsubst $(PACKAGE_LIB_DIR)/lib%.so,-l%,$(wildcard $(PACKAGE_LIB_DIR)/*.so))
EXECUTABLE_FILE := $(PACKAGE_BIN_DIR)/dataAnalysis

MAKE_LIST_FULL := $(shell find $(PACKAGE_SRC_DIR) -name "Makefile")
MAKE_DIR_NAMES := $(patsubst $(PACKAGE_SRC_DIR)/%/Makefile,%,$(MAKE_LIST_FULL))
MAKE_LIB_NAMES := $(patsubst %,-l%,$(subst /,,$(MAKE_DIR_NAMES)))

#$(warning $(MAKE_LIB_FILES))

PACKAGE_SOURCES := $(wildcard $(subst Makefile,*.cc,$(MAKE_LIST_FULL)))
#PACKAGE_HEADERS := $(wildcard $(subst Makefile,*.h,$(MAKE_LIST_FULL)) $(subst Makefile,*.hpp,$(MAKE_LIST_FULL))) $(wildcard $(PACKAGE_SRC_DIR)/Utilities/*.h $(PACKAGE_SRC_DIR)/Utilities/*.hpp)
PACKAGE_HEADERS := $(wildcard $(subst Makefile,*.h,$(MAKE_LIST_FULL)) $(subst Makefile,*.hpp,$(MAKE_LIST_FULL)))

NTUTOOL_LIBRARY := $(PACKAGE_LIB_DIR)/libNtuTool.so
NTUTOOL_SOURCES := $(wildcard $(PACKAGE_SRC_DIR)/NtuTool/Common/src/*cc)
NTUTOOL_HEADERS := $(wildcard $(PACKAGE_SRC_DIR)/NtuTool/Common/interface/*h $(PACKAGE_SRC_DIR)/NtuTool/Common/interface/*hpp)

CREATE_COMMANDS := $(patsubst %/Makefile,make       -f %/Makefile ;,$(MAKE_LIST_FULL))
CREATE_LIBNAMES := $(patsubst %/Makefile,make prod  -f %/Makefile ;,$(MAKE_LIST_FULL))
CREATE_EXT_DEPS := $(patsubst %/Makefile,make deps  -f %/Makefile ;,$(MAKE_LIST_FULL))
CREATE_ALLFLAGS := $(patsubst %/Makefile,make flags -f %/Makefile ;,$(MAKE_LIST_FULL))
CREATE_EXT_LIBS := $(patsubst %/Makefile,make libs  -f %/Makefile ;,$(MAKE_LIST_FULL))

LIB_FULL_LIST   := $(shell $(CREATE_LIBNAMES))
EXTERNAL_DEPS   := $(shell $(CREATE_EXT_DEPS))
COMPILE_FLAGS   := $(shell $(CREATE_ALLFLAGS))
EXTERNAL_LIBS   := $(shell $(CREATE_EXT_LIBS))

COMPILE_COMMAND := c++ -std=c++11 -Wall -pthread

.PHONY: all relink clean cleanall

$(EXECUTABLE_FILE): $(PACKAGE_SOURCES) $(PACKAGE_HEADERS) $(NTUTOOL_LIBRARY) $(EXTERNAL_DEPS)
	@echo "===== "$(THIS_MAKEFILE)" : target "$@" ====="
	mkdir -p $(PACKAGE_BIN_DIR)
	rm -f $@
	$(CREATE_COMMANDS)
	$(COMPILE_COMMAND) $(COMPILE_FLAGS) -o $@ -L $(PACKAGE_LIB_DIR) -Wl,--no-as-needed $(LIB_FULL_LIST) -lNtuTool $(EXTERNAL_LIBS)
#	$(COMPILE_COMMAND) $(COMPILE_FLAGS) -o $@ -L $(PACKAGE_LIB_DIR) -Wl,--no-as-needed $(MAKE_LIB_NAMES) -lNtuTool $(EXTERNAL_LIBS)

$(NTUTOOL_LIBRARY): $(NTUTOOL_SOURCES) $(NTUTOOL_HEADERS)
	@echo "===== "$(THIS_MAKEFILE)" : target "$@" ====="
	mkdir -p $(PACKAGE_LIB_DIR)
	$(COMPILE_COMMAND) -fPIC $(COMPILE_FLAGS) -I $(PACKAGE_SRC_DIR) -shared -o $@ $(NTUTOOL_SOURCES)

all: 
	$(CREATE_COMMANDS)

relink:
	@echo "===== "$(THIS_MAKEFILE)" : target "$@" ====="
	mkdir -p $(PACKAGE_BIN_DIR)
	rm -f $(EXECUTABLE_FILE)
	$(COMPILE_COMMAND) $(COMPILE_FLAGS) -o ${EXECUTABLE_FILE} -L $(PACKAGE_LIB_DIR) -Wl,--no-as-needed $(LIB_FULL_LIST) -lNtuTool $(EXTERNAL_LIBS)

clean:
	@echo "===== "$(THIS_MAKEFILE)" : target "$@" ====="
	rm -f $(EXECUTABLE_FILE)
	rm -f $(PACKAGE_BIN_DIR)/*

cleanall: clean
	@echo "===== "$(THIS_MAKEFILE)" : target "$@" ====="
	rm -rf $(PACKAGE_BIN_DIR)/*
	rm -rf $(PACKAGE_LIB_DIR)/*
	rm -rf $(PACKAGE_TOP_DIR)/obj/*

