
Data of the testbeam done in 2022 and 2023 are available on EOS,
and can be accessed via lxplus.

Currently all data can be found in the folder

/eos/experiment/mu-e/daq/calo/tb/testBeam_220720/dataFiles
/eos/experiment/mu-e/daq/calo/tb/testBeam_221019/dataFiles
/eos/experiment/mu-e/daq/calo/tb/testBeam_230531/dataFiles
/eos/experiment/mu-e/daq/calo/tb/testBeam_230621/dataFiles
/eos/experiment/mu-e/daq/calo/tb/testLaser_230403/dataFiles

*.dat files contain actual data, *.txt files contain the configuration
used for the DAQ.

********************* download *********************

Data can be analyzed with the same software used at datataking

git clone https://gitlab.cern.ch/paolocms/tbAnaMuOnE.git
git clone https://gitlab.cern.ch/muesli/calo-daq/tbjuly.git
cd tbjuly && git checkout ricMinimal && cd ..


the latter requires an authentication, and actually downloads a lot of
unnecessary code, so it's possible simly copy the file
/eos/experiment/mu-e/daq/calo/tb/testBeam_230531/dataFiles/tbjuly_slim.tgz
and unpack:

tar -xzf tbjuly_slim.tgz

then the environment must be set

source tbAnaMuOnE/uty/setup.[sh|csh]
source tbjuly/sw/ipbus/setup.[sh|csh]

and everything can be compiled

cd tbAnaMuOnE
make

Data analysis can be done in two ways:

***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****
*****                                                       *****
***** 1 - directly from raw data, that's the simplest to do *****
*****                                                       *****
***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****

[YOUR_PATH]/tbAnaMuOnE/uty/createROOTAnalyzer.sh class=MyAnalysis key=myAna

then recompile

cd [YOUR_PATH]/tbAnaMuOnE
make
( or also make -f [YOUR_PATH]/tbAnaMuOne/Makefile )

and run

rehash # sometime needed, that depend on linux
dataAnalysis -v input [DATA_PATH]/EleBeamWithLaser_230603_234809.dat myAna -v fileName myHist.root

or create a text file, e.g. cfg.txt like this:
input [DATA_PATH]/EleBeamWithLaser_230603_234809.dat
myAna
fileName myHist.root

and run
dataAnalysis -c cfg.txt

This creates some histograms and save them on myHist.root , or whatever other
filename you chhose at runtime.
The code is in
[YOUR_PATH]/tbAnaMuOnE/src/DataAnalysis/ROOT/MyAnalysis.[h,cc]

and contains the code to fill and save an histogram of the sum of the
signals.
Some parameters can be given in the command line e.g.
dataAnalysis ..... -v histoBins 50
or included in the cfg.txt file

...
histoBins 50
...

Inside the code there are examples with explaining comments

Finally, several operations are already coded in

/tbAnaMuOnE/src/DataOperations

A full list is given below in the "basic data operations" section.

- get the max position and value for each channel
- compute the pedestal and RMS (it excludes a region around the max.
  with a width of 10, that can be customized at runtime, e.g with
  -v pedestalExclusionWidth 15
- get an automatic noise/laser/beam event classification
- get the connection map, with the class ConnectionMap

ConnectionMap* connMap = ConnectionMap::instance();

int channel = ...;
int row = connMap->getRow();
int col = connMap->getCol();

that requires giving the actual map on the command line:

-c channelMap
OR
-c channelMapBackView

the channelMap and channelMapBackView files are in the same folder as data

***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****
*****                                                       *****
***** 2 - producing a ntuple, that implies double the data  *****
*****                                                       *****
***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****

A ntuple can be produced out-of-the-box , but that ntuple contains
- the very same information, i.e. the full readout channel data, so that
  data are essentially duplicated wasting a lot of disk space
- plus a calculation of pedestals and signal sizes, but those quantites
  are calculated with NO optimization, so they can be not so reliable

To produce the ntuple 

dataAnalysis -v input [DATA_PATH]/EleBeamWithLaser_230603_234809.dat -c channelMapBackView -v ntu ntupleEleBeamWithLaser_230603_234809.root

********************* DETAILED INSTRUCTIONS *********************

Here are some fairly detailed instructions to analyze testbeam data.
The software currently available can read data and produce some very
basic plot but can be quite easily expanded and customized to do much
more sophisticated analyses, as explained below.

********************* overall structure *********************

The framework is coded in several packages:
- Framework    : contains the basic structure, the main function, the Event
                 data format ...;
- DataSource   : contains the code to get data from different sources,
                 data files, simulation, online (currently data can be
                 obtained only from binary datafiles, other data sources
                 exist but currently do not work);
- DataAnalysis : contains code to analyze event data; data are taken from
                 a source and dispatched to several analyzers, chosen
                 at runtime;
- DataOperation: contains code to perform with event data operations
                 potentially needed by analyzers or other "operations";
- DataAnalysis/ROOT and DataOperation/ROOT contain classes in the
                 previous two classes respectively, using ROOT instructions:
                 they're in separate folders due to the need of specific
                 compilation options;
- NtuAnalysis  : contains code to analyze ntuples, produced as described later;
- Utilities    : contains very general utility code;
- NtuTool      : imported from another repository, in github, contains
                 code to interface with ntuples and set parameters at runtime.

Everything can be compiled simply issuing a "make" command in the
tbAnaMuOnE folder; the option "clean" removes the executables and the
option "cleanall" removes all the libraries and compiled object files, too.

The program currently do a very basic analysis and produces a few plots
and a simple ntuple, but can be easily expanded as explained in the following.

********************* analysis run *********************

The analysis program is run with the command

dataAnalysis -v input INPUT_FILE [-v nread EVENTS_TO_READ] [-v nskip EVENTS_TO_SKIP] ...

or

dataAnalysis -c CONFIGURATION_FILE

where CONFIGURATION_FILE contains lines as

input INPUT_FILE

and so on; look at src/NtuTool/Common/UserParametersManager.h for
documentation about the parameters.

For data taken with the "old" format, the option "-v signalTailAtBegin t"
must be included; to check for the format look at the "pchk" option below.

The other running options specify what's to be done; some options are
available out of the box but users can add their analysis code.
Currently the available options are the following, and more than one can
be used in the same execution:

dump :     dump the events to the standard output, or a file specified with
           "-v dump_name FILENAME" ;
           if the option "-v dump_mode b" is given data are dumped in binary
           form with the same format as used by the daq.
draw :     produce the plots of signal vs. sample number and save them to file,
           by default as .pdf but the other formats supported by ROOT can be
           chosen: the output file name is chosen with "-v draw_name NAME"
           (default is "evn" ) and the format with "-v draw_plotFormat FORMAT"
           (default is "pdf"); the event number by default is appended to the
           file name but that can be disabled with "-v draw_addEvNumber f" :
           in that case for each event the output plot file is overwritten.
           Signals are plotted on a per-channel basis; the connection map
           can be applied to give a visual representation of the calorimeter
           with the options "-v draw_applyConnectionMap t -c channelMap".
           In place of by-event plots, event sum or averages can be drawn
           using the options "-v draw_accumulateEvents t" and/or
           "-v draw_averageOverEvents t"; with the option draw_eventType
           noise, laser or beam events can be selected, using the option
           values n, l, b respectively.
           Plots can be drawn while running using the option
           "-v draw_useGraphic t".
tempPlot : produce for each event a color visualization for all channels of
           the max raw output, the pedestal-subtracted max signal, the
           signal over noise and spread of the signal position.
           Plots are saved to "evn.pdf" files as for the previous modules;
           similar otions can be given in the command line or the config. file
           (tempPlot_name , tempPlot_addEvNumber and so on).
hPos     : produce a 2D plot of the shower mean position; many options can be
           used as before: "hitPos_XXX'
pchk :     dump to standard output a list of readout_parity vs. number of
           data pairs: if the number of data is different in "even-events" (0)
           and "odd-events" (1) then data are written with the "old" format,
           if the number of events is the same for "even" (0) and odd (1)
           events, then data are written with the "new" format.

********************* add user analysis *********************

In addition to the modules described before, users can add their own ones;
that can be done with the command:

.../tbAnaMuOnE/uty/createAnalyzer.sh class=CLASS_NAME key=KEYWORD

or

.../tbAnaMuOnE/uty/createROOTAnalyzer.sh class=CLASS_NAME key=KEYWORD

if the new module is to contain ROOT instruction.
In the first case two files will be created in the folder

.../tbAnaMuOnE/src/DataAnalysis/CLASS_NAME.[h,cc]

in the second one the two files will be created in the folder

.../tbAnaMuOnE/src/DataAnalysis/ROOT

This is required to allow Makefile(s) apply the correct compilation
options; no changes to the main file/function or the Makefile(s) is required,
everything is compiled and included in the program automatically.
The user analysis module is actually run adding the chosen KEYWORD to
the execution command:

dataAnalysis -v input INPUT_FILE [-v nread EVENTS_TO_READ] [-v nskip EVENTS_TO_SKIP] KEYWORD ...

where KEYWORD is the one specified when creating the module.
Several user modules can be produced and more than one can be run in the
same execution.

Users can insert their code in the .../CLASS_NAME.[h,cc] ; each analysis
class implements three function: "beginJob()", "endJob()" and
"analyze(const Event& ev )".
The first two contains the code to be executed before and after the event
loop respectively (initializations, summaries and so on), the third one
contains the code to be executed for each event. All functions are called
automatically and users do not need modifying the main for that.
The "Event" object content and the functions to access them are described
in the next section.

Parameters used in the analysis can be set at runtime, look at

src/NtuTool/Common/UserParametersManager.h

for documentation.

The readout-channel-to-crystal connection map can be accessed with the
instructions

#include "Framework/ConnectionMap.h"
...
static const ConnectionMap* cm = ConnectionMap::instance();
unsigned int nr = cm->getNRows(); // get the number of rows
unsigned int nc = cm->getNCols(); // get the number of columns

int cha = cm->getChannel( row, col ); // get readout channel for row and column

int row = cm->getRow( channel ); // get row    for channel
int col = cm->getCol( channel ); // get column for channel


Some operations with Event data can be needed in more analysis modules,
to avoid code replication look at the "user-defined data operations" section.

********************* Event data format *********************

The "Event" class contains the raw data, grouped in "structs" defined
in the daq package

.../tbjuly/sw/ipbus/include/tbstructs.h

that can be accessed with the following functions:

const Event::acqHeader* acquisitionHeader()

      returns a pointer to an "Event::acqHeader" struct, that's an alias
      for "struct acqHeader_t" defined in "tbstruct.h";

int eventNumber()

      returns the event number;

const Event::evtHeader& eventHeader()

      returns a reference to an "Event::evtHeader" struct, that's an alias
      for "struct evtHeader_t" defined in "tbstruct.h";

const std::vector<int>& channels()

      returns a "std::vector" containing the identifiers of the channels,
      ranging from 0 to 31 included;

const Event::evtData* eventData( int channel )

      returns a pointer to an "Event::evtData" structure, that's an alias
      for "struct event_t" defined in "tbstruct.h", containing data for the
      channel given as argument.

Users do not need unpacking the event data structure, that's provided
by the class "GetSignal":

const vector<Event::data_type>& sig = GetSignal::instance()->signal( channel );

where "Event::data_type" is an alias for "unsigned short".

********************* basic data operations *********************

Several operations with Event data are needed in more analysis modules,
first of all the extraction of the channels output as well as the
calculation of the max. signal value and position in the readout buffer
and the calculation of the pedestal.
Those operations are done in "Singleton" modules, i.e. objects existing
in a single instance accessible from any part of the program.
Users can provide their ones, as explained below in the section
"user-defined data operations", some are available out-of-the-box:

static GetSignal       * gs = GetSignal       ::instance();
static GetPedestal     * gp = GetPedestal     ::instance();
static GetMax          * gm = GetMax          ::instance();
static GetEventType    * gt = GetEventType    ::instance();
static GetRawEventId   * gi = GetRawEventId   ::instance();
static GetLaserMonitor * gl = GetLaserMonitor ::instance();
static GetXYPos        * gc = GetXYPos        ::instance();

The results can be obtained using the functions:

// signal from channel "i"
const std::vector<Event::data_type>& sc = gs->signal( int channel );

// signal from all channels, queued in sequence
const std::vector<Event::data_type>& sq = gs->signal();

// pedestal mean value and RMS for channel
double ped = gp->getPedestal( channel );
double rms = gp->getPedestalRMS( channel );

Pedestal mean value and rms are computed excluding the buffer part around
the maximum with a +-10 width, that can be changed at runtime giving the
option "-v pedestalExclusionWidth PEAK_HALF_WIDTH".

// maximum value and position in buffer for a channel
int val = gm->getVal( channel );
int pos = gm->getPos( channel );

// event type: possible values
//   GetEventType::unclassified
//   GetEventType::noise
//   GetEventType::laser
//   GetEventType::beam
GetEventType::type = gt->getType();

// Events are classified looking at the min and max signal significance among
the 25 channels and the ratio between the max and min signals.

// event clock info
const std::vector<unsigned int>& orbitCount = gi->getOC();
const std::vector<unsigned int>& bunchCount = gi->getBC();

// The clock inf is available only for part of the data, up to now only
// October 2022 TB

// laser monitor channels
int laserChannel = gl->getLaserChannel(); // photodiode at the laser
int fiberChannel = gl->getFiberChannel(); // photodiode at the end of the fiber
const std::vector<unsigned int>& laserData = gs->signal( int laserChannel );
const std::vector<unsigned int>& fiberData = gs->signal( int fiberChannel );

// hit position
double xHit = gc->getX();
double yHit = gc->getY();
// OR
double xHit, yHit;
gc->getPos( xHit, yHit );

// The parameters uset to compute the shower center can be customized with the
// options 
// xSize (X size of crystal, default 3) 
// ySize (Y size of crystal, default 3) 
// maxWeight (max weight of a crystal, default 5.5) 

********************* user-defined data operations *********************

Users can easily implement operations giving results usable in all
parts of the analysis (e.g. the pedestals, already implemented and
described in previous section); the structure can be created issuing
the command

.../tbAnaMuOnE/uty/createOperation.sh CLASS_NAME

that creates two files

.../tbAnaMuOnE/src/DataOperation/CLASS_NAME.[h,cc]

containing a trivial example with some documentation/instruction in
the comments.
The object can be accessed with the instruction

static CLASS_NAME* op = CLASS_NAME::instance();

and the results can be obtained with the instruction

VAR_TYPE res = op->FUNCTION();

where "FUNCTION" is the name of an user-defined function; see inside
CLASS_NAME.[h,cc] for the details.

********************* work with ntuples *********************

The package includes the code to produce a very basic ntuple, issuing the
command

dataAnalysis -v input INPUT_FILE -v ntu NTUPLE_FILE_NAME

that produce a ROOT file "NTUPLE_FILE_NAME" ; the extension must be
explicitly included in the name and by default the file is not overwritten,
so the execution cashed if it's already existing, unless the option
"-v ntuRecreateFile t" is included in the command line.

The user can customize the ntuple modifying the file

.../tbAnaMuOnE/src/NtuAnalysis/Format/SimpleNtuple.h
or duplicating it and modifying the copy.
The "SimpleNtuple.h" file contains only the structure, and some documentation/
instruction is provided in the comments therein; the actual filling stays
in the files

.../tbAnaMuOnE/src/DataAnalysis/ROOT/WriteTree.[h,cc]

The "repetitive" part of the ntuple handling is delegated to the "NtuTool"
library, look at the INSTRUCTIONS file in the

https://github.com/ronchese/NtuTool

repository for details.

Currently the ntuple content is the following:

int iEvt;                            // event number
unsigned int nSig;                   // total number of signal data
std::vector<unsigned short>* signal; // signal data, with all channels
                                     // queued in a row
std::vector<int           >* signalOffsets; // start point of data for
                                            // each channel
std::vector<int           >* signalCounts;  // number of data for each channel
std::vector<double        >* pedestal;      // pedestal values for each channel
std::vector<double        >* pedRMS;        // pedestal RMS for each channel
std::vector<double        >* signOverRMS;   // pedestal-subtracted max-value
                                            // over pedestal RMS for
                                            // each channel

********************* ntuple analyses *********************

The ntuples produced as described in the previous section can be analyzed
with the code producedby ROOT with "MakeClass()" or in a compiled program,
much more efficient and flexible.

A trivial analysis example can be found in the files

.../tbAnaMuOnE/src/NtuAnalysis/Read/SimpleAnalysis.[h,cc]

and can be run with the command

ntuSimpleAnalysis NTU_FILE HIST_FILE

where NTU_FILE is the name of the file containing the ntuple and HIST_FILE is
the name of the produced ROOT file with histograms.

Some docuentation/instruction is included in "SimpleAnalysis.[h,cc]" in
the comments. Other details can be found in the INSTRUCTIONS file in the

https://github.com/ronchese/NtuTool

repository.

********************* user-defined ntuples *********************

THE INSTRUCTIONS IN THIS SECTION ARE OUTDATED, CURRENTLY NOT WORKING

Users can produce their own ntuple analysis class files with the command

.../tbAnaMuOnE/uty/createNtuAnalyzer.sh class=CLASS_NAME

or, if a ntuple different than "SimpleNtuple" is to be analyzed, the command

.../tbAnaMuOnE/uty/createNtuAnalyzer.sh class=CLASS_NAME ntuple=NTUPLE_NAME

When compiling with "make" an executable will be produced with name
"ntuCLASS_NAME"

********************* triggerless run simulation *********************

A simulator of a "triggerless+zero_suppression" running has been developed: 
it merges signal data from a binary file with noise from a pure noise data
run, reshuffling data to have the data from all channels clock by clock, 
and "filling" the clocks between two events with noise data.
WARNING: due to lengthy operation of unpacking and reshuffling data, 
the running can be quite slow; more details are given below in the description 
of the options.

The simulator can be run with the command

dataAnalysis -c config_trigLess

where "config_trigLess" is a configuration with a content as the following

// // // config_trigLess file begin // // //

// keyword to choose the running more
trigLessSim

// the following parameter MUST be set at 0, otherwise a multithread parallel
// run occurs, messing up the data sequence
eventBufferSize 0

// data files to merge
// in this example signal are taken from october TB, having the clock info
// recorded in the event data; for signal data from different periods 
// a clock internally generated can be used (see below)
signalData /eos/experiment/mu-e/daq/calo/tb/testBeam_221019/dataFiles/MuBeamHighIntensityTarget_221025_123123.dat
noiseData  /eos/experiment/mu-e/daq/calo/tb/testBeam_230531/dataFiles/Noise_230607_121231.dat

// re-run over an already produced stream, alternative to merging signal and
// noise (see below), this option is incompatible with signalData and noiseData
//streamFile savedStream.dat

// save the merged stream to a file, with NO SUPPRESSION:
// BE CAREFUL: it can produce an enormous amount of data! (see below)
streamDumpName stream.dat

// generate internally a clock for signal collected in periods when this info
// was not available: the orbit count starts from 100000 and it is increased 
// for each signal event by a flat random number between 1 and a max. value
// set with the following option; if the option is not set or it is set at 0
// then the orbit/bunch counts are read from the signal file
randomClockCount 5

// for all clocks between two events data are taken from the noise source file,
// implying that in long inter-spill time an enormous quantity of data is
// generated with extremely long execution times: this can be at least partly 
// avoided skipping orbit counts with no signal when their number exceeds
// a threshold set with the following threshold
orbitMDiff 0

// to investigate the number of selected events in case of pure noise input,
// signal can be removed setting at "true" the following option; in that case
// remove the orbitMDiff option
killSignal f

// max number of selected events (AFTER the zero-suppression);
// if set at -1 the run goes on until the whole signal file is read
maxSelectedEvents 1000

// max number of signal events to read;
// if set at -1 the run goes on until the whole signal file is read
maxSignalEvents 1000

// choose the zero-suppression algorithm:
// 1 : no suppression
// 2 : cut on the channel sum
// 3 : require at least one channel over the corresponding threshold
//     for a number of clocks
filterMode 3
// look for "Filter" in the 
// tbAnaMuOnE/src/DataSource/TrigLessSim.cc
// source file to modify or add suppression algorithms

// options specific for the different suppression algorithms

// option relevant for "filterMode 2"
//
// channel sum threshold
channelSumThreshold 605000

// options relevant for "filterMode 3"
//
// single channel thresholds
channelThreshold_0  27000
channelThreshold_1  26500
channelThreshold_2  27000
channelThreshold_3  27500
channelThreshold_4  27000
channelThreshold_5  27000
channelThreshold_6  26000
channelThreshold_7  27500
channelThreshold_8  27000
channelThreshold_9  27000
channelThreshold_10 26500
channelThreshold_11 27000
channelThreshold_12 27000
channelThreshold_20 25500
channelThreshold_21 25300
channelThreshold_22 25500
channelThreshold_23 35000
channelThreshold_24 26000
channelThreshold_25 26500
channelThreshold_26 25700
channelThreshold_27 28000
channelThreshold_28 26500
channelThreshold_29 27500
channelThreshold_30 26000
channelThreshold_31 26500
//
// number of consecutive clocks having at least a channel over threshold
triggerDepth 3

// save selected data to binary file
dump
dump_name selectedEvents.dat
dump_mode b

// save selected data as a stream
// (different than "streamDumpName" that svaes all samples with no filter)
// NOT DEBUGGED!
sample
sample_name strSel.dat

// other analysis modules can be activated and the corresponding options 
// can be added as described in the above sections

// // // config_trigLess file end // // //

