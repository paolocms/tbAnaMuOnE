
This repository contains the code to analyze MuOnE caloriemter testbeam data;
The software currently available can read data and produce some very
basic plot, but can be quite easily expanded and customized to do much
more sophisticated analyses, as explained in the file INSTRUCTIONS.

